/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2006 Louisiana State University

// $Id$

// Extract information from dataset file and send to stdout (so far).


#ifndef DS_EXTRACT_H
#define DS_EXTRACT_H


class DS_Extract {
public:
  DS_Extract(App *app);
  App* const app;
  char *var_name;
};

#endif
