/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2008 Louisiana State University

// $Id$

#ifndef READEX_H
#define READEX_H

#define READEX

#include "config.h"
#include <stdio.h>

#include FILE_SYS_ELF_H

typedef uint32_t rx_addr;

typedef struct { rx_addr start, end; } RX_Range;

typedef struct {
 rx_addr start,end;
 const char *source_name,*func_name,*func_name_demangled;
 char elf_type;
} RX_Sym_Info;

typedef struct LineInfoTag {
  rx_addr addr;
  int lineno;
  int llmno;    // Labels used in certain assemblers.
  const char *source_file_name;
} Line_Info;


RX_Range RX_sym_range(const char *name);
RX_Range RX_proc_range(char *name);
RX_Range RX_file_range(char *name);

const char *RX_addr_to_source(rx_addr addr);
// Return string describing symbol of size SIZE affecting ADDR.
// String must be copied.
const char *RX_addr_to_sym_expr(rx_addr addr, unsigned size = 0);
bool RX_get_text(rx_addr addr, uint32_t &text);


int RX_init(const char *exec);

extern RX_Range rx_text_range;  // Not correct for dynamically linked code.

 
Line_Info *RX_addr_to_line_info(rx_addr addr);

RX_Sym_Info *RX_find_sym(const char *symname);
RX_Sym_Info *RX_sym_at_addr(rx_addr addr);
RX_Sym_Info *RX_proc_info(rx_addr addr);



#endif /* READEX_H */
