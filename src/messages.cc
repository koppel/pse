/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2005 Louisiana State University

// $Id$

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#include "misc.h"
#include "pse.h"


// Used as a debugger breakpoint.
void
trap_exit(char *msg) {}

void
fatal(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);

  char *msg_part_1 = g_strdup_vprintf(fmt, ap);
  char *msg = g_strdup_printf("%s\nPSE closing.",msg_part_1);

  trap_exit(msg_part_1);

  if( app.gtk_inited )
    {
      GtkWidget *dialog =
        gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
                               GTK_MESSAGE_ERROR,
                               GTK_BUTTONS_OK,
                               msg);
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
    }
  else
    {
      fprintf(stderr,"%s\n",msg);
    }
  exit(1);
  va_end(ap);
}

void
dialog_warning(GtkWidget *w, const char *fmt,...)
{
  va_list ap;
  va_start(ap, fmt);
  char *msg_part_1 = g_strdup_vprintf(fmt, ap);
  char *msg = g_strdup_printf(msg_part_1);

  if( w )
    {
      GtkWidget *warning =
        gtk_message_dialog_new(GTK_WINDOW(w),
                               GTK_DIALOG_MODAL,
                               GTK_MESSAGE_WARNING,
                               GTK_BUTTONS_OK,
                               msg);

      gtk_dialog_run(GTK_DIALOG(warning));
      gtk_widget_destroy(warning);
    }
  else
    {
      fprintf(stderr,"Warning: %s\n",msg);
    }

  g_free(msg_part_1);
  g_free(msg);

  va_end(ap);
}

int
dialog_question(GtkWidget *parent_w, const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  char *msg_pt_1 = g_strdup_vprintf(fmt, ap);
  char *msg = g_strdup_printf(msg_pt_1);
  va_end(ap);

  GtkWidget *question =
      gtk_message_dialog_new(GTK_WINDOW(parent_w), GTK_DIALOG_MODAL,
                             GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, msg);

  free(msg_pt_1);
  free(msg);

  int response = gtk_dialog_run(GTK_DIALOG(question));
  gtk_widget_destroy(question);
  return response;
}

enum View_Columns_Dir
{
  VCD_DIR_NAME,
  VCD_N_COLS
};

enum View_Columns_File
{
  VCF_FILE_NAME,
  VCF_MODIFIED_TIME,
  VCF_FILE_SIZE,
  VCF_MODIFIED_EPOCH_TIME,
  VCF_N_COLS
};

enum User_Open_File_Dialog_Response
{
  UR_DIR_CHANGE
};

struct File_Open_Dialog_CB_Info
{
  GtkEntry *filename_entry;
  RString cur_dir, req_dir;
  GtkWidget *file_open_dialog;
};

static void
fbd_file_changed_cb(GtkTreeSelection *file_selected, gpointer data)
{
  GtkTreeIter itr;
  GtkTreeModel *model;
  if(gtk_tree_selection_get_selected(file_selected, &model, &itr))
    {
      File_Open_Dialog_CB_Info *file_open_dialog_ptr =
                                (File_Open_Dialog_CB_Info*)data;
      gchar *filename;
      gtk_tree_model_get(model, &itr, VCF_FILE_NAME, &filename, -1);
      gtk_entry_set_text(file_open_dialog_ptr->filename_entry,
                         filename);
    }
}

static void
fbd_dir_changed_cb(GtkTreeSelection *dir_selected, gpointer data)
{
  GtkTreeIter itr;
  GtkTreeModel *model;
  if(!gtk_tree_selection_get_selected(dir_selected, &model, &itr)) return;

  gchar *dirname;
  gtk_tree_model_get(model, &itr, VCD_DIR_NAME, &dirname, -1);

  if(!strcmp(dirname, ".")) return;

  File_Open_Dialog_CB_Info *file_open_dialog_ptr =
                            (File_Open_Dialog_CB_Info*)data;
  char *req_dir_path;
  if(!strcmp(dirname, ". .") || file_open_dialog_ptr->cur_dir == "")
    req_dir_path = g_path_get_dirname(file_open_dialog_ptr->cur_dir);

  else if(file_open_dialog_ptr->cur_dir == "/")
    req_dir_path = g_strdup_printf("%s%s",
                                   (char*)file_open_dialog_ptr->cur_dir,
                                   dirname);

  else
    req_dir_path = g_strdup_printf("%s/%s",
                                   (char*)file_open_dialog_ptr->cur_dir,
                                   dirname);

  file_open_dialog_ptr->req_dir.assign_take(req_dir_path);
  gtk_dialog_response(GTK_DIALOG(file_open_dialog_ptr->file_open_dialog),
                      UR_DIR_CHANGE);
}

static gint
string_compare(char **a, char **b)
{
  int result = strcmp(*a, *b);
  if(result < 0) return -1;
  else if(result > 0) return 1;
  else return 0;
}

char*
file_browse_dialog(GtkWidget *parent_w, const gchar *dialog_title,
                   gchar *start_dir, const gchar *file_ext)
{
  GtkWidget *file_open_dialog = gtk_dialog_new_with_buttons
    (dialog_title, (GtkWindow*)parent_w, GTK_DIALOG_MODAL,
     GTK_STOCK_OPEN, GTK_RESPONSE_OK,
     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);

  gtk_dialog_set_default_response
    (GTK_DIALOG(file_open_dialog), GTK_RESPONSE_OK);

  File_Open_Dialog_CB_Info file_open_dialog_data;
  file_open_dialog_data.file_open_dialog = file_open_dialog;

  file_open_dialog_data.cur_dir = "";
  file_open_dialog_data.req_dir = (char*)start_dir;

  GtkWidget *file_view = gtk_tree_view_new();
  GtkWidget *dir_view = gtk_tree_view_new();

  GtkTreeViewColumn *column1 = gtk_tree_view_column_new();
  GtkTreeViewColumn *column2 = gtk_tree_view_column_new();
  GtkTreeViewColumn *column3 = gtk_tree_view_column_new();
  GtkTreeViewColumn *column4 = gtk_tree_view_column_new();

  gtk_tree_view_column_set_title(column1, "File Name");
  gtk_tree_view_column_set_title(column2, "Modification Time");
  gtk_tree_view_column_set_title(column3, "Size");
  gtk_tree_view_column_set_title(column4, "Directories");

  gtk_tree_view_append_column(GTK_TREE_VIEW(file_view), column1);
  gtk_tree_view_append_column(GTK_TREE_VIEW(file_view), column2);
  gtk_tree_view_append_column(GTK_TREE_VIEW(file_view), column3);
  gtk_tree_view_append_column(GTK_TREE_VIEW(dir_view), column4);

  gtk_tree_view_column_set_sizing(column1, GTK_TREE_VIEW_COLUMN_AUTOSIZE);

  GtkCellRenderer *rer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(rer), "foreground", "black", NULL);
  GtkCellRenderer *rer1 = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(rer1), "foreground", "blue", NULL);
  GtkCellRenderer *rer2 = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(rer2), "foreground", "blue", NULL);
  GtkCellRenderer *rer3 = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(rer3), "foreground", "blue", NULL);

  gtk_tree_view_column_pack_start(column1, rer1, 1);
  gtk_tree_view_column_pack_start(column2, rer2, 1);
  gtk_tree_view_column_pack_start(column3, rer3, 1);
  gtk_tree_view_column_pack_start(column4, rer, 1);

  gtk_tree_view_column_set_attributes(column1, rer1, "text",
                                      VCF_FILE_NAME, NULL);
  gtk_tree_view_column_set_attributes(column2, rer2, "text",
                                      VCF_MODIFIED_TIME, NULL);
  gtk_tree_view_column_set_attributes(column3, rer3, "text",
                                      VCF_FILE_SIZE, NULL);
  gtk_tree_view_column_set_attributes(column4, rer,  "text",
                                      VCD_DIR_NAME, NULL);

  gtk_tree_view_column_set_sort_column_id(column1, VCF_FILE_NAME);
  gtk_tree_view_column_set_sort_column_id(column2, VCF_MODIFIED_EPOCH_TIME);
  gtk_tree_view_column_set_sort_column_id(column3, VCF_FILE_SIZE);
  gtk_tree_view_column_set_sort_column_id(column4, VCD_DIR_NAME);

  GtkTreeSelection *file_selected =
                  gtk_tree_view_get_selection(GTK_TREE_VIEW(file_view));
  g_signal_connect(G_OBJECT(file_selected), "changed",
                   G_CALLBACK(fbd_file_changed_cb),
                   (gpointer)&file_open_dialog_data);

  GtkTreeSelection *dir_selected =
                   gtk_tree_view_get_selection(GTK_TREE_VIEW(dir_view));
  g_signal_connect(G_OBJECT(dir_selected), "changed",
                   G_CALLBACK(fbd_dir_changed_cb),
                   (gpointer)&file_open_dialog_data);

  GtkWidget *dir_view_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(dir_view_window),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_container_add(GTK_CONTAINER(dir_view_window), dir_view);
  GtkWidget *dir_view_align = gtk_alignment_new(0, 0, 1, 1);
  gtk_container_add(GTK_CONTAINER(dir_view_align), dir_view_window);

  GtkWidget *file_view_window = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(file_view_window),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_container_add(GTK_CONTAINER(file_view_window), file_view);
  GtkWidget *file_view_align = gtk_alignment_new(0, 0, 1, 1);
  gtk_container_add(GTK_CONTAINER(file_view_align),
                    file_view_window);

  GtkWidget *file_name_label = gtk_label_new("File Name");
  GtkWidget *file_name_label_align = gtk_alignment_new(0, 1, 0, 0);
  gtk_container_add(GTK_CONTAINER(file_name_label_align),
                    file_name_label);

  GtkWidget *curr_dir_label =
             gtk_label_new(file_open_dialog_data.cur_dir);

  GtkWidget *curr_dir_label_frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(curr_dir_label_frame),
                            GTK_SHADOW_NONE);

  gtk_container_add(GTK_CONTAINER(curr_dir_label_frame),
                    curr_dir_label);

  GtkWidget *curr_dir_label_align = gtk_alignment_new(-1, 0, 0, 0);
  gtk_container_add(GTK_CONTAINER(curr_dir_label_align),
                    curr_dir_label_frame);

  GtkEntry *file_name_entry = GTK_ENTRY(gtk_entry_new());
  file_open_dialog_data.filename_entry = file_name_entry;
  gtk_editable_set_editable(GTK_EDITABLE(file_name_entry), TRUE);
  gtk_editable_set_position(GTK_EDITABLE(file_name_entry), -1);
  gtk_entry_set_activates_default(file_name_entry, TRUE);

  GtkWidget *hbox1 = gtk_hbox_new(0, 0);
  gtk_box_pack_start(GTK_BOX(hbox1), file_name_label_align, FALSE,
                     FALSE, 10);
  gtk_box_pack_start(GTK_BOX(hbox1), GTK_WIDGET(file_name_entry),
                     TRUE, TRUE, 0);

  GtkWidget *file_dir_hpane = gtk_hpaned_new();
  gtk_paned_pack1(GTK_PANED(file_dir_hpane), dir_view_align, 1, 0);
  gtk_paned_pack2(GTK_PANED(file_dir_hpane), file_view_align, 1, 0);

  GtkWidget *file_dir_hpane_frame = gtk_frame_new(NULL);
  gtk_container_add(GTK_CONTAINER(file_dir_hpane_frame),
                    file_dir_hpane);

  GtkBox *main_vbox = GTK_BOX(GTK_DIALOG(file_open_dialog)->vbox);
  gtk_box_pack_start(main_vbox, curr_dir_label_align, FALSE, FALSE, 5);
  gtk_box_pack_start(main_vbox, file_dir_hpane_frame, TRUE, TRUE, 0);
  gtk_box_pack_start(main_vbox, hbox1, FALSE, FALSE, 5);
  gtk_window_set_default_size(GTK_WINDOW(file_open_dialog), 600, 500);

  GtkListStore *file_list_store =
    gtk_list_store_new(VCF_N_COLS,
                       G_TYPE_STRING, G_TYPE_STRING, G_TYPE_UINT, G_TYPE_UINT);
  GtkListStore *dir_list_store = gtk_list_store_new(VCD_N_COLS, G_TYPE_STRING);
  gtk_tree_view_set_model(GTK_TREE_VIEW(file_view),
                          GTK_TREE_MODEL(file_list_store));
  gtk_tree_view_set_model(GTK_TREE_VIEW(dir_view),
                          GTK_TREE_MODEL(dir_list_store));
  g_object_unref(file_list_store);
  g_object_unref(dir_list_store);

  gtk_widget_show_all(file_open_dialog);

  char *open_file_path = NULL;
  while(1)
    {
      g_free(open_file_path); open_file_path = NULL;
      GError *err = NULL;
      GDir *dir_path = g_dir_open(file_open_dialog_data.req_dir,0,&err);
      if(!dir_path)
        {
          if(file_open_dialog_data.cur_dir == "")
            fatal("Expected directory %s to exist.",
                  (char*)file_open_dialog_data.req_dir);

          GtkWidget *dir_open_err =
                     gtk_message_dialog_new(NULL,
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_ERROR,
                                            GTK_BUTTONS_OK,
                                            "%s", err->message);
         gtk_dialog_run(GTK_DIALOG(dir_open_err));
         gtk_widget_destroy(dir_open_err);
       }

      else
        {
          file_open_dialog_data.cur_dir = file_open_dialog_data.req_dir;
          gtk_entry_set_text(file_name_entry,"");
          gchar *cdir_m =
            g_strdup_printf("<span weight=\"bold\" size=\"17000\">%s</span>",
                            (char*)file_open_dialog_data.cur_dir);

          gtk_label_set_markup(GTK_LABEL(curr_dir_label), cdir_m);
          g_free(cdir_m);

          gtk_list_store_clear(dir_list_store);
          gtk_list_store_clear(file_list_store);

          GtkTreeIter iter;
          gtk_list_store_append(dir_list_store, &iter);
          gtk_list_store_set(dir_list_store, &iter, VCF_FILE_NAME, ".", -1);
          gtk_list_store_append(dir_list_store, &iter);
          gtk_list_store_set(dir_list_store, &iter, VCF_FILE_NAME, ". ." ,-1);

          GPtrArray * const dir_array = g_ptr_array_new();
          GPtrArray * const file_array = g_ptr_array_new();

          while(const gchar *trace_path = g_dir_read_name(dir_path))
            {
              RString full_name;
              full_name.sprintf("%s/%s",
                                (char*)file_open_dialog_data.cur_dir,
                                trace_path);

              if(g_file_test(full_name, G_FILE_TEST_IS_DIR))
                g_ptr_array_add(dir_array, g_strdup(trace_path));

              else if(g_str_has_suffix(full_name, file_ext))
                g_ptr_array_add(file_array, full_name.remove());
            }

          g_dir_close(dir_path);

          g_ptr_array_sort(dir_array, (GCompareFunc)string_compare);
          g_ptr_array_sort(file_array, (GCompareFunc)string_compare);
 
          for(guint i = 0; i < dir_array->len; ++i)
            {
              gchar *dir_name = (gchar*)g_ptr_array_index(dir_array, i);
              gtk_list_store_append(dir_list_store, &iter);
              gtk_list_store_set(dir_list_store, &iter,
                                 VCD_DIR_NAME,
                                 dir_name, -1);
            }

          for(guint i = 0; i < file_array->len; ++i)
            {
              gchar *file_full_path = (gchar*)g_ptr_array_index(file_array, i);
              
              struct stat file_info_buf;
              if((stat(file_full_path, &file_info_buf)) != -1)
                {
                  char mod_time_str[24];
                  const time_t mod_time = file_info_buf.st_mtime;
                  int file_size = file_info_buf.st_size;
                  struct tm *tm = localtime(&mod_time);
                  strftime(mod_time_str, 24, "%d %b %y %k:%M ", tm);
                 
                  gtk_list_store_append(file_list_store, &iter);
                  char * const file_name = g_path_get_basename(file_full_path);
                  gtk_list_store_set(file_list_store, &iter,
                                     VCF_FILE_NAME, file_name,
                                     VCF_MODIFIED_TIME,
                                     mod_time_str,
                                     VCF_FILE_SIZE, file_size,
                                     VCF_MODIFIED_EPOCH_TIME,
                                     mod_time, -1);
                 g_free(file_name);
               }
            }
          g_ptr_array_free(dir_array, TRUE);
          g_ptr_array_free(file_array, TRUE);
        }

      int response = gtk_dialog_run(GTK_DIALOG(file_open_dialog));

      if(response == GTK_RESPONSE_CANCEL ||
         response == GTK_RESPONSE_DELETE_EVENT)
        {
          open_file_path = NULL;
          break;
        }
        
      if(response == GTK_RESPONSE_OK)
        {
          gchar *file_name =
            g_strstrip(g_strdup(gtk_entry_get_text(file_name_entry)));

          if(file_name[0] == '~')
            {
              const gchar *home_dir = g_get_home_dir();
                                         
              file_open_dialog_data.req_dir.assign_take
                (g_strdup_printf("%s%s", home_dir, &file_name[1]));
              g_free(file_name);
              continue;
            }
                 
          else if(file_name[0]== '.' && file_name[1]== '.')
            {
              char *parent_dir =
                    g_path_get_dirname(file_open_dialog_data.cur_dir);
              int i=2;
              for(; file_name[i+1] == '.'; i=i+3)
                {
                  if(file_name[i]== '/' && file_name[i+1]=='.'
                     && file_name[i+2]=='.')   
                    parent_dir = g_path_get_dirname(parent_dir);
          
                  else  break;
                }
          
              file_open_dialog_data.req_dir.assign_take
                (g_strdup_printf("%s%s", parent_dir, &file_name[i]));
              g_free(parent_dir);
              g_free(file_name);
              continue;
            }

          else if( !g_path_is_absolute(file_name) )
            open_file_path = 
              g_strdup_printf("%s/%s", (char*)file_open_dialog_data.cur_dir,
                              file_name);

          else
            {
              char *base_name = g_path_get_dirname(file_name);
              if(*base_name == '.')
                open_file_path = 
                  g_strdup_printf("%s/%s", (char*)file_open_dialog_data.cur_dir,
                                  file_name);
              else open_file_path = g_strdup(file_name);
              g_free(base_name);
            }

          if(g_file_test(open_file_path, G_FILE_TEST_IS_DIR))
            {
              file_open_dialog_data.req_dir = open_file_path;
              g_free(file_name);
              continue;
            }

          if(!g_file_test(open_file_path, G_FILE_TEST_EXISTS))
            {
              GtkWidget *couldnt =
                         gtk_message_dialog_new(NULL,
                                                GTK_DIALOG_MODAL,
                                                GTK_MESSAGE_ERROR,
                                                GTK_BUTTONS_OK,
                                                "Could not open %s :  %s.",
                                                (char*)open_file_path,
                                                strerror(errno));
              gtk_dialog_run(GTK_DIALOG(couldnt));
              gtk_widget_destroy(couldnt);
              gtk_editable_select_region(GTK_EDITABLE(file_name_entry), 0, -1);
            }

           else
            {
              gtk_widget_hide(file_open_dialog);
              g_free(file_name);
              break;
            }
        }
    }

  gtk_widget_destroy(file_open_dialog);

  return open_file_path;
}
