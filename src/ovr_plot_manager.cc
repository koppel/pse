/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2014 Louisiana State University

// $Id$

#include <gtk/gtk.h>
#include "dataset.h"
#include "ovr_plot_manager.h"
#include "rti_declarations.h"
#include "pse_window.h"

class OPM_Call_Back_Data {
public:
   OPM_Call_Back_Data(Dataset *ds, int i):ds(ds),i(i){};
   Dataset* const ds;
   const int i;
};

static void
ovr_plot_sort_callback(GtkWidget *w, gpointer cbdptr)
{
  Call_Back_Data* const cbd = (Call_Back_Data *)cbdptr;
  PSE_Window* const pw = cbd->pw;
  Dataset* const ds = pw->ds;
  const int idx = cbd->i;

  if ( ds->sdata_sort_idx == idx ) return;

  if ( idx == PSE_Window::OSK_Execution_Order )
    {
      pw->plot_ovr_by_ipc = false;
    }
  else
    {
      pw->plot_ovr_by_ipc = true;
      ds->sortby_ovr_idx(idx);
    }
  ds->sdata_sort_idx = idx;
  ds->invalidate_draw_areas();
}


static void
ovr_plot_display_toggle_callback(GtkWidget *w, gpointer cbdptr)
{
  OPM_Call_Back_Data* const cbd = (OPM_Call_Back_Data *)cbdptr;
  Dataset* const ds = cbd->ds;
  
  ds->opm->pane_labels_build_all(ds);
  ds->invalidate_draw_areas();
}
 
static void
ovr_plot_connect_toggle_callback(GtkWidget *w, gpointer cbdptr)
{
  OPM_Call_Back_Data* const cbd = (OPM_Call_Back_Data *)cbdptr;
  Dataset* const ds = cbd->ds;

  ds->invalidate_draw_areas();
}

static void
evnt_cb_ovr_plot_display_set_toggle_false(GtkWidget *w, gpointer cbdptr)
{
  OPM_Call_Back_Data *cbd = (OPM_Call_Back_Data *)cbdptr;
  Dataset *ds = cbd->ds;
  Ovr_Plot_Manager *opm = ds->opm;
  Ovr_Plot_Series *ops_toggled = &opm->plot_series[cbd->i];

  gtk_toggle_action_set_active(ops_toggled->display_toggle, false);

  opm->pane_labels_build_all(ds);
  ds->invalidate_draw_areas();
}

static void
evnt_cb_ovr_plot_series_changed(GtkWidget *w, gpointer cbdptr)
{ 
  OPM_Call_Back_Data* const cbd = (OPM_Call_Back_Data *)cbdptr;
  Dataset* const ds = cbd->ds;
  Ovr_Plot_Series* const ops_changed = &ds->opm->plot_series[cbd->i];

  ops_changed->range_min_usr =
     atof(gtk_entry_get_text(GTK_ENTRY(ops_changed->range_min_w)));
  ops_changed->range_max_usr =
     atof(gtk_entry_get_text(GTK_ENTRY(ops_changed->range_max_w)));
  ds->invalidate_draw_areas();
}

void
ovr_plot_series_menu_callback(gpointer pwptr, int action, GtkWidget *w)
{
  PSE_Window* const pw = (PSE_Window*) pwptr;
  pw->ds->opm->preferences_dialog_launch(pw);
}

void
ovr_plot_series_button_callback(GtkWidget *w, gpointer pwptr)
{
  ovr_plot_series_menu_callback((PSE_Window*)pwptr, 0, w);
}

void 
ovr_plot_values_hide_button_callback(GtkWidget *w, gpointer pwptr)
{
  PSE_Window* const pw = (PSE_Window*)pwptr;
  gtk_check_menu_item_set_active
    (GTK_CHECK_MENU_ITEM(pw->menu_check_ovr_plot_values_pane),false);
}

// CJM 2005-08-22 Mon 19:39
//  May want to change this to use the action, but the change isn't necessary.
void 
ovr_plot_values_menu_callback(gpointer pwptr, int action, GtkWidget *w)
{
  PSE_Window* const pw = (PSE_Window*)pwptr;
  Ovr_Plot_PSE_Window_Data* const opwd = pw->ovr_plot_data;

  const bool item_state =
    gtk_check_menu_item_get_active
    (GTK_CHECK_MENU_ITEM(pw->menu_check_ovr_plot_values_pane));

  if( pw->plot_is_ovr )
    rtiv_ovr_show_ovr_plot_pane = opwd->ovr_show_values = item_state;
  else
    rtiv_seg_show_ovr_plot_pane = opwd->seg_show_values = item_state;

  if( item_state )
      gtk_widget_show_all(opwd->pane_hbox);
  else
    gtk_widget_hide(opwd->pane_hbox);

}


void
Ovr_Plot_Manager::definition_read(Dataset_File& df)
{
  DSF_Entry e;
  char *name = NULL, *format = NULL, *sample_format = NULL, *doc = NULL;
  char *color = NULL;
  int index = -1, priority = 0;
  double r_max = 0.0, r_min = 0.0;

  while( df.get(e) )
    {
      if( e.type == DSF_CMD_NAMESPACE_END ) break;
      if( e.type != DSF_CMD_IDX_STR && e.type != DSF_CMD_IDX_IN4 &&
          e.type != DSF_CMD_IDX_DBL )
          fatal("Unexpected plot data type.");

      switch( e.key.i4 ){
      case DS_FMT_OVR_PLOT_SERIES_NAME:
        name = strescape(e.data.s);
        break;
      case DS_FMT_OVR_PLOT_SERIES_IDX:
        index = e.data.i4;
        break;
      case DS_FMT_OVR_PLOT_SERIES_DOCSTRING:
        doc = strescape(e.data.s);
        break;
      case DS_FMT_OVR_PLOT_SERIES_MAX:
        r_max = e.data.d;
        break;
      case DS_FMT_OVR_PLOT_SERIES_MIN:
        r_min = e.data.d;
        break;
      case DS_FMT_OVR_PLOT_SERIES_COLOR:
        color = strescape(e.data.s);  // Needs validity check.
      case DS_FMT_OVR_PLOT_SERIES_PRIORITY:
        priority = e.data.i4;
        break;
      case DS_FMT_OVR_PLOT_SERIES_FORMAT:
        format = strescape(e.data.s); // Needs validity check.
        break;
      case DS_FMT_OVR_PLOT_SERIES_SAMPLE_FORMAT:
        sample_format = strescape(e.data.s); // Needs validity check.
        break;

      default:
        ASSERTS( false );
      }
    }

  if( index == -1 ) fatal("Index was not specified in plot data.");

  series_new(index,name,format,sample_format,doc,r_max,r_min,color,priority);
}

void
Ovr_Plot_Manager::data_read(Dataset_File& df, Segment_Data* const sd)
{
  DSF_Entry e;

  sd->ovr_plot_value = (double *) calloc(plot_value_id_max + 1, sizeof(double));
  sd->ovr_plot_value_sample_count = 
     (int *) calloc(plot_value_id_max + 1, sizeof(double));

  df.peek(e);
  if( e.type == DSF_CMD_NAMESPACE_STR_BEGIN )
    {
      df.get(e);
      ASSERTS( e.key.s == DS_FMT_OVR_PLOT_SERIES );

      int ovr_plot_value_index = plot_value_id_max + 1;

      while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END )
        {
          switch( e.key.i4 ){

          case DS_FMT_OVR_PLOT_DATA_ID:
            ovr_plot_value_index = e.data.i4;
            if( ovr_plot_value_index > plot_value_id_max )
                fatal("Plot series ID out of range or not defined.");
            break;
          case DS_FMT_OVR_PLOT_DATA_VALUE:
            if( e.type != DSF_CMD_IDX_DBL )
                fatal("Unexpected plot value data type.");
            sd->ovr_plot_value[ovr_plot_value_index] = e.data.d;
            break;
          case DS_FMT_OVR_PLOT_DATA_SAMPLE_COUNT:
            if( e.type != DSF_CMD_IDX_IN4 )
                fatal("Unexpected plot sample count data type");
            sd->ovr_plot_value_sample_count[ovr_plot_value_index] = e.data.i4;
            break;
          default:
            ASSERTS( false );
          }
        }
    }

}

static Ovr_Plot_Series *
ovr_series_get(Ovr_Plot_Manager *opm, const char *series_name)
{
  //CJM 2005-02-15 Tue 20:21
  //  May implement as hash.
  for(int i = 0; i < opm->plot_series_num; i++)
    {
      if( strcmp(series_name, opm->plot_series[i].name) == 0 )
          return &opm->plot_series[i];
    }
  return NULL;
}

void
Ovr_Plot_Manager::series_new
(int value_index, const char *name, const char *value_format,
 const char *sample_format, const char *docstring, double r_max,
 double r_min, const char *color, int priority)
{
  if( plot_series_num >= plot_series_allocated )
      plot_series_realloc(plot_series_num);

  Ovr_Plot_Series* const ops_i = &plot_series[plot_series_num++];

  if( ops_i->name )
      fatal("Cannot create plot series %s because the index is used %s.",
             name, ops_i->name.s);

  plot_value_id_max =
     value_index > plot_value_id_max ? value_index : plot_value_id_max;
  ops_i->name = name;
  ops_i->value_index = value_index;
  ops_i->value_format = value_format;
  ops_i->sample_format = sample_format;
  ops_i->docstring = docstring;
  ops_i->range_max_dsf=ops_i->range_max_usr=ops_i->range_max_auto=r_max;
  ops_i->range_min_dsf=ops_i->range_min_usr=ops_i->range_min_auto=r_min;
  ops_i->color_default.spec = color;
  ops_i->priority = priority;
  ops_i->display_initial = priority == 0;

  if( !app.gui ) return;

  ops_i->connect_toggle = 
     gtk_toggle_action_new("Plot Series Connect", "",
                           "Connect points.", NULL);
  g_object_set(ops_i->connect_toggle, "label", NULL, NULL);
  gtk_toggle_action_set_active(ops_i->connect_toggle, false);
  ops_i->display_toggle = 
     gtk_toggle_action_new("Plot Series Display", "",
                           "Display series.", NULL);
  g_object_set(ops_i->display_toggle, "label", NULL, NULL);
  gtk_toggle_action_set_active((ops_i->display_toggle), ops_i->display_initial);

  GdkColor c;

#define toh(c) gushort(0x9fff + (int(c) - 0x7fff) / 5)
  gdk_color_parse(color, &c);
  ops_i->color_visited.red = toh(c.red);
  ops_i->color_visited.green = toh(c.green);
  ops_i->color_visited.blue = toh(c.blue);
#undef toh

  if( !gdk_color_alloc(app.cmap,&ops_i->color_visited) )
      fatal("Could not allocate color for visited segments.");

  gdk_color_parse(ops_i->color_default, ops_i->color_default);

  if( !gdk_color_alloc(app.cmap, ops_i->color_default) )
      fatal("Could not allocate the color %s", ops_i->color_default.spec);
}

void
Ovr_Plot_Manager::series_auto_scale_all(Segment_Data **sd_base, int occ)
{
  for(int i = 0; i < plot_series_num; i++)
      series_auto_scale(i, sd_base, occ);
}

void
Ovr_Plot_Manager::series_auto_scale(int series_index, Segment_Data **sd_base,
                                    int occ)
{
  Ovr_Plot_Series* const ops = &plot_series[series_index];

  if( !occ ) return;

  ops->range_max_auto = sd_base[0]->ovr_plot_value[ops->value_index];
  ops->range_min_auto = sd_base[0]->ovr_plot_value[ops->value_index];

  for(int i = 0; i < occ; i++)
    {
      Ovr_Plot_Series* const ops = &plot_series[series_index];
      const double opd_value = sd_base[i]->ovr_plot_value[ops->value_index];

      if( opd_value > ops->range_max_auto )
          ops->range_max_auto = opd_value;
      else if( opd_value < ops->range_min_auto)
          ops->range_min_auto = opd_value;
    }
}

typedef int(*Comp_Func)(const void *, const void *);

void
Ovr_Plot_Manager::series_prioritize()
{
  if( !plot_series_pri )
    plot_series_pri = new Ovr_Plot_Series*[plot_series_num];

  for(int i = 0; i < plot_series_num; i++)
    {
      plot_series_pri[i] = &plot_series[i];
      if( plot_series[i].priority )
        {
          if( app.gui )
            gtk_toggle_action_set_active(plot_series[i].display_toggle, false);
          plot_series[i].display_initial = false;
        }
    }

  qsort(plot_series_pri, plot_series_num, sizeof(Ovr_Plot_Series *),
        (Comp_Func)compare_priority);

  for(int i = 0; i < plot_series_num; i++)
    {
      if( app.gui )
        gtk_toggle_action_set_active(plot_series_pri[i]->display_toggle, true);
      plot_series_pri[i]->display_initial = true;
      if( i == PLOT_SERIES_DISPLAYED - 1 ) break;
    }
}

static bool
ovr_series_setting_next(char* &ss, const char* &name, const char* &opts)
{
  if( !*ss ) return false;

  name = ss;

  while( *ss != '*' ) if( !*++ss )
      fatal("Invalid setting for rtiv_ovr_series_settings, expected \"*\" "
            "after \"%s\"", name);
  
  if( ss == name )
      fatal("Invalid setting for rtiv_ovr_series_settings, expected a series"
            "name before \"*\"");
  
  *ss++ = 0;
  opts = ss;
    
  while( *ss && *ss != '!' ) ss++;
    
  if( *ss ) *ss++ = 0;
  
  return true; 
}   

void
Ovr_Plot_Manager::series_rti_to_settings()
{
  if( !app.gui ) return;
  RString ovr_series_settings_cpy(rtiv_ovr_series_settings);
  char *setting_str = ovr_series_settings_cpy;
  const char *name, *opts;

  while( ovr_series_setting_next(setting_str, name, opts) )
    {
      RString unused_settings("");
      if( Ovr_Plot_Series *ops = ovr_series_get(this, name) )
        {

          ops->from_settings_rti = true;

          for( char c = *opts; c != 0; c = *opts++ )
            {
              switch( c ) {
              case 'C':
                gtk_toggle_action_set_active(ops->connect_toggle, true);
                ops->connect_initial = true;
                break;
              case 'c':
                gtk_toggle_action_set_active(ops->connect_toggle, false);
                ops->connect_initial = false;
                break;
              case 'D':
                gtk_toggle_action_set_active(ops->display_toggle, true);
                ops->display_initial = true;
                break;
              case 'd':
                gtk_toggle_action_set_active(ops->display_toggle, false);
                ops->display_initial = false;
                break;
             default:
                fatal("Invalid option \"%c\" for \"%s\" in "
                      "rtiv_ovr_series_settings.", c, name);
                break;
              }
            }

        }
      else
        {
          unused_settings.sprintf("%s*%s", name, opts);
          ovr_rti_settings_unused += unused_settings.remove();
        }
    }

}

void
Ovr_Plot_Manager::series_settings_to_rti()
{
  if( !app.gui ) return;
  RString next_rti_settings("");
  int writes_left = rtiv_ovr_series_settings_max;

  for( int i = 0; i < plot_series_num && writes_left--; i++ )
    {
      Ovr_Plot_Series *ops = &plot_series[i];

      if( !ops->is_modified() && !ops->from_settings_rti ) continue;
      next_rti_settings.sprintf("%s*", ops->name.s);
      next_rti_settings += 
         gtk_toggle_action_get_active(ops->connect_toggle) ? "C" : "c";
      next_rti_settings += 
         gtk_toggle_action_get_active(ops->display_toggle) ? "D" : "d";
      next_rti_settings += "!";
    }

  while( writes_left-- > 0 && ovr_rti_settings_unused )
    {
      next_rti_settings += (char*)ovr_rti_settings_unused;
      next_rti_settings += "!";
    }

  rtiv_ovr_series_settings = next_rti_settings.remove();
}

void
Ovr_Plot_Manager::setup_before_pse_window_initialized(PSE_Window* const pw)
{
  Ovr_Plot_PSE_Window_Data *const opwd = 
    pw->ovr_plot_data = new Ovr_Plot_PSE_Window_Data;
  opwd->pane_value_label = NULL;
  opwd->pane_vbox = gtk_vbox_new(false, 0);
  opwd->pane_hbox = gtk_hbox_new(false, 0);
  opwd->ovr_show_values = rtiv_ovr_show_ovr_plot_pane;
  opwd->seg_show_values = rtiv_seg_show_ovr_plot_pane;
}
 

void
Ovr_Plot_Manager::setup_after_dataset_load(Dataset* const ds)
{
  if( !app.gui ) return;
  for(int i = 0; i < plot_series_num; i++)
    {
      Ovr_Plot_Series *ops = &plot_series[i];

      g_signal_connect(G_OBJECT(ops->connect_toggle), "toggled",
                       G_CALLBACK(ovr_plot_connect_toggle_callback),
                       new OPM_Call_Back_Data(ds, i));
      g_signal_connect(G_OBJECT(ops->display_toggle), "toggled",
                       G_CALLBACK(ovr_plot_display_toggle_callback),
                       new OPM_Call_Back_Data(ds, i));
    }
}
  
enum Plot_Text_Columns
{
  PTC_Display,
  PTC_Connect,
  PTC_Color,
  PTC_Name,
  PTC_Scale_Min,
  PTC_Scale_Max,
  PTC_Value
};


static void
evnt_cb_ovr_plot_series_response(GtkWidget *w, gint response, gpointer dsptr)
{
  Dataset* const ds = (Dataset *)dsptr;
  Ovr_Plot_Manager* const opm = ds->opm;

  if( response == GTK_RESPONSE_OK )
    { 
      for(int i = 0; i < opm->plot_series_num; i++)
        {  
           Ovr_Plot_Series* const ops = &opm->plot_series[i];

           ops->range_min_usr =
              atof( gtk_entry_get_text(GTK_ENTRY(ops->range_min_w)) );
           ops->range_max_usr =
              atof( gtk_entry_get_text(GTK_ENTRY(ops->range_max_w)) );
           ds->invalidate_draw_areas();
        }
    }

  gtk_widget_hide(opm->preferences_dialog);
}


void
Ovr_Plot_Manager::preferences_dialog_construct(PSE_Window* const pw)
{
  preferences_dialog = (PGtkDialogPtr)gtk_dialog_new_with_buttons
                       ("Overview Plot Series", (PGtkWindowPtr)pw->main_w,
                       (GtkDialogFlags)0,
                       GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
  
  PGtkScrolledWindowPtr scroll_w = gtk_scrolled_window_new(NULL, NULL);
  PGtkVBoxPtr vbox = preferences_dialog->vbox;
  PGtkTablePtr table = gtk_table_new(plot_series_num, 4, false);
  prefs_segment_label = gtk_label_new(NULL);
  pw->label_set += (GtkLabel*)prefs_segment_label;

  int row = 0;
  gtk_table_attach_defaults(table,
                            gtk_label_new("D"),
                            PTC_Display, PTC_Display+1, row, row+1);
  gtk_table_attach_defaults(table,
                            gtk_label_new("C"),
                            PTC_Connect, PTC_Connect+1, row, row+1);
  gtk_table_attach_defaults(table,
                            gtk_label_new("Name"),
                            PTC_Name, PTC_Name+1, row, row+1);
  gtk_table_attach_defaults(table,
                            gtk_label_new("Scale Min"),
                            PTC_Scale_Min, PTC_Scale_Min+1, row, row+1);
  gtk_table_attach_defaults(table,
                            gtk_label_new("Scale Max"),
                            PTC_Scale_Max, PTC_Scale_Max+1, row, row+1);
  gtk_table_attach_defaults(table,
                            prefs_segment_label,
                            PTC_Value, PTC_Value+1, row, row+1);
  row++;

  gtk_table_attach_defaults(table, gtk_hseparator_new(),
                            PTC_Display, PTC_Value+1, row, row+1);
  row++;

  for(int i = 0; i < plot_series_num; i++)
    {
      Ovr_Plot_Series* const ops = &plot_series[i];
      
      PGtkWidgetPtr range_min_w = ops->range_min_w = gtk_entry_new();
      PGtkWidgetPtr range_max_w = ops->range_max_w = gtk_entry_new();
      PGtkLabelPtr value_name_w = gtk_label_new(ops->name);
      PGtkLabelPtr value_w = ops->value_w = gtk_label_new(0);
      RString r_min_str(""), r_max_str("");
      r_min_str.sprintf("%.2lf", ops->range_min_usr);
      r_max_str.sprintf("%.2lf", ops->range_max_usr);

      gtk_misc_set_padding(value_name_w, 2, 2);
      gtk_misc_set_alignment(value_name_w, 0, 0);

      gtk_entry_set_text(GTK_ENTRY(range_min_w), r_min_str);
      gtk_widget_set_size_request(range_min_w, 80, -1);
      gtk_entry_set_text(GTK_ENTRY(range_max_w), r_max_str);
      gtk_widget_set_size_request(range_max_w, 80, -1);

      gtk_widget_set_size_request(value_w, 200, -1);
      gtk_misc_set_padding(value_w, 2, 2);
      gtk_misc_set_alignment(value_w, 0, 0);

      PGtkCheckButtonPtr display_toggle = gtk_check_button_new();
      PGtkCheckButtonPtr connect_toggle = gtk_check_button_new();
      gtk_action_connect_proxy(ops->connect_toggle, connect_toggle);
      gtk_action_connect_proxy(ops->display_toggle, display_toggle);

      gtk_table_attach_defaults(table, display_toggle,
                                PTC_Display, PTC_Display+1, row, row+1);
      gtk_table_attach_defaults(table, connect_toggle,
                                PTC_Connect, PTC_Connect+1, row, row+1);
      gtk_table_attach_defaults(table, value_name_w,
                                PTC_Name, PTC_Name+1, row, row+1);
      gtk_table_attach_defaults(table, range_min_w,
                                PTC_Scale_Min, PTC_Scale_Min+1, row, row+1);
      gtk_table_attach_defaults(table, range_max_w,
                                PTC_Scale_Max, PTC_Scale_Max+1, row, row+1);
      gtk_table_attach_defaults(table, value_w,
                                PTC_Value, PTC_Value+1, row, row+1);

      PGtkAspectFramePtr aframe = gtk_aspect_frame_new(NULL, 0.5, 0.5, 1,false);
      gtk_container_set_border_width(aframe, 0);

      PGtkDrawingAreaPtr draw_area = gtk_drawing_area_new();
      gtk_widget_set_size_request(draw_area, 20, -1);
      gtk_widget_modify_bg(draw_area,GTK_STATE_NORMAL,ops->color_default);
      gtk_container_add(aframe, draw_area);

      gtk_table_attach_defaults(table, aframe,
                                PTC_Color, PTC_Color+1, row, row+1);

      row++;

      g_signal_connect(G_OBJECT(ops->range_min_w), "activate",
                       G_CALLBACK(evnt_cb_ovr_plot_series_changed),
                       new OPM_Call_Back_Data(pw->ds, i));
      g_signal_connect(G_OBJECT(ops->range_max_w), "activate",
                       G_CALLBACK(evnt_cb_ovr_plot_series_changed),
                       new OPM_Call_Back_Data(pw->ds, i));
      
    }

  gtk_window_set_default_size(preferences_dialog, 700, 400);

  gtk_scrolled_window_set_policy((PGtkScrolledWindowPtr)scroll_w,
                                 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_add_with_viewport((PGtkScrolledWindowPtr)scroll_w,
                                        table);
  gtk_container_add(vbox, scroll_w);

  PGtkLabelPtr key_label = gtk_label_new("D: Display   C: Connect");
  gtk_misc_set_alignment(key_label, 0, 0);
  gtk_misc_set_padding(key_label, 2, 4);
  gtk_box_pack_start(vbox, key_label, false, true, 0);

  gtk_widget_show_all(preferences_dialog);

  g_signal_connect(preferences_dialog, "response",
                   G_CALLBACK(evnt_cb_ovr_plot_series_response), pw->ds);
      
  g_signal_connect(preferences_dialog, "delete_event",
                   G_CALLBACK(gtk_widget_hide_on_delete),
                   (gpointer)preferences_dialog);
    
}

void
Ovr_Plot_Manager::preferences_dialog_launch(PSE_Window* const pw)
{
  if( preferences_dialog )
      gtk_window_present(preferences_dialog);
  else
      preferences_dialog_construct(pw);
}

void
Ovr_Plot_Manager::pane_value_labels_update
(PSE_Window* const pw, Segment_Data* const sd)
{
  if( preferences_dialog )
    {
      RStringF segment_num_formatted("Value for Segment %d", 1+sd->segment_num);
      gtk_label_set_text(prefs_segment_label, segment_num_formatted.s);
    }

  for(int i = 0; i < plot_series_num; i++)
    {
      Ovr_Plot_Series* const ops = &plot_series[i];
      RString plot_value_formatted("");
      RString sample_count_formatted("");

      plot_value_formatted.safe_sprintf
        (ops->value_format, sd->ovr_plot_value[ops->value_index]);
      sample_count_formatted.safe_sprintf
        (ops->sample_format, sd->ovr_plot_value_sample_count[ops->value_index]);
      RStringF plot_value
        ("%s: %s, %s",
         ops->name.s, plot_value_formatted.s, sample_count_formatted.s);

      if( preferences_dialog )
        {
          RStringF plot_value_pref
            ("%s, %s", plot_value_formatted.s, sample_count_formatted.s);
          gtk_label_set_text(ops->value_w, plot_value_pref);
        }

      if( gtk_toggle_action_get_active(ops->display_toggle)
          && pw->ovr_plot_data->pane_value_label )
          gtk_label_set_text(pw->ovr_plot_data->pane_value_label[i], 
                             plot_value);
    }
}

void
Ovr_Plot_Manager::pane_labels_build_all(Dataset* const ds)
{
  for( PSE_Windows_Iterator pw(ds->pse_windows); pw; pw++ )
      pane_labels_build(pw);
}

void
Ovr_Plot_Manager::pane_labels_build(PSE_Window* const pw)
{
  Ovr_Plot_PSE_Window_Data* const opwd = pw->ovr_plot_data;
  Dataset* const ds = pw->ds;
  PGtkHBoxPtr row_box;
  PGtkVBoxPtr vbox = opwd->pane_vbox;
  PGtkHBoxPtr hbox = opwd->pane_hbox;
  int row = 0, col = 0;

  // Widgets created in PSE_Window::PSE_Window.
  g_signal_connect
    ( pw->ovr_sort_execution, 
      "clicked", G_CALLBACK(ovr_plot_sort_callback),
      new Call_Back_Data(pw, PSE_Window::OSK_Execution_Order));
  g_signal_connect
    ( pw->ovr_sort_pc, 
      "clicked", G_CALLBACK(ovr_plot_sort_callback),
      new Call_Back_Data(pw, PSE_Window::OSK_Insn_Static_Order));

  gtk_widget_destroy(vbox);

  opwd->pane_vbox = vbox = gtk_vbox_new(true, 0);
  gtk_box_pack_start(hbox, vbox, true, true, 0);

  for( int i = 0; i < plot_series_num; i++ )
    {
      Ovr_Plot_Series *ops = &ds->opm->plot_series[i];

      if( !gtk_toggle_action_get_active(ops->display_toggle) ) 
        { 
          opwd->pane_value_label[i]=NULL; 
          continue; 
        }

      if( col == 0 )
        { 
          row_box = gtk_hbox_new(true, 0);
          gtk_box_pack_start(vbox, row_box, false, false, 0);
        }

      PGtkHBoxPtr label_box = gtk_hbox_new(false, 1);
      PGtkDrawingAreaPtr draw_area = gtk_drawing_area_new();
      PGtkAspectFramePtr aframe = gtk_aspect_frame_new(NULL,0.5,0.5,1,false);
      
      PGtkButtonPtr b = gtk_button_new();
      GtkWidget *w = gtk_image_new_from_stock(GTK_STOCK_CLOSE,
                                              GTK_ICON_SIZE_SMALL_TOOLBAR);
      gtk_container_add(b, w);
      g_signal_connect(b, "clicked", 
                       G_CALLBACK(evnt_cb_ovr_plot_display_set_toggle_false),
                       new OPM_Call_Back_Data(ds, i));
      gtk_box_pack_start(label_box, b, false, true, 0);
      
      gtk_widget_set_size_request(draw_area, 15, 15);
      gtk_widget_modify_bg(draw_area, GTK_STATE_NORMAL, ops->color_default);
      gtk_container_add(aframe, draw_area);
      gtk_box_pack_start(label_box, aframe, false, false, 0);

      PGtkRadioButtonPtr sort_toggle = 
        gtk_radio_button_new_from_widget(pw->ovr_sort_execution);
      g_signal_connect
        (sort_toggle, "clicked", G_CALLBACK(ovr_plot_sort_callback),
         new Call_Back_Data(pw, i));
      gtk_box_pack_start(label_box, sort_toggle, false, false, 0);
      gtk_tooltips_set_tip(app.tips, sort_toggle, "Sort.", NULL);

      PGtkCheckButtonPtr pane_connect_toggle = gtk_check_button_new();
      gtk_action_connect_proxy(ops->connect_toggle, pane_connect_toggle);
      gtk_box_pack_start(label_box,pane_connect_toggle,false,false,0);
      gtk_tooltips_set_tip(app.tips, pane_connect_toggle,
                           "Connect points.", NULL);

      pw->label_set += opwd->pane_value_label[i] = gtk_label_new(NULL);
      gtk_misc_set_alignment(opwd->pane_value_label[i], 0, 0.5);
      gtk_box_pack_start(label_box, opwd->pane_value_label[i], true, true, 3);

      gtk_box_pack_start(row_box, label_box, true, true, 3);

      if( Segment_Data *pointed_seg = pw->ovr_pointed_segment )
        {
          RString plot_value_text;
          RString plot_value_formatted;
          RString sample_count_formatted;

          plot_value_formatted.safe_sprintf(ops->value_format,
                                 pointed_seg->ovr_plot_value[ops->value_index]);
          sample_count_formatted.safe_sprintf(ops->sample_format,
                    pointed_seg->ovr_plot_value_sample_count[ops->value_index]);
          plot_value_text.sprintf("%s:%s, %s", ops->name.s, 
                                               plot_value_formatted.s,
                                               sample_count_formatted.s);
          gtk_label_set_text(opwd->pane_value_label[i], plot_value_text);
        }

      if( ++col == 2 ) { col = 0; row++; }
    }

  if( pw->plot_is_ovr && !pw->ovr_plot_data->ovr_show_values
      || !pw->plot_is_ovr && !pw->ovr_plot_data->seg_show_values)
      gtk_widget_hide(hbox);
  else
      gtk_widget_show_all(hbox);
}
         
void 
Ovr_Plot_Manager::window_and_dataset_init(PSE_Window* const pw)
{
  Ovr_Plot_PSE_Window_Data* const opwd = pw->ovr_plot_data;
  
  if( opwd->pane_value_label == NULL )
      opwd->pane_value_label = new PGtkLabelPtr[plot_series_num];

  pane_labels_build(pw);
}
