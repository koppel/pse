/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef amodes_h_
#define amodes_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


/*
 * per-opcode control strings describing how
 * to disassemble and print operands fields.
 */
extern const char* const ihash_amodes[];


/*
 * amode : AM_CHAR char		print single character "char"
 *       | field format		print instruction "field" according to "format"
 *       | AM_I0 amode		print "amode" if i==0 (register mode)
 *       | AM_I1 amode		print "amode" if i==1 (immediate mode)
 */

#define AM_CHAR		'\''

#define AM_I0		':'
#define AM_I1		'?'

/*
 * fields
 */
#define AM_ASI		'a'
#define AM_DISP22	'B'
#define AM_DISP30	'C'
#define AM_IMM22	'i'
#define AM_RD		'd'
#define AM_RS1		'1'
#define AM_RS2		'2'
#define AM_SIMM13	'S'
#define AM_SHCNT32	'<'

#define AM_DISP19	'b'
#define AM_DISP2_14	'v'
#define AM_CC12_11	'7'
#define AM_CC21_20	'8'
#define AM_CC26_25	'9'
#define AM_SHCNT64	'>'
#define AM_SIMM10	'm'
#define AM_SIMM11	'M'

/*
 * formats
 */
#define AM_IMM		'u'	/* unsigned immediate constant */
#define AM_SIMM		's'	/* signed immediate constant */
#define AM_HI		'h'	/* %hi immediate constant */
#define AM_REG		'r'	/* integer register # */
#define AM_FREGS	'f'	/* single fp register # */
#define AM_FREGD	'd'	/* double fp register # */
#define AM_FREGQ	'q'	/* quad fp register # */
#define AM_PC		'.'	/* pc relative */
#define AM_ASR		'a'	/* ancillary state register # */

#define AM_ICC		'I'	/* integer condition code */
#define AM_FCC		'F'	/* fp condition code */
#define AM_PR		'p'	/* privileged register # */


#endif /*amodes_h_*/
