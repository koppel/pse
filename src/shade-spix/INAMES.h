/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef INAMES_h_
#define INAMES_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"



/*
 * op=00
 * op2=010
 * a=0
 */

#define I_COND_BN             0  /* cond=0000 */
#define I_COND_BE             1  /* cond=0001 */
#define I_COND_BLE            2  /* cond=0010 */
#define I_COND_BL             3  /* cond=0011 */
#define I_COND_BLEU           4  /* cond=0100 */
#define I_COND_BCS            5  /* cond=0101 */
#define I_COND_BNEG           6  /* cond=0110 */
#define I_COND_BVS            7  /* cond=0111 */
#define I_COND_BA             8  /* cond=1000 */
#define I_COND_BNE            9  /* cond=1001 */
#define I_COND_BG            10  /* cond=1010 */
#define I_COND_BGE           11  /* cond=1011 */
#define I_COND_BGU           12  /* cond=1100 */
#define I_COND_BCC           13  /* cond=1101 */
#define I_COND_BPOS          14  /* cond=1110 */
#define I_COND_BVC           15  /* cond=1111 */

/*
 * op=00
 * op2=110
 * a=0
 */

#define I_COND_FBN            0  /* cond=0000 */
#define I_COND_FBNE           1  /* cond=0001 */
#define I_COND_FBLG           2  /* cond=0010 */
#define I_COND_FBUL           3  /* cond=0011 */
#define I_COND_FBL            4  /* cond=0100 */
#define I_COND_FBUG           5  /* cond=0101 */
#define I_COND_FBG            6  /* cond=0110 */
#define I_COND_FBU            7  /* cond=0111 */
#define I_COND_FBA            8  /* cond=1000 */
#define I_COND_FBE            9  /* cond=1001 */
#define I_COND_FBUE          10  /* cond=1010 */
#define I_COND_FBGE          11  /* cond=1011 */
#define I_COND_FBUGE         12  /* cond=1100 */
#define I_COND_FBLE          13  /* cond=1101 */
#define I_COND_FBULE         14  /* cond=1110 */
#define I_COND_FBO           15  /* cond=1111 */

/*
 * op=10
 * op3=111010
 * cc12_11=00
 */


/*
 * op=00
 */

#define I_OP2_ILLTRAP        0  /* op2=000 */
#define I_OP2_BPCC           1  /* op2=001 */
#define I_OP2_BICC           2  /* op2=010 */
#define I_OP2_BPR            3  /* op2=011 */
#define I_OP2_SETHI          4  /* op2=100 */
#define I_OP2_FBPFCC         5  /* op2=101 */
#define I_OP2_FBFCC          6  /* op2=110 */

/*
 * op=11
 */

#define I_OP3_LDUW           0  /* op3=000000 */
#define I_OP3_LDUB           1  /* op3=000001 */
#define I_OP3_LDUH           2  /* op3=000010 */
#define I_OP3_LDD            3  /* op3=000011 */
#define I_OP3_STW            4  /* op3=000100 */
#define I_OP3_STB            5  /* op3=000101 */
#define I_OP3_STH            6  /* op3=000110 */
#define I_OP3_STD            7  /* op3=000111 */
#define I_OP3_LDSW           8  /* op3=001000 */
#define I_OP3_LDSB           9  /* op3=001001 */
#define I_OP3_LDSH          10  /* op3=001010 */
#define I_OP3_LDX           11  /* op3=001011 */
#define I_OP3_LDSTUB        13  /* op3=001101 */
#define I_OP3_STX           14  /* op3=001110 */
#define I_OP3_SWAP          15  /* op3=001111 */
#define I_OP3_LDUWA         16  /* op3=010000 */
#define I_OP3_LDUBA         17  /* op3=010001 */
#define I_OP3_LDUHA         18  /* op3=010010 */
#define I_OP3_LDDA          19  /* op3=010011 */
#define I_OP3_STWA          20  /* op3=010100 */
#define I_OP3_STBA          21  /* op3=010101 */
#define I_OP3_STHA          22  /* op3=010110 */
#define I_OP3_STDA          23  /* op3=010111 */
#define I_OP3_LDSWA         24  /* op3=011000 */
#define I_OP3_LDSBA         25  /* op3=011001 */
#define I_OP3_LDSHA         26  /* op3=011010 */
#define I_OP3_LDXA          27  /* op3=011011 */
#define I_OP3_LDSTUBA       29  /* op3=011101 */
#define I_OP3_STXA          30  /* op3=011110 */
#define I_OP3_SWAPA         31  /* op3=011111 */
#define I_OP3_LDF           32  /* op3=100000 */
#define I_OP3_LDQF          34  /* op3=100010 */
#define I_OP3_LDDF          35  /* op3=100011 */
#define I_OP3_STF           36  /* op3=100100 */
#define I_OP3_STQF          38  /* op3=100110 */
#define I_OP3_STDF          39  /* op3=100111 */
#define I_OP3_PREFETCH      45  /* op3=101101 */
#define I_OP3_LDFA          48  /* op3=110000 */
#define I_OP3_LDQFA         50  /* op3=110010 */
#define I_OP3_LDDFA         51  /* op3=110011 */
#define I_OP3_STFA          52  /* op3=110100 */
#define I_OP3_STQFA         54  /* op3=110110 */
#define I_OP3_STDFA         55  /* op3=110111 */
#define I_OP3_CASA          60  /* op3=111100 */
#define I_OP3_PREFETCHA     61  /* op3=111101 */
#define I_OP3_CASXA         62  /* op3=111110 */

/*
 * op=10
 */

#define I_OP3_ADD            0  /* op3=000000 */
#define I_OP3_AND            1  /* op3=000001 */
#define I_OP3_OR             2  /* op3=000010 */
#define I_OP3_XOR            3  /* op3=000011 */
#define I_OP3_SUB            4  /* op3=000100 */
#define I_OP3_ANDN           5  /* op3=000101 */
#define I_OP3_ORN            6  /* op3=000110 */
#define I_OP3_XNOR           7  /* op3=000111 */
#define I_OP3_ADDC           8  /* op3=001000 */
#define I_OP3_MULX           9  /* op3=001001 */
#define I_OP3_UMUL          10  /* op3=001010 */
#define I_OP3_SMUL          11  /* op3=001011 */
#define I_OP3_SUBC          12  /* op3=001100 */
#define I_OP3_UDIVX         13  /* op3=001101 */
#define I_OP3_UDIV          14  /* op3=001110 */
#define I_OP3_SDIV          15  /* op3=001111 */
#define I_OP3_ADDCC         16  /* op3=010000 */
#define I_OP3_ANDCC         17  /* op3=010001 */
#define I_OP3_ORCC          18  /* op3=010010 */
#define I_OP3_XORCC         19  /* op3=010011 */
#define I_OP3_SUBCC         20  /* op3=010100 */
#define I_OP3_ANDNCC        21  /* op3=010101 */
#define I_OP3_ORNCC         22  /* op3=010110 */
#define I_OP3_XNORCC        23  /* op3=010111 */
#define I_OP3_ADDCCC        24  /* op3=011000 */
#define I_OP3_UMULXCC       25  /* op3=011001 */
#define I_OP3_UMULCC        26  /* op3=011010 */
#define I_OP3_SMULCC        27  /* op3=011011 */
#define I_OP3_SUBCCC        28  /* op3=011100 */
#define I_OP3_SDIVXCC       29  /* op3=011101 */
#define I_OP3_UDIVCC        30  /* op3=011110 */
#define I_OP3_SDIVCC        31  /* op3=011111 */
#define I_OP3_TADDCC        32  /* op3=100000 */
#define I_OP3_TSUBCC        33  /* op3=100001 */
#define I_OP3_TADDCCTV      34  /* op3=100010 */
#define I_OP3_TSUBCCTV      35  /* op3=100011 */
#define I_OP3_MULSCC        36  /* op3=100100 */
#define I_OP3_RDASR         40  /* op3=101000 */
#define I_OP3_RDPR          42  /* op3=101010 */
#define I_OP3_FLUSHW        43  /* op3=101011 */
#define I_OP3_MOVCC         44  /* op3=101100 */
#define I_OP3_SDIVX         45  /* op3=101101 */
#define I_OP3_MOVR          47  /* op3=101111 */
#define I_OP3_WRASR         48  /* op3=110000 */
#define I_OP3_WRPR          50  /* op3=110010 */
#define I_OP3_FPOP1         52  /* op3=110100 */
#define I_OP3_FPOP2         53  /* op3=110101 */
#define I_OP3_IMPDEP1       54  /* op3=110110 */
#define I_OP3_IMPDEP2       55  /* op3=110111 */
#define I_OP3_JMPL          56  /* op3=111000 */
#define I_OP3_RETURN        57  /* op3=111001 */
#define I_OP3_TICC          58  /* op3=111010 */
#define I_OP3_FLUSH         59  /* op3=111011 */
#define I_OP3_SAVE          60  /* op3=111100 */
#define I_OP3_RESTORE       61  /* op3=111101 */

/*
 * op=10
 * op3=110100
 */

#define I_OPF_FMOVS          1  /* opf=000000001 */
#define I_OPF_FMOVD          2  /* opf=000000010 */
#define I_OPF_FMOVQ          3  /* opf=000000011 */
#define I_OPF_FNEGS          5  /* opf=000000101 */
#define I_OPF_FNEGD          6  /* opf=000000110 */
#define I_OPF_FNEGQ          7  /* opf=000000111 */
#define I_OPF_FABSS          9  /* opf=000001001 */
#define I_OPF_FABSD         10  /* opf=000001010 */
#define I_OPF_FABSQ         11  /* opf=000001011 */
#define I_OPF_FSQRTS        41  /* opf=000101001 */
#define I_OPF_FSQRTD        42  /* opf=000101010 */
#define I_OPF_FSQRTQ        43  /* opf=000101011 */
#define I_OPF_FADDS         65  /* opf=001000001 */
#define I_OPF_FADDD         66  /* opf=001000010 */
#define I_OPF_FADDQ         67  /* opf=001000011 */
#define I_OPF_FSUBS         69  /* opf=001000101 */
#define I_OPF_FSUBD         70  /* opf=001000110 */
#define I_OPF_FSUBQ         71  /* opf=001000111 */
#define I_OPF_FMULS         73  /* opf=001001001 */
#define I_OPF_FMULD         74  /* opf=001001010 */
#define I_OPF_FMULQ         75  /* opf=001001011 */
#define I_OPF_FDIVS         77  /* opf=001001101 */
#define I_OPF_FDIVD         78  /* opf=001001110 */
#define I_OPF_FDIVQ         79  /* opf=001001111 */
#define I_OPF_FSMULD       105  /* opf=001101001 */
#define I_OPF_FDMULQ       110  /* opf=001101110 */
#define I_OPF_FSTOX        129  /* opf=010000001 */
#define I_OPF_FDTOX        130  /* opf=010000010 */
#define I_OPF_FQTOX        131  /* opf=010000011 */
#define I_OPF_FXTOS        132  /* opf=010000100 */
#define I_OPF_FXTOD        136  /* opf=010001000 */
#define I_OPF_FXTOQ        140  /* opf=010001100 */
#define I_OPF_FITOS        196  /* opf=011000100 */
#define I_OPF_FDTOS        198  /* opf=011000110 */
#define I_OPF_FQTOS        199  /* opf=011000111 */
#define I_OPF_FITOD        200  /* opf=011001000 */
#define I_OPF_FSTOD        201  /* opf=011001001 */
#define I_OPF_FQTOD        203  /* opf=011001011 */
#define I_OPF_FITOQ        204  /* opf=011001100 */
#define I_OPF_FSTOQ        205  /* opf=011001101 */
#define I_OPF_FDTOQ        206  /* opf=011001110 */
#define I_OPF_FSTOI        209  /* opf=011010001 */
#define I_OPF_FDTOI        210  /* opf=011010010 */
#define I_OPF_FQTOI        211  /* opf=011010011 */

/*
 * op=10
 * op3=110101
 */

#define I_OPF_FMOVRSZ       37  /* opf=000100101 */
#define I_OPF_FMOVRDZ       38  /* opf=000100110 */
#define I_OPF_FMOVRQZ       39  /* opf=000100111 */
#define I_OPF_FMOVRSLEZ     69  /* opf=001000101 */
#define I_OPF_FMOVRDLEZ     70  /* opf=001000110 */
#define I_OPF_FMOVRQLEZ     71  /* opf=001000111 */
#define I_OPF_FMOVRSLZ     101  /* opf=001100101 */
#define I_OPF_FMOVRDLZ     102  /* opf=001100110 */
#define I_OPF_FMOVRQLZ     103  /* opf=001100111 */
#define I_OPF_FMOVRSNZ     165  /* opf=010100101 */
#define I_OPF_FMOVRDNZ     166  /* opf=010100110 */
#define I_OPF_FMOVRQNZ     167  /* opf=010100111 */
#define I_OPF_FMOVRSGZ     197  /* opf=011000101 */
#define I_OPF_FMOVRDGZ     198  /* opf=011000110 */
#define I_OPF_FMOVRQGZ     199  /* opf=011000111 */
#define I_OPF_FMOVRSGEZ    229  /* opf=011100101 */
#define I_OPF_FMOVRDGEZ    230  /* opf=011100110 */
#define I_OPF_FMOVRQGEZ    231  /* opf=011100111 */

/*
 * op=10
 * op3=110110
 */

#define I_OPF_EDGE8          0  /* opf=000000000 */
#define I_OPF_EDGE8L         2  /* opf=000000010 */
#define I_OPF_EDGE16         4  /* opf=000000100 */
#define I_OPF_EDGE16L        6  /* opf=000000110 */
#define I_OPF_EDGE32         8  /* opf=000001000 */
#define I_OPF_EDGE32L       10  /* opf=000001010 */
#define I_OPF_ARRAY8        16  /* opf=000010000 */
#define I_OPF_ARRAY16       18  /* opf=000010010 */
#define I_OPF_ARRAY32       20  /* opf=000010100 */
#define I_OPF_ALIGNADDR     24  /* opf=000011000 */
#define I_OPF_ALIGNADDRL    26  /* opf=000011010 */
#define I_OPF_FCMPLE16      32  /* opf=000100000 */
#define I_OPF_FCMPNE16      34  /* opf=000100010 */
#define I_OPF_FCMPLE32      36  /* opf=000100100 */
#define I_OPF_FCMPNE32      38  /* opf=000100110 */
#define I_OPF_FCMPGT16      40  /* opf=000101000 */
#define I_OPF_FCMPEQ16      42  /* opf=000101010 */
#define I_OPF_FCMPGT32      44  /* opf=000101100 */
#define I_OPF_FCMPEQ32      46  /* opf=000101110 */
#define I_OPF_FMUL8X16      49  /* opf=000110001 */
#define I_OPF_FMUL8X16AU    51  /* opf=000110011 */
#define I_OPF_FMUL8X16AL    53  /* opf=000110101 */
#define I_OPF_FMUL8SUX16    54  /* opf=000110110 */
#define I_OPF_FMUL8ULX16    55  /* opf=000110111 */
#define I_OPF_FMULD8SUX16   56  /* opf=000111000 */
#define I_OPF_FMULD8ULX16   57  /* opf=000111001 */
#define I_OPF_FPACK32       58  /* opf=000111010 */
#define I_OPF_FPACK16       59  /* opf=000111011 */
#define I_OPF_FPACKFIX      61  /* opf=000111101 */
#define I_OPF_PDIST         62  /* opf=000111110 */
#define I_OPF_FALIGNDATA    72  /* opf=001001000 */
#define I_OPF_FPMERGE       75  /* opf=001001011 */
#define I_OPF_FEXPAND       77  /* opf=001001101 */
#define I_OPF_FPADD16       80  /* opf=001010000 */
#define I_OPF_FPADD16S      81  /* opf=001010001 */
#define I_OPF_FPADD32       82  /* opf=001010010 */
#define I_OPF_FPADD32S      83  /* opf=001010011 */
#define I_OPF_FPSUB16       84  /* opf=001010100 */
#define I_OPF_FPSUB16S      85  /* opf=001010101 */
#define I_OPF_FPSUB32       86  /* opf=001010110 */
#define I_OPF_FPSUB32S      87  /* opf=001010111 */
#define I_OPF_FZERO         96  /* opf=001100000 */
#define I_OPF_FZEROS        97  /* opf=001100001 */
#define I_OPF_FNOR          98  /* opf=001100010 */
#define I_OPF_FNORS         99  /* opf=001100011 */
#define I_OPF_FANDNOT2     100  /* opf=001100100 */
#define I_OPF_FANDNOT2S    101  /* opf=001100101 */
#define I_OPF_FNOT2        102  /* opf=001100110 */
#define I_OPF_FNOT2S       103  /* opf=001100111 */
#define I_OPF_FANDNOT1     104  /* opf=001101000 */
#define I_OPF_FANDNOT1S    105  /* opf=001101001 */
#define I_OPF_FNOT1        106  /* opf=001101010 */
#define I_OPF_FNOT1S       107  /* opf=001101011 */
#define I_OPF_FXOR         108  /* opf=001101100 */
#define I_OPF_FXORS        109  /* opf=001101101 */
#define I_OPF_FNAND        110  /* opf=001101110 */
#define I_OPF_FNANDS       111  /* opf=001101111 */
#define I_OPF_FAND         112  /* opf=001110000 */
#define I_OPF_FANDS        113  /* opf=001110001 */
#define I_OPF_FXNOR        114  /* opf=001110010 */
#define I_OPF_FXNORS       115  /* opf=001110011 */
#define I_OPF_FSRC1        116  /* opf=001110100 */
#define I_OPF_FSRC1S       117  /* opf=001110101 */
#define I_OPF_FORNOT2      118  /* opf=001110110 */
#define I_OPF_FORNOT2S     119  /* opf=001110111 */
#define I_OPF_FSRC2        120  /* opf=001111000 */
#define I_OPF_FSRC2S       121  /* opf=001111001 */
#define I_OPF_FORNOT1      122  /* opf=001111010 */
#define I_OPF_FORNOT1S     123  /* opf=001111011 */
#define I_OPF_FOR          124  /* opf=001111100 */
#define I_OPF_FORS         125  /* opf=001111101 */
#define I_OPF_FONE         126  /* opf=001111110 */
#define I_OPF_FONES        127  /* opf=001111111 */
#define I_OPF_SHUTDOWN     128  /* opf=010000000 */


#endif /*INAMES_h_*/
