/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef reguse_h_
#define reguse_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


#define RUS_NRUS	5


typedef struct {
	char	ru_reg;		/* RU_RS1, etc., or RU_REG|reg# */
	char	ru_type;	/* register set, q.v. */
	char	ru_nregs;	/* 0 single, 1 double, 3 quadruple */
	char	ru_io;		/* 0 in, 1 out */
	char	ru_imm;		/* i_i==0 */
} RegUse;


/*
 * ru_reg values:
 */
#define RU_RD	0
#define RU_RS1	1
#define RU_RS2	2

#define RU_Q	4
#define RU_SR	5
#define RU_PSR	6
#define RU_TBR	7
#define RU_WIM	8
#define RU_Y	9

#define RU_REG	0x40


/*
 * ru_type values:
 */
#define RU_INT	0
#define RU_FP	1
#define RU_CP	2


typedef struct {
	char	rus_nrus;
	RegUse	rus_rus[RUS_NRUS];
} RegUses;


#endif /*reguse_h_*/
