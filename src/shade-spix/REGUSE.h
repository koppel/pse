/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */
/* @(#)Shade and Spixtools V5.33A, V9 SPARC Solaris */

	case IH_ADD:
	case IH_ADDC:
	case IH_ADDCC:
	case IH_ADDCCC:
	case IH_AND:
	case IH_ANDCC:
	case IH_ANDN:
	case IH_ANDNCC:
	case IH_JMPL:
	case IH_LDSB:
	case IH_LDSBA:
	case IH_LDSH:
	case IH_LDSHA:
	case IH_LDSTUB:
	case IH_LDSTUBA:
	case IH_LDSW:
	case IH_LDSWA:
	case IH_LDUB:
	case IH_LDUBA:
	case IH_LDUH:
	case IH_LDUHA:
	case IH_LDUW:
	case IH_LDUWA:
	case IH_LDX:
	case IH_LDXA:
	case IH_MULX:
	case IH_OR:
	case IH_ORCC:
	case IH_ORN:
	case IH_ORNCC:
	case IH_RESTORE:
	case IH_SAVE:
	case IH_SDIVX:
	case IH_SDIVXCC:
	case IH_SLL:
	case IH_SLLX:
	case IH_SRA:
	case IH_SRAX:
	case IH_SRL:
	case IH_SRLX:
	case IH_SUB:
	case IH_SUBC:
	case IH_SUBCC:
	case IH_SUBCCC:
	case IH_TADDCC:
	case IH_TADDCCTV:
	case IH_TSUBCC:
	case IH_TSUBCCTV:
	case IH_UDIVX:
	case IH_UMULXCC:
	case IH_XNOR:
	case IH_XNORCC:
	case IH_XOR:
	case IH_XORCC:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRRD
		break;

	case IH_MOVRGEZ:
	case IH_MOVRGZ:
	case IH_MOVRLEZ:
	case IH_MOVRLZ:
	case IH_MOVRNZ:
	case IH_MOVRZ:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRRD
		RURDRD
		break;

	case IH_ALIGNADDR:
	case IH_ALIGNADDRL:
	case IH_ARRAY16:
	case IH_ARRAY32:
	case IH_ARRAY8:
	case IH_EDGE16:
	case IH_EDGE16L:
	case IH_EDGE32:
	case IH_EDGE32L:
	case IH_EDGE8:
	case IH_EDGE8L:
		RURDRS1
		RURDRS2
		RUWRRD
		break;
	case IH_BA:
	case IH_BAA:
	case IH_BCC:
	case IH_BCCA:
	case IH_BCS:
	case IH_BCSA:
	case IH_BE:
	case IH_BEA:
	case IH_BG:
	case IH_BGA:
	case IH_BGE:
	case IH_BGEA:
	case IH_BGU:
	case IH_BGUA:
	case IH_BL:
	case IH_BLA:
	case IH_BLE:
	case IH_BLEA:
	case IH_BLEU:
	case IH_BLEUA:
	case IH_BN:
	case IH_BNA:
	case IH_BNE:
	case IH_BNEA:
	case IH_BNEG:
	case IH_BNEGA:
	case IH_BPAAPN:
	case IH_BPAAPT:
	case IH_BPAPN:
	case IH_BPAPT:
	case IH_BPCCAPN:
	case IH_BPCCAPT:
	case IH_BPCCPN:
	case IH_BPCCPT:
	case IH_BPCSAPN:
	case IH_BPCSAPT:
	case IH_BPCSPN:
	case IH_BPCSPT:
	case IH_BPEAPN:
	case IH_BPEAPT:
	case IH_BPEPN:
	case IH_BPEPT:
	case IH_BPGAPN:
	case IH_BPGAPT:
	case IH_BPGPN:
	case IH_BPGPT:
	case IH_BPGEAPN:
	case IH_BPGEAPT:
	case IH_BPGEPN:
	case IH_BPGEPT:
	case IH_BPGUAPN:
	case IH_BPGUAPT:
	case IH_BPGUPN:
	case IH_BPGUPT:
	case IH_BPLAPN:
	case IH_BPLAPT:
	case IH_BPLPN:
	case IH_BPLPT:
	case IH_BPLEAPN:
	case IH_BPLEAPT:
	case IH_BPLEPN:
	case IH_BPLEPT:
	case IH_BPLEUAPN:
	case IH_BPLEUAPT:
	case IH_BPLEUPN:
	case IH_BPLEUPT:
	case IH_BPNAPN:
	case IH_BPNAPT:
	case IH_BPNPN:
	case IH_BPNPT:
	case IH_BPNEAPN:
	case IH_BPNEAPT:
	case IH_BPNEPN:
	case IH_BPNEPT:
	case IH_BPNEGAPN:
	case IH_BPNEGAPT:
	case IH_BPNEGPN:
	case IH_BPNEGPT:
	case IH_BPOS:
	case IH_BPOSA:
	case IH_BPPOSAPN:
	case IH_BPPOSAPT:
	case IH_BPPOSPN:
	case IH_BPPOSPT:
	case IH_BPVCAPN:
	case IH_BPVCAPT:
	case IH_BPVCPN:
	case IH_BPVCPT:
	case IH_BPVSAPN:
	case IH_BPVSAPT:
	case IH_BPVSPN:
	case IH_BPVSPT:
	case IH_BVC:
	case IH_BVCA:
	case IH_BVS:
	case IH_BVSA:
	case IH_DONE:
	case IH_FBA:
	case IH_FBAA:
	case IH_FBE:
	case IH_FBEA:
	case IH_FBG:
	case IH_FBGA:
	case IH_FBGE:
	case IH_FBGEA:
	case IH_FBL:
	case IH_FBLA:
	case IH_FBLE:
	case IH_FBLEA:
	case IH_FBLG:
	case IH_FBLGA:
	case IH_FBN:
	case IH_FBNA:
	case IH_FBNE:
	case IH_FBNEA:
	case IH_FBO:
	case IH_FBOA:
	case IH_FBPAAPN:
	case IH_FBPAAPT:
	case IH_FBPAPN:
	case IH_FBPAPT:
	case IH_FBPEAPN:
	case IH_FBPEAPT:
	case IH_FBPEPN:
	case IH_FBPEPT:
	case IH_FBPGAPN:
	case IH_FBPGAPT:
	case IH_FBPGPN:
	case IH_FBPGPT:
	case IH_FBPGEAPN:
	case IH_FBPGEAPT:
	case IH_FBPGEPN:
	case IH_FBPGEPT:
	case IH_FBPLAPN:
	case IH_FBPLAPT:
	case IH_FBPLPN:
	case IH_FBPLPT:
	case IH_FBPLEAPN:
	case IH_FBPLEAPT:
	case IH_FBPLEPN:
	case IH_FBPLEPT:
	case IH_FBPLGAPN:
	case IH_FBPLGAPT:
	case IH_FBPLGPN:
	case IH_FBPLGPT:
	case IH_FBPNAPN:
	case IH_FBPNAPT:
	case IH_FBPNPN:
	case IH_FBPNPT:
	case IH_FBPNEAPN:
	case IH_FBPNEAPT:
	case IH_FBPNEPN:
	case IH_FBPNEPT:
	case IH_FBPOAPN:
	case IH_FBPOAPT:
	case IH_FBPOPN:
	case IH_FBPOPT:
	case IH_FBPUAPN:
	case IH_FBPUAPT:
	case IH_FBPUPN:
	case IH_FBPUPT:
	case IH_FBPUEAPN:
	case IH_FBPUEAPT:
	case IH_FBPUEPN:
	case IH_FBPUEPT:
	case IH_FBPUGAPN:
	case IH_FBPUGAPT:
	case IH_FBPUGPN:
	case IH_FBPUGPT:
	case IH_FBPUGEAPN:
	case IH_FBPUGEAPT:
	case IH_FBPUGEPN:
	case IH_FBPUGEPT:
	case IH_FBPULAPN:
	case IH_FBPULAPT:
	case IH_FBPULPN:
	case IH_FBPULPT:
	case IH_FBPULEAPN:
	case IH_FBPULEAPT:
	case IH_FBPULEPN:
	case IH_FBPULEPT:
	case IH_FBU:
	case IH_FBUA:
	case IH_FBUE:
	case IH_FBUEA:
	case IH_FBUG:
	case IH_FBUGA:
	case IH_FBUGE:
	case IH_FBUGEA:
	case IH_FBUL:
	case IH_FBULA:
	case IH_FBULE:
	case IH_FBULEA:
	case IH_FLUSHW:
	case IH_ILLTRAP:
	case IH_IMPDEP1:
	case IH_IMPDEP2:
	case IH_MEMBAR:
	case IH_NOP:
	case IH_RESTORED:
	case IH_RETRY:
	case IH_SAVED:
	case IH_SHUTDOWN:
	case IH_SIR:
		break;
	case IH_BRGEZAPN:
	case IH_BRGEZAPT:
	case IH_BRGEZPN:
	case IH_BRGEZPT:
	case IH_BRGZAPN:
	case IH_BRGZAPT:
	case IH_BRGZPN:
	case IH_BRGZPT:
	case IH_BRLEZAPN:
	case IH_BRLEZAPT:
	case IH_BRLEZPN:
	case IH_BRLEZPT:
	case IH_BRLZAPN:
	case IH_BRLZAPT:
	case IH_BRLZPN:
	case IH_BRLZPT:
	case IH_BRNZAPN:
	case IH_BRNZAPT:
	case IH_BRNZPN:
	case IH_BRNZPT:
	case IH_BRZAPN:
	case IH_BRZAPT:
	case IH_BRZPN:
	case IH_BRZPT:
		RURDRS1
		break;
	case IH_CALL:
		RUWRREG15
		break;
	case IH_CASA:
	case IH_CASXA:
		RURDRS1
		RURDRS2
		RURDRD
		RUWRRD
		break;
	case IH_FABSD:
	case IH_FDTOX:
	case IH_FMOVDA:
	case IH_FMOVDFA:
	case IH_FMOVDFN:
	case IH_FMOVDN:
	case IH_FMOVD:
	case IH_FNEGD:
	case IH_FNOT2:
	case IH_FSQRTD:
	case IH_FSRC2:
	case IH_FXTOD:
		RURD2FRS2
		RUWR2FRD
		break;

	case IH_FMOVDCC:
	case IH_FMOVDCS:
	case IH_FMOVDE:
	case IH_FMOVDFE:
	case IH_FMOVDFG:
	case IH_FMOVDFGE:
	case IH_FMOVDFL:
	case IH_FMOVDFLE:
	case IH_FMOVDFLG:
	case IH_FMOVDFNE:
	case IH_FMOVDFO:
	case IH_FMOVDFU:
	case IH_FMOVDFUE:
	case IH_FMOVDFUG:
	case IH_FMOVDFUGE:
	case IH_FMOVDFUL:
	case IH_FMOVDFULE:
	case IH_FMOVDG:
	case IH_FMOVDGE:
	case IH_FMOVDGU:
	case IH_FMOVDL:
	case IH_FMOVDLE:
	case IH_FMOVDLEU:
	case IH_FMOVDNE:
	case IH_FMOVDNEG:
	case IH_FMOVDPOS:
	case IH_FMOVDVC:
	case IH_FMOVDVS:
                RURDCC_13
		RURD2FRS2
		RUWR2FRD
		RURD2FRD
		break;

	case IH_FABSQ:
	case IH_FMOVQA:
	case IH_FMOVQFA:
	case IH_FMOVQFN:
	case IH_FMOVQN:
	case IH_FNEGQ:
	case IH_FSQRTQ:
		RURD4FRS2
		RUWR4FRD
		break;

	case IH_FMOVQ:
	case IH_FMOVQCC:
	case IH_FMOVQCS:
	case IH_FMOVQE:
	case IH_FMOVQFE:
	case IH_FMOVQFG:
	case IH_FMOVQFGE:
	case IH_FMOVQFL:
	case IH_FMOVQFLE:
	case IH_FMOVQFLG:
	case IH_FMOVQFNE:
	case IH_FMOVQFO:
	case IH_FMOVQFU:
	case IH_FMOVQFUE:
	case IH_FMOVQFUG:
	case IH_FMOVQFUGE:
	case IH_FMOVQFUL:
	case IH_FMOVQFULE:
	case IH_FMOVQG:
	case IH_FMOVQGE:
	case IH_FMOVQGU:
	case IH_FMOVQL:
	case IH_FMOVQLE:
	case IH_FMOVQLEU:
	case IH_FMOVQNE:
	case IH_FMOVQNEG:
	case IH_FMOVQPOS:
	case IH_FMOVQVC:
	case IH_FMOVQVS:
                RURDCC_13
		RURD4FRS2
		RUWR4FRD
		RURD4FRD
		break;

	case IH_FABSS:
	case IH_FITOS:
	case IH_FMOVSA:
	case IH_FMOVSFA:
	case IH_FMOVSFN:
	case IH_FMOVSN:
	case IH_FNEGS:
	case IH_FNOT2S:
	case IH_FSQRTS:
	case IH_FSRC2S:
	case IH_FSTOI:
		RURDFRS2
		RUWRFRD
		break;

	case IH_FMOVS:
	case IH_FMOVSCC:
	case IH_FMOVSCS:
	case IH_FMOVSE:
	case IH_FMOVSFE:
	case IH_FMOVSFG:
	case IH_FMOVSFGE:
	case IH_FMOVSFL:
	case IH_FMOVSFLE:
	case IH_FMOVSFLG:
	case IH_FMOVSFNE:
	case IH_FMOVSFO:
	case IH_FMOVSFU:
	case IH_FMOVSFUE:
	case IH_FMOVSFUG:
	case IH_FMOVSFUGE:
	case IH_FMOVSFUL:
	case IH_FMOVSFULE:
	case IH_FMOVSG:
	case IH_FMOVSGE:
	case IH_FMOVSGU:
	case IH_FMOVSL:
	case IH_FMOVSLE:
	case IH_FMOVSLEU:
	case IH_FMOVSNE:
	case IH_FMOVSNEG:
	case IH_FMOVSPOS:
	case IH_FMOVSVC:
	case IH_FMOVSVS:
                RURDCC_13
		RURDFRS2
		RURDFRD
		RUWRFRD
		break;

	case IH_FADDD:
	case IH_FALIGNDATA:
	case IH_FAND:
	case IH_FANDNOT1:
	case IH_FANDNOT2:
	case IH_FDIVD:
	case IH_FMUL8SUX16:
	case IH_FMUL8ULX16:
	case IH_FMULD:
	case IH_FNAND:
	case IH_FNOR:
	case IH_FOR:
	case IH_FORNOT1:
	case IH_FORNOT2:
	case IH_FPACK32:
	case IH_FPADD16:
	case IH_FPADD32:
	case IH_FPSUB16:
	case IH_FPSUB32:
	case IH_FSUBD:
	case IH_FXNOR:
	case IH_FXOR:
		RURD2FRS1
		RURD2FRS2
		RUWR2FRD
		break;
	case IH_FADDQ:
	case IH_FDIVQ:
	case IH_FMULQ:
	case IH_FSUBQ:
		RURD4FRS1
		RURD4FRS2
		RUWR4FRD
		break;
	case IH_FADDS:
	case IH_FANDNOT1S:
	case IH_FANDNOT2S:
	case IH_FANDS:
	case IH_FDIVS:
	case IH_FMULS:
	case IH_FNANDS:
	case IH_FNORS:
	case IH_FORNOT1S:
	case IH_FORNOT2S:
	case IH_FORS:
	case IH_FPADD16S:
	case IH_FPADD32S:
	case IH_FPSUB16S:
	case IH_FPSUB32S:
	case IH_FSUBS:
	case IH_FXNORS:
	case IH_FXORS:
		RURDFRS1
		RURDFRS2
		RUWRFRD
		break;
	case IH_FCMPD:
	case IH_FCMPED:
		RURD2FRS1
		RURD2FRS2
		break;
	case IH_FCMPEQ:
	case IH_FCMPQ:
		RURD4FRS1
		RURD4FRS2
		break;
	case IH_FCMPEQ16:
	case IH_FCMPEQ32:
	case IH_FCMPGT16:
	case IH_FCMPGT32:
	case IH_FCMPLE16:
	case IH_FCMPLE32:
	case IH_FCMPNE16:
	case IH_FCMPNE32:
		RURD2FRS1
		RURD2FRS2
		RUWRRD
		break;
	case IH_FCMPES:
	case IH_FCMPS:
		RURDFRS1
		RURDFRS2
		break;
	case IH_FDMULQ:
		RURD2FRS1
		RURD2FRS2
		RUWR4FRD
		break;
	case IH_FDTOI:
	case IH_FDTOS:
	case IH_FPACK16:
	case IH_FPACKFIX:
	case IH_FXTOS:
		RURD2FRS2
		RUWRFRD
		break;
	case IH_FDTOQ:
	case IH_FXTOQ:
		RURD2FRS2
		RUWR4FRD
		break;
	case IH_FEXPAND:
	case IH_FITOD:
	case IH_FSTOD:
	case IH_FSTOX:
		RURDFRS2
		RUWR2FRD
		break;
	case IH_FITOQ:
	case IH_FSTOQ:
		RURDFRS2
		RUWR4FRD
		break;
	case IH_FLUSH:
	case IH_PREFETCH:
	case IH_PREFETCHA:
	case IH_RETURN:
	case IH_TA:
	case IH_TCC:
	case IH_TCS:
	case IH_TE:
	case IH_TG:
	case IH_TGE:
	case IH_TGU:
	case IH_TL:
	case IH_TLE:
	case IH_TLEU:
	case IH_TN:
	case IH_TNE:
	case IH_TNEG:
	case IH_TPOSZ:
	case IH_TVC:
	case IH_TVS:
	case IH_WRASI:
	case IH_WRASR:
	case IH_WRCCR:
	case IH_WRFPRS:
	case IH_WRPR:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		break;
	case IH_FMOVRDGEZ:
	case IH_FMOVRDGZ:
	case IH_FMOVRDLEZ:
	case IH_FMOVRDLZ:
	case IH_FMOVRDNZ:
	case IH_FMOVRDZ:
		RURDRS1
		RURD2FRS2
		RURD2FRD
		RUWR2FRD
		break;
	case IH_FMOVRQGEZ:
	case IH_FMOVRQGZ:
	case IH_FMOVRQLEZ:
	case IH_FMOVRQLZ:
	case IH_FMOVRQNZ:
	case IH_FMOVRQZ:
		RURDRS1
		RURD4FRS2
		RURD4FRD
		RUWR4FRD
		break;
	case IH_FMOVRSGEZ:
	case IH_FMOVRSGZ:
	case IH_FMOVRSLEZ:
	case IH_FMOVRSLZ:
	case IH_FMOVRSNZ:
	case IH_FMOVRSZ:
		RURDRS1
		RURDFRS2
		RURDFRD
		RUWRFRD
		break;
	case IH_FMUL8X16:
		RURDFRS1
		RURD2FRS2
		RUWR2FRD
		break;
	case IH_FMUL8X16AL:
	case IH_FMUL8X16AU:
	case IH_FMULD8SUX16:
	case IH_FMULD8ULX16:
	case IH_FPMERGE:
	case IH_FSMULD:
		RURDFRS1
		RURDFRS2
		RUWR2FRD
		break;
	case IH_FNOT1:
	case IH_FSRC1:
		RURD2FRS1
		RUWR2FRD
		break;
	case IH_FNOT1S:
	case IH_FSRC1S:
		RURDFRS1
		RUWRFRD
		break;
	case IH_FONE:
	case IH_FZERO:
		RUWR2FRD
		break;
	case IH_FONES:
	case IH_FZEROS:
		RUWRFRD
		break;
	case IH_FQTOD:
	case IH_FQTOX:
		RURD4FRS2
		RUWR2FRD
		break;
	case IH_FQTOI:
	case IH_FQTOS:
		RURD4FRS2
		RUWRFRD
		break;
	case IH_LDD:
	case IH_LDDA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWR2RD
		break;
	case IH_LDDF:
	case IH_LDDFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWR2FRD
		break;
	case IH_LDF:
	case IH_LDFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRFRD
		break;
	case IH_LDFSR:
	case IH_LDXFSR:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRFSR
		break;
	case IH_LDQF:
	case IH_LDQFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWR4FRD
		break;
	case IH_MOVFE:
	case IH_MOVFG:
	case IH_MOVFGE:
	case IH_MOVFL:
	case IH_MOVFLE:
	case IH_MOVFLG:
	case IH_MOVFNE:
	case IH_MOVFO:
	case IH_MOVFU:
	case IH_MOVFUE:
	case IH_MOVFUG:
	case IH_MOVFUGE:
	case IH_MOVFUL:
	case IH_MOVFULE:
	case IH_MOVCC:
	case IH_MOVCS:
	case IH_MOVG:
	case IH_MOVGE:
	case IH_MOVGU:
	case IH_MOVL:
	case IH_MOVLE:
	case IH_MOVLEU:
	case IH_MOVNE:
	case IH_MOVNEG:
	case IH_MOVPOS:
	case IH_MOVVC:
	case IH_MOVVS:
                RURDCC_13
		if (RUNOTIMM) RURDRS2
		RURDRD
		RUWRRD
		break;

	case IH_MOVE:
	case IH_MOVA:
	case IH_MOVFA:
	case IH_MOVFN:
	case IH_MOVN:
	case IH_POPC:
		if (RUNOTIMM) RURDRS2
		RUWRRD
		break;
	case IH_MULSCC:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDY
		RUWRRD
		RUWRY
		break;
	case IH_PDIST:
		RURD2FRS1
		RURD2FRS2
		RUWR2RD
		break;
	case IH_RDASI:
	case IH_RDASR:
	case IH_RDCCR:
	case IH_RDFPRS:
	case IH_RDPC:
	case IH_RDPR:
	case IH_RDTICK:
	case IH_SETHI:
		RUWRRD
		break;
	case IH_RDY:
		RURDY
		RUWRRD
		break;
	case IH_SDIV:
	case IH_SDIVCC:
	case IH_UDIV:
	case IH_UDIVCC:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDY
		RUWRRD
		break;
	case IH_SMUL:
	case IH_SMULCC:
	case IH_UMUL:
	case IH_UMULCC:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRY
		RUWRRD
		break;
	case IH_STB:
	case IH_STBA:
	case IH_STH:
	case IH_STHA:
	case IH_STW:
	case IH_STWA:
	case IH_STX:
	case IH_STXA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDRD
		break;
	case IH_STD:
	case IH_STDA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURD2RD
		break;
	case IH_STDF:
	case IH_STDFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURD2FRD
		break;
	case IH_STF:
	case IH_STFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDFRD
		break;
	case IH_STFSR:
	case IH_STXFSR:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDFSR
		break;
	case IH_STQF:
	case IH_STQFA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURD4FRD
		break;
	case IH_SWAP:
	case IH_SWAPA:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RURDRD
		RUWRRD
		break;
	case IH_WRY:
		RURDRS1
		if (RUNOTIMM) RURDRS2
		RUWRY
		break;
