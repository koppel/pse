/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef INSTR_h_
#define INSTR_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


typedef union {
	struct { instr_t :2, _a:1, :29; } u_a;
#define i_a u_a._a
#define getA(x)	(((((uint32)(x)) <<  2)) >> 31)
#define putA(x)	((((uint32)(x)) << 31) >>  2)

	struct { instr_t :19, _asi:8, :5; } u_asi;
#define i_asi u_asi._asi
#define getASI(x)	(((((uint32)(x)) << 19)) >> 24)
#define putASI(x)	((((uint32)(x)) << 24) >> 19)

	struct { instr_t :3, _cond:4, :25; } u_cond;
#define i_cond u_cond._cond
#define getCOND(x)	(((((uint32)(x)) <<  3)) >> 28)
#define putCOND(x)	((((uint32)(x)) << 28) >>  3)

	struct { instr_t :10, _disp22:22; } u_disp22;
#define i_disp22 u_disp22._disp22
#define I_DISP22a(x)	((int32)(((uint32)((x)->i_disp22)) << 10) >> 8)
#define I_DISP22x(x)	((int32)(((uint32)((x)->i_disp22)) << 10) >> 10)
#define getDISP22(x)	(((int32)(((uint32)(x)) << 10)) >> 10)
#define putDISP22(x)	((((uint32)(x)) << 10) >> 10)

	struct { instr_t :2, _disp30:30; } u_disp30;
#define i_disp30 u_disp30._disp30
#define I_DISP30a(x)	((int32)(((uint32)((x)->i_disp30)) << 2) >> 0)
#define I_DISP30x(x)	((int32)(((uint32)((x)->i_disp30)) << 2) >> 2)
#define getDISP30(x)	(((int32)(((uint32)(x)) <<  2)) >>  2)
#define putDISP30(x)	((((uint32)(x)) <<  2) >>  2)

	struct { instr_t :18, _i:1, :13; } u_i;
#define i_i u_i._i
#define getI(x)	(((((uint32)(x)) << 18)) >> 31)
#define putI(x)	((((uint32)(x)) << 31) >> 18)

	instr_t i_instr;
#define getINSTR(x)	(((((uint32)(x)))))
#define putINSTR(x)	((((uint32)(x))))

	struct { instr_t :10, _imm22:22; } u_imm22;
#define i_imm22 u_imm22._imm22
#define getIMM22(x)	(((((uint32)(x)) << 10)) >> 10)
#define putIMM22(x)	((((uint32)(x)) << 10) >> 10)

	struct { instr_t _op:2, :30; } u_op;
#define i_op u_op._op
#define getOP(x)	(((((uint32)(x)))) >> 30)
#define putOP(x)	((((uint32)(x)) << 30))

	struct { instr_t :7, _op2:3, :22; } u_op2;
#define i_op2 u_op2._op2
#define getOP2(x)	(((((uint32)(x)) <<  7)) >> 29)
#define putOP2(x)	((((uint32)(x)) << 29) >>  7)

	struct { instr_t :7, _op3:6, :19; } u_op3;
#define i_op3 u_op3._op3
#define getOP3(x)	(((((uint32)(x)) <<  7)) >> 26)
#define putOP3(x)	((((uint32)(x)) << 26) >>  7)

	struct { instr_t :18, _opf:9, :5; } u_opf;
#define i_opf u_opf._opf
#define getOPF(x)	(((((uint32)(x)) << 18)) >> 23)
#define putOPF(x)	((((uint32)(x)) << 23) >> 18)

	struct { instr_t :2, _rd:5, :25; } u_rd;
#define i_rd u_rd._rd
#define getRD(x)	(((((uint32)(x)) <<  2)) >> 27)
#define putRD(x)	((((uint32)(x)) << 27) >>  2)

	struct { instr_t :13, _rs1:5, :14; } u_rs1;
#define i_rs1 u_rs1._rs1
#define getRS1(x)	(((((uint32)(x)) << 13)) >> 27)
#define putRS1(x)	((((uint32)(x)) << 27) >> 13)

	struct { instr_t :27, _rs2:5; } u_rs2;
#define i_rs2 u_rs2._rs2
#define getRS2(x)	(((((uint32)(x)) << 27)) >> 27)
#define putRS2(x)	((((uint32)(x)) << 27) >> 27)

	struct { instr_t :19, _simm13:13; } u_simm13;
#define i_simm13 u_simm13._simm13
#define I_SIMM13(x)	((int32)(((uint32)((x)->i_simm13)) << 19) >> 19)
#define getSIMM13(x)	(((int32)(((uint32)(x)) << 19)) >> 19)
#define putSIMM13(x)	((((uint32)(x)) << 19) >> 19)

	struct { instr_t :27, _shcnt32:5; } u_shcnt32;
#define i_shcnt32 u_shcnt32._shcnt32
#define getSHCNT32(x)	(((((uint32)(x)) << 27)) >> 27)
#define putSHCNT32(x)	((((uint32)(x)) << 27) >> 27)

	struct { instr_t :19, _cc12_11:2, :11; } u_cc12_11;
#define i_cc12_11 u_cc12_11._cc12_11
#define getCC12_11(x)	(((((uint32)(x)) << 19)) >> 30)
#define putCC12_11(x)	((((uint32)(x)) << 30) >> 19)

	struct { instr_t :10, _cc21_20:2, :20; } u_cc21_20;
#define i_cc21_20 u_cc21_20._cc21_20
#define getCC21_20(x)	(((((uint32)(x)) << 10)) >> 30)
#define putCC21_20(x)	((((uint32)(x)) << 30) >> 10)

	struct { instr_t :5, _cc26_25:2, :25; } u_cc26_25;
#define i_cc26_25 u_cc26_25._cc26_25
#define getCC26_25(x)	(((((uint32)(x)) <<  5)) >> 30)
#define putCC26_25(x)	((((uint32)(x)) << 30) >>  5)

	struct { instr_t :14, _cond17_14:4, :14; } u_cond17_14;
#define i_cond17_14 u_cond17_14._cond17_14
#define getCOND17_14(x)	(((((uint32)(x)) << 14)) >> 28)
#define putCOND17_14(x)	((((uint32)(x)) << 28) >> 14)

	struct { instr_t :18, _disp14:14; } u_disp14;
#define i_disp14 u_disp14._disp14
#define I_DISP14a(x)	((int32)(((uint32)((x)->i_disp14)) << 18) >> 16)
#define I_DISP14x(x)	((int32)(((uint32)((x)->i_disp14)) << 18) >> 18)
#define getDISP14(x)	(((((uint32)(x)) << 18)) >> 18)
#define putDISP14(x)	((((uint32)(x)) << 18) >> 18)

	struct { instr_t :13, _disp19:19; } u_disp19;
#define i_disp19 u_disp19._disp19
#define I_DISP19a(x)	((int32)(((uint32)((x)->i_disp19)) << 13) >> 11)
#define I_DISP19x(x)	((int32)(((uint32)((x)->i_disp19)) << 13) >> 13)
#define getDISP19(x)	(((int32)(((uint32)(x)) << 13)) >> 13)
#define putDISP19(x)	((((uint32)(x)) << 13) >> 13)

	struct { instr_t :10, _disp2:2, :20; } u_disp2;
#define i_disp2 u_disp2._disp2
#define I_DISP2(x)	((int32)(((uint32)((x)->i_disp2)) << 10) >> 30)
#define getDISP2(x)	(((int32)(((uint32)(x)) << 10)) >> 30)
#define putDISP2(x)	((((uint32)(x)) << 30) >> 10)

	struct { instr_t :18, _if_13:1, :13; } u_if_13;
#define i_if_13 u_if_13._if_13
#define getIF_13(x)	(((((uint32)(x)) << 18)) >> 31)
#define putIF_13(x)	((((uint32)(x)) << 31) >> 18)

	struct { instr_t :13, _if_18:1, :18; } u_if_18;
#define i_if_18 u_if_18._if_18
#define getIF_18(x)	(((((uint32)(x)) << 13)) >> 31)
#define putIF_18(x)	((((uint32)(x)) << 31) >> 13)

	struct { instr_t :2, _opf29_27:3, :27; } u_opf29_27;
#define i_opf29_27 u_opf29_27._opf29_27
#define getOPF29_27(x)	(((((uint32)(x)) <<  2)) >> 29)
#define putOPF29_27(x)	((((uint32)(x)) << 29) >>  2)

	struct { instr_t :12, _p:1, :19; } u_p;
#define i_p u_p._p
#define getP(x)	(((((uint32)(x)) << 12)) >> 31)
#define putP(x)	((((uint32)(x)) << 31) >> 12)

	struct { instr_t :19, _rcond12_10:3, :10; } u_rcond12_10;
#define i_rcond12_10 u_rcond12_10._rcond12_10
#define getRCOND12_10(x)	(((((uint32)(x)) << 19)) >> 29)
#define putRCOND12_10(x)	((((uint32)(x)) << 29) >> 19)

	struct { instr_t :26, _shcnt64:6; } u_shcnt64;
#define i_shcnt64 u_shcnt64._shcnt64
#define getSHCNT64(x)	(((((uint32)(x)) << 26)) >> 26)
#define putSHCNT64(x)	((((uint32)(x)) << 26) >> 26)

	struct { instr_t :22, _simm10:10; } u_simm10;
#define i_simm10 u_simm10._simm10
#define I_SIMM10(x)	((int32)(((uint32)((x)->i_simm10)) << 22) >> 22)
#define getSIMM10(x)	(((int32)(((uint32)(x)) << 22)) >> 22)
#define putSIMM10(x)	((((uint32)(x)) << 22) >> 22)

	struct { instr_t :21, _simm11:11; } u_simm11;
#define i_simm11 u_simm11._simm11
#define I_SIMM11(x)	((int32)(((uint32)((x)->i_simm11)) << 21) >> 21)
#define getSIMM11(x)	(((int32)(((uint32)(x)) << 21)) >> 21)
#define putSIMM11(x)	((((uint32)(x)) << 21) >> 21)

	struct { instr_t :19, _x_12:1, :12; } u_x_12;
#define i_x_12 u_x_12._x_12
#define getX_12(x)	(((((uint32)(x)) << 19)) >> 31)
#define putX_12(x)	((((uint32)(x)) << 31) >> 19)

	struct { instr_t :18, _x_13:1, :13; } u_x_13;
#define i_x_13 u_x_13._x_13
#define getX_13(x)	(((((uint32)(x)) << 18)) >> 31)
#define putX_13(x)	((((uint32)(x)) << 31) >> 18)

} Instr;


#endif /*INSTR_h_*/
