/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef _instr_h
#define _instr_h

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


#include <spixtypes.h>

typedef uint32	instr_t;	/* machine instruction */

#include "INSTR.h"

#define ISIZE	4
#define ISHIFT	2

#define I_DISP2_14a(x)	((int)((((x)->i_disp2 << 14) | (x)->i_disp14) << 16) >> 14)
#define I_DISP2_14x(x)	((int)((((x)->i_disp2 << 14) | (x)->i_disp14) << 16) >> 16)
#define getDISP2_14(x)	((getDISP2 (x) << 14) | getDISP14 (x))

#define I_ENCODE_FREGD(f)	((f) & 30 | ((f) >> 5))		/* odd double */
#define I_DECODE_FREGD(r)	((r) & 30 | ((r) & 1) << 5)
#define I_ENCODE_FREGQ(f)	((f) & 28 | ((f) >> 5))		/* odder quad */
#define I_DECODE_FREGQ(r)	((r) & 28 | ((r) & 3) << 5)


#endif /*_instr_h*/
