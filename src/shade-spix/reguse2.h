/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef reguse2_h_
#define reguse2_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


#ifndef RURD2CRD
#define RURD2CRD ;
#endif

#ifndef RURD2FRD
#define RURD2FRD ;
#endif

#ifndef RURD2FRS1
#define RURD2FRS1 ;
#endif

#ifndef RURD2FRS2
#define RURD2FRS2 ;
#endif

#ifndef RURD2RD
#define RURD2RD ;
#endif

#ifndef RURD4FRS1
#define RURD4FRS1 ;
#endif

#ifndef RURD4FRS2
#define RURD4FRS2 ;
#endif

#ifndef RURDCQ
#define RURDCQ ;
#endif

#ifndef RURDCRD
#define RURDCRD ;
#endif

#ifndef RURDCSR
#define RURDCSR ;
#endif

#ifndef RURDFQ
#define RURDFQ ;
#endif

#ifndef RURDFRD
#define RURDFRD ;
#endif

#ifndef RURDFRS1
#define RURDFRS1 ;
#endif

#ifndef RURDFRS2
#define RURDFRS2 ;
#endif

#ifndef RURDFSR
#define RURDFSR ;
#endif

#ifndef RURDPSR
#define RURDPSR ;
#endif

#ifndef RURDRD
#define RURDRD ;
#endif

#ifndef RURDRS1
#define RURDRS1 ;
#endif

#ifndef RURDRS2
#define RURDRS2 ;
#define RUNOTIMM 0
#endif

#ifndef RURDTBR
#define RURDTBR ;
#endif

#ifndef RURDWIM
#define RURDWIM ;
#endif

#ifndef RURDY
#define RURDY ;
#endif

#ifndef RUWR2CRD
#define RUWR2CRD ;
#endif

#ifndef RUWR2FRD
#define RUWR2FRD ;
#endif

#ifndef RUWR2RD
#define RUWR2RD ;
#endif

#ifndef RUWR4FRD
#define RUWR4FRD ;
#endif

#ifndef RUWRCRD
#define RUWRCRD ;
#endif

#ifndef RUWRCSR
#define RUWRCSR ;
#endif

#ifndef RUWRFRD
#define RUWRFRD ;
#endif

#ifndef RUWRFSR
#define RUWRFSR ;
#endif

#ifndef RUWRPSR
#define RUWRPSR ;
#endif

#ifndef RUWRRD
#define RUWRRD ;
#endif

#ifndef RUWRREG15
#define RUWRREG15 ;
#endif

#ifndef RUWRTBR
#define RUWRTBR ;
#endif

#ifndef RUWRWIM
#define RUWRWIM ;
#endif

#ifndef RUWRY
#define RUWRY ;
#endif


#ifndef RURD4FRD
#define RURD4FRD ;
#endif


#endif /*reguse2_h_*/
