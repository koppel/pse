/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef IHASH_h_
#define IHASH_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


typedef short ih_t;

#define NIHASH 644

#define L_opcode (11)

#define IH_ADD           (0)    /* op=10 op3=000000 */
#define IH_ADDC          (1)    /* op=10 op3=001000 */
#define IH_ADDCC         (2)    /* op=10 op3=010000 */
#define IH_ADDCCC        (3)    /* op=10 op3=011000 */
#define IH_ALIGNADDR     (4)    /* op=10 op3=110110 opf=000011000 */
#define IH_ALIGNADDRL    (5)    /* op=10 op3=110110 opf=000011010 */
#define IH_AND           (6)    /* op=10 op3=000001 */
#define IH_ANDCC         (7)    /* op=10 op3=010001 */
#define IH_ANDN          (8)    /* op=10 op3=000101 */
#define IH_ANDNCC        (9)    /* op=10 op3=010101 */
#define IH_ARRAY16      (10)    /* op=10 op3=110110 opf=000010010 */
#define IH_ARRAY32      (11)    /* op=10 op3=110110 opf=000010100 */
#define IH_ARRAY8       (12)    /* op=10 op3=110110 opf=000010000 */
#define IH_BA           (13)    /* op=00 op2=010 a=0 cond=1000 */
#define IH_BAA          (14)    /* op=00 op2=010 a=1 cond=1000 */
#define IH_BCC          (15)    /* op=00 op2=010 a=0 cond=1101 */
#define IH_BCCA         (16)    /* op=00 op2=010 a=1 cond=1101 */
#define IH_BCS          (17)    /* op=00 op2=010 a=0 cond=0101 */
#define IH_BCSA         (18)    /* op=00 op2=010 a=1 cond=0101 */
#define IH_BE           (19)    /* op=00 op2=010 a=0 cond=0001 */
#define IH_BEA          (20)    /* op=00 op2=010 a=1 cond=0001 */
#define IH_BG           (21)    /* op=00 op2=010 a=0 cond=1010 */
#define IH_BGA          (22)    /* op=00 op2=010 a=1 cond=1010 */
#define IH_BGE          (23)    /* op=00 op2=010 a=0 cond=1011 */
#define IH_BGEA         (24)    /* op=00 op2=010 a=1 cond=1011 */
#define IH_BGU          (25)    /* op=00 op2=010 a=0 cond=1100 */
#define IH_BGUA         (26)    /* op=00 op2=010 a=1 cond=1100 */
#define IH_BL           (27)    /* op=00 op2=010 a=0 cond=0011 */
#define IH_BLA          (28)    /* op=00 op2=010 a=1 cond=0011 */
#define IH_BLE          (29)    /* op=00 op2=010 a=0 cond=0010 */
#define IH_BLEA         (30)    /* op=00 op2=010 a=1 cond=0010 */
#define IH_BLEU         (31)    /* op=00 op2=010 a=0 cond=0100 */
#define IH_BLEUA        (32)    /* op=00 op2=010 a=1 cond=0100 */
#define IH_BN           (33)    /* op=00 op2=010 a=0 cond=0000 */
#define IH_BNA          (34)    /* op=00 op2=010 a=1 cond=0000 */
#define IH_BNE          (35)    /* op=00 op2=010 a=0 cond=1001 */
#define IH_BNEA         (36)    /* op=00 op2=010 a=1 cond=1001 */
#define IH_BNEG         (37)    /* op=00 op2=010 a=0 cond=0110 */
#define IH_BNEGA        (38)    /* op=00 op2=010 a=1 cond=0110 */
#define IH_BPAAPN       (39)    /* op=00 op2=001 cond=1000 a=1 p=0 */
#define IH_BPAAPT       (40)    /* op=00 op2=001 cond=1000 a=1 p=1 */
#define IH_BPAPN        (41)    /* op=00 op2=001 cond=1000 a=0 p=0 */
#define IH_BPAPT        (42)    /* op=00 op2=001 cond=1000 a=0 p=1 */
#define IH_BPCCAPN      (43)    /* op=00 op2=001 cond=1101 a=1 p=0 */
#define IH_BPCCAPT      (44)    /* op=00 op2=001 cond=1101 a=1 p=1 */
#define IH_BPCCPN       (45)    /* op=00 op2=001 cond=1101 a=0 p=0 */
#define IH_BPCCPT       (46)    /* op=00 op2=001 cond=1101 a=0 p=1 */
#define IH_BPCSAPN      (47)    /* op=00 op2=001 cond=0101 a=1 p=0 */
#define IH_BPCSAPT      (48)    /* op=00 op2=001 cond=0101 a=1 p=1 */
#define IH_BPCSPN       (49)    /* op=00 op2=001 cond=0101 a=0 p=0 */
#define IH_BPCSPT       (50)    /* op=00 op2=001 cond=0101 a=0 p=1 */
#define IH_BPEAPN       (51)    /* op=00 op2=001 cond=0001 a=1 p=0 */
#define IH_BPEAPT       (52)    /* op=00 op2=001 cond=0001 a=1 p=1 */
#define IH_BPEPN        (53)    /* op=00 op2=001 cond=0001 a=0 p=0 */
#define IH_BPEPT        (54)    /* op=00 op2=001 cond=0001 a=0 p=1 */
#define IH_BPGAPN       (55)    /* op=00 op2=001 cond=1010 a=1 p=0 */
#define IH_BPGAPT       (56)    /* op=00 op2=001 cond=1010 a=1 p=1 */
#define IH_BPGPN        (57)    /* op=00 op2=001 cond=1010 a=0 p=0 */
#define IH_BPGPT        (58)    /* op=00 op2=001 cond=1010 a=0 p=1 */
#define IH_BPGEAPN      (59)    /* op=00 op2=001 cond=1011 a=1 p=0 */
#define IH_BPGEAPT      (60)    /* op=00 op2=001 cond=1011 a=1 p=1 */
#define IH_BPGEPN       (61)    /* op=00 op2=001 cond=1011 a=0 p=0 */
#define IH_BPGEPT       (62)    /* op=00 op2=001 cond=1011 a=0 p=1 */
#define IH_BPGUAPN      (63)    /* op=00 op2=001 cond=1100 a=1 p=0 */
#define IH_BPGUAPT      (64)    /* op=00 op2=001 cond=1100 a=1 p=1 */
#define IH_BPGUPN       (65)    /* op=00 op2=001 cond=1100 a=0 p=0 */
#define IH_BPGUPT       (66)    /* op=00 op2=001 cond=1100 a=0 p=1 */
#define IH_BPLAPN       (67)    /* op=00 op2=001 cond=0011 a=1 p=0 */
#define IH_BPLAPT       (68)    /* op=00 op2=001 cond=0011 a=1 p=1 */
#define IH_BPLPN        (69)    /* op=00 op2=001 cond=0011 a=0 p=0 */
#define IH_BPLPT        (70)    /* op=00 op2=001 cond=0011 a=0 p=1 */
#define IH_BPLEAPN      (71)    /* op=00 op2=001 cond=0010 a=1 p=0 */
#define IH_BPLEAPT      (72)    /* op=00 op2=001 cond=0010 a=1 p=1 */
#define IH_BPLEPN       (73)    /* op=00 op2=001 cond=0010 a=0 p=0 */
#define IH_BPLEPT       (74)    /* op=00 op2=001 cond=0010 a=0 p=1 */
#define IH_BPLEUAPN     (75)    /* op=00 op2=001 cond=0100 a=1 p=0 */
#define IH_BPLEUAPT     (76)    /* op=00 op2=001 cond=0100 a=1 p=1 */
#define IH_BPLEUPN      (77)    /* op=00 op2=001 cond=0100 a=0 p=0 */
#define IH_BPLEUPT      (78)    /* op=00 op2=001 cond=0100 a=0 p=1 */
#define IH_BPNAPN       (79)    /* op=00 op2=001 cond=0000 a=1 p=0 */
#define IH_BPNAPT       (80)    /* op=00 op2=001 cond=0000 a=1 p=1 */
#define IH_BPNPN        (81)    /* op=00 op2=001 cond=0000 a=0 p=0 */
#define IH_BPNPT        (82)    /* op=00 op2=001 cond=0000 a=0 p=1 */
#define IH_BPNEAPN      (83)    /* op=00 op2=001 cond=1001 a=1 p=0 */
#define IH_BPNEAPT      (84)    /* op=00 op2=001 cond=1001 a=1 p=1 */
#define IH_BPNEPN       (85)    /* op=00 op2=001 cond=1001 a=0 p=0 */
#define IH_BPNEPT       (86)    /* op=00 op2=001 cond=1001 a=0 p=1 */
#define IH_BPNEGAPN     (87)    /* op=00 op2=001 cond=0110 a=1 p=0 */
#define IH_BPNEGAPT     (88)    /* op=00 op2=001 cond=0110 a=1 p=1 */
#define IH_BPNEGPN      (89)    /* op=00 op2=001 cond=0110 a=0 p=0 */
#define IH_BPNEGPT      (90)    /* op=00 op2=001 cond=0110 a=0 p=1 */
#define IH_BPOS         (91)    /* op=00 op2=010 a=0 cond=1110 */
#define IH_BPOSA        (92)    /* op=00 op2=010 a=1 cond=1110 */
#define IH_BPPOSAPN     (93)    /* op=00 op2=001 cond=1110 a=1 p=0 */
#define IH_BPPOSAPT     (94)    /* op=00 op2=001 cond=1110 a=1 p=1 */
#define IH_BPPOSPN      (95)    /* op=00 op2=001 cond=1110 a=0 p=0 */
#define IH_BPPOSPT      (96)    /* op=00 op2=001 cond=1110 a=0 p=1 */
#define IH_BPVCAPN      (97)    /* op=00 op2=001 cond=1111 a=1 p=0 */
#define IH_BPVCAPT      (98)    /* op=00 op2=001 cond=1111 a=1 p=1 */
#define IH_BPVCPN       (99)    /* op=00 op2=001 cond=1111 a=0 p=0 */
#define IH_BPVCPT      (100)    /* op=00 op2=001 cond=1111 a=0 p=1 */
#define IH_BPVSAPN     (101)    /* op=00 op2=001 cond=0111 a=1 p=0 */
#define IH_BPVSAPT     (102)    /* op=00 op2=001 cond=0111 a=1 p=1 */
#define IH_BPVSPN      (103)    /* op=00 op2=001 cond=0111 a=0 p=0 */
#define IH_BPVSPT      (104)    /* op=00 op2=001 cond=0111 a=0 p=1 */
#define IH_BRGEZAPN    (105)    /* op=00 op2=011 cond=0111 a=1 p=0 */
#define IH_BRGEZAPT    (106)    /* op=00 op2=011 cond=0111 a=1 p=1 */
#define IH_BRGEZPN     (107)    /* op=00 op2=011 cond=0111 a=0 p=0 */
#define IH_BRGEZPT     (108)    /* op=00 op2=011 cond=0111 a=0 p=1 */
#define IH_BRGZAPN     (109)    /* op=00 op2=011 cond=0110 a=1 p=0 */
#define IH_BRGZAPT     (110)    /* op=00 op2=011 cond=0110 a=1 p=1 */
#define IH_BRGZPN      (111)    /* op=00 op2=011 cond=0110 a=0 p=0 */
#define IH_BRGZPT      (112)    /* op=00 op2=011 cond=0110 a=0 p=1 */
#define IH_BRLEZAPN    (113)    /* op=00 op2=011 cond=0010 a=1 p=0 */
#define IH_BRLEZAPT    (114)    /* op=00 op2=011 cond=0010 a=1 p=1 */
#define IH_BRLEZPN     (115)    /* op=00 op2=011 cond=0010 a=0 p=0 */
#define IH_BRLEZPT     (116)    /* op=00 op2=011 cond=0010 a=0 p=1 */
#define IH_BRLZAPN     (117)    /* op=00 op2=011 cond=0011 a=1 p=0 */
#define IH_BRLZAPT     (118)    /* op=00 op2=011 cond=0011 a=1 p=1 */
#define IH_BRLZPN      (119)    /* op=00 op2=011 cond=0011 a=0 p=0 */
#define IH_BRLZPT      (120)    /* op=00 op2=011 cond=0011 a=0 p=1 */
#define IH_BRNZAPN     (121)    /* op=00 op2=011 cond=0101 a=1 p=0 */
#define IH_BRNZAPT     (122)    /* op=00 op2=011 cond=0101 a=1 p=1 */
#define IH_BRNZPN      (123)    /* op=00 op2=011 cond=0101 a=0 p=0 */
#define IH_BRNZPT      (124)    /* op=00 op2=011 cond=0101 a=0 p=1 */
#define IH_BRZAPN      (125)    /* op=00 op2=011 cond=0001 a=1 p=0 */
#define IH_BRZAPT      (126)    /* op=00 op2=011 cond=0001 a=1 p=1 */
#define IH_BRZPN       (127)    /* op=00 op2=011 cond=0001 a=0 p=0 */
#define IH_BRZPT       (128)    /* op=00 op2=011 cond=0001 a=0 p=1 */
#define IH_BVC         (129)    /* op=00 op2=010 a=0 cond=1111 */
#define IH_BVCA        (130)    /* op=00 op2=010 a=1 cond=1111 */
#define IH_BVS         (131)    /* op=00 op2=010 a=0 cond=0111 */
#define IH_BVSA        (132)    /* op=00 op2=010 a=1 cond=0111 */
#define IH_CALL        (133)    /* op=01 */
#define IH_CASA        (134)    /* op=11 op3=111100 */
#define IH_CASXA       (135)    /* op=11 op3=111110 */
#define IH_DONE        (136)    /* op=10 op3=111110 rd=00000 */
#define IH_EDGE16      (137)    /* op=10 op3=110110 opf=000000100 */
#define IH_EDGE16L     (138)    /* op=10 op3=110110 opf=000000110 */
#define IH_EDGE32      (139)    /* op=10 op3=110110 opf=000001000 */
#define IH_EDGE32L     (140)    /* op=10 op3=110110 opf=000001010 */
#define IH_EDGE8       (141)    /* op=10 op3=110110 opf=000000000 */
#define IH_EDGE8L      (142)    /* op=10 op3=110110 opf=000000010 */
#define IH_FABSD       (143)    /* op=10 op3=110100 opf=000001010 */
#define IH_FABSQ       (144)    /* op=10 op3=110100 opf=000001011 */
#define IH_FABSS       (145)    /* op=10 op3=110100 opf=000001001 */
#define IH_FADDD       (146)    /* op=10 op3=110100 opf=001000010 */
#define IH_FADDQ       (147)    /* op=10 op3=110100 opf=001000011 */
#define IH_FADDS       (148)    /* op=10 op3=110100 opf=001000001 */
#define IH_FALIGNDATA  (149)    /* op=10 op3=110110 opf=001001000 */
#define IH_FAND        (150)    /* op=10 op3=110110 opf=001110000 */
#define IH_FANDNOT1    (151)    /* op=10 op3=110110 opf=001101000 */
#define IH_FANDNOT1S   (152)    /* op=10 op3=110110 opf=001101001 */
#define IH_FANDNOT2    (153)    /* op=10 op3=110110 opf=001100100 */
#define IH_FANDNOT2S   (154)    /* op=10 op3=110110 opf=001100101 */
#define IH_FANDS       (155)    /* op=10 op3=110110 opf=001110001 */
#define IH_FBA         (156)    /* op=00 op2=110 a=0 cond=1000 */
#define IH_FBAA        (157)    /* op=00 op2=110 a=1 cond=1000 */
#define IH_FBE         (158)    /* op=00 op2=110 a=0 cond=1001 */
#define IH_FBEA        (159)    /* op=00 op2=110 a=1 cond=1001 */
#define IH_FBG         (160)    /* op=00 op2=110 a=0 cond=0110 */
#define IH_FBGA        (161)    /* op=00 op2=110 a=1 cond=0110 */
#define IH_FBGE        (162)    /* op=00 op2=110 a=0 cond=1011 */
#define IH_FBGEA       (163)    /* op=00 op2=110 a=1 cond=1011 */
#define IH_FBL         (164)    /* op=00 op2=110 a=0 cond=0100 */
#define IH_FBLA        (165)    /* op=00 op2=110 a=1 cond=0100 */
#define IH_FBLE        (166)    /* op=00 op2=110 a=0 cond=1101 */
#define IH_FBLEA       (167)    /* op=00 op2=110 a=1 cond=1101 */
#define IH_FBLG        (168)    /* op=00 op2=110 a=0 cond=0010 */
#define IH_FBLGA       (169)    /* op=00 op2=110 a=1 cond=0010 */
#define IH_FBN         (170)    /* op=00 op2=110 a=0 cond=0000 */
#define IH_FBNA        (171)    /* op=00 op2=110 a=1 cond=0000 */
#define IH_FBNE        (172)    /* op=00 op2=110 a=0 cond=0001 */
#define IH_FBNEA       (173)    /* op=00 op2=110 a=1 cond=0001 */
#define IH_FBO         (174)    /* op=00 op2=110 a=0 cond=1111 */
#define IH_FBOA        (175)    /* op=00 op2=110 a=1 cond=1111 */
#define IH_FBPAAPN     (176)    /* op=00 op2=101 cond=1000 a=1 p=0 */
#define IH_FBPAAPT     (177)    /* op=00 op2=101 cond=1000 a=1 p=1 */
#define IH_FBPAPN      (178)    /* op=00 op2=101 cond=1000 a=0 p=0 */
#define IH_FBPAPT      (179)    /* op=00 op2=101 cond=1000 a=0 p=1 */
#define IH_FBPEAPN     (180)    /* op=00 op2=101 cond=1001 a=1 p=0 */
#define IH_FBPEAPT     (181)    /* op=00 op2=101 cond=1001 a=1 p=1 */
#define IH_FBPEPN      (182)    /* op=00 op2=101 cond=1001 a=0 p=0 */
#define IH_FBPEPT      (183)    /* op=00 op2=101 cond=1001 a=0 p=1 */
#define IH_FBPGAPN     (184)    /* op=00 op2=101 cond=0110 a=1 p=0 */
#define IH_FBPGAPT     (185)    /* op=00 op2=101 cond=0110 a=1 p=1 */
#define IH_FBPGPN      (186)    /* op=00 op2=101 cond=0110 a=0 p=0 */
#define IH_FBPGPT      (187)    /* op=00 op2=101 cond=0110 a=0 p=1 */
#define IH_FBPGEAPN    (188)    /* op=00 op2=101 cond=1011 a=1 p=0 */
#define IH_FBPGEAPT    (189)    /* op=00 op2=101 cond=1011 a=1 p=1 */
#define IH_FBPGEPN     (190)    /* op=00 op2=101 cond=1011 a=0 p=0 */
#define IH_FBPGEPT     (191)    /* op=00 op2=101 cond=1011 a=0 p=1 */
#define IH_FBPLAPN     (192)    /* op=00 op2=101 cond=0100 a=1 p=0 */
#define IH_FBPLAPT     (193)    /* op=00 op2=101 cond=0100 a=1 p=1 */
#define IH_FBPLPN      (194)    /* op=00 op2=101 cond=0100 a=0 p=0 */
#define IH_FBPLPT      (195)    /* op=00 op2=101 cond=0100 a=0 p=1 */
#define IH_FBPLEAPN    (196)    /* op=00 op2=101 cond=1101 a=1 p=0 */
#define IH_FBPLEAPT    (197)    /* op=00 op2=101 cond=1101 a=1 p=1 */
#define IH_FBPLEPN     (198)    /* op=00 op2=101 cond=1101 a=0 p=0 */
#define IH_FBPLEPT     (199)    /* op=00 op2=101 cond=1101 a=0 p=1 */
#define IH_FBPLGAPN    (200)    /* op=00 op2=101 cond=0010 a=1 p=0 */
#define IH_FBPLGAPT    (201)    /* op=00 op2=101 cond=0010 a=1 p=1 */
#define IH_FBPLGPN     (202)    /* op=00 op2=101 cond=0010 a=0 p=0 */
#define IH_FBPLGPT     (203)    /* op=00 op2=101 cond=0010 a=0 p=1 */
#define IH_FBPNAPN     (204)    /* op=00 op2=101 cond=0000 a=1 p=0 */
#define IH_FBPNAPT     (205)    /* op=00 op2=101 cond=0000 a=1 p=1 */
#define IH_FBPNPN      (206)    /* op=00 op2=101 cond=0000 a=0 p=0 */
#define IH_FBPNPT      (207)    /* op=00 op2=101 cond=0000 a=0 p=1 */
#define IH_FBPNEAPN    (208)    /* op=00 op2=101 cond=0001 a=1 p=0 */
#define IH_FBPNEAPT    (209)    /* op=00 op2=101 cond=0001 a=1 p=1 */
#define IH_FBPNEPN     (210)    /* op=00 op2=101 cond=0001 a=0 p=0 */
#define IH_FBPNEPT     (211)    /* op=00 op2=101 cond=0001 a=0 p=1 */
#define IH_FBPOAPN     (212)    /* op=00 op2=101 cond=1111 a=1 p=0 */
#define IH_FBPOAPT     (213)    /* op=00 op2=101 cond=1111 a=1 p=1 */
#define IH_FBPOPN      (214)    /* op=00 op2=101 cond=1111 a=0 p=0 */
#define IH_FBPOPT      (215)    /* op=00 op2=101 cond=1111 a=0 p=1 */
#define IH_FBPUAPN     (216)    /* op=00 op2=101 cond=0111 a=1 p=0 */
#define IH_FBPUAPT     (217)    /* op=00 op2=101 cond=0111 a=1 p=1 */
#define IH_FBPUPN      (218)    /* op=00 op2=101 cond=0111 a=0 p=0 */
#define IH_FBPUPT      (219)    /* op=00 op2=101 cond=0111 a=0 p=1 */
#define IH_FBPUEAPN    (220)    /* op=00 op2=101 cond=1010 a=1 p=0 */
#define IH_FBPUEAPT    (221)    /* op=00 op2=101 cond=1010 a=1 p=1 */
#define IH_FBPUEPN     (222)    /* op=00 op2=101 cond=1010 a=0 p=0 */
#define IH_FBPUEPT     (223)    /* op=00 op2=101 cond=1010 a=0 p=1 */
#define IH_FBPUGAPN    (224)    /* op=00 op2=101 cond=0101 a=1 p=0 */
#define IH_FBPUGAPT    (225)    /* op=00 op2=101 cond=0101 a=1 p=1 */
#define IH_FBPUGPN     (226)    /* op=00 op2=101 cond=0101 a=0 p=0 */
#define IH_FBPUGPT     (227)    /* op=00 op2=101 cond=0101 a=0 p=1 */
#define IH_FBPUGEAPN   (228)    /* op=00 op2=101 cond=1100 a=1 p=0 */
#define IH_FBPUGEAPT   (229)    /* op=00 op2=101 cond=1100 a=1 p=1 */
#define IH_FBPUGEPN    (230)    /* op=00 op2=101 cond=1100 a=0 p=0 */
#define IH_FBPUGEPT    (231)    /* op=00 op2=101 cond=1100 a=0 p=1 */
#define IH_FBPULAPN    (232)    /* op=00 op2=101 cond=0011 a=1 p=0 */
#define IH_FBPULAPT    (233)    /* op=00 op2=101 cond=0011 a=1 p=1 */
#define IH_FBPULPN     (234)    /* op=00 op2=101 cond=0011 a=0 p=0 */
#define IH_FBPULPT     (235)    /* op=00 op2=101 cond=0011 a=0 p=1 */
#define IH_FBPULEAPN   (236)    /* op=00 op2=101 cond=1110 a=1 p=0 */
#define IH_FBPULEAPT   (237)    /* op=00 op2=101 cond=1110 a=1 p=1 */
#define IH_FBPULEPN    (238)    /* op=00 op2=101 cond=1110 a=0 p=0 */
#define IH_FBPULEPT    (239)    /* op=00 op2=101 cond=1110 a=0 p=1 */
#define IH_FBU         (240)    /* op=00 op2=110 a=0 cond=0111 */
#define IH_FBUA        (241)    /* op=00 op2=110 a=1 cond=0111 */
#define IH_FBUE        (242)    /* op=00 op2=110 a=0 cond=1010 */
#define IH_FBUEA       (243)    /* op=00 op2=110 a=1 cond=1010 */
#define IH_FBUG        (244)    /* op=00 op2=110 a=0 cond=0101 */
#define IH_FBUGA       (245)    /* op=00 op2=110 a=1 cond=0101 */
#define IH_FBUGE       (246)    /* op=00 op2=110 a=0 cond=1100 */
#define IH_FBUGEA      (247)    /* op=00 op2=110 a=1 cond=1100 */
#define IH_FBUL        (248)    /* op=00 op2=110 a=0 cond=0011 */
#define IH_FBULA       (249)    /* op=00 op2=110 a=1 cond=0011 */
#define IH_FBULE       (250)    /* op=00 op2=110 a=0 cond=1110 */
#define IH_FBULEA      (251)    /* op=00 op2=110 a=1 cond=1110 */
#define IH_FCMPD       (252)    /* op=10 op3=110101 opf=001010010 opf29_27=000 */
#define IH_FCMPED      (253)    /* op=10 op3=110101 opf=001010110 opf29_27=000 */
#define IH_FCMPEQ      (254)    /* op=10 op3=110101 opf=001010111 opf29_27=000 */
#define IH_FCMPEQ16    (255)    /* op=10 op3=110110 opf=000101010 */
#define IH_FCMPEQ32    (256)    /* op=10 op3=110110 opf=000101110 */
#define IH_FCMPES      (257)    /* op=10 op3=110101 opf=001010101 opf29_27=000 */
#define IH_FCMPGT16    (258)    /* op=10 op3=110110 opf=000101000 */
#define IH_FCMPGT32    (259)    /* op=10 op3=110110 opf=000101100 */
#define IH_FCMPLE16    (260)    /* op=10 op3=110110 opf=000100000 */
#define IH_FCMPLE32    (261)    /* op=10 op3=110110 opf=000100100 */
#define IH_FCMPNE16    (262)    /* op=10 op3=110110 opf=000100010 */
#define IH_FCMPNE32    (263)    /* op=10 op3=110110 opf=000100110 */
#define IH_FCMPQ       (264)    /* op=10 op3=110101 opf=001010011 opf29_27=000 */
#define IH_FCMPS       (265)    /* op=10 op3=110101 opf=001010001 opf29_27=000 */
#define IH_FDIVD       (266)    /* op=10 op3=110100 opf=001001110 */
#define IH_FDIVQ       (267)    /* op=10 op3=110100 opf=001001111 */
#define IH_FDIVS       (268)    /* op=10 op3=110100 opf=001001101 */
#define IH_FDMULQ      (269)    /* op=10 op3=110100 opf=001101110 */
#define IH_FDTOI       (270)    /* op=10 op3=110100 opf=011010010 */
#define IH_FDTOQ       (271)    /* op=10 op3=110100 opf=011001110 */
#define IH_FDTOS       (272)    /* op=10 op3=110100 opf=011000110 */
#define IH_FDTOX       (273)    /* op=10 op3=110100 opf=010000010 */
#define IH_FEXPAND     (274)    /* op=10 op3=110110 opf=001001101 */
#define IH_FITOD       (275)    /* op=10 op3=110100 opf=011001000 */
#define IH_FITOQ       (276)    /* op=10 op3=110100 opf=011001100 */
#define IH_FITOS       (277)    /* op=10 op3=110100 opf=011000100 */
#define IH_FLUSH       (278)    /* op=10 op3=111011 */
#define IH_FLUSHW      (279)    /* op=10 op3=101011 */
#define IH_FMOVD       (280)    /* op=10 op3=110100 opf=000000010 */
#define IH_FMOVDA      (281)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1000 if_13=1 */
#define IH_FMOVDCC     (282)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1101 if_13=1 */
#define IH_FMOVDCS     (283)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0101 if_13=1 */
#define IH_FMOVDE      (284)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0001 if_13=1 */
#define IH_FMOVDFA     (285)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1000 if_13=0 */
#define IH_FMOVDFE     (286)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1001 if_13=0 */
#define IH_FMOVDFG     (287)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0110 if_13=0 */
#define IH_FMOVDFGE    (288)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1011 if_13=0 */
#define IH_FMOVDFL     (289)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0100 if_13=0 */
#define IH_FMOVDFLE    (290)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1101 if_13=0 */
#define IH_FMOVDFLG    (291)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0010 if_13=0 */
#define IH_FMOVDFN     (292)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0000 if_13=0 */
#define IH_FMOVDFNE    (293)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0001 if_13=0 */
#define IH_FMOVDFO     (294)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1111 if_13=0 */
#define IH_FMOVDFU     (295)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0111 if_13=0 */
#define IH_FMOVDFUE    (296)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1010 if_13=0 */
#define IH_FMOVDFUG    (297)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0101 if_13=0 */
#define IH_FMOVDFUGE   (298)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1100 if_13=0 */
#define IH_FMOVDFUL    (299)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0011 if_13=0 */
#define IH_FMOVDFULE   (300)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1110 if_13=0 */
#define IH_FMOVDG      (301)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1010 if_13=1 */
#define IH_FMOVDGE     (302)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1011 if_13=1 */
#define IH_FMOVDGU     (303)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1100 if_13=1 */
#define IH_FMOVDL      (304)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0011 if_13=1 */
#define IH_FMOVDLE     (305)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0010 if_13=1 */
#define IH_FMOVDLEU    (306)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0100 if_13=1 */
#define IH_FMOVDN      (307)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0000 if_13=1 */
#define IH_FMOVDNE     (308)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1001 if_13=1 */
#define IH_FMOVDNEG    (309)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0110 if_13=1 */
#define IH_FMOVDPOS    (310)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1110 if_13=1 */
#define IH_FMOVDVC     (311)    /* op=10 op3=110101 opf=xxx000010 cond17_14=1111 if_13=1 */
#define IH_FMOVDVS     (312)    /* op=10 op3=110101 opf=xxx000010 cond17_14=0111 if_13=1 */
#define IH_FMOVQ       (313)    /* op=10 op3=110100 opf=000000011 */
#define IH_FMOVQA      (314)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1000 if_13=1 */
#define IH_FMOVQCC     (315)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1101 if_13=1 */
#define IH_FMOVQCS     (316)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0101 if_13=1 */
#define IH_FMOVQE      (317)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0001 if_13=1 */
#define IH_FMOVQFA     (318)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1000 if_13=0 */
#define IH_FMOVQFE     (319)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1001 if_13=0 */
#define IH_FMOVQFG     (320)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0110 if_13=0 */
#define IH_FMOVQFGE    (321)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1011 if_13=0 */
#define IH_FMOVQFL     (322)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0100 if_13=0 */
#define IH_FMOVQFLE    (323)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1101 if_13=0 */
#define IH_FMOVQFLG    (324)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0010 if_13=0 */
#define IH_FMOVQFN     (325)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0000 if_13=0 */
#define IH_FMOVQFNE    (326)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0001 if_13=0 */
#define IH_FMOVQFO     (327)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1111 if_13=0 */
#define IH_FMOVQFU     (328)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0111 if_13=0 */
#define IH_FMOVQFUE    (329)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1010 if_13=0 */
#define IH_FMOVQFUG    (330)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0101 if_13=0 */
#define IH_FMOVQFUGE   (331)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1100 if_13=0 */
#define IH_FMOVQFUL    (332)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0011 if_13=0 */
#define IH_FMOVQFULE   (333)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1110 if_13=0 */
#define IH_FMOVQG      (334)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1010 if_13=1 */
#define IH_FMOVQGE     (335)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1011 if_13=1 */
#define IH_FMOVQGU     (336)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1100 if_13=1 */
#define IH_FMOVQL      (337)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0011 if_13=1 */
#define IH_FMOVQLE     (338)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0010 if_13=1 */
#define IH_FMOVQLEU    (339)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0100 if_13=1 */
#define IH_FMOVQN      (340)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0000 if_13=1 */
#define IH_FMOVQNE     (341)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1001 if_13=1 */
#define IH_FMOVQNEG    (342)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0110 if_13=1 */
#define IH_FMOVQPOS    (343)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1110 if_13=1 */
#define IH_FMOVQVC     (344)    /* op=10 op3=110101 opf=xxx000011 cond17_14=1111 if_13=1 */
#define IH_FMOVQVS     (345)    /* op=10 op3=110101 opf=xxx000011 cond17_14=0111 if_13=1 */
#define IH_FMOVRDGEZ   (346)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=111 */
#define IH_FMOVRDGZ    (347)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=110 */
#define IH_FMOVRDLEZ   (348)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=010 */
#define IH_FMOVRDLZ    (349)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=011 */
#define IH_FMOVRDNZ    (350)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=101 */
#define IH_FMOVRDZ     (351)    /* op=10 op3=110101 opf=0xxx00110 rcond12_10=001 */
#define IH_FMOVRQGEZ   (352)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=111 */
#define IH_FMOVRQGZ    (353)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=110 */
#define IH_FMOVRQLEZ   (354)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=010 */
#define IH_FMOVRQLZ    (355)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=011 */
#define IH_FMOVRQNZ    (356)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=101 */
#define IH_FMOVRQZ     (357)    /* op=10 op3=110101 opf=0xxx00111 rcond12_10=001 */
#define IH_FMOVRSGEZ   (358)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=111 */
#define IH_FMOVRSGZ    (359)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=110 */
#define IH_FMOVRSLEZ   (360)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=010 */
#define IH_FMOVRSLZ    (361)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=011 */
#define IH_FMOVRSNZ    (362)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=101 */
#define IH_FMOVRSZ     (363)    /* op=10 op3=110101 opf=0xxx00101 rcond12_10=001 */
#define IH_FMOVS       (364)    /* op=10 op3=110100 opf=000000001 */
#define IH_FMOVSA      (365)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1000 if_13=1 */
#define IH_FMOVSCC     (366)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1101 if_13=1 */
#define IH_FMOVSCS     (367)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0101 if_13=1 */
#define IH_FMOVSE      (368)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0001 if_13=1 */
#define IH_FMOVSFA     (369)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1000 if_13=0 */
#define IH_FMOVSFE     (370)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1001 if_13=0 */
#define IH_FMOVSFG     (371)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0110 if_13=0 */
#define IH_FMOVSFGE    (372)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1011 if_13=0 */
#define IH_FMOVSFL     (373)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0100 if_13=0 */
#define IH_FMOVSFLE    (374)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1101 if_13=0 */
#define IH_FMOVSFLG    (375)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0010 if_13=0 */
#define IH_FMOVSFN     (376)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0000 if_13=0 */
#define IH_FMOVSFNE    (377)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0001 if_13=0 */
#define IH_FMOVSFO     (378)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1111 if_13=0 */
#define IH_FMOVSFU     (379)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0111 if_13=0 */
#define IH_FMOVSFUE    (380)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1010 if_13=0 */
#define IH_FMOVSFUG    (381)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0101 if_13=0 */
#define IH_FMOVSFUGE   (382)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1100 if_13=0 */
#define IH_FMOVSFUL    (383)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0011 if_13=0 */
#define IH_FMOVSFULE   (384)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1110 if_13=0 */
#define IH_FMOVSG      (385)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1010 if_13=1 */
#define IH_FMOVSGE     (386)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1011 if_13=1 */
#define IH_FMOVSGU     (387)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1100 if_13=1 */
#define IH_FMOVSL      (388)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0011 if_13=1 */
#define IH_FMOVSLE     (389)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0010 if_13=1 */
#define IH_FMOVSLEU    (390)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0100 if_13=1 */
#define IH_FMOVSN      (391)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0000 if_13=1 */
#define IH_FMOVSNE     (392)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1001 if_13=1 */
#define IH_FMOVSNEG    (393)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0110 if_13=1 */
#define IH_FMOVSPOS    (394)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1110 if_13=1 */
#define IH_FMOVSVC     (395)    /* op=10 op3=110101 opf=xxx000001 cond17_14=1111 if_13=1 */
#define IH_FMOVSVS     (396)    /* op=10 op3=110101 opf=xxx000001 cond17_14=0111 if_13=1 */
#define IH_FMUL8SUX16  (397)    /* op=10 op3=110110 opf=000110110 */
#define IH_FMUL8ULX16  (398)    /* op=10 op3=110110 opf=000110111 */
#define IH_FMUL8X16    (399)    /* op=10 op3=110110 opf=000110001 */
#define IH_FMUL8X16AL  (400)    /* op=10 op3=110110 opf=000110101 */
#define IH_FMUL8X16AU  (401)    /* op=10 op3=110110 opf=000110011 */
#define IH_FMULD       (402)    /* op=10 op3=110100 opf=001001010 */
#define IH_FMULD8SUX16 (403)    /* op=10 op3=110110 opf=000111000 */
#define IH_FMULD8ULX16 (404)    /* op=10 op3=110110 opf=000111001 */
#define IH_FMULQ       (405)    /* op=10 op3=110100 opf=001001011 */
#define IH_FMULS       (406)    /* op=10 op3=110100 opf=001001001 */
#define IH_FNAND       (407)    /* op=10 op3=110110 opf=001101110 */
#define IH_FNANDS      (408)    /* op=10 op3=110110 opf=001101111 */
#define IH_FNEGD       (409)    /* op=10 op3=110100 opf=000000110 */
#define IH_FNEGQ       (410)    /* op=10 op3=110100 opf=000000111 */
#define IH_FNEGS       (411)    /* op=10 op3=110100 opf=000000101 */
#define IH_FNOR        (412)    /* op=10 op3=110110 opf=001100010 */
#define IH_FNORS       (413)    /* op=10 op3=110110 opf=001100011 */
#define IH_FNOT1       (414)    /* op=10 op3=110110 opf=001101010 */
#define IH_FNOT1S      (415)    /* op=10 op3=110110 opf=001101011 */
#define IH_FNOT2       (416)    /* op=10 op3=110110 opf=001100110 */
#define IH_FNOT2S      (417)    /* op=10 op3=110110 opf=001100111 */
#define IH_FONE        (418)    /* op=10 op3=110110 opf=001111110 */
#define IH_FONES       (419)    /* op=10 op3=110110 opf=001111111 */
#define IH_FOR         (420)    /* op=10 op3=110110 opf=001111100 */
#define IH_FORNOT1     (421)    /* op=10 op3=110110 opf=001111010 */
#define IH_FORNOT1S    (422)    /* op=10 op3=110110 opf=001111011 */
#define IH_FORNOT2     (423)    /* op=10 op3=110110 opf=001110110 */
#define IH_FORNOT2S    (424)    /* op=10 op3=110110 opf=001110111 */
#define IH_FORS        (425)    /* op=10 op3=110110 opf=001111101 */
#define IH_FPACK16     (426)    /* op=10 op3=110110 opf=000111011 */
#define IH_FPACK32     (427)    /* op=10 op3=110110 opf=000111010 */
#define IH_FPACKFIX    (428)    /* op=10 op3=110110 opf=000111101 */
#define IH_FPADD16     (429)    /* op=10 op3=110110 opf=001010000 */
#define IH_FPADD16S    (430)    /* op=10 op3=110110 opf=001010001 */
#define IH_FPADD32     (431)    /* op=10 op3=110110 opf=001010010 */
#define IH_FPADD32S    (432)    /* op=10 op3=110110 opf=001010011 */
#define IH_FPMERGE     (433)    /* op=10 op3=110110 opf=001001011 */
#define IH_FPSUB16     (434)    /* op=10 op3=110110 opf=001010100 */
#define IH_FPSUB16S    (435)    /* op=10 op3=110110 opf=001010101 */
#define IH_FPSUB32     (436)    /* op=10 op3=110110 opf=001010110 */
#define IH_FPSUB32S    (437)    /* op=10 op3=110110 opf=001010111 */
#define IH_FQTOD       (438)    /* op=10 op3=110100 opf=011001011 */
#define IH_FQTOI       (439)    /* op=10 op3=110100 opf=011010011 */
#define IH_FQTOS       (440)    /* op=10 op3=110100 opf=011000111 */
#define IH_FQTOX       (441)    /* op=10 op3=110100 opf=010000011 */
#define IH_FSMULD      (442)    /* op=10 op3=110100 opf=001101001 */
#define IH_FSQRTD      (443)    /* op=10 op3=110100 opf=000101010 */
#define IH_FSQRTQ      (444)    /* op=10 op3=110100 opf=000101011 */
#define IH_FSQRTS      (445)    /* op=10 op3=110100 opf=000101001 */
#define IH_FSRC1       (446)    /* op=10 op3=110110 opf=001110100 */
#define IH_FSRC1S      (447)    /* op=10 op3=110110 opf=001110101 */
#define IH_FSRC2       (448)    /* op=10 op3=110110 opf=001111000 */
#define IH_FSRC2S      (449)    /* op=10 op3=110110 opf=001111001 */
#define IH_FSTOD       (450)    /* op=10 op3=110100 opf=011001001 */
#define IH_FSTOI       (451)    /* op=10 op3=110100 opf=011010001 */
#define IH_FSTOQ       (452)    /* op=10 op3=110100 opf=011001101 */
#define IH_FSTOX       (453)    /* op=10 op3=110100 opf=010000001 */
#define IH_FSUBD       (454)    /* op=10 op3=110100 opf=001000110 */
#define IH_FSUBQ       (455)    /* op=10 op3=110100 opf=001000111 */
#define IH_FSUBS       (456)    /* op=10 op3=110100 opf=001000101 */
#define IH_FXNOR       (457)    /* op=10 op3=110110 opf=001110010 */
#define IH_FXNORS      (458)    /* op=10 op3=110110 opf=001110011 */
#define IH_FXOR        (459)    /* op=10 op3=110110 opf=001101100 */
#define IH_FXORS       (460)    /* op=10 op3=110110 opf=001101101 */
#define IH_FXTOD       (461)    /* op=10 op3=110100 opf=010001000 */
#define IH_FXTOQ       (462)    /* op=10 op3=110100 opf=010001100 */
#define IH_FXTOS       (463)    /* op=10 op3=110100 opf=010000100 */
#define IH_FZERO       (464)    /* op=10 op3=110110 opf=001100000 */
#define IH_FZEROS      (465)    /* op=10 op3=110110 opf=001100001 */
#define IH_ILLTRAP     (466)    /* op=00 op2=000 */
#define IH_IMPDEP1     (467)    /* op=10 op3=110110 */
#define IH_IMPDEP2     (468)    /* op=10 op3=110111 */
#define IH_JMPL        (469)    /* op=10 op3=111000 */
#define IH_LDD         (470)    /* op=11 op3=000011 */
#define IH_LDDA        (471)    /* op=11 op3=010011 */
#define IH_LDDF        (472)    /* op=11 op3=100011 */
#define IH_LDDFA       (473)    /* op=11 op3=110011 */
#define IH_LDF         (474)    /* op=11 op3=100000 */
#define IH_LDFA        (475)    /* op=11 op3=110000 */
#define IH_LDFSR       (476)    /* op=11 op3=100001 rd=00000 */
#define IH_LDQF        (477)    /* op=11 op3=100010 */
#define IH_LDQFA       (478)    /* op=11 op3=110010 */
#define IH_LDSB        (479)    /* op=11 op3=001001 */
#define IH_LDSBA       (480)    /* op=11 op3=011001 */
#define IH_LDSH        (481)    /* op=11 op3=001010 */
#define IH_LDSHA       (482)    /* op=11 op3=011010 */
#define IH_LDSTUB      (483)    /* op=11 op3=001101 */
#define IH_LDSTUBA     (484)    /* op=11 op3=011101 */
#define IH_LDSW        (485)    /* op=11 op3=001000 */
#define IH_LDSWA       (486)    /* op=11 op3=011000 */
#define IH_LDUB        (487)    /* op=11 op3=000001 */
#define IH_LDUBA       (488)    /* op=11 op3=010001 */
#define IH_LDUH        (489)    /* op=11 op3=000010 */
#define IH_LDUHA       (490)    /* op=11 op3=010010 */
#define IH_LDUW        (491)    /* op=11 op3=000000 */
#define IH_LDUWA       (492)    /* op=11 op3=010000 */
#define IH_LDX         (493)    /* op=11 op3=001011 */
#define IH_LDXA        (494)    /* op=11 op3=011011 */
#define IH_LDXFSR      (495)    /* op=11 op3=100001 rd=00001 */
#define IH_MEMBAR      (496)    /* op=10 op3=101000 rs1=01111 rd=00000 */
#define IH_MOVA        (497)    /* op=10 op3=101100 cond17_14=1000 if_18=1 */
#define IH_MOVCC       (498)    /* op=10 op3=101100 cond17_14=1101 if_18=1 */
#define IH_MOVCS       (499)    /* op=10 op3=101100 cond17_14=0101 if_18=1 */
#define IH_MOVE        (500)    /* op=10 op3=101100 cond17_14=0001 if_18=1 */
#define IH_MOVFA       (501)    /* op=10 op3=101100 cond17_14=1000 if_18=0 */
#define IH_MOVFE       (502)    /* op=10 op3=101100 cond17_14=1001 if_18=0 */
#define IH_MOVFG       (503)    /* op=10 op3=101100 cond17_14=0110 if_18=0 */
#define IH_MOVFGE      (504)    /* op=10 op3=101100 cond17_14=1011 if_18=0 */
#define IH_MOVFL       (505)    /* op=10 op3=101100 cond17_14=0100 if_18=0 */
#define IH_MOVFLE      (506)    /* op=10 op3=101100 cond17_14=1101 if_18=0 */
#define IH_MOVFLG      (507)    /* op=10 op3=101100 cond17_14=0010 if_18=0 */
#define IH_MOVFN       (508)    /* op=10 op3=101100 cond17_14=0000 if_18=0 */
#define IH_MOVFNE      (509)    /* op=10 op3=101100 cond17_14=0001 if_18=0 */
#define IH_MOVFO       (510)    /* op=10 op3=101100 cond17_14=1111 if_18=0 */
#define IH_MOVFU       (511)    /* op=10 op3=101100 cond17_14=0111 if_18=0 */
#define IH_MOVFUE      (512)    /* op=10 op3=101100 cond17_14=1010 if_18=0 */
#define IH_MOVFUG      (513)    /* op=10 op3=101100 cond17_14=0101 if_18=0 */
#define IH_MOVFUGE     (514)    /* op=10 op3=101100 cond17_14=1100 if_18=0 */
#define IH_MOVFUL      (515)    /* op=10 op3=101100 cond17_14=0011 if_18=0 */
#define IH_MOVFULE     (516)    /* op=10 op3=101100 cond17_14=1110 if_18=0 */
#define IH_MOVG        (517)    /* op=10 op3=101100 cond17_14=1010 if_18=1 */
#define IH_MOVGE       (518)    /* op=10 op3=101100 cond17_14=1011 if_18=1 */
#define IH_MOVGU       (519)    /* op=10 op3=101100 cond17_14=1100 if_18=1 */
#define IH_MOVL        (520)    /* op=10 op3=101100 cond17_14=0011 if_18=1 */
#define IH_MOVLE       (521)    /* op=10 op3=101100 cond17_14=0010 if_18=1 */
#define IH_MOVLEU      (522)    /* op=10 op3=101100 cond17_14=0100 if_18=1 */
#define IH_MOVN        (523)    /* op=10 op3=101100 cond17_14=0000 if_18=1 */
#define IH_MOVNE       (524)    /* op=10 op3=101100 cond17_14=1001 if_18=1 */
#define IH_MOVNEG      (525)    /* op=10 op3=101100 cond17_14=0110 if_18=1 */
#define IH_MOVPOS      (526)    /* op=10 op3=101100 cond17_14=1110 if_18=1 */
#define IH_MOVRGEZ     (527)    /* op=10 op3=101111 rcond12_10=111 */
#define IH_MOVRGZ      (528)    /* op=10 op3=101111 rcond12_10=110 */
#define IH_MOVRLEZ     (529)    /* op=10 op3=101111 rcond12_10=010 */
#define IH_MOVRLZ      (530)    /* op=10 op3=101111 rcond12_10=011 */
#define IH_MOVRNZ      (531)    /* op=10 op3=101111 rcond12_10=101 */
#define IH_MOVRZ       (532)    /* op=10 op3=101111 rcond12_10=001 */
#define IH_MOVVC       (533)    /* op=10 op3=101100 cond17_14=1111 if_18=1 */
#define IH_MOVVS       (534)    /* op=10 op3=101100 cond17_14=0111 if_18=1 */
#define IH_MULSCC      (535)    /* op=10 op3=100100 */
#define IH_MULX        (536)    /* op=10 op3=001001 */
#define IH_NOP         (537)    /* op=00 op2=100 imm22=0 rd=0 */
#define IH_OR          (538)    /* op=10 op3=000010 */
#define IH_ORCC        (539)    /* op=10 op3=010010 */
#define IH_ORN         (540)    /* op=10 op3=000110 */
#define IH_ORNCC       (541)    /* op=10 op3=010110 */
#define IH_PDIST       (542)    /* op=10 op3=110110 opf=000111110 */
#define IH_POPC        (543)    /* op=10 op3=101110 rs1=00000 */
#define IH_PREFETCH    (544)    /* op=11 op3=101101 */
#define IH_PREFETCHA   (545)    /* op=11 op3=111101 */
#define IH_RDASI       (546)    /* op=10 op3=101000 rs1=00011 */
#define IH_RDASR       (547)    /* op=10 op3=101000 */
#define IH_RDCCR       (548)    /* op=10 op3=101000 rs1=00010 */
#define IH_RDFPRS      (549)    /* op=10 op3=101000 rs1=00110 */
#define IH_RDPC        (550)    /* op=10 op3=101000 rs1=00101 */
#define IH_RDPR        (551)    /* op=10 op3=101010 */
#define IH_RDTICK      (552)    /* op=10 op3=101000 rs1=00100 */
#define IH_RDY         (553)    /* op=10 op3=101000 rs1=00000 */
#define IH_RESTORE     (554)    /* op=10 op3=111101 */
#define IH_RESTORED    (555)    /* op=10 op3=110001 rd=00001 */
#define IH_RETRY       (556)    /* op=10 op3=111110 rd=00001 */
#define IH_RETURN      (557)    /* op=10 op3=111001 */
#define IH_SAVE        (558)    /* op=10 op3=111100 */
#define IH_SAVED       (559)    /* op=10 op3=110001 rd=00000 */
#define IH_SDIV        (560)    /* op=10 op3=001111 */
#define IH_SDIVCC      (561)    /* op=10 op3=011111 */
#define IH_SDIVX       (562)    /* op=10 op3=101101 */
#define IH_SDIVXCC     (563)    /* op=10 op3=011101 */
#define IH_SETHI       (564)    /* op=00 op2=100 */
#define IH_SHUTDOWN    (565)    /* op=10 op3=110110 opf=010000000 */
#define IH_SIR         (566)    /* op=10 op3=110000 rd=01111 rs1=00000 i=1 */
#define IH_SLL         (567)    /* op=10 op3=100101 x_12=0 */
#define IH_SLLX        (568)    /* op=10 op3=100101 x_12=1 */
#define IH_SMUL        (569)    /* op=10 op3=001011 */
#define IH_SMULCC      (570)    /* op=10 op3=011011 */
#define IH_SRA         (571)    /* op=10 op3=100111 x_12=0 */
#define IH_SRAX        (572)    /* op=10 op3=100111 x_12=1 */
#define IH_SRL         (573)    /* op=10 op3=100110 x_12=0 */
#define IH_SRLX        (574)    /* op=10 op3=100110 x_12=1 */
#define IH_STB         (575)    /* op=11 op3=000101 */
#define IH_STBA        (576)    /* op=11 op3=010101 */
#define IH_STD         (577)    /* op=11 op3=000111 */
#define IH_STDA        (578)    /* op=11 op3=010111 */
#define IH_STDF        (579)    /* op=11 op3=100111 */
#define IH_STDFA       (580)    /* op=11 op3=110111 */
#define IH_STF         (581)    /* op=11 op3=100100 */
#define IH_STFA        (582)    /* op=11 op3=110100 */
#define IH_STFSR       (583)    /* op=11 op3=100101 rd=00000 */
#define IH_STH         (584)    /* op=11 op3=000110 */
#define IH_STHA        (585)    /* op=11 op3=010110 */
#define IH_STQF        (586)    /* op=11 op3=100110 */
#define IH_STQFA       (587)    /* op=11 op3=110110 */
#define IH_STW         (588)    /* op=11 op3=000100 */
#define IH_STWA        (589)    /* op=11 op3=010100 */
#define IH_STX         (590)    /* op=11 op3=001110 */
#define IH_STXA        (591)    /* op=11 op3=011110 */
#define IH_STXFSR      (592)    /* op=11 op3=100101 rd=00001 */
#define IH_SUB         (593)    /* op=10 op3=000100 */
#define IH_SUBC        (594)    /* op=10 op3=001100 */
#define IH_SUBCC       (595)    /* op=10 op3=010100 */
#define IH_SUBCCC      (596)    /* op=10 op3=011100 */
#define IH_SWAP        (597)    /* op=11 op3=001111 */
#define IH_SWAPA       (598)    /* op=11 op3=011111 */
#define IH_TA          (599)    /* op=10 op3=111010 cond=1000 */
#define IH_TADDCC      (600)    /* op=10 op3=100000 */
#define IH_TADDCCTV    (601)    /* op=10 op3=100010 */
#define IH_TCC         (602)    /* op=10 op3=111010 cond=1101 */
#define IH_TCS         (603)    /* op=10 op3=111010 cond=0101 */
#define IH_TE          (604)    /* op=10 op3=111010 cond=0001 */
#define IH_TG          (605)    /* op=10 op3=111010 cond=1010 */
#define IH_TGE         (606)    /* op=10 op3=111010 cond=1011 */
#define IH_TGU         (607)    /* op=10 op3=111010 cond=1100 */
#define IH_TL          (608)    /* op=10 op3=111010 cond=0011 */
#define IH_TLE         (609)    /* op=10 op3=111010 cond=0010 */
#define IH_TLEU        (610)    /* op=10 op3=111010 cond=0100 */
#define IH_TN          (611)    /* op=10 op3=111010 cond=0000 */
#define IH_TNE         (612)    /* op=10 op3=111010 cond=1001 */
#define IH_TNEG        (613)    /* op=10 op3=111010 cond=0110 */
#define IH_TPOSZ       (614)    /* op=10 op3=111010 cond=1110 */
#define IH_TSUBCC      (615)    /* op=10 op3=100001 */
#define IH_TSUBCCTV    (616)    /* op=10 op3=100011 */
#define IH_TVC         (617)    /* op=10 op3=111010 cond=1111 */
#define IH_TVS         (618)    /* op=10 op3=111010 cond=0111 */
#define IH_UDIV        (619)    /* op=10 op3=001110 */
#define IH_UDIVCC      (620)    /* op=10 op3=011110 */
#define IH_UDIVX       (621)    /* op=10 op3=001101 */
#define IH_UMUL        (622)    /* op=10 op3=001010 */
#define IH_UMULCC      (623)    /* op=10 op3=011010 */
#define IH_UMULXCC     (624)    /* op=10 op3=011001 */
#define IH_WRASI       (625)    /* op=10 op3=110000 rd=00011 */
#define IH_WRASR       (626)    /* op=10 op3=110000 */
#define IH_WRCCR       (627)    /* op=10 op3=110000 rd=00010 */
#define IH_WRFPRS      (628)    /* op=10 op3=110000 rd=00110 */
#define IH_WRPR        (629)    /* op=10 op3=110010 */
#define IH_WRY         (630)    /* op=10 op3=110000 rd=00000 */
#define IH_XNOR        (631)    /* op=10 op3=000111 */
#define IH_XNORCC      (632)    /* op=10 op3=010111 */
#define IH_XOR         (633)    /* op=10 op3=000011 */
#define IH_XORCC       (634)    /* op=10 op3=010011 */

// Added for PSE

#define IH_SIAM        (635)    /* op=10 op3=110110 opf=010000001 */

// Fujitsu Only
//
#define IH_FMADDS      (636)    /* op=10 op3=110111 opf8_5=0001 */
#define IH_FMADDD      (637)    /* op=10 op3=110111 opf8_5=0010 */
#define IH_FMSUBS      (638)    /* op=10 op3=110111 opf8_5=0101 */
#define IH_FMSUBD      (639)    /* op=10 op3=110111 opf8_5=0110 */
#define IH_FNMSUBS     (640)    /* op=10 op3=110111 opf8_5=1001 */
#define IH_FNMSUBD     (641)    /* op=10 op3=110111 opf8_5=1010 */
#define IH_FNMADDS     (642)    /* op=10 op3=110111 opf8_5=1101 */
#define IH_FNMADDD     (643)    /* op=10 op3=110111 opf8_5=1110 */

#endif /*IHASH_h_*/
