/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef _spixtypes_h_
#define _spixtypes_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


/* signed or unsigned, == 8 bits */
#ifndef INT8
#define INT8 char
typedef INT8 int8;
#endif

/* unsigned, == 8 bits */
#ifndef UINT8
#define UINT8 unsigned char
typedef UINT8 uint8;
#endif

/* signed, == 16 bits */
#ifndef INT16
#define INT16 short
typedef INT16 int16;
#endif

/* unsigned, == 16 bits */
#ifndef UINT16
#define UINT16 unsigned short
typedef UINT16 uint16;
#endif

/* signed, == 32 bits */
#ifndef INT32
#define INT32 int
typedef INT32 int32;
#endif

/* unsigned, == 32 bits */
#ifndef UINT32
#define UINT32 unsigned
typedef UINT32 uint32;
#endif

/* signed, == 64 bits */
#ifndef INT64
#define INT64 long long
typedef INT64 int64;
#endif

/* unsigned, == 64 bits */
#ifndef UINT64
#define UINT64 unsigned long long
typedef UINT64 uint64;
#endif

#ifndef REAL32
#define REAL32 float
typedef REAL32 real32;
#endif

#ifndef REAL64
#define REAL64 double
typedef REAL64 real64;
#endif

#ifndef REAL128
#define REAL128 long double
typedef REAL128 real128;
#endif

#endif  /* #ifndef _spixtypes_h_ */
