/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef inames_h_
#define inames_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


#include "INAMES.h"

/*
 * i_op values
 */
#define I_OP_CALL	1


/*
 * i_rs1, i_rs2, and i_rd values
 */
#define I_REG_g0	0
#define I_REG_g1	1
#define I_REG_g2	2
#define I_REG_g3	3
#define I_REG_g4	4
#define I_REG_g5	5
#define I_REG_g6	6
#define I_REG_g7	7
#define I_REG_o0	8
#define I_REG_o1	9
#define I_REG_o2	10
#define I_REG_o3	11
#define I_REG_o4	12
#define I_REG_o5	13
#define I_REG_o6	14
#define I_REG_o7	15
#define I_REG_l0	16
#define I_REG_l1	17
#define I_REG_l2	18
#define I_REG_l3	19
#define I_REG_l4	20
#define I_REG_l5	21
#define I_REG_l6	22
#define I_REG_l7	23
#define I_REG_i0	24
#define I_REG_i1	25
#define I_REG_i2	26
#define I_REG_i3	27
#define I_REG_i4	28
#define I_REG_i5	29
#define I_REG_i6	30
#define I_REG_i7	31

#define I_REG_sp	I_REG_o6
#define I_REG_fp	I_REG_i6


/*
 * i_rcond* values
 */
#define I_RCOND_NZ	5
#define I_RCOND_Z	1
#define I_RCOND_POS	7
#define I_RCOND_NEG	3
#define I_RCOND_NEGZ	2
#define I_RCOND_POSNZ	6


/*
 * Low two bits of i_opf field.
 */
#define I_OPF_S 	1
#define I_OPF_D 	2
#define I_OPF_Q 	3


#endif /*inames_h_*/
