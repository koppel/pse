/* Copyright 1998 Sun Microsystems, Inc.  All rights reserved. */

#ifndef ITYPES_h_
#define ITYPES_h_

#pragma ident "@(#)Shade and Spixtools V5.33A, V9 SPARC Solaris"


#include <spixtypes.h>

extern uint32	ihash_types[];

#define ih_isvalid(ih)	((unsigned)(ih) < 644)

#define IT_NONE         0x0
#define ih_isnone(ih)	(0)

#define IT_ILOAD        0x1
#define ih_isiload(ih)	(ihash_types[(ih)] & IT_ILOAD)
#define IT_LOAD         0x2
#define ih_isload(ih)	(ihash_types[(ih)] & IT_LOAD)
#define IT_ISTORE       0x4
#define ih_isistore(ih)	(ihash_types[(ih)] & IT_ISTORE)
#define IT_STORE        0x8
#define ih_isstore(ih)	(ihash_types[(ih)] & IT_STORE)
#define IT_BICC         0x10
#define ih_isbicc(ih)	(ihash_types[(ih)] & IT_BICC)
#define IT_FBFCC        0x20
#define ih_isfbfcc(ih)	(ihash_types[(ih)] & IT_FBFCC)
#define IT_BPCC         0x40
#define ih_isbpcc(ih)	(ihash_types[(ih)] & IT_BPCC)
#define IT_FBPFCC       0x80
#define ih_isfbpfcc(ih)	(ihash_types[(ih)] & IT_FBPFCC)
#define IT_BPR          0x100
#define ih_isbpr(ih)	(ihash_types[(ih)] & IT_BPR)
#define IT_BRANCH       0x200
#define ih_isbranch(ih)	(ihash_types[(ih)] & IT_BRANCH)
#define IT_ANNUL        0x400
#define ih_isannul(ih)	(ihash_types[(ih)] & IT_ANNUL)
#define IT_UNCOND       0x800
#define ih_isuncond(ih)	(ihash_types[(ih)] & IT_UNCOND)
#define IT_BAA          0x1000
#define ih_isbaa(ih)	(ihash_types[(ih)] & IT_BAA)
#define IT_DCTI         0x2000
#define ih_isdcti(ih)	(ihash_types[(ih)] & IT_DCTI)
#define IT_PROC         0x4000
#define ih_isproc(ih)	(ihash_types[(ih)] & IT_PROC)
#define IT_TICC         0x8000
#define ih_isticc(ih)	(ihash_types[(ih)] & IT_TICC)
#define IT_ALU          0x10000
#define ih_isalu(ih)	(ihash_types[(ih)] & IT_ALU)
#define IT_SETCCR       0x20000
#define ih_issetccr(ih)	(ihash_types[(ih)] & IT_SETCCR)
#define IT_USECCR       0x40000
#define ih_isuseccr(ih)	(ihash_types[(ih)] & IT_USECCR)
#define IT_MOVCC        0x80000
#define ih_ismovcc(ih)	(ihash_types[(ih)] & IT_MOVCC)
#define IT_FMOVCC       0x100000
#define ih_isfmovcc(ih)	(ihash_types[(ih)] & IT_FMOVCC)
#define IT_MOVR         0x200000
#define ih_ismovr(ih)	(ihash_types[(ih)] & IT_MOVR)
#define IT_FMOVR        0x400000
#define ih_isfmovr(ih)	(ihash_types[(ih)] & IT_FMOVR)
#define IT_FPOP1        0x800000
#define ih_isfpop1(ih)	(ihash_types[(ih)] & IT_FPOP1)
#define IT_SETFCC       0x1000000
#define ih_issetfcc(ih)	(ihash_types[(ih)] & IT_SETFCC)
#define IT_FPCVT        0x2000000
#define ih_isfpcvt(ih)	(ihash_types[(ih)] & IT_FPCVT)
#define IT_PRIV         0x4000000
#define ih_ispriv(ih)	(ihash_types[(ih)] & IT_PRIV)
#define IT_VIS          0x8000000
#define ih_isvis(ih)	(ihash_types[(ih)] & IT_VIS)
#define IT_ANY          0x10000000
#define ih_isany(ih)	(ihash_types[(ih)] & IT_ANY)


#endif /*ITYPES_h_*/
