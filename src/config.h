//  -*- c++ -*-
/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2007 Louisiana State University

// $Id$

#ifdef __linux__
#include <stdint.h>
#include <byteswap.h>
#if __BYTE_ORDER == __LITTLE_ENDIAN
#define HOST_LITTLE_ENDIAN
#endif
#else
#include <sys/types.h>
#ifdef _LITTLE_ENDIAN
#define HOST_LITTLE_ENDIAN
#endif
#endif

#ifdef __linux__
#define FILE_SYS_ELF_H <elf.h>
#else
#define FILE_SYS_ELF_H <sys/elf.h>
#endif

#ifdef __linux__
#define FILE_STAB_H <stab.h>
#else
#define FILE_STAB_H </opt/SUNWspro/SC4.0/include/cc/stab.h>
#define HAVE_STRUCT_NLIST
#endif

// DMK  9 September 2002, 17:07:45 CDT
// This doesn't exactly belong here.

#ifdef HAVE_BSWAP
#define B8(i64) bswap_64((uint64_t)(i64))
#define B4(i32) bswap_32((uint32_t)(i32))
#define B2(i16) bswap_16((uint16_t)(i16))
#else
#define B2(i) ( ( (i) & 0xff ) << 8 | (i) >> 8 & 0xff )
#define B4(i) ( B2(i) << 16 | B2((i)>>16) )
#define B8(i) ( B4(i) << 32 | B4((i)>>32) )
#endif

#define BF(o) B4(o)
