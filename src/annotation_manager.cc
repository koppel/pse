/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2010 Louisiana State University

// $Id$

#include <gtk/gtk.h>
#include "pse.h"
#include "pse_window.h"
#include "dataset.h"
#include "rti_declarations.h"
#include "annotation_manager.h"

static void enable_plot_area_cb(GtkToggleButton *tb, gpointer data);

void
Annotation_Manager::setup_before_dataset_load()
{
  annotation_definitions_version = 1;
  ASSERTS( annotation_definitions.next_free == AID_Instruction_Message );
  Annotation_Definition* const a0 = annotation_definitions.next();
  a0->id = AID_Instruction_Message;
  a0->name = "Ad-Hoc/Instruction Message";
  a0->target = AT_Instruction;
  a0->put("Message","%s");
  a0->put("Description","Instruction annotations without a definition.");
  Annotation_Definition* const a1 = annotation_definitions.next();
  a1->id = AID_Partition_Message;
  a1->name = "Ad-Hoc/Snip (Partition) Message";
  a1->target = AT_Partition;
  a1->put("Message","%s");
  a1->put("Description","Partition (snip) annotations without a definition.");
  Annotation_Definition* const a2 = annotation_definitions.next();
  a2->id = AID_Squash_Group_Message;
  a2->name = "Ad-Hoc/Squash Group";
  a2->target = AT_Squash_Group;
  a2->put("Message","%s");
  a2->put
    ("Description",
     "Squash group (instructions that were flushed together) annotations\n"
     "without a definition. (Definitions for squash groups not yet supported\n"
     "so don't blame the simulator user.)");

  while( annotation_definitions.next_free < AID_ID_First_Available )
    {
      Annotation_Definition* const a = annotation_definitions.next();
      a->name = "Reserved for future expansion.";
      a->id = -1;
    }
  for(int i=0; i<AID_ID_First_Available; i++)
    {
      Annotation_Definition* const ad = annotation_definitions[i];
      if( ad->id == -1 ) continue;
      ASSERTS( ! name_to_definition.present(ad->name) );
      name_to_definition[ad->name] = ad;
    }
  annotation_target_str_to_enum.set_empty_rv(AT_Unknown);
  annotation_target_str_to_enum["instruction"] = AT_Instruction;
  annotation_target_str_to_enum["partition"] = AT_Partition;

  if( !app.gui ) return;

  app.init_color("#aaa",fg_name_disabled);
  enable_plot_area_action = gtk_toggle_action_new
    ("Plot-Area Instruction Annotations",
     "Plot-Area Instruction Annotations",
     "Plot area rendered unreadable by an annotation-mad simulator hacker?"
     "Then just turn off plot-area annotations!  It's that easy!!",
     NULL);
  gtk_toggle_action_set_active
    (enable_plot_area_action,rtiv_annotation_enable_plot_area);
  g_signal_connect
    (enable_plot_area_action, "toggled",
     G_CALLBACK(enable_plot_area_cb), (gpointer)ds);
}

void
Annotation_Manager::definition_read(Dataset_File& df)
{
  const int next_free = annotation_definitions.next_free;
  Annotation_Definition* const a = annotation_definitions.next();
  a->id = -1;
  DSF_Entry e;

  while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END ) switch( e.type ){
  case DSF_CMD_STR_IN4:
    if( e.key.s == "_annotation id" )
      a->id = e.data.i4;
    else
      ASSERTS( false );
    break;
  case DSF_CMD_STR_STR:
    if( e.key.s == "_annotation target" )
      {
        a->target = annotation_target_str_to_enum[e.data.s];
        if( a->target == AT_Unknown )
          fatal("Unknown annotation target, %s",e.data.s.str());
      }
    else if ( e.key.s == "_annotation name" )
      a->name = strescape(e.data.s);
    else
      a->put(e.key.s,e.data.s.dup());
    break;
  default:
    ASSERTA( false );
  }
  if( !a->target ) fatal("Did not find annotation target.\n");
  if( !a->name ) fatal("Did not find annotation name.\n");
  if( name_to_definition.present(a->name) )
    for( int i=1; true; i++ )
      {
        RStringF new_name("%s_%d",a->name.s,i);
        if( name_to_definition.present(new_name.s) ) continue;
        a->name.assign_take(new_name.remove());
        break;
      }
  name_to_definition[a->name] = a;
  ASSERTS( a->id == next_free );
}

void
Annotation_Manager::setup_after_dataset_load()
{
  while( Annotation_Definition* const ad =
         annotation_definitions.iterate() )
      if( ad->id >= 0 ) names_sorted.insert(ad->name,ad);

  group_largest_depth = 0;

  while( Annotation_Definition* const ad = names_sorted.iterate() )
    {
      PSplit pieces(ad->name,'/');
      group_largest_depth = max(group_largest_depth,pieces);
      RString prefix;
      PStack<char*> prefixes;
      while( char* const piece = pieces )
        {
          prefix += "/";  prefix += piece;
          if( !pieces.occ() ) prefix += "/";
          prefixes += prefix.dup();
        }

      for(int leafdist = 0; true ; leafdist++ )
        {
          if( !prefixes ) { ad->group_info[leafdist] = NULL;  break; }
          char* const prefix = prefixes;
          Annotation_Group_Info*& gi = name_to_group[prefix];
          if( gi ) free(prefix);
          else gi = new Annotation_Group_Info(prefix);
          gi->definitions += ad;
          gi->leaf_distance_max = max(gi->leaf_distance_max,leafdist);
          ad->group_info[leafdist] = gi;
        }
    }
  PSplit group_settings(rtiv_annotation_group_settings,'!');
  while( char* const group_setting = group_settings )
    {
      PSplit parts(group_setting,'*');
      if( parts != 2 ) {printf("Warning, invalid group setting.\n"); continue;}
      char* const gname = parts;
      char* settings = parts;
      Annotation_Group_Info* gi;
      if( name_to_group.lookup(gname,gi) )
        while( const char c = *settings++ )
          switch ( c ) {
          case 'd': gi->rti_dont_display = true; break;
          default: gi->rti_settings_unknown += c; break;
          }
      else
        rti_group_unknown += strdup(group_setting);
    }
  if( !app.gui ) return;

  enable_plot_area = gtk_toggle_action_get_active(enable_plot_area_action);
  preferences_dialog_construct(ds->pw_get());
}

void
Annotation_Manager::rti_file_write_prepare()
{
  RString new_rti(200);
  int rti_size = 0;
  if( !preferences_dialog ) return;
  for( Annotation_Group_Info* gi; name_to_group.iterate(gi); )
    {
      if( gtk_toggle_button_get_active(gi->on_off)
          && !gi->rti_settings_unknown.len() ) continue;
      new_rti.sprintf("%s*%s%s!",
                      gi->name,
                      gtk_toggle_button_get_active(gi->on_off) ? "" : "d",
                      gi->rti_settings_unknown.s);
      rti_size++;
    }

  for( char* entry; rti_group_unknown.iterate(entry); )
    {
      if( rti_size >= rtiv_ovr_series_settings_max ) continue;
      new_rti += entry; new_rti += "!";
    }
  rtiv_annotation_group_settings = new_rti.remove();
}


///
/// User Interface Code
///

class AM_Call_Back_Data {
public:
  AM_Call_Back_Data(Dataset *ds, Annotation_Group_Info *gi)
    :ds(ds),gi(gi),i(0){};
  AM_Call_Back_Data(Dataset *ds, int i)
    :ds(ds),gi(NULL),i(i){};
  Dataset* const ds;
  Annotation_Group_Info* const gi;
  const int i;
};

void
Annotation_Manager::setup_before_pse_window_initialized(PSE_Window *pw)
{
  Annotation_PSE_Window_Data* const apwd =
    pw->annotation_data = new Annotation_PSE_Window_Data;
  apwd->message_window_partitions_enabled = true;
  pw->seg_plot_annotation_label = apwd->message_large_ebox.label;
  pw->seg_plot_annotation_pane_label = apwd->message_pane_ebox.label;
}

static void
cb_size_allocate(GtkWidget *w, GtkAllocation *allo, gpointer data);
static void group_on_off_cb(GtkToggleButton* tb, gpointer cbdp);
static bool
annotate_row_enter(GtkWidget* tb, GdkEventCrossing* e, gpointer cbdp);
static bool
annotate_row_leave(GtkWidget* tb, GdkEventCrossing* e, gpointer cbdp);
static bool annotate_blink(gpointer data);

void
annotate_button_callback(GtkWidget *w, gpointer pwptr)
{
  PSE_Window* const pw = (PSE_Window*) pwptr;
  if( pw->ds->am ) pw->ds->am->preferences_dialog_launch(pw);
}

void
annotate_menu_callback(gpointer pwptr, int action, GtkWidget *w)
{
  PSE_Window* const pw = (PSE_Window*) pwptr;
  if( !pw->ds->am ) return;
  switch(action){
  case 'a': pw->ds->am->preferences_dialog_launch(pw); break;
  case 'm': pw->ds->am->message_window_launch(pw); break;
  default:ASSERTS( false );
  }
}

void
Annotation_Manager::preferences_dialog_launch(PSE_Window *pw)
{
  dialog_launch
    (pw, pw->ds->am->preferences_dialog,
     rtiv_annotation_dialog_width, rtiv_annotation_dialog_height);
}

void
Annotation_Manager::dialog_launch
(PSE_Window* pw, PGtkDialogPtr dialog, int width, int height)
{
  if( !GTK_WIDGET_VISIBLE(dialog) )
    {
      gint w, h; // w,h: not including frame.
      GdkRectangle window_rect;
      gdk_window_get_frame_extents(pw->main_w->window,&window_rect);
      gtk_window_get_size(GTK_WINDOW(pw->main_w),&w,&h);
      const gint pad = window_rect.width - w;  // Width of WM's frame.
      const gint pad_top = window_rect.height - h - pad;
      const gint gap = 2; // Desired gap between dialog and main window.
      GdkScreen* const screen = gdk_screen_get_default();
      const gint screen_w = gdk_screen_get_width(screen);
      const gint x_window_right = window_rect.x + window_rect.width + gap;
      const gint x_screen_right = screen_w - width - pad/2;
      const gint x_right_side = min(x_window_right,x_screen_right);
      const gint x_window_left = window_rect.x - width - pad - gap;
      const gint x_left_side = max(x_window_left,-pad/2);
      const gint overlap_right =
        max(0,window_rect.x + window_rect.width - x_right_side);
      const gint overlap_left = max(0,x_left_side + width - window_rect.x);
      GtkWidget* const ref_widget =
        pw->plot_is_ovr ? pw->draw_area_ovr : pw->draw_area;
      gint rx, ry;
      gtk_widget_translate_coordinates(ref_widget,pw->main_w,0,0,&rx,&ry);
      gint place_x =
        !overlap_right ? x_right_side :
        !overlap_left ? x_left_side :
        0.5 * overlap_left < overlap_right ? x_left_side : x_right_side;
      gint place_y =
        window_rect.y + (min(overlap_right,overlap_left)>5?ry+pad_top:0);
      // Eventually, check for overlap with other dialogs here.
      gtk_window_move(dialog, place_x, place_y );
    }
  gtk_window_present(dialog);
}

void
Annotation_Manager::preferences_dialog_construct(PSE_Window* pw)
{
  PGtkDialogPtr annotations_dialog =
    (PGtkDialogPtr)gtk_dialog_new_with_buttons
    ("Annotation Preferences", GTK_WINDOW(pw->main_w),
     (GtkDialogFlags)(GTK_DIALOG_NO_SEPARATOR|GTK_DIALOG_DESTROY_WITH_PARENT),
     NULL);

  gtk_window_set_accept_focus(annotations_dialog,false);
  gtk_window_set_destroy_with_parent(annotations_dialog,false);
  g_signal_connect
    (annotations_dialog, "size-allocate", G_CALLBACK(cb_size_allocate), NULL);
  PGtkVBoxPtr dialog_vbox = annotations_dialog->vbox;
  PLabelEventBoxPtr label_top;
  gtk_label_set_markup
    (label_top,
     "<big>Annotation Preferences</big>\n"
     "Use checkboxes to enable or disable the annotations listed\n"
     "below.  The rightmost box controls an individual annotation,\n"
     "the others control groups. Hover name to blink annotations.");
  gtk_alignment_set_padding(label_top,3,10,5,5);
  gtk_box_pack_start(dialog_vbox, label_top, false, true, 0);

  PGtkCheckButtonPtr enable_plot_area_check_button = gtk_check_button_new();
  gtk_action_connect_proxy
    (enable_plot_area_action, enable_plot_area_check_button);
  gtk_box_pack_start(dialog_vbox,enable_plot_area_check_button,false,true,0);

  PGtkScrolledWindowPtr scroll_w = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_shadow_type(scroll_w,GTK_SHADOW_NONE);
  gtk_window_set_default_size
    (annotations_dialog,
     rtiv_annotation_dialog_width,rtiv_annotation_dialog_height);
  gtk_scrolled_window_set_policy
    (scroll_w, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  const int button_end_col = group_largest_depth + 2;
  const int area_col = button_end_col;
  const int tip_col = area_col + 1;
  const int label_col = tip_col + 1;
  const int rows = 2 * names_sorted.occ() + 2;
  PGtkTablePtr table = gtk_table_new( rows, label_col + 1, false);
  PGtkAlignmentPtr table_alignment = gtk_alignment_new(0,0,1,1);
  gtk_alignment_set_padding(table_alignment,5,5,5,5);
  gtk_container_add(table_alignment,table);
  gtk_scrolled_window_add_with_viewport(scroll_w, table_alignment);
  gtk_box_pack_start(dialog_vbox, scroll_w, true, true, 0);
  gtk_table_set_col_spacing(table,button_end_col-1,14);
  gtk_table_set_col_spacing(table,button_end_col,10);
  gtk_table_set_col_spacing(table,tip_col,10);
  gtk_table_set_row_spacing(table,1,5);

  PString_Hash<bool> visited_groups;
  int row = 0;
  int min_check_cols[rows+4];
  for(int i=0; i<=rows; i++) min_check_cols[i] = button_end_col;
  PLabelEventBoxPtr heading_enable("<b>Display</b>",true);
  PLabelEventBoxPtr heading_location("<b>Area</b>",true);
  PLabelEventBoxPtr heading_name("<b>Annotation Name</b>",true);
  gtk_table_attach
    (table, heading_enable, 1, button_end_col, row,row+1,
     GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach
    (table, gtk_hseparator_new(), 1, button_end_col, row+1,row+2,
     GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach
    (table, heading_location, area_col, area_col+1, row,row+1,
     GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach
    (table, gtk_hseparator_new(), area_col, area_col+1, row+1,row+2,
     GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach
    (table, heading_name, label_col, label_col + 1, row,row+1,
     GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach
    (table, gtk_hseparator_new(), label_col, label_col + 1, row+1,row+2,
     GTK_FILL, GTK_FILL, 0, 0);

  row = 2;

  while( Annotation_Definition* const ad = names_sorted.iterate() )
    {
      int min_check_col = button_end_col;
      for(int i=0; ad->group_info[i]; i++)
        {
          Annotation_Group_Info* const gi = ad->group_info[i];
          const char* const gn = gi->name;
          if( visited_groups[gn] ) continue;
          visited_groups[gn] = true;
          const int descendants = gi->definitions.occ();
          PGtkVBoxPtr cbox = gtk_vbox_new(false,0);
          PGtkCheckButtonPtr on_off = gi->on_off = gtk_check_button_new();
          gtk_box_pack_start(cbox,on_off,true,true,0);
          const int col = button_end_col - 1 - gi->leaf_distance_max;
          const int bottom_row = row+2*descendants;
          if( i && descendants > 1 )
            {
              min_check_col = min(min_check_col,col);
              min_check_cols[bottom_row] = min(min_check_cols[bottom_row],col);
            }
          gtk_table_attach
            (table,cbox,col,col+1, row, bottom_row, GTK_FILL,GTK_FILL,0,0);
          gtk_toggle_button_set_active(on_off,ad->enabled);
          g_signal_connect
            (on_off, "toggled", G_CALLBACK(group_on_off_cb),
             new AM_Call_Back_Data(ds,gi));
        }
      int line_col = min(min_check_col,min_check_cols[row]);
      if( row > 2 && line_col != button_end_col )
        {
          gtk_table_attach
            (table,gtk_hseparator_new(),
             line_col, button_end_col, row-1, row,
             GTK_FILL,GTK_FILL,0,0);
        }
      PGtkHBoxPtr letter_hbox = gtk_hbox_new(true,0);
      PLabelEventBoxPtr elabel_p(ad->location_plot ? "<b>P</b>" : "p",true);
      PLabelEventBoxPtr elabel_a(ad->location_assembler?"<b>A</b>":"a",true);
      PLabelEventBoxPtr elabel_m(ad->location_message ?"<b>M</b>":"m",true);
      ad->letter_plot = elabel_p;
      ad->letter_plot_label = elabel_p.label;
      ad->letter_assembler = elabel_a;
      ad->letter_message = elabel_m;
      gtk_box_pack_start(letter_hbox,elabel_p,true,true,0);
      gtk_box_pack_start(letter_hbox,elabel_a,true,true,0);
      gtk_box_pack_start(letter_hbox,elabel_m,true,true,0);
      gtk_table_attach
        (table, letter_hbox, button_end_col+0, button_end_col+1, row,row+1,
         GTK_FILL, GTK_FILL, 0, 0);

      if( const char* const desc = ad->get("Description") )
        {
          PLabelEventBoxPtr tip("<b>?</b>",true);
          gtk_tooltips_set_tip(app.tips,tip,desc,"");
          gtk_table_attach
            (table, tip, tip_col, tip_col+1, row,row+1,
             GTK_FILL, GTK_FILL, 0, 0);
        }

      PLabelEventBoxPtr label(ad->name);
      ad->name_for_label = ad->name;
      ad->dialog_row = label.event_box;
      ad->dialog_row_label = label.label;
      gtk_table_attach
        (table, label.event_box, label_col, label_col+1, row,row+1,
         GTK_FILL, GTK_FILL, 0, 0);
      gtk_widget_add_events
        (label, GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);
      g_signal_connect
        (label,"enter-notify-event",G_CALLBACK(annotate_row_enter),
         new AM_Call_Back_Data(ds,ad->id));
      g_signal_connect
        (label,"leave-notify-event",G_CALLBACK(annotate_row_leave),
         new AM_Call_Back_Data(ds,ad->id));
      row+=2;
    }
  const int line_col = min_check_cols[row];
  if( line_col != button_end_col )
    gtk_table_attach
      (table,gtk_hseparator_new(),
       line_col, button_end_col, row-1, row, GTK_FILL,GTK_FILL,0,0);
  ASSERTS( row == rows );
  RStringF key_label_text
    ("<b>?</b>: Hover for annotation description.\n"
     "Letters: <b>P</b>: Plot area;  <b>A</b>: Assembly pane;  "
     "<b>M</b>: Message label.\n"
     "Letter insensitive: Annotated instruction not in that area.\n"
     "Letter lower case: Insn in area, but annotation has no effect there.\n"
     "Letter background: <span background=\"%s\">pointer</span> "
     "or <span background=\"%s\">cursor</span> over annotated instruction.\n",
     app.c_pointer_bg.spec, app.c_cursor_locked_bg.spec);
  PLabelEventBoxPtr key_label(key_label_text.remove(),true);
  gtk_alignment_set_padding(key_label,5,0,5,5);
  gtk_box_pack_start(dialog_vbox, key_label, false, true, 0);
  g_signal_connect
    (annotations_dialog, "delete_event",
     G_CALLBACK(gtk_widget_hide_on_delete), annotations_dialog);
  g_signal_connect
    (annotations_dialog, "response",
     G_CALLBACK(gtk_widget_hide), annotations_dialog);
  preferences_dialog = annotations_dialog;
  gtk_widget_show_all(dialog_vbox);

  {
    PLabelEventBoxPtr sample(NULL);
    bg_letter_normal =
      &gtk_widget_get_style(sample.event_box)->bg[GTK_STATE_NORMAL];
    GdkColor& c1 = app.c_cursor_locked_bg.gdkcolor;
    GdkColor& c2 = app.c_pointer_bg.gdkcolor;
    RStringF spec
      ("#%02x%02x%02x",
       (c1.red + c2.red)>>9, (c1.green + c2.green)>>9, (c1.blue + c2.blue)>>9);
    app.init_color(spec,bg_letter_cursor_and_pointer);
  }

  for( Annotation_Group_Info* gi; name_to_group.iterate(gi); )
    if( gi->rti_dont_display ) gtk_toggle_button_set_active(gi->on_off,false);
}

static bool
annotate_row_enter(GtkWidget* tb, GdkEventCrossing* e, gpointer cbdp)
{
  AM_Call_Back_Data* const cbd = (AM_Call_Back_Data*) cbdp;
  Dataset* const ds = cbd->ds;
  Annotation_Definition* const ad = ds->am->annotation_definitions[cbd->i];
  if( ( ad->location_plot ? ad->use_count_last_repaint_plot : 0 ) +
      ( ad->location_assembler ? ad->use_count_last_repaint_assembler : 0 ) +
      ( ad->location_message ? ad->use_count_last_repaint_message : 0 ) == 0 )
    return false;
  gtk_widget_set_state(tb,GTK_STATE_PRELIGHT);
  g_timeout_add
    (200,(GSourceFunc)annotate_blink, new AM_Call_Back_Data(ds,cbd->i));

  ad->blink_countdown = 500;
  return false;
}

static bool
annotate_blink(gpointer data)
{
  yield_to_gtk(); // Re-spinning annotations is expensive. (21 June 2005)
  AM_Call_Back_Data* const cbd = (AM_Call_Back_Data*) data;
  Dataset* const ds = cbd->ds;
  Annotation_Definition* const ad = ds->am->annotation_definitions[cbd->i];
  ad->blink_countdown--;
  ds->am->annotation_definitions_version++;
  if( ad->blink_countdown <= 0 )
    {
      if( ad->blink ) ds->invalidate_draw_areas();
      ad->blink = false;
      free(cbd);
      return false;
    }
  ad->blink = !ad->blink;
  ds->invalidate_draw_areas();
  return true;
}

static bool
annotate_row_leave(GtkWidget* tb, GdkEventCrossing* e, gpointer cbdp)
{
  AM_Call_Back_Data* const cbd = (AM_Call_Back_Data*) cbdp;
  Dataset* const ds = cbd->ds;
  Annotation_Definition* const ad = ds->am->annotation_definitions[cbd->i];
  gtk_widget_set_state(tb,GTK_STATE_NORMAL);
  ad->blink_countdown = 0;
  ds->am->annotation_definitions_version++;
  if( ad->blink ) ds->invalidate_draw_areas();
  ad->blink = false;
  return false;
}

static void
group_on_off_cb(GtkToggleButton* tb, gpointer cbdp)
{
  AM_Call_Back_Data* const cbd = (AM_Call_Back_Data*) cbdp;
  Dataset* const ds = cbd->ds;
  Annotation_Group_Info* const gi = cbd->gi;
  const bool on = gtk_toggle_button_get_active(tb);
  for( Annotation_Definition* ad = NULL; gi->definitions.iterate(ad); )
    {
      if( on ) ad->groups_disabled_count++; else ad->groups_disabled_count--;
      if( ad->enabled == !ad->groups_disabled_count ) continue;
      ad->enabled = on;
      GdkColor* const color = on ? &app.gdk_black : ds->am->fg_name_disabled;
      gtk_widget_modify_fg(ad->dialog_row_label, GTK_STATE_NORMAL,color);
      gtk_widget_modify_fg(ad->dialog_row_label, GTK_STATE_PRELIGHT,color);
      gtk_widget_modify_fg(ad->dialog_row_label, GTK_STATE_ACTIVE,color);
    }
  ds->am->annotation_definitions_version++;
  ds->invalidate_draw_areas();
}

static void
enable_plot_area_cb(GtkToggleButton *tb, gpointer data)
{
  Dataset* const ds = (Dataset*) data;
  Annotation_Manager* const am = ds->am;
  const bool on = gtk_toggle_action_get_active(am->enable_plot_area_action);
  rtiv_annotation_enable_plot_area = on;
  am->enable_plot_area = on;
  GdkColor* const color = on ? &app.gdk_black : am->fg_name_disabled;
  if( am->preferences_dialog )
    while( Annotation_Definition* ad = am->annotation_definitions.iterate() )
      {
        if( !ad->p.marker_color_specified ) continue;
        gtk_widget_modify_fg(ad->letter_plot_label, GTK_STATE_NORMAL, color);
      }
  am->annotation_definitions_version++;
  ds->invalidate_draw_areas();
}

static void
cb_size_allocate(GtkWidget *w, GtkAllocation *allo, gpointer data)
{
  rtiv_annotation_dialog_width = allo->width;
  rtiv_annotation_dialog_height = allo->height;
}

static void
message_window_size_cb(GtkWidget *w, GtkAllocation *allo, gpointer data)
{
  rtiv_annotation_message_window_width = allo->width;
  rtiv_annotation_message_window_height = allo->height;
}

static void
message_window_enable_partitions_cb(GtkToggleAction *w, gpointer data)
{
  PSE_Window* const pw = (PSE_Window*) data;
  pw->annotation_data->message_window_partitions_enabled =
    gtk_toggle_action_get_active(w);
  pw->ds->am->annotation_definitions_version++;
  pw->invalidate_draw_area();
}

static gint
message_window_button_press_cb
(GtkLabel *l, GdkEventButton *be, gpointer data)
{
  PSE_Window* const pw = (PSE_Window*) data;
  if( be->type != GDK_2BUTTON_PRESS ) return false;
  if( be->button != 1 ) return false;
  pw->ds->am->message_window_launch(pw);
  return true;
}

GtkWidget*
Annotation_Manager::message_pane_construct(PSE_Window *pw)
{
  return message_common_construct(pw,true);
}

GtkWidget*
Annotation_Manager::message_common_construct(PSE_Window *pw, bool pane)
{
  Annotation_PSE_Window_Data* const apwd = pw->annotation_data;

  if( !apwd->message_enable_partitions_action )
    {
      apwd->message_enable_partitions_action = gtk_toggle_action_new
        ("M", "M", "Show Snip Messages", NULL);
      gtk_toggle_action_set_active(apwd->message_enable_partitions_action,true);
      g_signal_connect
        (apwd->message_enable_partitions_action,"toggled",
         G_CALLBACK(message_window_enable_partitions_cb),gpointer(pw));
    }

  PGtkBoxPtr main_box;
  PGtkBoxPtr controls_box;
  if( pane )
    {
      main_box = gtk_hbox_new(false,0);
      controls_box = gtk_vbox_new(true,0);
    }
  else
    {
      main_box = gtk_vbox_new(false,0);
      controls_box = gtk_hbox_new(true,0);
    }
  gtk_box_pack_start(main_box,controls_box, false, true, 0);

  PGtkCheckButtonPtr check_snips = gtk_check_button_new();
  gtk_box_pack_start(controls_box,check_snips,true,true,0);
  gtk_action_connect_proxy
    (apwd->message_enable_partitions_action,check_snips);
  gtk_button_set_label
    (check_snips, pane ? "M" : "Show Snip Messages");
  gtk_tooltips_set_tip
    (app.tips, check_snips,
     "Show messages attached to snips.", "");

  insert_new_button
    (controls_box, "A", annotate_button_callback, gpointer(pw),
     "Show annotation preferences dialog.");

  PGtkScrolledWindowPtr scroll_w = gtk_scrolled_window_new(NULL, NULL);
  gtk_box_pack_start(main_box,scroll_w,true,true,0);

  gtk_scrolled_window_set_shadow_type(scroll_w,GTK_SHADOW_NONE);
  gtk_scrolled_window_set_policy
    (scroll_w, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  PLabelEventBoxPtr& ebox =
    pane ? apwd->message_pane_ebox : apwd->message_large_ebox;
  gtk_alignment_set(ebox,0,0,0,0);
  gtk_label_set_selectable(ebox,true);
  pw->label_set += ebox.label;
  gtk_alignment_set_padding(ebox,5,5,10,10);
  gtk_scrolled_window_add_with_viewport(scroll_w,ebox);
  if( pane )
    g_signal_connect
      (ebox.label,"button-press-event",
       G_CALLBACK(message_window_button_press_cb),gpointer(pw));

  return main_box;
}

void
Annotation_Manager::message_window_launch(PSE_Window *pw)
{
  Annotation_PSE_Window_Data* const apwd = pw->annotation_data;
  PGtkDialogPtr mw =
    apwd->message_window ? apwd->message_window
    : (PGtkDialogPtr)gtk_dialog_new_with_buttons
    ("Message Annotations", GTK_WINDOW(pw->main_w),
     (GtkDialogFlags)(GTK_DIALOG_NO_SEPARATOR|GTK_DIALOG_DESTROY_WITH_PARENT),
     NULL);
  if( apwd->message_window ) {gtk_window_present(mw); return;}
  const gpointer pwptr = gpointer(pw);
  //  gtk_window_set_accept_focus(mw,false);
  g_signal_connect
    (mw, "delete_event", G_CALLBACK(gtk_widget_hide_on_delete), mw);
  g_signal_connect(mw, "response", G_CALLBACK(gtk_widget_hide), mw);

  PGtkVBoxPtr dialog_vbox = mw->vbox;
  GtkWidget* const message_common_widget = message_common_construct(pw,false);
  gtk_box_pack_start(dialog_vbox, message_common_widget, true, true, 0);
  gtk_window_set_default_size
    (mw, rtiv_annotation_message_window_width,
     rtiv_annotation_message_window_height);
  apwd->message_window = mw;
  g_signal_connect
    (mw, "size-allocate", G_CALLBACK(message_window_size_cb),pwptr);
  gtk_widget_show_all(mw);
  gtk_widget_hide(mw);
  dialog_launch
    (pw, mw, rtiv_annotation_message_window_width,
     rtiv_annotation_message_window_height);
  pw->invalidate_draw_area();
}

void
Annotation_Manager::setup_before_pse_window_destroyed(PSE_Window *pw)
{
  if( pw->annotation_data ) delete pw->annotation_data;
}
