/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2009 Louisiana State University

// $Id$

//
// Read symbol information from executable file.
//

#include <stdio.h>
#ifdef __linux__
#include <sys/procfs.h>
#else
#include <sys/elf.h>
#endif
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include "dwarf.h"
#include <vector>
#include <map>
#include <string>
#include "readex.h"
#ifdef MAIN
#else
#include "messages.h"
#endif
#include "misc.h"

#include FILE_STAB_H

#ifndef HOST_LITTLE_ENDIAN
#undef B8
#define B8(n) n
#undef B4
#define B4(n) n
#undef B2
#define B2(n) n
#undef BF
#define BF(n) n
#endif

#ifndef HAVE_STRUCT_NLIST

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Definition of the sparc stack frame (when it is pushed on the stack).
 */
struct frame_ilp32 {
        int32_t fr_local[8];            /* saved locals */
        int32_t fr_arg[6];              /* saved arguments [0 - 5] */
        /* struct frame_ilp32 *fr_savfp;  (Original declaration.) */
        uint32_t fr_savfp;               /* saved frame pointer */
        int32_t fr_savpc;               /* saved program counter */
        /* char    *fr_stret;  (Original declaration. ) */
        uint32_t fr_stret;              /* struct return addr */
        int32_t fr_argd[6];             /* arg dump area */
        int32_t fr_argx[1];             /* array of args past the sixth */
};

#ifdef	__cplusplus
}
#endif

struct nlist {
        union {
          uint32_t n_name_x; // Originally n_name. Renamed to make sure not used.
          int32_t  n_strx;
        } n_un;
        unsigned char   n_type;
        char    n_other;
        int16_t   n_desc;
        uint32_t   n_value;
};

#define	N_SO	0x64		/* source file name: name,,0,0,0 */
#define	N_SOL	0x84		/* #included file name: name,,0,0,0 */
#define	N_FUN	0x24		/* procedure: name,,0,linenumber,0 */
#define	N_SLINE	0x44		/* src line: 0,,0,linenumber,function relative */

#endif /* STAB_PATH */



static Elf32_Ehdr exec_header;

typedef struct {
  char *file;
  rx_addr start, end;
  int count;
} File_Info;

static File_Info *fInfo;
static int file_table_size;

RX_Range rx_text_range;

/* --------------------------------------------------------------------------*/
/* Stuff that might be put in header file later. */


/* --------------------------------------------------------------------------*/
/* Utility Functions */


#define AlloArray(ptr,num,type)							\
 ptr=(type*)malloc((num)*sizeof(type));						\
 if((ptr)==NULL)exit(1);							\
 memset((char*)ptr,0,(num)*sizeof(type));


#define ReadArray(strm,ptr,num,type) \
	read(fileno(strm),(char*)ptr,sizeof(type)*num)

#define nextinitsize 16

#define NEXT(base,iter,last,type)                                               \
 if( iter == NULL )                                                             \
   {                                                                            \
     iter = base = (type*) malloc( nextinitsize * sizeof( base[0] ) );          \
     if( !base ) fatal("Could not allocate memory in NEXT.");                   \
     last = &base[nextinitsize];                                                \
   }                                                                            \
 else                                                                           \
   {                                                                            \
     iter++;                                                                    \
     if( iter == last )                                                         \
       {                                                                        \
	 int count = iter - base;                                               \
	 int newcount = count << 1;                                             \
	 base = (type*) realloc( base, newcount * sizeof( base[0] ) );          \
	 if( !base ) fatal("Could not reallocate memory in NEXT.");             \
	 iter = &base[count];                                                   \
	 last = &base[newcount];                                                \
       }                                                                        \
   }
// You guessed it, this was originally written in C!

#define LAST(base,iter,last,count,type)                                         \
{                                                                               \
  if( base ) {                                                                  \
    count = iter - base + 1;                                                    \
    base = (type*) realloc(base, count * sizeof( base[0] ) );                   \
  } else { count = 0; }                                                         \
}


#ifdef MAIN
void fatal(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);

    fprintf(stderr, "Fatal Error: ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n\n");

    fprintf(stderr,"Execution aborted.\n");

    exit(1);

    va_end(ap);
}
#endif

extern int errno;

/* Globals */

static Line_Info *lInfo;        /* Sorted by address. */
static RX_Sym_Info *noProc_InfoPtr,noProc_Info;
static int lineTableSize;

/* Search performed on the first word of each element.   */
static void *
bSearchA(void **base_p,int Stride,int nelem,rx_addr key)
{
  rx_addr* const base = (rx_addr*) base_p;
  int stride = Stride >> 2;
  int index=stride,delta,afterEnd=nelem*stride;

  if(nelem==0)return NULL;

  while(index<afterEnd)index<<=1;
  delta=index;

  while(1)
    {
      rx_addr cur;
      if( index >= afterEnd )
	{
	  cur = base[ afterEnd-stride ];
          if( cur < key )return &base[ afterEnd - stride ];
	}
      else
	  cur = base[index];

      if( cur == key ) break;
      if(  delta == stride )
	{
	  if( cur > key ) index = index == 0 ? index : index - stride;
	  break;
	}
      delta >>= 1;
      if( cur > key ) index -= delta; else index += delta;
    }

  if( base[index] > key ) return NULL;

  return &base[index];

}


static int
fi_comp(File_Info *a, File_Info *b)
{
  return strcmp(a->file,b->file);
}

static int li_comp_addr(Line_Info *a, Line_Info *b)
{ return a->addr - b->addr; }
static int fi_comp_addr(File_Info *a, File_Info *b)
{ return a->start - b->start; }

typedef int(*Comp_Func)(const void*, const void*);

////////////////////////////////////////

struct RX_Image_Text_Block
{
  rx_addr block_start_pc;
  rx_addr block_end_pc;
  off_t block_start_file_offset;
};

using namespace std;

// DMK: Eventually move exec-specific stuff here to allow multiple files.
class RX_Executable_Info
{
public:
  RX_Executable_Info()
  {
    sec_debug_line_size = 0;
  }

  bool get_text(rx_addr addr, uint32_t &text_host_order);

  void load_info_dwarf();

  int me_fd;
  char *bfdi_fprintf_string;
  off_t sec_debug_line_offset;
  int sec_debug_line_size;


private:

  void load_dwarf_lines_compilation_unit(off_t limit);

  //
  // Utility
  //

  void file_seek(off_t offset){lseek(me_fd,offset,SEEK_SET);}
  bool file_reached(off_t offset)
  {
    const off_t now = lseek(me_fd,0,SEEK_CUR);
    return now >= offset;
  }
  ssize_t read_check(char* buf, int size)
  {
    const ssize_t rv = read(me_fd,buf,size);
    if( rv == -1 )
      fatal("Could not read file because: %s\n",strerror(errno));
    return rv;
  }
  uint32_t file_read_uint32()
  {
    uint32_t buf;
    read_check((char*)&buf,4);
    return B4(buf);
  }
  int16_t file_read_int16()
  {
    int16_t buf;
    read_check((char*)&buf,2);
    return B2(buf);
  }
  uint16_t file_read_uint16()
  {
    uint16_t buf;
    read_check((char*)&buf,2);
    return B2(buf);
  }
  char file_read_byte() {char buf; read_check(&buf,1); return buf;}
  unsigned char file_read_ubyte()
  {unsigned char buf; read_check((char*)&buf,1); return buf;}
  char* file_read_string()
  {
    const int chunk_size = 10;
    char chunk[chunk_size+1];
    RString str("");
    while(1)
      {
        const int amt = read_check(chunk,chunk_size);
        chunk[amt] = 0;
        str += chunk;
        const int len = strlen(chunk);
        if( len < amt ) {lseek(me_fd,-amt+len+1,SEEK_CUR); break;}
      }
    return str.remove();
  }
  uint file_read_dwarf_uleb128()
  { return file_read_dwarf_xleb128<false,uint>();}
  int file_read_dwarf_leb128()
  { return file_read_dwarf_xleb128<true,int>();}

  template<bool SIGNED, class TYPE>
  TYPE file_read_dwarf_xleb128()
  {
    int next_byte = file_read_ubyte();
    if( !next_byte ) return 0;
    TYPE acc = 0;
    int shift = 0;
    while(1)
      {
        acc |= ( next_byte & 0x7f ) << shift;
        shift += 7;
        if( ! ( next_byte & 0x80 ) ) break;
        next_byte = file_read_ubyte();
      }
    if( !SIGNED ) return acc;
    const int empty_bits = (sizeof(TYPE)*8) - shift;
    return ( acc << empty_bits ) >> empty_bits;
  }


  // Used for dwarf only so far. (18 September 2004, 14:37:42 CDT)
  Line_Info *line_info_ptr, *line_info_last;
  vector<char*> strings_to_free_at_destroy;

  // Used for everything.
public:
  PSList<RX_Sym_Info*,const char*> proc_by_name;
  PSList<RX_Sym_Info*,rx_addr> proc_by_addr;
  PSList<RX_Sym_Info*,const char*> sym_by_name;
  PSList<RX_Sym_Info*,rx_addr> sym_by_addr;

  PSList<RX_Image_Text_Block*> text_blocks;
};

RX_Executable_Info *ei;

void
RX_Executable_Info::load_info_dwarf()
{
  lInfo = NULL;
  line_info_ptr = NULL;
  line_info_last = NULL;
  const off_t done = sec_debug_line_offset + sec_debug_line_size;

  for ( off_t file_position = sec_debug_line_offset; file_position < done; )
    {
      file_seek(file_position);
      const int total_length = file_read_uint32();
      if ( total_length == 0 )
        break;
      const int version = file_read_uint16();
      const off_t unit_end = file_position + total_length + 4;
      if( version > 2 ) fatal("Unrecognized dwarf version.");
      load_dwarf_lines_compilation_unit(unit_end);
      file_position = unit_end;
    }
  LAST(lInfo, line_info_ptr,line_info_last,lineTableSize,Line_Info);
}

void
RX_Executable_Info::load_dwarf_lines_compilation_unit(off_t limit)
{
  const int prolog_length = file_read_uint32();
  const int minimum_instruction_length = file_read_ubyte();
  const int reg_is_stmt_initial = file_read_ubyte();
  const int line_base = file_read_byte();
  const int line_range = file_read_ubyte();
  const int opcode_base = file_read_ubyte();
  if( false && prolog_length ) fatal("Pacify compiler.");

  for(int i=1; i<opcode_base; i++)
    {
      const int opcode_len = ei->file_read_ubyte();
      if(0)printf("Opcode %d len is %d\n",i,opcode_len);
    }

  vector<const char*> dirs;
  dirs.push_back("");
  while(1)
    {
      char* const path = file_read_string();
      if( !*path ) break;
      dirs.push_back(path);
      strings_to_free_at_destroy.push_back(path);
    }
  vector<const char*> source_files;
  source_files.push_back("");
  while(1)
    {
      char* const file = file_read_string();
      if( !*file ) break;
      const int file_dir_idx = file_read_dwarf_uleb128();
      const int file_mod_time = file_read_dwarf_uleb128();
      const int file_size = file_read_dwarf_uleb128();
      if(0)
        printf("File \"%s\", dir_idx %s, time %d, size %d\n",
               file, dirs[file_dir_idx], file_mod_time, file_size);
      const char* const dir = dirs[file_dir_idx];
      PSplit dir_pieces(dir,'/');
      RString dir_start(dir_pieces ? dir_pieces[0] : "");
      RString source_path("");
      if ( dir_start != "home" )
        source_path += dirs[file_dir_idx];
      source_path += file;
      char* const str = source_path.remove();
      strings_to_free_at_destroy.push_back(str);
      source_files.push_back(str);
      free(file);
    }

  //
  // WARNING: Most opcodes below untested. (DMK 18 September 2004)
  // WARNING: Line information recorded based on gcc, 3.3.x.
  //

  int count = 0;
  int index = 0;

  int reg_line = -1;
  bool reg_is_stmt = 0;
  uint32_t reg_address = 0xf000000;
  int reg_file = 0x7000000;
  bool reg_end_sequence = true;

  while ( !file_reached(limit) )
    {
      if ( reg_end_sequence )
        {
          reg_line = 1;
          reg_is_stmt = reg_is_stmt_initial;
          reg_address = 0;
          reg_file = 1;
          //  reg_basic_block = false;
          reg_end_sequence = false;
        }

      count++;
      const int opcode = file_read_ubyte();
      switch( opcode ){
      case 0: // Extended opcode.
        {
          const int insn_length = file_read_dwarf_uleb128(); // Length;
          if ( !insn_length ) 
            {
              // This seems to work but dwarf 2.0 spec says nothing.
              // -DMK 12 February 2008, 18:56:24 CST
              reg_end_sequence = true;
              break;
            }
          const int eopcode = file_read_ubyte();
          switch( eopcode ){
          case DW_LNE_set_address:
            index++;
            reg_address = file_read_uint32();
            break;
          case DW_LNE_end_sequence:
            reg_end_sequence = true;
            break;
          case DW_LNE_define_file:
            {
              char *source_file = file_read_string();
              int dir_idx = file_read_dwarf_uleb128();
              int file_mod_time = file_read_dwarf_uleb128();
              int file_length = file_read_dwarf_uleb128();
              fatal("Dwarf: Not implemented %s, %d, %d, %d",
                    source_file, dir_idx, file_mod_time,file_length);
            }
            break;
          default:
            fatal("Dwarf command unimplemented.");
          }
        }
        break;
      case DW_LNS_advance_pc:
        reg_address += file_read_dwarf_uleb128() * minimum_instruction_length;
        break;
      case DW_LNS_advance_line:
        reg_line += file_read_dwarf_leb128();
        break;
      case DW_LNS_set_file:
        reg_file = file_read_dwarf_uleb128();
        break;
      case DW_LNS_set_column:
        //  reg_column = file_read_dwarf_uleb128();
        break;
      case DW_LNS_negate_stmt:
        reg_is_stmt = !reg_is_stmt;
        break;
      case DW_LNS_set_basic_block:
        //  reg_basic_block = true;
        break;
      case DW_LNS_const_add_pc:
        fatal("Unimpl.");
      case DW_LNS_copy:
        // Fall through...
      default:
        // Special Opcode
        if( opcode >= opcode_base )
          {
            const int adjusted_opcode = opcode - opcode_base;
            reg_address +=
              adjusted_opcode / line_range * minimum_instruction_length;
            reg_line += line_base + adjusted_opcode % line_range;
          }
        else if ( opcode != DW_LNS_copy )
          fatal("Unrecognized Dwarf line opcode..");

        {
          NEXT(lInfo,line_info_ptr,line_info_last,Line_Info);
          line_info_ptr->addr = reg_address;
          line_info_ptr->lineno = reg_line;
          line_info_ptr->llmno = index;
          line_info_ptr->source_file_name = source_files[reg_file];
        }
        //  reg_basic_block = false;
        // reg_prologue_end = false;
        // reg_epilogue_begin = false;
        if( false && count < 100 )
          printf("PC 0x%x, %s::%d\n",
                 reg_address, source_files[reg_file], reg_line);

        break;
      }
    }
}


#define SSYM_NAME(index) (&sym_str[B4(ssym[index]->st_name)])
#define ELF_HAS_NAME(symb) \
	( (symb).st_name && sym_str[ B4((symb).st_name) ] )


RX_Range
RX_proc_range(char *name)
{
  RX_Range r = {0, 0};

  if ( name == NULL ) return r;

  RX_Sym_Info* const pi = ei->proc_by_name.get_eq(name);

  if ( pi == NULL ) return r;
  
  r.start = pi->start;
  r.end = pi->end;
  return r;
}

RX_Range
RX_file_range(char *name)
{
  File_Info *fi, key;
  RX_Range r = {0, 0};

  if ( !file_table_size || name == NULL ) return r;

  key.file = name;
  fi = (File_Info*) bsearch(&key, fInfo, file_table_size,
			    sizeof(File_Info), (Comp_Func)fi_comp);
  if( fi == NULL ) return r;
  r.start = fi->start;
  r.end = fi->end;
  return r;
}


RX_Range
RX_sym_range(const char *name)
{
  RX_Range r;
  RX_Sym_Info* const s = RX_find_sym(name);
  if( s == NULL ) {
    r.start = r.end = 0;
  } else {
    r.start = s->start;
    r.end = s->end;
  }
  return r;
}

RX_Sym_Info *
RX_find_sym(const char *key)
{
  return ei->sym_by_name.get_eq(key);
}


RX_Sym_Info *
RX_sym_at_addr(rx_addr addr)
{
  return ei->sym_by_addr.get_le(addr);
}

char *RX_addr_text_to_sym(rx_addr addr);

const char *
RX_addr_to_sym_expr(rx_addr addr, unsigned sizep)
{
  RX_Sym_Info* const sym = RX_sym_at_addr(addr);
  int index, misalign;
  const int bsze = 500;
  static char buf[bsze];

  if( !sym ) return "";

  ASSERTS( addr >= sym->start );
  if ( addr < sym->start ) return "";  // Shouldn't happen.

  uint32_t indexc =  addr - sym->start;

  if ( indexc > 0x100 && addr >= sym->end ) return "";

  const char *cast;
  unsigned size;
  const char *lbracket = "[", *rbracket = "]"; // May be reassigned.

  if( sizep == 0 )
    {
      if ( sym->elf_type == STT_FUNC )
        {
          size = 4; cast = "";
          lbracket = "+"; rbracket = "";
        }
      else
        {
          size = 1;
          cast = "(char*)";
        }
    }
  else
    {
      cast = "";
      size = sizep;
    }

  index =  indexc / size;
  misalign = indexc - index * size;

  if( misalign )
    snprintf(buf,bsze,"%s%s[%d+%d/%d]",
             cast,
             sym->func_name,
             index,misalign,size);
  else
    snprintf(buf,bsze,"%s%s%s%d%s",
             cast,
             sym->func_name,
             lbracket,
             index,
             rbracket
             );

  return buf;
}

RX_Sym_Info *
RX_proc_info(rx_addr addr)
{
 RX_Sym_Info* const pi = ei->proc_by_addr.get_le(addr);
 if( pi==NULL ||  pi->end <= addr )
   return NULL; else return pi;
}

int
RX_addr_to_line(rx_addr addr)
{
  Line_Info *line = RX_addr_to_line_info(addr);
  if(line==NULL)return -1; else return line->lineno;
}

Line_Info *
RX_addr_to_line_info(rx_addr addr)
{
  Line_Info *line = (Line_Info*)
    bSearchA((void**)lInfo,sizeof(Line_Info),lineTableSize,addr);
  RX_Sym_Info* const pi = RX_proc_info(addr);
  if( !pi || !line || line->addr < pi->start || line->addr > pi->end )
    return NULL;
  return line;
}

const char *
RX_addr_to_source(rx_addr addr)
{
  if ( !ei ) return NULL;
  RX_Sym_Info* const pi = RX_proc_info(addr);
  if( !pi ) return "";
  RString str(100);
  str = "";
  Line_Info* const line = (Line_Info*)
    bSearchA((void**)lInfo,sizeof(Line_Info),lineTableSize,addr);
  if( !line || line->addr < pi->start || line->addr > pi->end )
    {
      const uint32_t proc_offset_bytes = addr - pi->start;
      const int proc_offset = 
        addr > pi->end && proc_offset_bytes > 0x20
        ? 0 : proc_offset_bytes >> 2;

      str = pi->func_name_demangled;
      if( proc_offset ) str.sprintf("+%d",proc_offset);
      if( *pi->source_name )
        {
          str += " ";
          str += pi->source_name;
        }
    }
  else
    {
      const uint32_t line_offset_bytes = addr - line->addr;
      const int line_offset = line_offset_bytes >> 2;
      str.sprintf(".LLM%d", line->llmno);
      if( line_offset ) str.sprintf("+%d",line_offset);
      str += "  ";
      str += pi->func_name_demangled;
      str.sprintf("  %s:%d",
                  line->source_file_name
                  ? line->source_file_name : pi->source_name,
                  line->lineno);
    }
  return str.remove();
}

/*
   There were problems using memory-mapped file IO with shade.
   (Segmentation faults when shade called malloc.)
*/

#define MMAP(size,offset) mmaprep(size,offset,ei->me_fd)
#define MMAPB(size,offset,buffer) mmaprep(size,offset,ei->me_fd,(char*)buffer)
static char *
mmaprep(int size, int offset, int me_fd, char *buffer = NULL)
{
  char *temp = buffer ? buffer : (char*) malloc(size);
  if( temp == NULL ) fatal("Could not allocate storage for mmaprep.");
  lseek(me_fd,offset,SEEK_SET);
  if( read(me_fd,temp,size) == -1 )
    fatal("Could not read file because: %s\n",strerror(errno));
  return temp;
}

static bool is_digit(char c){ return c >= '0' && c <= '9'; }

static
char* 
demangle_itanium(const char *mangled)
{
  // Note: The Itanium ABI defines the format, but its similar to the
  // one being used by g++.
  RString demangled("");
  // I don't know what I'm doing.  DMK  5 February 2008, 18:30:20 CST
  const char *s = mangled;
  if ( *s++ != '_' || *s++ != 'Z' ) return strdup(mangled);
  static bool inited = false;
  static std::map<string,const char*> abbrevs;
  static std::map<string,const char*>::iterator no_abbrev;
  if ( !inited )
    {
      abbrevs["Ss"] = "std::basic_string";
      abbrevs["Sa"] = "std::allocator";
      abbrevs["St"] = "std";
      abbrevs["Sb"] = "std::basic_string";
      abbrevs["Si"] = "std::basic_istream";
      abbrevs["So"] = "std::basic_ostream";
      abbrevs["Sd"] = "std::basic_iostream";
      abbrevs["C0"] = "constructor";

      // Operator Abbreviations
      abbrevs["nw"] = "new";
      abbrevs["na"] = "new[]";
      abbrevs["dl"] = "delete";
      abbrevs["da"] = "delete[]";
      abbrevs["ps"] = "+";
      abbrevs["ng"] = "-";
      abbrevs["ad"] = "&";
      abbrevs["de"] = "*";
      abbrevs["co"] = "~";
      abbrevs["pl"] = "+";
      abbrevs["mi"] = "-";
      abbrevs["ml"] = "*";
      abbrevs["dv"] = "/";
      abbrevs["rm"] = "%";
      abbrevs["an"] = "&";
      abbrevs["or"] = "|";
      abbrevs["eo"] = "^";
      abbrevs["aS"] = "=";
      abbrevs["pL"] = "+=";
      abbrevs["mI"] = "-=";
      abbrevs["mL"] = "*=";
      abbrevs["dV"] = "/=";
      abbrevs["rM"] = "%=";
      abbrevs["aN"] = "&=";
      abbrevs["oR"] = "|=";
      abbrevs["eO"] = "^=";
      abbrevs["ls"] = "<<";
      abbrevs["rs"] = ">>";
      abbrevs["lS"] = "<<=";
      abbrevs["rS"] = ">>=";
      abbrevs["eq"] = "==";
      abbrevs["ne"] = "!=";
      abbrevs["lt"] = "<";
      abbrevs["gt"] = ">";
      abbrevs["le"] = "<=";
      abbrevs["ge"] = ">=";
      abbrevs["nt"] = "!";
      abbrevs["aa"] = "&&";
      abbrevs["oo"] = "||";
      abbrevs["pp"] = "++";
      abbrevs["mm"] = "--";
      abbrevs["cm"] = ",";
      abbrevs["pm"] = "->*";
      abbrevs["pt"] = "->";
      abbrevs["cl"] = "()";
      abbrevs["ix"] = "[]";
      abbrevs["qu"] = "?";
      abbrevs["st"] = "sizeof (a type)";
      abbrevs["sz"] = "sizeof (an expression)";

      no_abbrev = abbrevs.end();
      inited = true;
    }
  const bool nested = *s == 'N';
  if ( nested ) s++;
  int component = 0;
  while ( *s )
    {
      if ( component && !nested || *s == 'E' ) break;
      if ( component++ ) demangled += "::";

      if ( is_digit(*s) )
        {
          int len = 0;
          while ( is_digit( *s ) )
            {
              len = 10 * len + *s - '0';
              s++;
            }
          while ( len-- > 0 ) demangled += *s++;
          continue;
        }

      string twochar(s,2);
      std::map<string,const char*>::iterator iter = abbrevs.find(twochar);
      if ( iter != no_abbrev )
        {
          demangled += iter->second;
          s += 2;
        }
      else
        {
          switch( *s++ ){
          case 'D': s++; break;
          default: break;
          }
        }
    }

  return demangled.remove();
}

static
const char* 
demangle_sun(const char *mangled)
{
  RString demangled("");
  const char *s = mangled;
  const char* const s_end = s + strlen(s);
  if ( *s++ != '_' || *s++ != '_' || *s++ !='1' ) return mangled;
  // I don't know what I'm doing.  DMK  5 February 2008, 18:30:20 CST

  const char czero = *s++;
  const char cone = *s++;
  if ( false ) printf("czero %c cone %c\n",czero,cone);

  RString second_part("");

  while ( const int c = *s )
    {
      if ( is_digit(c) ) break;
      const int offset = c - 'A';
      const char* const maybe_6 = s + offset + 1;
      if ( offset < 26 && maybe_6 < s_end && is_digit(*maybe_6) )
        {
          second_part.assign_substring(s+1,maybe_6);
          s = maybe_6;
          break;
        }
      demangled += *s++;
    }

  RString first_part(demangled);

  if ( *s == '2' )
    {
      s++;
#define C(let,text) case let: second_part = text; break;
      switch ( *s++ ){
        C('a',"&");
        C('b',"<");
        C('c',"~");
        C('e',"==");
        C('k',"delete");
        C('v',"void");
      case 't': second_part = first_part; break;
        C('A',"&=");
        C('B',">=");
        C('C',"!");
        C('D',"-=");
        C('E',"!=");
        C('F',"[]");
        C('G',"=");
        C('H',"<=");
        C('I',"--");
        C('J',"||");
        C('K',"delete[]");
        C('L',"<<=");
        C('M',"%=");
        C('N',"new");
        C('O',"|=");
        C('P',"*=");
        C('Q',"/=");
        C('R',">>=");
        C('S',"+=");
        C('T',"~");
      default: second_part.sprintf("??%c??",s[-1]); break;
      }
#undef C
    }
  if ( second_part.len() )
    demangled.sprintf("::%s",second_part.s);
  return demangled.remove();
}

static
const char* 
demangle(const char *mangled)
{
  const char* const s = mangled;
  if ( s[0] != '_' ) return mangled;
  if ( s[1] == 'Z' ) return demangle_itanium(mangled);
  if ( s[1] != '_' ) return mangled;
  if ( s[2] == '1' ) return demangle_sun(mangled);
  return mangled;
}

bool
RX_Executable_Info::get_text(rx_addr addr, uint32_t &text_host_order)
{
  RX_Image_Text_Block* const tb = text_blocks.get_ge(addr);
  if ( !tb ) return false;
  if ( addr < tb->block_start_pc || addr >= tb->block_end_pc ) return false;
  file_seek( tb->block_start_file_offset + addr - tb->block_start_pc );
  text_host_order = file_read_uint32();
  return true;
}

bool
RX_get_text(rx_addr addr, uint32_t &text_host_order)
{
  if ( !ei ) return false;
  return ei->get_text(addr,text_host_order);
}

int
RX_init(const char *moi)
{
  struct nlist *ste = NULL;
  char *str=NULL,*thisDir;
  int symTsize = 0;
  FILE *me;
  static int inited = 0;

  if( inited ) fatal("readexinit called twice.");
  inited = 1;

  ei = new RX_Executable_Info;

  me=fopen(moi,"rb");
  if(!me) return 0;

  ei->me_fd = fileno(me);

  {
    char *eos;
    thisDir=strdup(moi);
    eos=thisDir+strlen(thisDir);
    while(--eos>thisDir)if(*eos=='/')break;
    *eos=0;
  }

    ReadArray(me,&exec_header,1,Elf32_Ehdr);

    const Elf32_Half e_type = B2(exec_header.e_type);
    const bool core = e_type == ET_CORE;

    if ( e_type != ET_EXEC && e_type != ET_CORE )
      fatal("Unexpected object file format.");
    Elf32_Half e_machine = B2(exec_header.e_machine);
    if( e_machine != EM_SPARC &&
        e_machine != EM_SPARC32PLUS &&
        e_machine != EM_SPARCV9 )
      fatal("Unexpected object file format.");

    int header_count     = B2(exec_header.e_shnum);
    int header_size      = B2(exec_header.e_shentsize);
    int string_table_ind = B2(exec_header.e_shstrndx);

    const Elf32_Half e_phnum = B2(exec_header.e_phnum);

    if( exec_header.e_shoff == 0 )fatal("Section header missing.");

    Elf32_Shdr *sec_header = (Elf32_Shdr*)
      MMAP(header_size*header_count, BF(exec_header.e_shoff));

    if( sec_header == MAP_FAILED )
      fatal("Could not map object file, code %d.", errno);

    Elf32_Shdr *str_sec_header = &sec_header[string_table_ind];
    if( B4(str_sec_header->sh_type) != SHT_STRTAB )
      fatal("Unexpected file format.");

    char *f_strtab =
      MMAP(B4(str_sec_header->sh_size),BF(str_sec_header->sh_offset));
    if( f_strtab == MAP_FAILED )fatal("Could not map string table.");

    char* const zero_length_string = strdup("");

    for(int i=0; i<header_count; i++){
      char *sec_name = &f_strtab[B4(sec_header[i].sh_name)];
      Elf32_Off sh_offset = BF(sec_header[i].sh_offset);
      Elf32_Word sh_size = B4(sec_header[i].sh_size);
      if( !strcmp(sec_name ,".stab") ){
        ste = (struct nlist*) MMAP(sh_size, sh_offset);
        symTsize = sh_size / B4(sec_header[i].sh_entsize);
      } else if( !strcmp(sec_name,".debug_line") ) {
        ei->sec_debug_line_offset = sh_offset;
        ei->sec_debug_line_size = sh_size;
      } else if( !strcmp(sec_name,".stabstr") ) {
        str = (char*) MMAP(sh_size, sh_offset);
      } else if( !strcmp(sec_name,".symtab") )
        while ( true ) {
          const int sidx = i + 1;
          Elf32_Sym* const sym = (Elf32_Sym*) MMAP(sh_size, sh_offset);
          const int st_num = sh_size / B4(sec_header[i].sh_entsize);
          const rx_addr sh_addr = B4(sec_header[i].sh_addr);

          const Elf32_Word str_sh_type = B4(sec_header[sidx].sh_type);
          if ( str_sh_type != SHT_STRTAB ) break;
          const Elf32_Off str_sh_offset = BF(sec_header[sidx].sh_offset);
          const Elf32_Word str_sh_size = B4(sec_header[sidx].sh_size);
          char* const sym_str = (char*) MMAP(str_sh_size, str_sh_offset);

          // Guess. Another guess: Look for symbol _START_.
          const rx_addr base_addr = sh_addr >= 0x40000000 ? sh_addr : 0;
          char *curr_file = zero_length_string;

          int stype_file_last_idx = 0;
          for ( int i=0; i<st_num; i++)
            if ( ELF32_ST_TYPE(sym[i].st_info) == STT_FILE )
              stype_file_last_idx = i;
                    
          for ( int i=0; i<st_num; i++)
              {
                const rx_addr start = B4(sym[i].st_value) + base_addr;
                const rx_addr end = start + B4(sym[i].st_size);
                char* const name = &sym_str[ B4(sym[i].st_name) ];
                const char stype = ELF32_ST_TYPE(sym[i].st_info);
                if ( stype == STT_FILE )
                  {
                    curr_file = strdup(name);
                    continue;
                  }
                if ( stype != STT_FUNC && stype != STT_OBJECT ) continue;
                if ( sym[i].st_size == 0 && stype != STT_FUNC ) continue;

                if ( i > stype_file_last_idx + 10 )
                  curr_file = zero_length_string;

                if(0)printf("%3d Adding func. %-20s: %2d  %4d 0x%8x 0x%8x\n",
                            i, name,
                            ELF32_ST_BIND(sym[i].st_info),
                            B2(sym[i].st_shndx),
                            (unsigned)start,(unsigned)end);

                RX_Sym_Info* const pfi = new RX_Sym_Info;

                bzero(pfi,sizeof(*pfi));
                pfi->start = start;
                pfi->end = end;
                pfi->func_name = strdup( name );
                pfi->func_name_demangled = demangle( pfi->func_name );
                pfi->source_name = curr_file;
                pfi->elf_type = stype;

                if ( stype == STT_FUNC )
                  {
                    ei->proc_by_addr.insert(start,pfi);
                    ei->proc_by_name.insert(pfi->func_name,pfi);
                  }
                ei->sym_by_addr.insert(start,pfi);
                ei->sym_by_name.insert(pfi->func_name,pfi);
              }
          break;
      }

      if ( core ) continue;

      const uint32_t flags = B4(sec_header[i].sh_flags);
      if ( flags & SHF_EXECINSTR )
        {
          const rx_addr block_start_pc = B4(sec_header[i].sh_addr);
          const rx_addr block_end_pc = block_start_pc + sh_size;
          RX_Image_Text_Block* const tb = new RX_Image_Text_Block;
          ei->text_blocks.insert(block_end_pc-1,tb);
          tb->block_start_pc = block_start_pc;
          tb->block_end_pc = block_end_pc;
          tb->block_start_file_offset = sh_offset;
        }
    }

    Elf32_Phdr* const ptable =
      (Elf32_Phdr*) MMAP(e_phnum * sizeof(Elf32_Phdr), BF(exec_header.e_phoff));
    if ( ptable == MAP_FAILED )
      fatal("Could not map program header because %s",strerror(errno));

    if ( core )
      for ( int i = 0;  i < e_phnum; i++ )
        {
          const Elf32_Word seg_type = B4(ptable[i].p_type);
          const uint32_t flags = B4(ptable[i].p_flags);
          const rx_addr base = (rx_addr) B4(ptable[i].p_vaddr);
          const int32_t file_size = B4(ptable[i].p_filesz);
          const int32_t mem_size = B4(ptable[i].p_memsz);
          const Elf32_Off offset = BF(ptable[i].p_offset);

          if ( seg_type != PT_LOAD ) continue;
          if ( mem_size == 0 ) continue;
          if ( ! ( flags & PF_X ) ) continue;
          if ( !offset ) continue;

          const rx_addr block_end_pc = base + file_size;
          RX_Image_Text_Block* const tb = new RX_Image_Text_Block;
          ei->text_blocks.insert(block_end_pc-1,tb);
          tb->block_start_pc = base;
          tb->block_end_pc = block_end_pc;
          tb->block_start_file_offset = offset;
        }

    if ( ei->sym_by_name.occ() == 0 ) return 0;

    const bool use_dwarf = ei->sec_debug_line_size;
    if( use_dwarf ) ei->load_info_dwarf();

    if(0)
      {
        int nf=0;
        int start=0;
        char *this_tab=NULL,*next_tab=str;
        for(int i=start; nf<40 && i<symTsize; i++){
          if( ste[i].n_type == 0 ){
            this_tab = next_tab; next_tab += B4(ste[i].n_value);}
          if( ste[i].n_type != N_FUN && ste[i].n_type != N_SO
              && ste[i].n_type != N_SOL
              && ste[i].n_type != N_EINCL
              && ste[i].n_type != N_SLINE
              && ste[i].n_type != N_EXCL
              && ste[i].n_type != N_BINCL
              )continue;
          nf++;
          printf("%5d %6d %-20s Type: %3x  Desc. %u  Value: %x\n"
                 ,i
                 ,ste[i].n_un.n_strx
                 ,(ste[i].n_un.n_strx==0) ? "NULL" : &this_tab[ste[i].n_un.n_strx]
                 ,(unsigned int)ste[i].n_type
                 ,(unsigned int)ste[i].n_desc
                 ,ste[i].n_value);}}

  if( !use_dwarf && ste ) {
    rx_addr addr_base = 0;
    char *sourceDir = NULL, *currSource = NULL;
    int stab_68_count = -1;
    char *str_tab = str, *next_tab = str;
    char temp[260];
    File_Info *fi=NULL, *flast=NULL;
    Line_Info *lfi=NULL,*lflast=NULL;

    for (int i=0; i<symTsize; i++)
      {
        int ntype = ste[i].n_type;
        switch( ntype ) {
        case 0:
          str_tab = next_tab; next_tab += B4(ste[i].n_value);
          break;
        case N_SO:
          stab_68_count = 0;
        case N_SOL:
          {
            char *dirMaybe = strdup(&str_tab[B4(ste[i].n_un.n_strx)]);

            if( dirMaybe[ strlen(dirMaybe)-1 ] == '/' ){
              if( !strcmp(dirMaybe,thisDir) )
                sourceDir = dirMaybe; else sourceDir = NULL;
              break;
            }

            temp[0] = 0;
            if( sourceDir )
              { strcpy(temp,sourceDir); free(sourceDir); sourceDir = NULL; }
            strcat(temp,dirMaybe); currSource = strdup(temp);

            if( ntype == N_SO ) {
              if( !fi || fi->start ){NEXT(fInfo,fi,flast,File_Info);}
              fi->start = fi->end = 0;
              fi->file = currSource;
            }
          }
          break;

        case N_FUN:
          if( B4(ste[i].n_un.n_strx) ){
            char *tp;
            strcpy(temp,&str_tab[B4(ste[i].n_un.n_strx)]);
            for(tp=temp+strlen(temp);tp-->temp;)if(*tp==':'){*tp=0;break;}

            RX_Sym_Info* const pi = ei->proc_by_name.get_eq(temp);
            if( pi == NULL ) break;
            pi->source_name = currSource;
            addr_base = pi->start;
            if( !fi->start ) fi->start = addr_base;
          }
          break;

        case N_SLINE:
          stab_68_count++;
          NEXT(lInfo,lfi,lflast,Line_Info);
          lfi->addr = B4(ste[i].n_value) + addr_base;
          lfi->lineno = (int)B2(ste[i].n_desc);
          lfi->llmno = stab_68_count;
          lfi->source_file_name = currSource;
          break;

        default:
          break;
        }
      }

    if( fi ) {if( !fi->start ) fi--; fi->end = rx_text_range.end;}

    LAST(lInfo,lfi,lflast,lineTableSize,Line_Info);
    LAST(fInfo,fi,flast,file_table_size,File_Info);

  }

  qsort(lInfo, lineTableSize, sizeof(Line_Info), (Comp_Func)li_comp_addr);
  qsort(fInfo, file_table_size, sizeof(File_Info), (Comp_Func)fi_comp_addr);
  for ( int i=0; i<file_table_size-1; i++ ) fInfo[i].end = fInfo[i+1].start;
  qsort(fInfo, file_table_size, sizeof(File_Info), (Comp_Func)fi_comp);

  /*
    {int nf=0;
    for(i=0; nf<20 && i<symTsize; i++){
    if(ste[i].n_type!=N_FUN &&
    ste[i].n_type!=N_SO)continue;
    nf++;
    printf("%5d %-20s Type: %3x  Desc. %u  Value: %x\n"
    ,i
    ,(((ste[i])).n_un.n_strx==0)?"NULL":str+((ste[i])).n_un.n_strx
    ,(unsigned int)(((ste[i])).n_type)
    ,(unsigned int)((ste[i])).n_desc
    ,((ste[i])).n_value);}}  */

  if(ste)free(ste);
  noProc_Info.start=0;
  noProc_Info.source_name="Unavailable.";
  noProc_Info.func_name="Unavailable.";
  noProc_InfoPtr=&noProc_Info;

  return 1;
}


#ifdef MAIN
int main(int argv, char **argc)
  {
    char *exfile;
    int proccount = 10;
    int filecount = 5;
    void *symaddr = NULL;

    if( argv == 1 ) {
      exfile = "rx";
    } else {
      if ( argv > 4 ) fatal("Usage: %s [executable_file] [addr] [count]",argc[0]);
      exfile = argc[1];
      if( argv > 2 ) symaddr = (void*) strtol(argc[2],NULL,0);
      if( argv > 3 ) proccount = atoi(argc[3]);
      if( proccount < 0 ) proccount = 0xeffffffe;

    }

    RX_init(exfile);

    printf("Found %d named symbol table entries.\n",ssym_count);
    printf("Found %d lines, %d named procs, %d files.\n",
	   lineTableSize, procTableSize, file_table_size);

    if( proccount || filecount )
      {
	int i,j;
	char spcs[80],*spaces = &spcs[40];

	for(i=0;i<40;i++)spcs[i]=32;
	for(;i<79;i++)spcs[i]=0;
	for(i=0; i<file_table_size && i < filecount; i++)
	  printf("File %-20s, 0x%x 0x%x\n",
		 fInfo[i].file,
		 (unsigned)fInfo[i].start,
		 (unsigned)fInfo[i].end);
	for(i=0; i<procTableSize && i < proccount; i++)
	  printf("Proc %s:%s, %s 0x%x 0x%x\n",
		 pInfo[i].source_name,
		 pInfo[i].func_name,
		 &spaces[ j = -30 + strlen(pInfo[i].source_name) +
                          strlen(pInfo[i].func_name)],
		 (unsigned)pInfo[i].start,
		 (unsigned)pInfo[i].end);
      }

    {
      int count=0;
      int i;
      for(i=0; count<10 && i<ssym_count; i++,count++)
	{
	  printf("%30s, %d, %3d, 0x%x\n",
		 &sym_str[ B4(ssym[i]->st_name) ],
		 ELF32_ST_TYPE(ssym[i]->st_info),
		 B4(ssym[i]->st_size),
		 (unsigned)B4(ssym[i]->st_value));
	}
      {
        int i;
        int ecount = 0;
        for(i=0; i<ssym_count; i++)
          {
            char *addr =
              ((char*)B4(ssym[i]->st_value))
              + 0 * ( random() % B4(ssym[i]->st_size) );
            Elf32_Sym *s = RX_sym_at_addr(addr);
            if( !s )
              {
                ecount++;
                if( ecount < 10 )
                  printf("Could not find symbol %s at 0x%x (really 0x%x)\n",
                         &sym_str[ B4(ssym[i]->st_name) ],
                         (unsigned)B4(ssym[i]->st_value), (unsigned)addr);
              }
            else if ( B4(ssym[i]->st_size) > 0 &&
                     strcmp( &sym_str[ B4(ssym[i]->st_name)],
                                  &sym_str[ B4(s->st_name)]) )
                {
                  ecount++;
                  if( ecount < 10 )
                    printf("Address duplicated: %s %s\n",
                           &sym_str[ B4(ssym[i]->st_name)],
                           &sym_str[ B4(s->st_name)]);
                }
          }
      }

      {
	Elf32_Sym *s = RX_find_sym("val");
	if( s == NULL ) printf("Could not find symbol.\n");
	else
	  printf("%30s, %d, %3d, 0x%x\n",
		 &sym_str[ B4(s->st_name) ],
		 ELF32_ST_TYPE(s->st_info),
		 B4(s->st_size),
		 (unsigned)B4(s->st_value));
      }
    }

    if( symaddr )
      {
        char *src = RX_addr_to_source(symaddr);
        Elf32_Sym *sym = RX_sym_at_addr(symaddr);
        char *src_expr = RX_addr_to_sym_expr(symaddr,4);

        printf("For address 0x%x:\n",(unsigned)symaddr);
        if(src)printf(" Source: %s\n",src);
        if(sym)
	  printf("%30s, %d, %3d, 0x%x\n",
		 &sym_str[ B4(sym->st_name) ],
		 ELF32_ST_TYPE(sym->st_info),
		 B4(sym->st_size),
		 (unsigned)B4(sym->st_value));

        if( src_expr )
          printf("Symbol expr: %s\n",src_expr);

      }

    return 0;
  }
#endif
