/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2004 Louisiana State University

// $Id$

#if !defined(rti_all_h) || defined(RTI_REG)

  #define rti_all_h

  #ifdef RTI_REG

    #include "rti.h"

    #undef RTI
    #define RTI(var,type,def,vmin,vmax,doc) \
      {rti(&var,#var,#type,vmin,vmax,doc);};

 void rti_register()
 {
  #else

    #undef RTI
    #define RTI(name,type,def,vmin,vmax,doc) type name = def;

  #endif

  #include "rti_declarations.h"

  #ifdef RTI_REG
}
  #endif

  #undef RTI
  #define RTI(name,type,def,vmin,vmax,doc) type name;

#else

  #error This file should only be included in see.cc

#endif
