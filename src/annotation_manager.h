/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2010 Louisiana State University

// $Id$

#ifndef ANNOTATION_MANAGER_H
#define ANNOTATION_MANAGER_H

#include <gtk/gtk.h>
#include "pse.h"
#include "pse_window.h"
#include "dataset.h"

static const int group_depth_limit = 10;

enum Annotation_ID_Reserved {
  AID_Instruction_Message,
  AID_Partition_Message,
  AID_Squash_Group_Message,
  AID_ID_First_Available = 10
};

enum Annotation_Target {  // That which is being annotated.
  AT_Unknown,
  AT_Instruction,  // Dynamic Instruction
  AT_Partition,
  AT_Squash_Group,
  AT_Time
};

class Annotation_Definition;

struct Annotation_Group_Info {
  Annotation_Group_Info(const char* name):
    name(name),definitions(),
    leaf_distance_max(0),
    rti_dont_display(false),
    rti_settings_unknown("")
  {};
  const char* const name;
  PStack<Annotation_Definition*> definitions;

  // Distance to furthest member leaf; 0, all members leaves; 1 most furthest
  // member leaf at distance 1; etc.
  //
  int leaf_distance_max;

  // If true, don't display ('d') in rti file, if false nothing in rti file.
  bool rti_dont_display;
  RString rti_settings_unknown;
  PGtkCheckButtonPtr on_off;
};


////
 /// Class: Annotation_Definition
////
// One object instantiated for each annotation definition encountered
// in the dataset file in file_first_pass. Later, code might set
// members of "p" based upon oav for faster access.
//
class Annotation_Definition {
public:
  Annotation_Definition():enabled(true),oav(6)
  {
    oav.set_empty_rv(NULL);
    groups_disabled_count = 0;
    blink = false;
    memset(&p,0,sizeof(p));
  }
  int id;
  RString name;
  Annotation_Group_Info* group_info[group_depth_limit];
  int groups_disabled_count;
  bool enabled, blink;
  int blink_countdown;
  bool pointed, cursor;  // Pointer or cursor over item with this annotation.
  Annotation_Target target;       // That which is being annotated.
  PGtkEventBoxPtr dialog_row;
  PGtkLabelPtr dialog_row_label;
  PGtkEventBoxPtr letter_plot, letter_assembler, letter_message;
  PGtkLabelPtr letter_plot_label;
  bool location_plot, location_assembler, location_message;
  int use_count_last_repaint_plot;
  int use_count_last_repaint_assembler;
  int use_count_last_repaint_message;

  RString name_for_label;
  void put(const char* attr, const char* value) {oav[attr]=value;}
  const char* get(const char* attr)
  {
    const char *rv;
    if( oav.lookup(attr,rv) ) return rv;
    return NULL;
  }

  // Convenience copies of attribute values and other data.
  struct {
    // Upper case is from dataset file (with minimal processing).
    // Lower case is processed.
    RString Assembler_Markup_Before;
    RString Assembler_Markup_After;
    RString Marker_Color;
    RString Message;
    GdkColor marker_color_gdk;
    bool marker_color_specified;
    int snipmate_indicator_left_attributes;
  } p;

private:
  PString_Hash<const char*> oav;  // Attributes and their values.
};

////
 /// struct:  Annotation_Elt
////
//
// One struct used for each annotation read from the dataset file (in
// prepare_rob_data).
//
struct Annotation_Elt {
  int id;
  int cyc;         // Time, relative to segment start.
  int data_start;  // Index into annotation_data array.
  int data_stop;   // Index into annotation_data array.
  int next;
  void link(int *l, int this_idx){ next = *l;  *l = this_idx; }
};

class Annotation_Segment_Data {
public:
  Annotation_Segment_Data(){
    annotation_definitions_version = 0;
  }
  Auto_Array<PVT_Scalar> annotation_data;
  Auto_Array<Annotation_Elt> annotations;
  int annotation_definitions_version;
};

struct Annotation_PSE_Window_Data {
  PGtkDialogPtr message_window;
  PLabelEventBoxPtr message_large_ebox;
  PLabelEventBoxPtr message_pane_ebox;
  bool message_window_partitions_enabled;
  int message_pane_height;
  PGtkToggleActionPtr message_enable_partitions_action;
};

class Annotation_Manager {
public:
  Annotation_Manager(Dataset *ds):ds(ds)
  {
    preferences_dialog = NULL;
    setup_before_dataset_load();
  }

  void setup_before_dataset_load();
  void definition_read(Dataset_File& df);
  void setup_after_dataset_load();
  void setup_before_pse_window_initialized(PSE_Window *pw);
  void setup_before_pse_window_destroyed(PSE_Window *pw);
private:
  void preferences_dialog_construct(PSE_Window *pw);
  void dialog_launch(PSE_Window* pw, PGtkDialogPtr dialog, int w, int h);
  GtkWidget* message_common_construct(PSE_Window *pw, bool pane);
public:
  void preferences_dialog_launch(PSE_Window *pw);
  void message_window_launch(PSE_Window *pw);
  GtkWidget* message_pane_construct(PSE_Window *pw);
  void rti_file_write_prepare();

  Annotation_Segment_Data* get_sd(Segment_Data *sd)
  {return segment_data[sd->segment_num];}
  Annotation_Segment_Data* allocate_sd(Segment_Data *sd)
  {return segment_data.put(sd->segment_num);}

  Dataset* const ds;
  PString_Hash<enum Annotation_Target> annotation_target_str_to_enum;
  Auto_Array<Annotation_Definition> annotation_definitions;
  PString_Hash<Annotation_Definition*> name_to_definition;
  PSList<Annotation_Definition*,char*> names_sorted;
  PString_Hash<Annotation_Group_Info*> name_to_group;
  int group_largest_depth;
  PStack<char*> rti_group_unknown;

  int annotation_definitions_version; // Incremented on enable/disable.
  Auto_Array<Annotation_Segment_Data> segment_data;
  bool enable_plot_area;

  PGtkDialogPtr preferences_dialog;
  PGtkToggleActionPtr enable_plot_area_action;

  GdkColor *bg_letter_normal;
  Color_Info bg_letter_cursor_and_pointer;
  Color_Info fg_name_disabled;
};

class Annotation_Iterator {
public:
  Annotation_Iterator
  (Dataset* ds, Segment_Data* sd, Insn_Dataset_Info* di):
    ds(ds),am(ds->am),sd(am->get_sd(sd))
  { ae_next = di ? di->annotation_elt : -1;  init(); };
  Annotation_Iterator
  (Dataset* ds, Segment_Data* sd, Insn_Partition_Info* pi):
    ds(ds),am(ds->am),sd(am->get_sd(sd))
  { ae_next = pi ? pi->annotation_elt : -1;  init();} ;

  operator bool () { return ae; }
  Annotation_Elt* operator -> () { return ae; }
  bool operator ++ () { return fwd(); }
  bool operator ++ (int) { Annotation_Elt* const rv = ae; fwd(); return rv; }
  char* sprintf_dup(const char* fmt)
  {return pvt_sprintf_dup(fmt, data, size);}
  Annotation_Elt* fwd()
  {
    while( 1 )
      {
        ae = sd->annotations[ae_next];
        if( !ae ) return NULL;
        ae_next = ae->next;
        if( set() ) return ae;
      }
  }
  Annotation_Definition *def;
  PVT_Scalar *data;
  int size;

private:
  Dataset* const ds;
  Annotation_Manager* const am;
  Annotation_Elt *ae;
  Annotation_Segment_Data* const sd;
  int ae_next;

  void init()
  {
    def = NULL;  data = NULL;  size = 0;
    fwd();
  }

  bool set()
  {
    if( !ae ) return false;
    def = am->annotation_definitions[ae->id];
    ASSERTS( def );
    if( !def ) return false;
    size = ae->data_stop - ae->data_start;
    if( !size ) return true;
    data = sd->annotation_data[ae->data_start];
    ASSERTS( data );
    if( !data ) return false;
    return true;
  }
};

void annotate_button_callback(GtkWidget *w, gpointer pwptr);
void annotate_menu_callback(gpointer pwptr, int action, GtkWidget *w);
void annotate_dialog_launch(PSE_Window* pw);

#endif
