/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2005 Louisiana State University

// $Id$

#ifndef MESSAGES_H
#define MESSAGES_H

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdarg.h>

// Assertion may not be checked in high-performance compilations.
#define ASSERT(cond) ASSERTA(cond)

// Always active.  Can be used for cond with side effect.
#define ASSERTA(cond) \
 if(!(cond))fatal("Assertion failure at %s:%d.",__FILE__,__LINE__)

// ASSERTS used when code can continue despite error.
#ifdef STRICT
#define ASSERTS(cond) \
 if(!(cond))fatal("Strict assertion failure at %s:%d.",__FILE__,__LINE__)
#else
#define ASSERTS(cond)
#endif


void dialog_warning(GtkWidget *w, const char *fmt,...);
int dialog_question(GtkWidget *w, const char *fmt, ...);

void fatal(const char *fmt, ...);
#define FATAL fatal("Internal error");

char *file_browse_dialog(GtkWidget *w, const gchar *dialog_title, 
                         gchar *start_dir, const gchar *file_ext);
#endif
