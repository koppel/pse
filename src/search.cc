/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2014 Louisiana State University

// $Id$

// Search for PC, tag, cycle.

#include <ctype.h>
#include <gdk/gdkkeysyms.h>
#include "search.h"
#include "pse_window.h"
#include "gtk_utility.h"
#include "misc.h"
#include "enames.h"
#include "readex.h"
#include "decode_sparc.h"

RString search_entry_tip_normal;
RString search_entry_tip_invalid;
RString search_entry_tip_no_hits_pc;
RString search_entry_tip_no_hits_tag;
RString search_entry_tip_no_hits_cycle;

struct Search_Call_Back_Data {
  Search_Call_Back_Data(PSE_Window *pw, char action)
    :pw(pw),action(action){};
  PSE_Window* const pw;
  const char action;
};

static void search_segment_index_build(Dataset *ds, Segment_Data *sd);
static gboolean search_dataset_build_idle_handler(gpointer data);
static bool
search_bar_focus_handler(GtkWidget *w, GdkEventFocus *e, gpointer d);
static gboolean search_bar_key_press_event_handler
(GtkWidget *w, GdkEventKey *e, gpointer d);
static gboolean
search_bar_scroll_handler(GtkWidget *w, GdkEventScroll *e, gpointer d);
static gboolean search_bar_change_timeout_handler(gpointer d);
static void search_bar_target_apply(PSE_Window *pw);
static void search_bar_click_handler(GtkWidget *w, gpointer d);


 ///
 /// Search History
 /// 
// Provides history for a GtkEntry widget.  If this is improved
// further move to gtk_utility. (GtkComboBoxEntry can be used instead.)
// DMK 29 September 2005,  9:47:03 CDT

class Entry_History {
public:
  Entry_History(GtkEntry *entry):entry(entry)
  {
    current = base = NULL;
    keep_base = false;
  };
  void current_keep()
  {
    update();
    if( base != current ) return;
    keep_base = true;
    for(PList<RString> *e = base->next; e; e = e->next)
      if( base->data == e->data ) {delete e->remove(); break;}
  }
  void update()
  {
    const char* const ctext = gtk_entry_get_text(entry);
    if( !base ) current = base = new PList<RString>();
    if( current->data == ctext ) return;
    if( keep_base )
      {
        base = base->before_insert();
        keep_base = false;
      }
    base->data = ctext;
    current = base;
  }
  void clear_or_restore()
  {
    const char* const ctext = gtk_entry_get_text(entry);
    const char* cp = ctext;
    while( *cp && isspace(*cp) ) cp++;
    if( *cp )
      {
        gtk_entry_set_text(entry,"");
        update();
      }
    else
      {
        ASSERTS( current == base );
        if( !base ) return;
        base = current->remove(base);
        delete current;
        current = base;
        if( !base ) return;
        gtk_entry_set_text(entry,base->data);
        keep_base = true;
      }
  }
  void newer()
  {
    update();
    if( base != current ) current = current->prev;
    gtk_entry_set_text(entry,current->data);
  }
  void older()
  {
    update();
    if( current->next ) current = current->next;
    gtk_entry_set_text(entry,current->data);
  }

private:
  PGtkEntryPtr entry;
  PList<RString> *base, *current;
  bool keep_base;
};


 ///
 /// Search Initialization and Construction
 ///

void
search_app_init()
{
  const char* const search_entry_tip_examples =
    "Examples: PC, \"0x12c0\";  Tag, \"123,456,789\"; "
    "Cycle, \"c 1,000,000\".";
  search_entry_tip_normal = "Search Target (PC, Tag, or Cycle).  ";
  search_entry_tip_normal += search_entry_tip_examples;
  search_entry_tip_invalid = "Search Target Invalid.  ";
  search_entry_tip_invalid += search_entry_tip_examples;
  search_entry_tip_no_hits_pc = "No Hits for PC Search.  ";
  search_entry_tip_no_hits_pc += search_entry_tip_examples;
  search_entry_tip_no_hits_tag = "No Hits for Tag Search.  ";
  search_entry_tip_no_hits_tag += search_entry_tip_examples;
  search_entry_tip_no_hits_cycle = "No Hits for Cycle Search.  ";
  search_entry_tip_no_hits_cycle += search_entry_tip_examples;
}

void
search_dataset_init(Dataset *ds)
{
  search_app_init();
  ds->search_segment_unindexed = 0;
  g_idle_add(search_dataset_build_idle_handler,gpointer(ds));
}

void
search_window_and_dataset_init(PSE_Window *pw)
{
  pw->search_segment_hit =
    (bool*)calloc(pw->ds->sdata.occ,sizeof(bool)); 
  gtk_widget_show(pw->search_toolbar);
}

void
search_segment_init(Segment_Data *sd)
{
  sd->search_indexed = false;
  sd->search_coverage_pc_tree = NULL;
}

GtkWidget*
toolbar_insert_button
(PGtkToolbarPtr bar, PGtkToolButtonPtr button,
 WCallback handler, gpointer data, const char *tip)
{
  g_signal_connect(button, "clicked", G_CALLBACK(handler), data);
  gtk_toolbar_insert(bar,button,-1);
  if( tip ) gtk_tool_item_set_tooltip(button, GTK_TOOLTIPS(app.tips), tip, tip);
  return button;
}

GtkWidget*
toolbar_insert_stock_icon_toggle
(PGtkToolbarPtr bar, const char *stock_id, WCallback handler, gpointer data,
 bool initial, const char *tip)
{
  PGtkToggleToolButtonPtr item = (GtkWidget*)gtk_toggle_tool_button_new();
  PGtkImagePtr img =
    gtk_image_new_from_stock(stock_id, gtk_toolbar_get_icon_size(bar));
  gtk_tool_button_set_icon_widget(item,img);
  gtk_toggle_tool_button_set_active(item,initial);
  return toolbar_insert_button(bar, item, handler, data, tip);
}

GtkWidget*
toolbar_insert_stock_icon_button
(PGtkToolbarPtr bar, const char *stock_id, WCallback handler,
 gpointer data, const char *tip)
{
  PGtkImagePtr img =
    gtk_image_new_from_stock(stock_id, gtk_toolbar_get_icon_size(bar));
  PGtkToolButtonPtr item = (GtkWidget*)gtk_tool_button_new(img,NULL);
  return toolbar_insert_button(bar, item, handler, data, tip);
}

GtkWidget*
search_bar_construct(PSE_Window *pw)
{
  PGtkToolbarPtr search_toolbar = gtk_toolbar_new();
  pw->search_toolbar = search_toolbar;
  gtk_toolbar_set_tooltips(search_toolbar,true);
  gtk_toolbar_set_style(search_toolbar,GTK_TOOLBAR_ICONS);
  gtk_toolbar_set_icon_size(search_toolbar,GTK_ICON_SIZE_SMALL_TOOLBAR);

  pw->search_change_timeout_id = 0;
  pw->search_pc = PC_NEVER;
  pw->search_tag_or_cycle_absolute = TAG_NEVER;
  pw->search_target_type = STT_Unset;

  pw->search_bttn_clear =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_CLEAR,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'x'),
     "Clear or restore search target.");
  pw->search_bttn_first =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_GOTO_FIRST,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'f'),
     "Move cursor to first match.");
  pw->search_bttn_prev =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_GO_BACK,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'p'),
     "Move cursor to previous match.");

  PGtkToolItemPtr search_tool_item = gtk_tool_item_new();
  gtk_toolbar_insert(search_toolbar,search_tool_item,-1);

  pw->search_entry = gtk_entry_new();
  gtk_container_add(search_tool_item,pw->search_entry);
  gtk_widget_modify_text(pw->search_entry,GTK_STATE_NORMAL,&app.gdk_black);
  gtk_tooltips_set_tip
    (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
  pw->search_entry_history = new Entry_History(pw->search_entry);
  g_signal_connect
    (pw->search_entry,"focus-in-event",G_CALLBACK(search_bar_focus_handler),
     new Search_Call_Back_Data(pw,'i'));
  g_signal_connect
    (pw->search_entry,"focus-out-event",G_CALLBACK(search_bar_focus_handler),
     new Search_Call_Back_Data(pw,'o'));
  g_signal_connect
    (pw->search_entry,"key-press-event",
     G_CALLBACK(search_bar_key_press_event_handler),
     new Search_Call_Back_Data(pw,0));
  g_signal_connect
    (search_toolbar,"scroll-event",
     G_CALLBACK(search_bar_scroll_handler),
     new Search_Call_Back_Data(pw,'s'));

  pw->search_bttn_next =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_GO_FORWARD,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'n'),
     "Move cursor to next match.");
  pw->search_bttn_last =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_GOTO_LAST,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'l'),
     "Move cursor to last match.");
  pw->search_bttn_jump =
    toolbar_insert_stock_icon_button
    (search_toolbar,GTK_STOCK_JUMP_TO,search_bar_click_handler,
     new Search_Call_Back_Data(pw,'j'),
     "Move cursor to first match.");

  gtk_widget_set_sensitive(pw->search_bttn_first,false);
  gtk_widget_set_sensitive(pw->search_bttn_prev,false);
  gtk_widget_set_sensitive(pw->search_bttn_next,false);
  gtk_widget_set_sensitive(pw->search_bttn_last,false);
  gtk_widget_set_sensitive(pw->search_bttn_jump,false);
  return search_toolbar;
}

 ///
 ///  Construct Coverage Index of PCs in Segments
 ///

struct Coverage_PC_Key {
  Coverage_PC_Key(uint32_t pc){ start = pc; end = pc + 4; }
  uint32_t start;
  uint32_t end; // End is just outside of the range.
};

class Coverage_Search_State {
public:
  Coverage_Search_State(uint32_t target):target(target),tnext(target+4)
  {
    match = abut_low = abut_high = NULL;
  }
  int examine(Coverage_PC_Key *key)
  {
    if( target == key->end ) { abut_low = key; if( abut_high ) return 0; }
    if( tnext == key->start ) { abut_high = key; if( abut_low ) return 0; }
    if( target >= key->start && target < key->end ) { match = key; return 0; }
    return target - key->start;
  }
  Coverage_PC_Key *abut_low, *abut_high, *match;
private:
  const uint32_t target, tnext;
};

static gint
cc_comp(gconstpointer a, gconstpointer b)
{
  return ((Coverage_PC_Key*)a)->start - ((Coverage_PC_Key*)b)->start;
}

static gint
cc_search(gconstpointer kp, gconstpointer data)
{
  Coverage_Search_State* const css = (Coverage_Search_State*) data;
  Coverage_PC_Key* const key = (Coverage_PC_Key*) kp;
  return css->examine(key);
}

bool
search_segment_contains_pc(Segment_Data *sd, uint32_t pc)
{
  if( !sd->search_indexed ) return false;
  Coverage_Search_State css(pc);
  return g_tree_search(sd->search_coverage_pc_tree,cc_search,gpointer(&css));
}

bool
search_segment_contains_target_pc(PSE_Window *pw, Segment_Data *sd)
{
  if( pw->search_target_type != STT_PC || pw->search_pc == PC_NEVER )
    return false;
  return search_segment_contains_pc(sd,pw->search_pc);
}

static gboolean
search_dataset_build_idle_handler(gpointer data)
{
  Dataset* const ds = (Dataset*) data;
  if( ds->search_segment_unindexed >= ds->sdata.occ ) return false;
  PSE_Window* const pw = ds->pw_get();
  if( pw->ovr_pointed_segment && !pw->ovr_pointed_segment->search_indexed )
    search_segment_index_build(ds,pw->ovr_pointed_segment);
  else
    search_segment_index_build(ds,ds->sdata[ds->search_segment_unindexed++]);
  return true;
}

struct Search_Insn_Info
{
  Search_Insn_Info():count(0){};
  int count;
  bool isa_call;
};

static void
search_segment_index_build(Dataset *ds, Segment_Data* sd)
{
  if( sd->search_indexed ) return;
  Dataset_File &df = ds->df;
  int* const cmd_dispatch_segment = ds->cmd_dispatch_segment;
  GTree*& gtree = sd->search_coverage_pc_tree;
  gtree = g_tree_new(cc_comp);
  int run_count = 0;  // For tuning and debugging.
  df.reset_position_state();
  df.seek(sd->start_pos);
  DSF_Entry e;
  Auto_Hash<uint32_t> tag_to_pc;

  // Table showing the number of times each static instruction called
  // in this segment. Info is lost after this routine returns. What a
  // shame.
  Auto_Hash<Search_Insn_Info> pc_to_count;

  ds_num start_tag = sd->seg_start_tag;
  uint32_t *pc_ptr = tag_to_pc.put(-1);
  uint32_t* const dummy_ptr = pc_ptr;

  // Scan through this segment of the dataset file and record the number
  // of occurrences of each PC.
  //
  while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END )
    {
      ds_num full_tag;
      switch( cmd_dispatch_segment[e.type] ){

      // Skip over a namespace region, which does not contain information
      // of interest.
      //
      case DSF_CMD_NAMESPACE_STR_BEGIN:
      case DSF_CMD_NAMESPACE_IDX_BEGIN: df.namespace_finish(); continue;

      // An instruction state change.
      //
      case DS_Disp_State_in4: full_tag = e.data.i4; goto SKIP;
      case DS_Disp_State_in8: full_tag = e.data.i8; 
      SKIP:
        {
          // Retrieve the tag (like a serial number) associated with
          // this instruction.
          //
          if( start_tag == -1 ) start_tag = full_tag;
          const int tag = full_tag >= start_tag ? full_tag - start_tag : -1;

          // Use the tag to look up a pointer to where the PC for this
          // dynamic instruction should be written. If this is the
          // first event for this dynamic instruction, then *pc_ptr
          // will be uninitialized. *pc_ptr is written by the
          // DS_Disp_PC event (see below).
          //
          pc_ptr = tag_to_pc.put(tag);

          // If this is the last event for an instruction then record
          // the PC.
          //
          if( e.type == RTRC_STATE_COMMIT && pc_ptr != dummy_ptr ) break;

          continue;
        }

      // Record the pc using pc_ptr which was set by the most recent
      // instruction event (see above).
      //
      case DS_Disp_PC_in4: *pc_ptr = uint32_t(e.data.i4); continue;
      case DS_Disp_PC_in8: *pc_ptr = uint32_t(e.data.i8); continue;
      default: continue;
      }

      const uint32_t pc = *pc_ptr;

      // Retrieve a structure in which information about the static
      // instruction will be stored.
      //
      Search_Insn_Info* const ii = pc_to_count.put(pc);

      if ( !ii->count )
        {
          // If we haven't seen this static instruction before, initialize
          // the "is a procedure call" member.
          uint32_t text;
          ii->isa_call = RX_get_text(pc,text) && decode_insn_isa_call(text);
        }

      // Increment the count of the number of times this instruction
      // has been seen.
      ii->count++;

      // Update a structure which is used to quickly determine whether a
      // PC is within this segment.
      //
      Coverage_Search_State css(pc);
      if( g_tree_search(gtree,cc_search,gpointer(&css)) ) continue;
      if( !css.abut_low && !css.abut_high )
        {
          Coverage_PC_Key* const key = new Coverage_PC_Key(pc);
          g_tree_insert(gtree,key,key);
          run_count++;
        }
      else if( css.abut_low && css.abut_high )
        {
          css.abut_low->end = css.abut_high->end;
          g_tree_remove(gtree,css.abut_high);
          run_count--;
          delete css.abut_high;
        }
      else if( css.abut_low )
        {
          css.abut_low->end += 4;
        }
      else
        {
          css.abut_high->start -= 4;
        }
    }

  sd->search_indexed = true;

  // Find most commonly executed instruction and most commonly
  // executed call instruction.
  //
  int max_count = 0;
  int max_count_pc = 0;
  int max_call_count = 0;
  int max_call_count_pc = 0;
  for( long pc = pc_to_count.iterate_setup(); pc_to_count.iterate_keys(pc); )
    {
      if( !pc ) continue;
      Search_Insn_Info* const ii = pc_to_count[pc];
      if( ii->count > max_count )
        {
          max_count = ii->count;
          max_count_pc = pc;
        }
      if( ii->isa_call && ii->count > max_call_count )
        {
          max_call_count = ii->count;
          max_call_count_pc = pc;
        }
    }

  // Initialize text labels for this segment that describe
  // the most common instruction and call instruction.
  //
  if( max_count_pc )
    {
      sd->source_most_frequent_pc = max_count_pc;
      sd->source_most_frequent_insn.sprintf
        ("%3d times - 0x%x", max_count, max_count_pc);
      const char* const src = RX_addr_to_source(max_count_pc);
      if ( src && *src )
        sd->source_most_frequent_insn.sprintf(": %s",src);        
    }
  if( max_call_count_pc )
    {
      sd->source_most_frequent_call_insn.sprintf
        ("%3d times - 0x%x", max_call_count, max_call_count_pc);
      const char* const src = RX_addr_to_source(max_call_count_pc);
      if ( src && *src )
        sd->source_most_frequent_call_insn.sprintf(": %s",src);
    }
  if(0)printf("Segment %d run count %d. 0x%x %d times\n",
              sd->segment_num,run_count, max_count_pc, max_count);

  // Just in case user has already entered a search term, update
  // the search.
  //
  for(PSE_Windows_Iterator pw(ds->pse_windows);pw;pw++)
    if( pw->plot_is_ovr ) search_bar_target_apply(pw);
}


 ///
 /// Search Bar
 ///

static bool
search_bar_focus_handler(GtkWidget *w, GdkEventFocus *e, gpointer d)
{
  Search_Call_Back_Data* const cbd = (Search_Call_Back_Data*)d;
  PSE_Window* const pw = cbd->pw;
  const bool debug = false;

  switch( cbd->action ){
  case 'i':
    // Focus in.
    gtk_window_remove_accel_group(GTK_WINDOW(pw->main_w),pw->accel_group);
    pw->key_handler_suppress = true;
    // Something after the focus in un-selects the region. DMK 29 Sep 2005
    //  gtk_editable_select_region((GtkEditable*)pw->search_entry.w,0,-1);
    if( debug ) printf("Focus in.\n");
    return false;
  case 'o':
    // Focus out.
    gtk_window_add_accel_group(GTK_WINDOW(pw->main_w),pw->accel_group);
    pw->key_handler_suppress = false;
    if( pw->search_change_timeout_id )
      {
        g_source_remove(pw->search_change_timeout_id);
        pw->search_change_timeout_id = 0;
      }
    search_bar_target_apply(pw);
    if( pw->search_hit_first_segment )
      pw->search_entry_history->current_keep();
    if( debug ) printf("Focus out.\n");
    return false;
  case 'c':
    if( debug ) printf("Clicked.\n");
    return true;
  default:
    return false;
  }
}

static gboolean
search_bar_scroll_handler(GtkWidget *w, GdkEventScroll *e, gpointer d)
{
  PSE_Window* const pw = ((Search_Call_Back_Data*)d)->pw;
  if( pw->search_change_timeout_id )
    g_source_remove(pw->search_change_timeout_id);
  pw->search_change_timeout_id = 0;
  if( e->direction ) pw->search_entry_history->newer();
  else               pw->search_entry_history->older();
  search_bar_target_apply(pw);
  return true;
}

static gboolean
search_bar_key_press_event_handler
(GtkWidget *w, GdkEventKey *event, gpointer d)
{
  Search_Call_Back_Data* const cbd = (Search_Call_Back_Data*)d;
  PSE_Window* const pw = cbd->pw;
  if( pw->search_change_timeout_id )
    g_source_remove(pw->search_change_timeout_id);
  pw->search_change_timeout_id = 0;

  switch( event->keyval ){

  case GDK_Tab:
    search_bar_target_apply(pw);
    if( pw->plot_is_ovr )
      {
        search_pw_cursor_move(pw,SSA_Insn_First_Search_Target);
      }
    else
      {
        search_bttn_sensitivity_set(pw);
        search_pw_cursor_move(pw,SSA_Insn_Next_Search_Target);
      }
    return true;

  case GDK_ISO_Left_Tab:
    search_bar_target_apply(pw);
    if( pw->plot_is_ovr )
      {
        search_pw_cursor_move(pw,SSA_Insn_First_Search_Target);
      }
    else
      {
        search_bttn_sensitivity_set(pw);
        search_pw_cursor_move(pw,SSA_Insn_Prev_Search_Target);
      }
    return true;

  case GDK_Return:
    search_bar_target_apply(pw);
    search_bttn_sensitivity_set(pw);
    if( pw->plot_is_ovr )
      {
        search_pw_cursor_move(pw,SSA_Insn_First_Search_Target);
      }
    else
      {
        search_pw_cursor_move(pw,SSA_Insn_Next_Search_Target);
      }
    return true;

  case GDK_p:
    if( ! ( event->state & GDK_CONTROL_MASK ) ) break;
  case GDK_Up:
    pw->search_entry_history->older();
    search_bar_target_apply(pw);
    return true;
  case GDK_n:
    if( ! ( event->state & GDK_CONTROL_MASK ) ) break;
  case GDK_Down:
    pw->search_entry_history->newer();
    search_bar_target_apply(pw);
    return true;

  default:
    break;
  }

  pw->search_change_timeout_id =
    g_timeout_add(2000,search_bar_change_timeout_handler,cbd);
  gtk_widget_modify_text
    (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_black);
  return false;
}

static gboolean
search_bar_change_timeout_handler(gpointer d)
{
  PSE_Window* const pw = ((Search_Call_Back_Data*)d)->pw;
  pw->search_change_timeout_id = 0;
  search_bar_target_apply(pw);
  return false;
}

void
search_bar_click_handler(GtkWidget *w, gpointer d)
{
  Search_Call_Back_Data* const cbd = (Search_Call_Back_Data*)d;
  PSE_Window* const pw = cbd->pw;

  switch( cbd->action ){
  case 'j':
  case 'f': search_pw_cursor_move(pw,SSA_Insn_First_Search_Target); break;
  case 'p': search_pw_cursor_move(pw,SSA_Insn_Prev_Search_Target); break;
  case 'n': search_pw_cursor_move(pw,SSA_Insn_Next_Search_Target); break;
  case 'l': search_pw_cursor_move(pw,SSA_Insn_Last_Search_Target); break;
  case 'x':
    pw->search_entry_history->clear_or_restore();
    search_bar_target_apply(pw);
    break;

  default: ASSERTS( false );
  }
}

static void
search_bar_target_apply(PSE_Window *pw)
{
  if( !pw->ds->dataset_loaded ) return;
  const char* const entry_text = gtk_entry_get_text(pw->search_entry);
  RString text(g_ascii_strdown(entry_text,-1),true);
  const char* tp = text;
  Search_Target_Type& stt = pw->search_target_type;
  pw->search_hit_ordering = NULL;
  pw->search_tag_or_cycle_absolute = TAG_NEVER;
  pw->search_pc = PC_NEVER;
  pw->search_hit_first_segment = pw->search_hit_last_segment = NULL;

# define WS_SKIP(tp) while( *(tp) && isspace(*(tp)) ) (tp)++;
# define WS_OR_COL_SKIP(tp) \
  if( *(tp) == ':' ) { (tp)++; } else { WS_SKIP(tp); }

  WS_SKIP(tp);

  if( !tp[0] )
    {
      stt = STT_Unset;
    }
  else if( tp[0] == '0' && tp[1] == 'x' )
    {
      tp += 2;
      stt = STT_PC;
    }
  else if( tp[0] == 'c' )
    {
      tp++;
      WS_OR_COL_SKIP(tp);
      stt = STT_Cycle;
    }
  else if( tp[0] == 't' )
    {
      tp++;
      WS_OR_COL_SKIP(tp);
      stt = STT_Tag;
    }
  else if( tp[0] >= '0' && tp[0] <= '9' )
    {
      stt = STT_Tag;
    }
  else
    {
      stt = STT_Invalid;
    }

  if( stt == STT_PC )
    {
      RString num_text("");
      for( char cn = *tp; cn; cn = *++tp )
        if( isxdigit(cn) ) num_text += cn;
        else if( cn != ',' && cn != '_' ) break;
      pw->search_pc = strtoul(num_text,NULL,16);
      if( num_text == "" || pw->search_pc & 0x3 ) stt = STT_Invalid;
    }
  else if( stt == STT_Tag || stt == STT_Cycle )
    {
      RString num_text("");
      for( char cn = *tp; cn; cn = *++tp )
        if( isdigit(cn) ) num_text += cn;
        else if( cn != ',' && cn != '_' ) break;
      pw->search_tag_or_cycle_absolute = atoll(num_text);
      if( num_text == "" ) stt = STT_Invalid;
    }

  WS_SKIP(tp);
  if( *tp ) stt = STT_Invalid;

  Segment_Data** const sdata = pw->ds->sdata_chron;
  const int num_segs = pw->ds->sdata.occ;

  switch( stt ) {
  case STT_Tag:
  case STT_Cycle:
    {
      const ds_num tag_or_cycle = pw->search_tag_or_cycle_absolute;
      const bool tags = stt == STT_Tag;
      for(int i = 0; i < num_segs; i++)
        {
          Segment_Data* const sd = sdata[i];
          const ds_num start = tags ? sd->seg_start_tag : sd->seg_start_ac;
          const ds_num end = tags ? sd->seg_end_tag : sd->seg_end_ac;
          const bool hit = tag_or_cycle >= start && tag_or_cycle <= end;
          pw->search_segment_hit[sd->segment_num] = hit;
          if( hit ) pw->search_hit_first_segment = sd;
        }
    }
    break;

  case STT_PC:
    {
      const uint32_t pc = pw->search_pc;
      for(int i = 0; i < num_segs; i++)
        {
          Segment_Data* const sd = sdata[i];
          const bool hit = search_segment_contains_pc(sd,pc);
          pw->search_segment_hit[sd->segment_num] = hit;
          if( !hit ) continue;
          if( !pw->search_hit_first_segment )
            pw->search_hit_first_segment = sd;
          pw->search_hit_last_segment = sd;
        }
    }
    break;

  case STT_Unset:
  case STT_Invalid:
    pw->search_tag_or_cycle_absolute = TAG_NEVER;
    pw->search_pc = PC_NEVER;
    for(int i = 0; i < num_segs; i++)
      pw->search_segment_hit[sdata[i]->segment_num] = false;
    break;
  }

  if( pw->plot_is_ovr ) search_bttn_sensitivity_set(pw);
  pw->invalidate_draw_area();
}

void
search_at_overview(PSE_Window *pw)
{
  search_bttn_sensitivity_set(pw);
}

bool
search_pw_cursor_move(PSE_Window *pw, Search_Scan_Action action)
{
  if( !pw->ds->dataset_loaded ) return false;
  const Search_Target_Type stt = pw->search_target_type;

  if( pw->search_hit_first_segment )
    pw->search_entry_history->current_keep();

  if( pw->plot_is_ovr )
    {
      if( !pw->search_hit_first_segment ) return false;
      pw->segment_goto(pw->search_hit_first_segment);
      pw->search_hit_ordering = NULL;
      search_bttn_sensitivity_set(pw);
    }

  Insn_Dataset_Info** const idx_to_info = pw->idx_to_info();
  const int idx_max = pw->sd->idx_max;

  if( stt == STT_Tag || stt == STT_Cycle )
    {
      if( pw->search_hit_first_idx == IDX_NEVER ) return false;
      pw->cursor_cmd_goto_idx = pw->search_hit_first_idx;
      pw->cursor_cmd = PSE_Window::CC_Goto;
      pw->cursor_locked = true;
      pw->dis_locked = true;
      pw->invalidate_draw_area();
      return true;
    }

  int& cursor_idx = pw->cursor_idx;
  const bool cursor_relative =
    !( action == SSA_Insn_First_Search_Target ||
       action == SSA_Insn_Last_Search_Target );
  if( cursor_relative && cursor_idx < 0 ) return false;
  Insn_Dataset_Info* const di = idx_to_info[cursor_idx];
  if( cursor_relative && !di ) return false;
  const int32_t pc =
    action == SSA_Insn_Next || action == SSA_Insn_Prev ? di->pc : pw->search_pc;
  const bool dir_fwd =
    action == SSA_Insn_First_Search_Target
    || action == SSA_Insn_Next_Search_Target
    || action == SSA_Insn_Next;
  const int incr = dir_fwd ? 1 : -1;
  const int start_idx =
    action == SSA_Insn_First_Search_Target ? 0 :
    action == SSA_Insn_Last_Search_Target ? idx_max :
    cursor_idx + incr;
  const int limit = dir_fwd ? idx_max + 1 : -1;
  if( start_idx - incr == limit ) return false;
  for( int i = start_idx;  i != limit;  i += incr )
    {
      Insn_Dataset_Info* const dii = idx_to_info[i];
      if( !dii ) continue;
      if( dii->pc != pc ) continue;
      if( dii->doomed ) continue;
      if( i == cursor_idx ) return false;
      pw->cursor_cmd = PSE_Window::CC_Goto;
      pw->cursor_cmd_goto_idx = i;
      pw->cursor_locked = true;
      pw->invalidate_draw_area();
      return true;
    }
  return false;
}

static void
search_bttn_sensitivity_set_(PSE_Window *pw)
{
  const Search_Target_Type stt = pw->search_target_type;

  // Defaults, some changed further below.
  //
  gtk_widget_set_sensitive(pw->search_bttn_first,false);
  gtk_widget_set_sensitive(pw->search_bttn_prev,false);
  gtk_widget_set_sensitive(pw->search_bttn_next,false);
  gtk_widget_set_sensitive(pw->search_bttn_last,false);
  gtk_widget_set_sensitive(pw->search_bttn_jump,false);

  switch( stt ){
  case STT_Invalid:
    gtk_widget_modify_text
      (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_red);
    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,search_entry_tip_invalid,NULL);
    return;
  case STT_Unset:
    gtk_widget_modify_text
      (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_black);
    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
    return;
  default:
    break;
  }

  // Defaults, some changed further below.
  //
  gtk_widget_modify_text
    (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_gray);

  Insn_Dataset_Info** const idx_to_info =
    pw->plot_is_ovr ? NULL : pw->idx_to_info();
  const int idx_max = pw->sd->idx_max;
  const bool ttag = stt == STT_Tag;

  switch( stt ){

  case STT_Tag:
  case STT_Cycle:

    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,
       ttag ? search_entry_tip_no_hits_tag : search_entry_tip_no_hits_cycle,
       NULL);

    if( !pw->search_hit_first_segment ) return;

    if( pw->plot_is_ovr )
      {
        gtk_widget_modify_text
          (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_search_hit.gdkcolor);
        gtk_tooltips_set_tip
          (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
        gtk_widget_set_sensitive(pw->search_bttn_jump,true);
        return;
      }

    if( pw->search_hit_first_segment != pw->sd ) return;

    if( pw->search_hit_ordering != idx_to_info )
      {
        pw->search_hit_ordering = idx_to_info;
        pw->search_hit_first_idx = pw->search_hit_last_idx = IDX_NEVER;
        if( ttag )
          {
            const int rtag =
              pw->sd->tag_absolute_to_relative
              (pw->search_tag_or_cycle_absolute,TAG_NEVER);
            if( rtag == TAG_NEVER ) return;
            Insn_Dataset_Info* const di = pw->sd->tag_to_info[rtag];
            if( !di ) return;
            pw->search_hit_first_idx = pw->info_to_idx(di);
          }
        else
          {
            const int cycle_c = pw->ac_to_c(pw->search_tag_or_cycle_absolute);
            for( int i = 0;  i <= idx_max; i++ )
              {
                Insn_Dataset_Info* const di = idx_to_info[i];
                if( di->rob_enqueue_c < cycle_c ) continue;
                pw->search_hit_first_idx = i;
                break;
              }
          }
      }

    if( pw->search_hit_first_idx == IDX_NEVER ) return;

    gtk_widget_modify_text
      (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_search_hit.gdkcolor);
    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
    gtk_widget_set_sensitive
      (pw->search_bttn_prev, pw->search_hit_first_idx < pw->cursor_idx );
    gtk_widget_set_sensitive
      (pw->search_bttn_next, pw->search_hit_first_idx > pw->cursor_idx );

    return;

  case STT_PC:
    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,search_entry_tip_no_hits_pc,NULL);
    if( pw->plot_is_ovr )
      {
        if( !pw->search_hit_first_segment ) return;
        gtk_widget_modify_text
          (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_search_hit.gdkcolor);
        gtk_tooltips_set_tip
          (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
        gtk_widget_set_sensitive(pw->search_bttn_jump,true);
        return;
      }
    if( pw->search_hit_ordering != idx_to_info )
      {
        pw->search_hit_ordering = idx_to_info;
        pw->search_hit_first_idx = pw->search_hit_last_idx = IDX_NEVER;
        const int32_t pc = pw->search_pc;
        for( int i = 0;  i <= idx_max;  i++ )
          {
            Insn_Dataset_Info* const dii = idx_to_info[i];
            if( !dii ) continue;
            if( dii->pc != pc ) continue;
            if( dii->doomed ) continue;
            pw->search_hit_first_idx = i;
            break;
          }
        for( int i = idx_max;  i >=0;  i-- )
          {
            Insn_Dataset_Info* const dii = idx_to_info[i];
            if( !dii ) continue;
            if( dii->pc != pc ) continue;
            if( dii->doomed ) continue;
            pw->search_hit_last_idx = i;
            break;
          }
      }
    if( pw->search_hit_first_idx == IDX_NEVER ) return;
    gtk_widget_modify_text
      (pw->search_entry,GTK_STATE_NORMAL,&app.gdk_search_hit.gdkcolor);
    gtk_tooltips_set_tip
      (app.tips,pw->search_entry,search_entry_tip_normal,NULL);
    gtk_widget_set_sensitive
      (pw->search_bttn_first, pw->cursor_idx > pw->search_hit_first_idx);
    gtk_widget_set_sensitive
      (pw->search_bttn_prev, pw->cursor_idx > pw->search_hit_first_idx);
    gtk_widget_set_sensitive
      (pw->search_bttn_next, pw->cursor_idx < pw->search_hit_last_idx);
    gtk_widget_set_sensitive
      (pw->search_bttn_last, pw->cursor_idx < pw->search_hit_last_idx);
    break;
  default:
    ASSERTS( false );
  }
}

void
search_bttn_sensitivity_set(PSE_Window *pw)
{
  if ( !pw->ds->dataset_loaded ) return;

  // The buttons are hidden as a bug workaround.
  // See PSE Bug 664 and http://bugzilla.gnome.org/show_bug.cgi?id=56070

  gtk_widget_hide(pw->search_bttn_first);
  gtk_widget_hide(pw->search_bttn_prev);
  gtk_widget_hide(pw->search_bttn_next);
  gtk_widget_hide(pw->search_bttn_last);
  gtk_widget_hide(pw->search_bttn_jump);

  search_bttn_sensitivity_set_(pw);

  if ( pw->plot_is_ovr )
    {
      gtk_widget_show(pw->search_bttn_jump);
    }
  else
    {
      gtk_widget_show(pw->search_bttn_first);
      gtk_widget_show(pw->search_bttn_prev);
      gtk_widget_show(pw->search_bttn_next);
      gtk_widget_show(pw->search_bttn_last);
    }
}
