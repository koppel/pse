/// PSE - A micro-architecture simulation dataset viewer. -*- c++ -*-
/// Copyright (c) 2004 Louisiana State University

// $Id$

// Code for locating and archiving the benchmark files needed to
// display assembly code, dependencies, etc.

// Look for the benchmark and return the path.  Also offer to create and
// move benchmark to archive, if necessary.
char* benchmark_path_get(Dataset *ds);
