/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2015 Louisiana State University

// $Id$

#ifndef PSE_H
#define PSE_H

#include <gtk/gtk.h>
#include "decode_sparc.h"
#include "option_manager.h"
#include "gtk_utility.h"

struct Dataset;
struct PSE_Window;
struct Segment_Data;
class Annotation_Manager;
struct Annotation_PSE_Window_Data;
struct Ovr_Plot_PSE_Window_Data;

struct Color_Info
{
  Color_Info(){ spec = NULL; };
  inline operator bool() { return spec != NULL; }
  inline operator const char*() { return spec; }
  inline operator GdkColor*() { return &gdkcolor; }
  GdkColor gdkcolor;
  const char *spec;
};

struct App
{
public:
  bool gtk_inited;
  Option_Manager option_manager;

  static int const version_major = 0;
  static int const version_minor = 1;
  static int const version_micro = 0;

  PGHookList hooks_after_args;
  PGHookList hooks_after_init;

  // DMK: Need a way of keeping track of multiple datasets. 21 June 2002
  Dataset *ds;

  GdkColormap *cmap;
  GtkTooltips *tips;
  GdkPixbuf *icon_pixbuf;
  GdkCursor *cursor_arrow;
  GdkCursor *cursor_watch;
  GdkCursor *cursor_fleur;
  GdkCursor *cursor_crosshair;
  GdkCursor *cursor_corners;
  GdkCursor *cursor_dis_drag;
  GdkCursor *cursor_insn_follow;

  GdkColor c_visited;
  GdkColor c_ovr_bg_gray_0;
  GdkColor c_ovr_bg_gray_1;
  GdkColor gdk_black, gdk_white, gdk_red, gdk_amber, gdk_purple, gdk_gray;
  GdkColor gdk_blue;
  GdkColor gdk_same_sin, gdk_pointed, gdk_obscure;
  Color_Info c_same_line;
  Color_Info gdk_search_hit;
  GdkColor gdk_color_snip_pointed;
  GdkColor gdk_color_snip_pointed_pred_1, gdk_color_snip_pointed_pred_x;
  GdkColor gdk_color_snip_pointed_succ_1, gdk_color_snip_pointed_succ_x;
  GdkColor gdk_color_snip_separator_in_order, gdk_color_snip_separator_gap;
  GdkColor gdk_color_snip_separator_out_of_order;
  Color_Info c_tear_light, c_tear_dark;
  Color_Info c_ovr_plot_shadow;
  Color_Info c_cursor, c_cursor_locked;
  Color_Info c_dis_outline, c_dis_outline_locked;
  Color_Info c_dis_window, c_dis_window_locked;
  Color_Info c_grid;
  Color_Info c_cursor_unlocked_bg, c_pointer_bg, c_cursor_locked_bg;
  GdkColor c_rob_half_full, c_rob_half_empty;
  Color_Info dependency_color[OR_num];

  char *last_file_loaded_dir;
  class Run_Time_Initialization *run_time_initialization;
  class DS_Extract *ds_extract;

  App();
  ~App();
  void init();
  void init_gui();
  void init_color
  (const char *spec, GdkColor &cc, const char *backup_spec = NULL);
  void init_color(const char *spec, Color_Info &ci);
  bool gui;
};

extern App app;

#endif
