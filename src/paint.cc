/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2015 Louisiana State University

// $Id$

//
// PSE Window Painting
//

#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include "enames.h"
#include "readex.h"
#include "misc.h"
#include "gtk_utility.h"
#include "messages.h"
#include "pse_window.h"
#include "annotation_manager.h"
#include "ovr_plot_manager.h"

void
View::init(Dataset *ds)
{
  start_c = 1;
  valid = 0;
  top_idx = 0;
  pixels_per_inst = 1;
  pixels_per_cycle = pixels_per_inst;
};

class Font_Get_Monospace {
public:
  Font_Get_Monospace():family_is_monospace(),widget(NULL) {}
  void init(GtkWidget *w)
  {
    widget = w;
    layout_l = gtk_widget_create_pango_layout(w,"l");
    layout_m = gtk_widget_create_pango_layout(w,"m");
    // Code based on the line below didn't work for every font. ( 5 August 2005, )
    // if( !pango_font_family_is_monospace(family) )
  }
  bool operator [] (PangoFontDescription* font_desc)
  {
    const char *family = pango_font_description_get_family(font_desc);
    bool known_monospace;
    if( family_is_monospace.lookup(family,known_monospace) ) return known_monospace;
    pango_layout_set_font_description(layout_l,font_desc);
    pango_layout_set_font_description(layout_m,font_desc);
    int width_l, width_m;
    pango_layout_get_pixel_size(layout_l,&width_l,NULL);
    pango_layout_get_pixel_size(layout_m,&width_m,NULL);
    return family_is_monospace[family] = width_l == width_m;
  }
private:
  PString_Hash<bool> family_is_monospace;
  GtkWidget *widget;
  PangoLayout* layout_l, *layout_m;
};
Font_Get_Monospace font_get_monospace;

// Window coordinate kludge thing.
//
// This is too special purpose to put in its own file.

class VPos {
public:
  VPos(){ font = NULL; };
  void init(PSE_Window *sw, GtkWidget *wp, GdkGC *gcp,
            GdkDrawable *pmp, GdkEventExpose *eep)
  {
    si = sw->ds->state_info;
    ps = NULL;  gc = gcp;  y_pt_scale = 2;
    pm = pmp;  ee = eep;  w = wp;
    gdk_window_get_size(wp->window,&width,&height);
    vpos = 0;  hpos = 0;
    black = wp->style->black;
  }
  void init(PSE_Window *sw, GtkWidget *wp, GdkGC *gcp, FILE *psp, int heightp,
            int widthp, int hposp = 0)
  {
    si = sw->ds->state_info;  gc = gcp;
    ps = psp;  height = heightp;  vpos = 0;  hpos = hposp;
    y_pt_scale = 2;   width = widthp;  w = wp;
  }

  ~VPos(){};

  void down_pt(int amt)
  {
    vpos += amt * y_pt_scale;
    if( vpos >= height ) fatal("Moved below bottom.");
    psf("%d %d moveto\n",hpos,ytops(vpos));
  }

  // Set scale so that BOTTOM_YP is at bottom of plot area and TOP_YP
  // is at top.  If BOTTOM_YP < TOP_YP then BOTTOM_YP is within the
  // plot area (plotted at bottom_pos - 1) and TOP_YP is just outside
  // it.  Otherwise, TOP_YP is within plot area (plotted at top_pos)
  // and BOTTOM_YP is just outside it.
  void set_scale_bottom_top(int bottom_yp,int top_yp)
  {
    bottom_y = bottom_yp;
    top_y = top_yp;
    delta_y = double(plot_area_pixel_height) / ( bottom_y - top_y );
    set_scale_dependent_variables();
  }

  // Set scale so that TOP_YA is at top of plotting area and so that
  // point y + 1 is MULTIPLE pixels below y.  If MULTIPLE > 0 then
  // TOP_YA is within plotting area, otherwise it is just above
  // plotting area.  Not tested for negative multiples.
  void set_scale_top_mult(int top_ya,int multiple)
  {
    top_y = top_ya;
    delta_y = multiple;
    bottom_y = top_y + plot_area_pixel_height / multiple;
    set_scale_dependent_variables();
  }

  void set_scale_dependent_variables()
  {
    delta_y_inv = 1.0 / delta_y;
    if( top_y < bottom_y )
      {
        height_y_abs = bottom_y - top_y;
        origin_y = top_y;
        origin_pos = top_pos;
      }
    else
      {
        height_y_abs = top_y - bottom_y;
        origin_y = bottom_y;
        origin_pos = bottom_pos - 1;
      }
  }

  int down_f_height(double pct)
  {
    int amt = (int)(height * pct);
    top_pos = vpos;
    vpos += amt;
    bottom_pos = vpos;
    plot_area_pixel_height = bottom_pos - top_pos;
    if( vpos >= height ) fatal("Moved below bottom.");
    psf("%d %d moveto\n",hpos,ytops(vpos));
    return amt;
  }

  int down_f_remain(double pct, int multiplep = 1)
  {
    int amt = (int)((height-vpos) * pct);

    int multiple = multiplep > amt ? 1 : multiplep;

    amt -= amt % multiple;

    top_pos = vpos;
    vpos += amt;
    bottom_pos = vpos;
    plot_area_pixel_height = bottom_pos - top_pos;

    psf("%d %d moveto\n",hpos,ytops(vpos));

    return amt;
  }

  int ytops(int y){ return height - y;}
  double ytops(double y){ return height - y;}
  int xclip(int x)
  { if( x < 0 ) return 0;  if( x >= width ) return width -1;  return x; }

  int scale_y(int y)
  {
    return  int( ( y - origin_y ) * delta_y ) + origin_pos;
  }

  // Return false if P, when converted to y space, lies outside range
  // [R1Y,R1Y+LEN).  If it lies inside the range return true and if
  // UNSCALED is non-null set *UNSCALED to the interpolated y value
  // for p.
  bool in_range_ypl(int r1y, int p, int len, int *unscaled = NULL)
  {
    if( len == 0 ) return false;
    if( len < 0 ) FATAL;
    if( p < top_pos || p >= bottom_pos ) return false;

    int pya = int ( ( p - origin_pos ) * delta_y_inv );
    int pyb = origin_y + pya - r1y;
    int pyc = pyb % height_y_abs;
    int pyd = pyc < 0 ? height_y_abs + pyc : pyc;

    if( pyd >= len ) return false;
    if( unscaled ) *unscaled = r1y + pyd;
    return true;
  }

  void ps_clip_to_plot_area_start()
  { ps_clip_to_plot_area_save_restore(true); }
  void ps_clip_to_plot_area_end()
  { ps_clip_to_plot_area_save_restore(false); }
private:
  void ps_clip_to_plot_area_save_restore(bool on)
  {
    if ( !ps ) return;
    if ( on )
      {
        fprintf(ps,"gsave newpath\n");
        fprintf(ps," %d %d moveto  %d %d lineto  %d %d lineto  %d %d lineto\n",
                0, 0, width-1, 0, width-1, height, 0, height);
        fprintf(ps,"clip\n");
      }
    else
      {
        fprintf(ps,"grestore\n");
      }
  }
public:
  void rectangle(int st, int x1, int y1, int x2, int y2)
  {
    int y1a = scale_y(y1);
    int y2a = scale_y(y2);

    rectangle_raw(st,x1,min(y1a,y2a),x2,max(y1a,y2a));

    return;
  }
  void rectangle_raw(int st, int x1, int y1, int x2, int y2)
  {
    if( ps )
      {
        psf("st%d ", st);
        psf("%d %d %d %d r\n",xclip(x1),ytops(y1),xclip(x2),ytops(y2));
      }
    else
      {
        gdk_gc_set_foreground(gc,&si[st].gdk_color);
        gdk_draw_rectangle(pm,gc,TRUE, x1, y1, x2 - x1, y2 - y1);
      }
  }

  void rectangle_raw(Color_Info& ci, int x1, int y1, int x2, int y2)
  {
    gdk_gc_set_foreground(gc,ci);
    gdk_draw_rectangle(pm,gc,TRUE, x1, y1, x2 - x1, y2 - y1);
  }

  void gdk_draw_rectangle(GdkDrawable *dr, GdkGC *gc, gint filled,
                          gint x, gint y, gint w, gint h)
  {
    PGDK_Points <5> p(x,y);
    p.l(w);  p.d(h);  p.r(w); p.d(-h);
    gdk_draw_polygon(dr,gc,filled,p,p);
  }

  void gdk_draw_polygon(GdkDrawable *dr, GdkGC *gc, gint filled,
                        GdkPoint *points, int np)
  {
    gdk_draw_lines_or_polygon(dr,gc,points,np,filled);
  }
  void gdk_draw_polygon(GdkGC *gc, gint filled, GdkPoint *points, int np)
  {
    gdk_draw_lines_or_polygon(pm,gc,points,np,filled);
  }
  void gdk_draw_polygon
  (Color_Info &ci, gint filled, GdkPoint *points, int np)
  {
    gdk_gc_set_foreground(gc,ci);
    gdk_draw_lines_or_polygon(pm,gc,points,np,filled);
  }
  void gdk_draw_lines(GdkDrawable *dr, GdkGC *gc, GdkPoint *points, int np)
  {
    gdk_draw_lines_or_polygon(dr,gc,points,np,false);
  }
  void gdk_draw_lines(GdkGC *gc, GdkPoint *points, int np)
  {
    gdk_draw_lines_or_polygon(pm,gc,points,np,false);
  }
  void gdk_draw_lines_or_polygon(GdkDrawable *drawable, GdkGC *gc,
                      GdkPoint *points, int np, bool fill)
  {
    if( !ps )
      {
        if( fill )
          ::gdk_draw_polygon(drawable, gc, true, points, np);
        else
          ::gdk_draw_lines(drawable, gc, points, np);
        return;
      }

    if( np < 2 ) return;

    GdkGCValues values;
    gdk_gc_get_values(gc,&values);
    const double scale = 1.0 / 255.0;
    // red, green, and blue fields did not seem to be set.
    fprintf(ps,"%.3f %.3f %.3f setrgbcolor\n",
            ( values.foreground.pixel >> 16 ) * scale,
            ( ( values.foreground.pixel >> 8 ) & 0xff ) * scale,
            ( values.foreground.pixel & 0xff ) * scale);

    GdkPoint *start = &points[0];
    fprintf(ps,"newpath\n" " %d %d moveto\n", start->x, ytops(start->y));
    for(int i=1; i<np; i++)
      fprintf(ps," %d %d lineto\n", points[i].x, ytops(points[i].y));
    fprintf(ps,fill ? "fill\n" : "stroke\n");
  }

  void printf(GtkWidget *label)
  {
    if( !ps || !GTK_WIDGET_VISIBLE(label) ) return;

    const int bit_mono = 2;
    const int bit_italic = 1;
    const int bit_bold = 0;
    const char* const fonts[] =
      {
        "Helvetica",
        "Helvetica-Bold",
        "Helvetica-Italic",
        "Helvetica-Bold-Italic",
        "Courier",
        "Courier-Bold",
        "Courier-Italic",
        "Courier-Bold-Italic"
      };

    gint lorigin_x, lorigin_y;
    ASSERTA( gtk_widget_translate_coordinates(label,w,0,0,
                                              &lorigin_x,&lorigin_y) );

    PangoLayout *layout = gtk_label_get_layout(GTK_LABEL(label));
    const gchar *text = gtk_label_get_text(GTK_LABEL(label));

    fprintf(ps, " 0 0 0 setrgbcolor\n");

    PangoLayoutIter *iter = pango_layout_get_iter(layout);

    do{
      int baseline_layout = pango_layout_iter_get_baseline(iter);
      double baseline_pos = baseline_layout * 0.001;
      if( baseline_pos > bottom_pos ) break;
      PangoLayoutRun *run = pango_layout_iter_get_run(iter);
      if( !run ) continue;
      PangoItem *item = run->item;
      PangoRectangle ink_rect, logical_rect;
      pango_layout_iter_get_run_extents(iter,&ink_rect,&logical_rect);

      GSList *extra_attr = item->analysis.extra_attrs;
      int limit = 10;

      fprintf(ps, "gsave\n");

      PangoFont *font = item->analysis.font;
      PangoFontDescription *font_desc = pango_font_describe(font);
      bool font_bold = pango_font_description_get_weight(font_desc) > 500;
      const bool font_italic =
        pango_font_description_get_style(font_desc) != PANGO_STYLE_NORMAL;
      const bool font_monospace = font_get_monospace[font_desc];

      fputs("/",ps);
      fputs(fonts[ ( font_bold << bit_bold )
                   + ( font_italic << bit_italic )
                   + ( font_monospace << bit_mono ) ],
            ps );
      fputs(" findfont 12 scalefont setfont\n",ps);
      double dx = lorigin_x + double(logical_rect.x) * 0.001;
      double dy = lorigin_y + baseline_pos;

      while( extra_attr && limit-- > 0)
        {
          PangoAttribute *attr = (PangoAttribute*) extra_attr->data;
          PangoAttrType type = attr->klass->type;

          switch( type ){
          case PANGO_ATTR_WEIGHT:
            ::printf("Found weight.\n");
          break;

          case PANGO_ATTR_BACKGROUND:
            {
              fputs("gsave\n",ps);
              const double scale = 1.0 / 65535.0;
              PangoColor &color = ((PangoAttrColor*) attr)->color;
              fprintf(ps, " %.3f %.3f %.3f setrgbcolor\n",
                      double(color.red) * scale,
                      double(color.green) * scale,
                      double(color.blue) * scale);
              fprintf
                (ps," newpath %.1f %.1f moveto\n",
                 dx, ytops(lorigin_y + 0.001*(logical_rect.y+logical_rect.height)));
              fprintf(ps," %.1f %.1f rlineto\n", .001 * logical_rect.width,0.0);
              fprintf(ps," %.1f %.1f rlineto\n",0.0,.001 * logical_rect.height);
              fprintf(ps," %.1f %.1f rlineto\n",-0.001 * logical_rect.width,0.0);
              fputs(" closepath fill\n",ps);
              fputs("grestore\n",ps);
            }
            break;
          case PANGO_ATTR_FOREGROUND:
            {
              const double scale = 1.0 / 65535.0;
              PangoColor &color = ((PangoAttrColor*) attr)->color;
              fprintf(ps, " %.3f %.3f %.3f setrgbcolor\n",
                      double(color.red) * scale,
                      double(color.green) * scale,
                      double(color.blue) * scale);
            }
            break;

          default:
            ::printf("Found something else.\n");
          }
          extra_attr = extra_attr->next;
        }

      fprintf(ps, " %.1f %.1f moveto\n (", dx, ytops(dy));

      for(int i = item->offset; i < item->offset + item->length; i++)
        putc(text[i],ps);

      fprintf(ps, ") show\n");
      fprintf(ps, "grestore\n");

    }
    while( pango_layout_iter_next_run(iter) );

    pango_layout_iter_free(iter);
  }

  void printf(GtkWidget *label, const char *fmt, ...)
  {
    if( label && !GTK_WIDGET_VISIBLE(label) ) return;

    va_list ap;
    va_start(ap, fmt);
    char *buf = g_strdup_vprintf(fmt,ap);

    if( ps && label )
      {
        gint dx,dy;
        if( !gtk_widget_translate_coordinates(label,w,0,0,&dx,&dy) ) FATAL;
        fprintf(ps,"gsave\n"
                " %d %d moveto\n"
                " 0 0 0 setrgbcolor\n (%s) show\ngrestore\n",
                dx, ytops(dy)-10*y_pt_scale+5, buf);
      }
    else if ( ps )
      {
        fprintf(ps,"gsave\n 0 0 0 setrgbcolor\n (%s) show\ngrestore\n",buf);
      }
    else
      {
        if( label )
          gtk_label_set_text((GtkLabel*)label,buf);
        else
          {
            gdk_gc_set_foreground(gc,&black);
            gdk_draw_string(pm,font,gc,hpos,vpos,buf);
          }
      }

    free(buf);

    va_end(ap);
  }

  void psf(const char *fmt, ...)
  {
    va_list ap;
    va_start(ap, fmt);
    if( ps ) vfprintf(ps,fmt,ap);
    va_end(ap);
  }

  void psprintf(const char *fmt, ...)
  {
    va_list ap;
    va_start(ap, fmt);

    if( ps )
      {
        fprintf(ps,"gsave\n(");
        vfprintf(ps, fmt, ap);
        fprintf(ps, ") show\ngrestore\n");
      }
    va_end(ap);
  }

  void draw_pixmap()
  {
    if( ps ) return;
    gdk_draw_pixmap(w->window, gc, pm,
                    ee->area.x, ee->area.y, ee->area.x, ee->area.y,
                    ee->area.width, ee->area.height);
  }
  void finish_ps()
  {
    if( !ps ) return;
    psf("showpage\n");
  }

  State_Info *si;
  int top_y;                    // Topmost y value.
  int bottom_y;                 // Bottommost y value.
  int origin_y;                 // Origin min(top_y,bottom_y).
  int top_pos;                  // Topmost pixel value in plot area.
  int bottom_pos;               // First pixel below (outside) plot area.
  int origin_pos;
  int plot_area_pixel_height;
  int height_y_abs;
  double delta_y, delta_y_inv;
  GtkWidget *w;
  GdkEventExpose *ee;
  GdkDrawable *pm;
  gint width, height, vpos, hpos, y_pt_scale;
  FILE *ps;
  GdkFont *font;
  GdkGC *gc;
  GdkColor black;
  int clip;
};


gint
display_animation_step(gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  if( !sw->animating ) return false;

  const bool clipped = sw->plot_ped_clipped && sw->plot_segment_as_ped;

  if( sw->animating_insn_motion != clipped )
    {
      sw->animating = false;
      sw->velocity_d = 0.0;
      return 0;
    }

  double now           = time_fp();
  double delta_t       = now - sw->view_update_t;
  double &velocity_d   = sw->velocity_d;
  double &accel        = sw->accel;
  double &view_start_d = sw->view_start_d;
  double &goal_d       = sw->goal_d;
  double previous_d    = view_start_d;

  if( sw->base_accel )
    {
      double &turn_around_t = sw->turn_around_t;
      double &arrival_t = sw->arrival_t;

      if( now < turn_around_t )
        {
          view_start_d += velocity_d * delta_t + 0.5*accel * delta_t * delta_t;
          velocity_d += accel * delta_t;
        }
      else if( now < arrival_t )
        {
          double tta = arrival_t - now;
          view_start_d = goal_d - 0.5 * accel * tta * tta;
          velocity_d = accel * tta;
        }
      else
        {
          view_start_d = goal_d;
          velocity_d = 0;
        }
    }

  const bool forward = view_start_d > previous_d;

  sw->view_update_t = now;

  if( clipped )
    {
      Segment_Data* const sd = sw->sd;
      sw->view.top_idx = (int) view_start_d;
      sw->view.valid = false;
      Insn_Dataset_Info* const di =
        sw->idx_to_info(sd->idx_inrange((int)view_start_d));
      if( sw->ped_plot_idxt )
        {
          sw->view.start_c = di->first_event_c - sw->view.offset_vc;
        }
      else
        {
          const int start_c_maybe = di->rob_dequeue_c - sw->view.offset_vc;
          sw->view.start_c =
            abs( start_c_maybe - sw->view.start_c ) > ( sw->ve.width >> 1 )
            ? start_c_maybe :
            forward ? max(sw->view.start_c,start_c_maybe)
            : min(sw->view.start_c,start_c_maybe);
        }
    }
  else sw->view.start_c = (int)view_start_d;

  if( sw->base_accel )
    {
      if( ( clipped ? sw->view.top_idx == sw->goal_d
            : sw->view.start_c == sw->goal_d )
          && 5 * fabs(sw->velocity_d) < sw->base_accel )
        {
          sw->animating = false;
          sw->velocity_d = 0.0;
        }
    }
  else
    {
      // DMK: Constant velocity code probably stale. 12 July 2002, 17:28:33 CDT
      if( sw->velocity_d > 0 && view_start_d > goal_d
          || sw->velocity_d < 0 && view_start_d < goal_d )
        {
          sw->animating = false;
          sw->velocity_d = 0.0;
        }
    }

  sw->invalidate_draw_area();
  return sw->animating;
}

void
seg_plot_annotations_init(Dataset *ds)
{
  PString_Hash<int> style_to_bitmask;
  style_to_bitmask["hollow"] = 0x8;
  style_to_bitmask["wide"] = 0x10;
  while( Annotation_Definition* const ad =
         ds->am->annotation_definitions.iterate() )
    {
      ad->location_plot = false;
      ad->location_message = false;
      ad->location_assembler = false;
      if( const char* const mc = ad->get("Marker_Color") )
        {
          app.init_color(mc,ad->p.marker_color_gdk,"black");
          ad->p.marker_color_specified = true;
          ad->location_plot = true;
        }
      if( const char* const s = ad->get("Marker_Left_Style") )
        {
          ad->p.snipmate_indicator_left_attributes |= style_to_bitmask[s];
          ad->location_plot = true;
        }
      if( const char* const m = ad->get("Assembler_Markup_Before") )
        {
          ad->p.Assembler_Markup_Before.assign_take(strescape(m));
          ad->location_assembler = true;
        }
      if( const char* const m = ad->get("Assembler_Markup_After") )
        {
          ad->p.Assembler_Markup_After = strescape(m);
          ad->location_assembler = true;
        }
      if( const char* const m = ad->get("Message") )
        {
          ad->p.Message = strescape(m);
          ad->location_message = true;
        }
    }
}

void
seg_plot_annotations_reset(Dataset *ds, PSE_Window *pw, Segment_Data *sd)
{
  Annotation_Manager* const am = ds->am;
  Annotation_PSE_Window_Data* const apwd = pw->annotation_data;
  int& sd_definitions_version = am->get_sd(sd)->annotation_definitions_version;
  if( sd_definitions_version == am->annotation_definitions_version ) return;
  sd_definitions_version = am->annotation_definitions_version;

  while( Insn_Dataset_Info* const di = sd->tag_to_info.iterate() )
    {
      di->seg_plot_annotation_assembly_verified = false;
      di->seg_plot_annotation_markers.reset();
      di->seg_plot_annotation_messages.reset(true);
      RString before_text("");
      RString after_text("");
      PStack<char*> before(NULL);
      PStack<char*> after(NULL);
      for( Annotation_Iterator ae(ds,sd,di); ae; ae++ )
        {
          const bool pa_display =
            ( ae.def->enabled && am->enable_plot_area ) ^ ae.def->blink;
          const bool other_display = ae.def->enabled ^ ae.def->blink;

          if( pa_display && ae.def->p.marker_color_specified )
            di->seg_plot_annotation_markers +=
              Feature(&ae.def->p.marker_color_gdk,true);

          if( !other_display ) continue;

          char* const markup_before = ae.def->p.Assembler_Markup_Before;
          char* const markup_after = ae.def->p.Assembler_Markup_After;
          if( markup_before ) before += ae.sprintf_dup(markup_before);
          if( markup_after )
            if( markup_before )
              after_text.append_take(ae.sprintf_dup(markup_after));
            else
              after += ae.sprintf_dup(markup_after);
          if( char* const m = ae.def->p.Message )
            {
              RStringF a("<span foreground=\"%s\"><i>"
                         "t = %s cyc   tag = %s</i>    --%s</span>\n",
                         ae.def->target == AT_Squash_Group ? "red" : "blue",
                         comma(sd->seg_start_ac+ae->cyc),
                         comma(sd->seg_start_tag+di->tag),
                         ae.def->name.s);
              a.append_take(g_strstrip(ae.sprintf_dup(m)));
              di->seg_plot_annotation_messages.insert(ae->cyc,a.remove());
            }
        }
      while( char* const s = before ) before_text.append_take(s);
      while( char* const s = after ) after_text.append_take(s);
      di->seg_plot_annotation_assembly_before = before_text.remove();
      di->seg_plot_annotation_assembly_after = after_text.remove();
    }

  const bool message_partitions_enabled =
    apwd->message_window_partitions_enabled;

  while( Insn_Partition_Info* const pi =
         sd->partition_sequence_to_info.iterate() )
    {
      pi->seg_plot_annotation_assembly_verified = false;
      pi->seg_plot_annotation_messages.reset(true);
      pi->attributes &= 7;
      RString before_text("");
      RString after_text("");
      PStack<char*> before(NULL);
      for( Annotation_Iterator ae(ds,sd,pi); ae; ae++ )
        {
          if( !( ae.def->enabled ^ ae.def->blink ) ) continue;
          if( char* const b = ae.def->p.Assembler_Markup_Before )
            before += ae.sprintf_dup(b);
          if( char* const a = ae.def->p.Assembler_Markup_After )
            after_text.append_take(ae.sprintf_dup(a));
          if( message_partitions_enabled && ae.def->p.Message )
            {
              RStringF a("<span foreground=\"green3\"><i>"
                         "t = %s cyc   snip serial = %s</i>    --%s</span>\n",
                         comma(sd->seg_start_ac+ae->cyc),
                         comma(pi->snip_serial),
                         ae.def->name.s);
              a.append_take(g_strstrip(ae.sprintf_dup(ae.def->p.Message)));
              pi->seg_plot_annotation_messages.insert(ae->cyc,a.remove());
            }
          pi->attributes |= ae.def->p.snipmate_indicator_left_attributes;
        }
      while( char* const s = before ) before_text.append_take(s);
      pi->seg_plot_annotation_assembly_before = before_text.remove();
      pi->seg_plot_annotation_assembly_after = after_text.remove();
    }
}

#define EA_LINE(addr) ((addr)&ds->targ_line_mask)

void
PSE_Window::paint_seg(GtkWidget *w, GdkEventExpose *ee)
{
  const int ps = w == NULL;
  GdkWindow* const ww = ps ? NULL : w->window;
  Annotation_Manager* const am = ds->am;

  font_get_monospace.init(dis_lines_label);
  seg_plot_annotations_reset(ds,this,sd);

  while(Annotation_Definition* const ad = am->annotation_definitions.iterate())
    {
      ad->pointed = false;
      ad->cursor = false;
      ad->use_count_last_repaint_plot = 0;
      ad->use_count_last_repaint_assembler = 0;
      ad->use_count_last_repaint_message = 0;
    }

  // Handles coordinate transformations and device-independent plotting.
  VPos vpos;

  gint w_height, w_width;
  int redraw_min_p;
  int redraw_max_p;

  GdkWindow *win = draw_area_win;
  gdk_window_get_size(win,&w_width,&w_height);
  GtkStyle *style = NULL;

  // DMK: Need to determine the exact minimum height to avoid an
  // execution error (which ruins the stack making debugging difficult).
  //  1 April 2003, 16:59:23 CST
  if( w_height <= view.pixels_per_inst ) return;

  // Prepare for painting.
  //
  // gtk: Prepare a new pixel map, if possible copying portions of old one.
  // ps:  Open file for PostScript output.
  // Both cases: initialize vpos.

  if( !ps )
    {
      GdkPixmap *pm_next;
      style = gtk_widget_get_style(w);

      ViewEnv ve_next;
      ve_next.sd = sd;
      ve_next.width = w_width;
      ve_next.height = w_height;
      ve_next.plot_ped_clipped = plot_ped_clipped;
      ve_next.plot_segment_as_ped = plot_segment_as_ped;
      ve_next.plot_rob_decode_positioning = plot_rob_decode_positioning;

      if( pm_view == view && ve == ve_next )
        {
          pm_next = NULL;
          redraw_min_p = w_width;  redraw_max_p = w_width;
        }
      else if( ( !plot_segment_as_ped || !plot_ped_clipped )
               && pm_sd == sd
               && ve == ve_next
               && pm_height == w_height
               && pm_pixels_per_cycle == view.pixels_per_cycle
               && ve.plot_rob_decode_positioning == plot_rob_decode_positioning
               && view.top_idx == pm_view.top_idx
               && plot_rob_decode_positioning != PRDP_Fit )
        {
          pm_next = gdk_pixmap_new(win,w_width,w_height,-1);
          gdk_draw_rectangle(pm_next,style->white_gc,1,0,0,w_width,w_height);
          int to_x = ( pm_view_start_c-view.start_c )
            * view.pixels_per_cycle;

          int to_x_clip = to_x < 0 ? 0 : to_x;

          int from_x = to_x_clip - to_x;

          int copy_width_from = pm_width - from_x;
          int copy_width_to = w_width - to_x_clip;
          int copy_width =
            copy_width_from < copy_width_to ? copy_width_from : copy_width_to;

          gdk_draw_pixmap(pm_next,style->white_gc,
                          pm,
                          from_x, segment_plot_top,
                          to_x_clip, segment_plot_top,
                          copy_width, w_height - segment_plot_top);

          if( to_x_clip == 0 )
            {
              redraw_min_p = copy_width;  redraw_max_p = w_width;
            }
          else
            {
              redraw_min_p = 0;  redraw_max_p = to_x_clip;
            }
        }
      else
        {
          pm_next = gdk_pixmap_new(win,w_width,w_height,-1);
          gdk_draw_rectangle(pm_next,style->white_gc,1,0,0,w_width,w_height);
          redraw_min_p = 0;  redraw_max_p = w_width;
        }

      if( pm_next )
        {
          if( pm ) gdk_pixmap_unref(pm);
          pm                  = pm_next;
          pm_width            = w_width;
          pm_height           = w_height;
          pm_pixels_per_cycle = view.pixels_per_cycle;
          pm_view_start_c     = view.start_c;
          pm_sd               = sd;

          pm_view = view;
        }

      ve = ve_next;
      ve.pm_width = pm_width;

      vpos.init(this,w,gc,pm,ee);
    }
  else
    {
      if( !pm ) fatal("Need a pixel map.");
      redraw_min_p = 0;  redraw_max_p = w_width;

      gint pl_height, pl_width; // Plot with labels.
      gint pl_height2, pl_width2;
      gint pd_height, pd_width;
      gdk_window_get_size(plot_vbox->window, &pl_width, &pl_height);
      gdk_window_get_size(main_w->window, &pl_width2, &pl_height2);
      gdk_window_get_size(draw_dis_vpaned.w->window, &pd_width, &pd_height);

      vpos.init(this,draw_area,gc,ps_file_str,w_height,w_width,0);

      GdkRectangle bb;
      bb.x = 0; bb.y = 0;
      bb.width = pd_width;  bb.height = w_height; // [sic]
      while( PGtkLabelPtr l = label_set.iterate() )
        {
          if( !GTK_WIDGET_VISIBLE(l) ) continue;
          gint dx, dy;
          if( !gtk_widget_translate_coordinates(l,plot_vbox,0,0,&dx,&dy) )
            continue;
          PangoRectangle ink_rect, logical_rect;
          pango_layout_get_pixel_extents
            (gtk_label_get_layout(l),&ink_rect,&logical_rect);
          logical_rect.x += dx;
          logical_rect.y += dy;
          gdk_rectangle_union((GdkRectangle*)&logical_rect,&bb,&bb);
        }

      vpos.psf("%%!PS-Adobe-3.0 EPSF-3.0\n"
               "%%%%Creator: PSE Simulator Dataset Viewer\n"
               "%%%%BoundingBox: %d %d %d %d\n"
               "%%%%EndComments\n"
               "\n"
               "/r {\n"
               " newpath\n"
               " 3 index 3 index moveto\n"
               " 4 -1 roll 1 index lineto  %% y1 x2 y2\n"
               " 1 index exch lineto %% y1 x2\n"
               " exch lineto\n"
               " \n"
               " closepath\n"
               " fill\n"
               "} def\n"
               "\n"
               "/arrow {\n"
               " /y2 exch def /x2 exch def /y1 exch def /x1 exch def\n"
               " x1 y1 moveto  x2 y2 lineto  stroke\n"
               " /alpha y1 y2 sub x1 x2 sub atan def\n"
               " /theta alpha 30 sub def\n"
               " /theta2 alpha 30 add def\n"
               " x2 y2 moveto\n"
               " x2 10 theta cos mul add\n"
               " y2 10 theta sin mul add\n"
               " lineto\n"
               " stroke\n"
               " x2 y2 moveto\n"
               " x2 10 theta2 cos mul add\n"
               " y2 10 theta2 sin mul add\n"
               " lineto\n"
               " stroke\n"
               "} def\n"
               "/Helvetica findfont 14 scalefont setfont\n",
               bb.x - 5, bb.y - 25, bb.x + bb.width, bb.y + bb.height - 10
               );
    }

  ve.valid = true;

  vpos.clip = plot_ped_clipped;

  if( ps ) for(int i = 0; i < 512; i++)
    if( ds->state_info[i].ps_color_code )
      vpos.psf("%s", ds->state_info[i].ps_color_code );

  const int view_width_c     = w_width / view.pixels_per_cycle;
  const ds_num view_start_ac = sd->seg_start_ac + view.start_c;
  const ds_num view_first_ac = max(sd->seg_start_ac, view_start_ac);
  const ds_num view_end_ac   = view_start_ac + view_width_c - 1;
  const ds_num view_last_ac  = min(sd->seg_end_ac,view_end_ac);

  VPos vp_ipc_line = vpos;

  segment_plot_top = vpos.vpos;

  const bool ped_view = plot_segment_as_ped;
  const int idx_max = sd->idx_max;
  Insn_Dataset_Info** const idx_to_info = PSE_Window::idx_to_info();

  //
  // View Information
  //

  const int view_start_c = view.start_c;
  view_end_c = view_start_c + view_width_c;
  const int pixels_per_cycle = view.pixels_per_cycle;
  int first_c = c_valid_closest(view_start_c); // modified by next_used
  const int first_ridx = sd->rri_idx.this_or_next_used(first_c)[0];
  ASSERTS( first_ridx != -1 );

  int decode_pos;

  switch( plot_rob_decode_positioning ){
  case PRDP_ROB_Size:
    decode_pos = ds->dsvv_rob_size + 1;
    break;
  case PRDP_Fit:
    {
      int ci = first_c;
      int occ_max = 0;
      int ri;

      while( ci < view_end_c && ( ri = sd->rri_idx.next_used(ci)[0] ) != -1 )
        {
          ROB_Rect_Info *rri = sd->rob_rectangle_info.iterate_set(ri-1,0);

          for( int c = rri->x1;
               rri->x1 == c;
               rri = sd->rob_rectangle_info.iterate_backward() )
            if( rri->st != RTRC_STATE_DECODE
                && rri->st != RTRC_STATE_DECODE - 100 )
              {
                if( rri->y2 > occ_max ) occ_max = rri->y2;
                break;
              }
        }
      decode_pos = occ_max + 1;
    }
    break;
  default:
    decode_pos = 0;
    FATAL;
  }

  const int plot_area_multiple = !ped_view ? 1 : view.pixels_per_inst;

  vpos.down_f_remain(0.99,plot_area_multiple);

  const int pixels_per_inst = view.pixels_per_inst;
  const int height_insn = vpos.plot_area_pixel_height / pixels_per_inst;
  view_height_insn = height_insn;

  if( !ped_view )
    vpos.set_scale_bottom_top(0, 1 + decode_pos + ds->dsvv_issue_width);

  view_idx_adj = view_height_insn - view.top_idx % view_height_insn;
  view_idx_wrap_base = -0xfffffff; // Force a recalculation.
  view.valid = 1;

  //  View completed at this point.
  if( ped_view )
    {
      vpos.ps_clip_to_plot_area_start();
      vpos.set_scale_bottom_top(0,vpos.plot_area_pixel_height);
      gdk_gc_set_foreground(gc, app.c_grid);
      const int decode_width = ds->dsvv_issue_width;
      {
        // Draw vertical (or ideal-rate-paced) grid lines.
        
        double &grid_stride_cf = grid_spacing_used_cyc;
        const bool fixed_spacing = grid_spacing_cyc > 0;
        const int min_pixel_spacing = fixed_spacing ? 20 : -grid_spacing_cyc;

        const double spacings_f[] =
          {0.25, 0.5, 1, 2, 3, 4, 5, 10, 20, 50, 100, 
           200, 500, 1000, 2000, 5000, 1e10};

        const double line_dx_c = double(view_height_insn) / decode_width;
        const double line_dx_f =
          opt_grid_paced ? line_dx_c * pixels_per_cycle : 1.0;

        if( fixed_spacing )
          {
            grid_stride_cf = grid_spacing_cyc;
          }
        else
          {
            double grid_stride_f;
            for(const double *spacing = &spacings_f[0];
                ( grid_stride_f = *spacing * line_dx_f ) < min_pixel_spacing;
                spacing++);
            grid_stride_cf = grid_stride_f / pixels_per_cycle;
          }

        if( grid_stride_cf < 1 ) grid_stride_cf = 1;

        const double offset_cf = double(view_idx_adj) / decode_width;
        const double view_min_c =
          view_start_c + double(redraw_min_p) / pixels_per_cycle;
        const double start_c =
          floor( view_min_c / line_dx_c ) * line_dx_c - offset_cf;

        GdkPoint pts[2];
        pts[0].y = 0;  pts[1].y = vpos.plot_area_pixel_height - 1;
        const int line_dx2 = opt_grid_paced ? gint(line_dx_f) : 0;
        const double redraw_max_cf =
          double(redraw_max_p) / pixels_per_cycle + view_start_c;

        for(double cf = start_c; cf <= redraw_max_cf; cf += grid_stride_cf )
          {
            const gint i = gint(( cf - view_start_c ) * pixels_per_cycle);
            const gint x2 = i + line_dx2;
            if( x2 <= redraw_min_p ) continue;
            pts[0].x = i;  pts[1].x = x2;
            vpos.gdk_draw_lines(gc,pts,2);
          }
      }
      {
        // Draw horizontal grid lines.

        const bool fixed_cyc_spacing = grid_spacing_inst > 0;
        const int min_pixel_spacing =
          fixed_cyc_spacing ? 40 : -grid_spacing_inst;
        int grid_stride = 0;
        const int pixel_mult =
          pixels_per_inst
          * ( opt_grid_mult_decode_width ? ds->dsvv_issue_width : 1 );

        if( fixed_cyc_spacing )
          {
            grid_stride = grid_spacing_inst * pixels_per_inst;
            grid_spacing_used_inst = grid_spacing_inst;
          }
        else
          {
            const int spacings[] =
              {1,2,5,10,20,50,100,200,500,1000,2000,5000,0x7fffffff,0};
            for ( const int *spacing = &spacings[0];
                  *spacing &&
                    ( grid_stride = *spacing * pixel_mult ) < min_pixel_spacing;
                  spacing++);
            grid_spacing_used_inst = max( 1, grid_stride / pixels_per_inst );
          }

        int offset =
          ( grid_spacing_used_inst - view.top_idx % grid_spacing_used_inst )
          * pixels_per_inst;

        GdkPoint pts[2];
        pts[0].x = redraw_min_p;  pts[1].x = redraw_max_p;

        for( pts[0].y = pts[1].y = offset;
             pts[0].y < vpos.plot_area_pixel_height;
             pts[0].y = pts[1].y = pts[0].y + grid_stride )
            vpos.gdk_draw_lines(gc,pts,2);            
      }

      vpos.ps_clip_to_plot_area_end();

      RString cyc_text;
      if( grid_spacing_used_cyc == int(grid_spacing_used_cyc) )
        cyc_text.sprintf("%.0f",grid_spacing_used_cyc);
      else
        cyc_text.sprintf("%.3f",grid_spacing_used_cyc);

      vpos.printf(grd_label,"Grid %d insn X %s cyc",
                  grid_spacing_used_inst, cyc_text.s);
    }
  else
    vpos.printf(grd_label,"");

  RString top_insn;
  top_insn.sprintf("Top Insn: %s", sd->source_most_frequent_insn.s);
  if( sd->source_most_frequent_call_insn.len() > 0 )
    top_insn.sprintf(" / Top Call: %s", sd->source_most_frequent_call_insn.s);
  vpos.printf(frq_label,"%s",top_insn.s);

  int commit_i = 0;

  gint pointer_x_p, pointer_y_p;
  GdkModifierType modifiers;
  if( ps ) { pointer_x_p = 0;  pointer_y_p = 0; }
  else     gdk_window_get_pointer(win,&pointer_x_p,&pointer_y_p,&modifiers);
  const bool shift_key_down = modifiers & GDK_SHIFT_MASK;

  int pointer_c = p_to_c(pointer_x_p);

  segment_plot_bottom = vpos.vpos;
  pointed_idx = -1;
  int pointed_state = -1;

  const bool animating_insn = plot_segment_as_ped && indexing_animating;
  const int yfrac =
    animating && plot_ped_clipped && pixels_per_inst > 1
    ? (int)(double(pixels_per_inst)*(view_start_d-(int)view_start_d)): 0;

  // Plot Graph
  //

  vpos.ps_clip_to_plot_area_start();

  if( ped_view )
    {
      float *rob_pos = NULL;
      const float rob_delta_y =
        float(vpos.plot_area_pixel_height) / ( decode_pos + 2 );
      const float rob_dcd_y = ds->dsvv_issue_width * rob_delta_y;
      if ( seg_rob_ped_animating )
        {
          rob_pos = new float[view_width_c];
          for ( int c=0; c<view_width_c; c++ ) rob_pos[c] = vpos.top_y;
        }
      const bool bttn_as_ped =
        gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pbutton));
      const float rob_f = bttn_as_ped ?
        1 - indexing_animation_fraction : indexing_animation_fraction;
      const int pixels_per_inst_fi =
        max(1, int( 0.5 + pixels_per_inst
                    + ( rob_delta_y - pixels_per_inst ) * rob_f ) );

      if( offset_vc_do_update )
        {
          Insn_Dataset_Info* const di =
            idx_to_info[sd->idx_inrange(view.top_idx)];
          view.offset_vc =
            ( ped_plot_idxt ? di->first_event_c : di->rob_dequeue_c )
            - view_start_c;
          offset_vc_do_update = false;
        }

      view_first_idx =
        !animating_insn && plot_ped_clipped ? sd->idx_inrange(view.top_idx) : 0;
      const int break_idx = !animating_insn && plot_ped_clipped
        ? sd->idx_inrange(view.top_idx + height_insn) : idx_max+1;
      const int break_idx_a = plot_ped_clipped
        ? sd->idx_inrange(view.top_idx + height_insn) : idx_max+1;

      bool reset_offset = !plot_ped_clipped;

      gdk_gc_set_line_attributes
        (gc,1,GDK_LINE_SOLID,GDK_CAP_ROUND,GDK_JOIN_ROUND);

      bool start_idx_updated = false;
      const double f = indexing_animation_fraction;
      const int delta_idx = indexing_animation_delta_idx;
      const bool draw_v_gaps =
        pixels_per_cycle >= int(grid_spacing_cyc_unit_threshold);
      const bool draw_h_gaps =
        pixels_per_inst >= int(grid_spacing_inst_unit_threshold);

      const int ref_vp = idx_to_vertical_position(view.top_idx);
      const int ref_column_start = view.top_idx - ref_vp;
      const int ref_column_stop = ref_column_start + height_insn;
      const int tear_mark_width = max(2,pixels_per_cycle>>1);
      int envelope_right_c = -100;

      const int pre_dcd_vis_c = 3;
      const int post_cmt_vis_c = 2;
      const int margin_c = 6;

      {
        int envelope_left_c = INT_MAX;
        for ( int idx = break_idx - 1;  idx >= view_first_idx;  idx-- )
          {
            Insn_Dataset_Info* const di = idx_to_info[idx];
            const int core_left_c =
              max( di->rob_enqueue_c - pre_dcd_vis_c, di->first_event_c );
            di->envelope_left_c = setmin(envelope_left_c,core_left_c);
          }
      }

      for( int idx = view_first_idx;  idx < break_idx;  idx++ )
        {
          Insn_Dataset_Info* const di = idx_to_info[idx];
          di->yp = -1;  // Overwritten if used.
          int yy1v;

          const int idx_vp = idx_to_vertical_position(idx);
          const bool ref_column =
            idx >= ref_column_start && idx < ref_column_stop;

          const int idx_right_up = idx + height_insn - min(idx_vp,3);
          Insn_Dataset_Info* const di_right_up =
            idx_right_up < break_idx ? idx_to_info[idx_right_up] : NULL;

          const int idx_left_dn =
            idx - height_insn + min( height_insn - 1 - idx_vp, 3 );
          Insn_Dataset_Info* const di_left_dn =
            idx_left_dn >= view_first_idx ? idx_to_info[idx_left_dn] : NULL;

          const int limit_left_c =
            di_left_dn ? di_left_dn->envelope_right_c + margin_c : 0;

          const int neighbor_core_left_c =
            !di_right_up ? INT_MAX : di_right_up->envelope_left_c;

          const int extent_core_right_c =
            min( di->rob_dequeue_c + post_cmt_vis_c, di->last_event_end_c);
          const int extent_nonref_right_c =
            min( di->last_event_end_c, neighbor_core_left_c - margin_c );

          const int extent_right_c =
            seg_rob_ped_animating
            ? di->rob_dequeue_c
            : ref_column
            ? max( extent_core_right_c, extent_nonref_right_c )
            : extent_nonref_right_c;

          setmax(envelope_right_c, extent_right_c);
          di->envelope_right_c = envelope_right_c;

          if ( di->rob_enqueue_c >= view_end_c ) continue;
          if ( di->rob_dequeue_c <= view_start_c ) continue;

          const int extent_left_c =
            seg_rob_ped_animating
            ? di->first_event_c : max( limit_left_c, di->first_event_c );

          di->extent_x_left = extent_left_c - view_start_c;
          di->extent_x_right = extent_right_c - view_start_c;

          if( animating_insn )
            {
              const int alt_idx =
                ( ped_plot_idxt ? di->idxst : di->idxt ) + delta_idx;
              const double y1 = alt_idx + (idx-alt_idx) * f;
              if( plot_ped_clipped &&
                  ( y1 < view.top_idx || y1 >= break_idx_a ) ) continue;
              yy1v = idx_to_p(y1);
            }
          else
            {
              yy1v = idx_to_p(idx);
              if( reset_offset && idx == vpos.top_y )
                {
                  view.offset_vc = di->first_event_c - view_start_c;
                  reset_offset = false;
                }
            }

          view_last_idx = idx;
          const int yy1 = di->yp = yy1v - yfrac;
          const int yy2 = yy1 + pixels_per_inst;
          const int tag = di->tag;
          bool dcd_found = false;

          if ( !seg_rob_ped_animating && extent_right_c < di->rob_dequeue_c
               && extent_right_c > extent_left_c )
            {
              const int xx2 = 
                ( extent_right_c - view_start_c ) * pixels_per_cycle;
              Color_Info& ci = 
                idx_vp & 1 ? app.c_tear_dark : app.c_tear_light;
              vpos.rectangle_raw(ci, xx2, yy1, xx2+tear_mark_width, yy2);
            }

          if ( !seg_rob_ped_animating && extent_left_c > di->first_event_c 
               && extent_right_c >= extent_left_c )
            {
              const int xx2 =
                ( extent_left_c - view_start_c ) * pixels_per_cycle;
              Color_Info& ci = 
                idx_vp & 1 ? app.c_tear_light : app.c_tear_dark;
              vpos.rectangle_raw(ci, xx2, yy1, xx2-tear_mark_width, yy2);
            }

          sd->state_changes.iterate_set(di->first_state_change);
          while( State_Change* const sc = sd->state_changes.iterate() )
            {
              if( sc->tag != tag ) break;
              if( sc->plot_state == -1 ) continue;
              if( !start_idx_updated )
                {start_idx_updated = true; view_first_idx = idx;}

              if( sc->st == RTRC_STATE_COMMIT ) commit_i++;

              const int x1 = max(extent_left_c,sc->c) - view_start_c;
              const int x2 = min(extent_right_c,sc->limit_c) - view_start_c;

              if ( x1 >= x2 ) continue;
              if ( seg_rob_ped_animating && sc->c > di->rob_dequeue_c ) break;

              const int xx1 = x1 * pixels_per_cycle;
              const int xx2 = x2 * pixels_per_cycle;

              if ( pointer_x_p >= xx1 && pointer_x_p < xx2
                  && pointer_y_p >= yy1 && pointer_y_p < yy2 )
                {
                  pointed_idx = idx;  pointed_state = sc->plot_state;
                }

              if( xx2 < redraw_min_p ) continue;
              if( xx1 > redraw_max_p ) continue; // To keep commit count valid.

              if ( seg_rob_ped_animating )
                {
                  const bool dcd = 
                    sc->plot_state == RTRC_STATE_DECODE
                    || sc->plot_state == RTRC_STATE_DECODE - 100;
                  dcd_found |= dcd;
                  if ( !dcd_found ) continue;
                  const int c_start = max( x1, 0 );
                  const int c_stop = min( x2, view_width_c );
                  int x = xx1;
                  for( int cv = c_start; cv < c_stop; cv++ )
                    {
                      if ( dcd && rob_pos[cv] > rob_dcd_y )
                        rob_pos[cv] = rob_dcd_y;
                      const int y1 = yy1 + int(( rob_pos[cv] - yy1 ) * rob_f);
                      const int y2 = y1 + pixels_per_inst_fi;
                      rob_pos[cv] -= rob_delta_y;
                      ASSERTS( cv < view_width_c );
                      const int nextx = x + pixels_per_cycle;
                      vpos.rectangle_raw(sc->plot_state,x,y1,nextx,y2);
                      x = nextx;
                    }
                  continue;
                }
              else
                vpos.rectangle_raw(sc->plot_state,xx1,yy1,xx2,yy2);

              if( !draw_v_gaps && !draw_h_gaps ) continue;

              gdk_gc_set_foreground (gc, &app.gdk_white);
              if( draw_v_gaps )
                {
                  GdkPoint pts[2];
                  pts[0].y = yy1;  pts[1].y = yy2;
                  for( int x = xx1; x < xx2; x+=pixels_per_cycle )
                    {
                      pts[0].x = pts[1].x = x;
                      vpos.gdk_draw_lines(gc,pts,2);
                    }
                }

              if( draw_h_gaps )
                {
                  GdkPoint pts[2];
                  pts[0].x = xx1;  pts[0].y = yy2-1;
                  pts[1].x = xx2;  pts[1].y = yy2-1;
                  vpos.gdk_draw_lines(gc,pts,2);
                }
            }
        }
      if ( rob_pos ) delete rob_pos;
    }
  else
    {
      int y2 = 0;
      int last_x = -1;
      bool dcd_found = false;
      for( ROB_Rect_Info *rri = sd->rob_rectangle_info.iterate_set(first_ridx);
           rri;  rri = sd->rob_rectangle_info.iterate() )
        {
          int x1 = rri->x1 - view_start_c;
          if( x1 < 0 ) continue;
          if( x1 >= view_width_c ) break;
          if( rri->st == RTRC_STATE_COMMIT ) commit_i += rri->y2 - rri->y1;
          if( rri->y1 < 0 ) continue;  // Not in ROB. (Execute store.)
          if( x1 != last_x )
            {
              y2 = 0;  dcd_found = false;  last_x = x1;
            }

          if( !dcd_found && ( rri->st == RTRC_STATE_DECODE
                              || rri->st == RTRC_STATE_DECODE - 100 ) )
            {
              y2 = decode_pos;
              dcd_found = true;
            }

          const int y1 = y2;
          const int insn_count = rri->y2 - rri->y1;
          y2 += insn_count;
          view_last_idx = rri->t1 + insn_count - 1;

          const int x2 = rri->x2 - view_start_c;
          const int xx2 = x2 * pixels_per_cycle;
          if( xx2 <= redraw_min_p ) continue;
          const int xx1 = x1 * pixels_per_cycle;
          if( xx2 > redraw_max_p ) continue; // To keep commit count valid.

          rri->y1_p = y1;

          vpos.rectangle(rri->st,xx1,y1,xx2,y2);
        }
      // DMK: Band-aid until rri's store idxt instead of tags. (Bug 44.)
      view_last_idx = sd->idx_inrange(view_last_idx);
    }

  vpos.ps_clip_to_plot_area_end();

  vp_ipc_line.printf(rnk_label,
                      "Segment %s, Rank %d", sd->segment_label, 1+sd->rank);
  vp_ipc_line.printf(ipc_label,
                     "%.2f IPC over %.0f cycles.",
                     double(commit_i) /
                     max((ds_num)1, view_last_ac - view_first_ac ),
                     double(view_last_ac - view_first_ac));

  // Find the idx of the first instruction that crosses the upper
  // left boundary of a zoom window being drawn by the user.
  //
  if( ped_view && zoom_c_1 )
    {
      int top_p  = min(gint(zoom_p_1),pointer_y_p);
      int left_c = min(zoom_c_1,pointer_c);

      int idx = sd->idx_inrange
        ( view.top_idx % height_insn
          + ( top_p - vpos.top_pos ) / view.pixels_per_inst );

      int idx_enqueue_within = -1;
      int idx_dequeue_after_start = -1;

      while( Insn_Dataset_Info* const di = idx_to_info[idx] )
        {
          if( idx_enqueue_within == -1 && di->first_event_c > left_c )
            { idx_enqueue_within = idx; break; }
          if( idx_dequeue_after_start == -1 && di->rob_dequeue_c > left_c )
            idx_dequeue_after_start = idx;
          idx += height_insn;
        }
      zoom_top_idx =
        idx_enqueue_within != -1 ? idx_enqueue_within :
        idx_dequeue_after_start != -1 ? idx_dequeue_after_start : view.top_idx;
    }

  if( !ped_view )
    {
      pointed_idx = -1;
      pointed_state = -1;
      int idx = sd->rri_idx[pointer_c][0];
      while( 1 )
        {
          ROB_Rect_Info *rri = sd->rob_rectangle_info[idx++];
          if( !rri ) break;
          if( rri->x1 != pointer_c ) break;
          if( !vpos.in_range_ypl(rri->y1_p, pointer_y_p,
                                 rri->y2 - rri->y1,
                                 &pointed_idx) ) continue;
          pointed_idx += -rri->y1 + rri->t1 - 1;
          pointed_state = rri->st;
          break;
        }
      // Band aid until rri stores idx, probably Bug 44.
      if( pointed_idx != -1 ) pointed_idx = sd->idx_inrange(pointed_idx);
    }

  if( !ps && !button_down_motion )
    gdk_window_set_cursor
      (draw_area_win,
       dis_drag_grab_rect.in( pointer_x_p, pointer_y_p ) ? app.cursor_dis_drag :
       pointed_idx == -1 ? app.cursor_arrow :
       cursor_locked ? app.cursor_corners :
       app.cursor_insn_follow
       );

  bool cursor_move;
  const int prev_cursor_idx = cursor_idx;

  switch( cursor_cmd ){
  case CC_None:
    if( !cursor_locked && pointed_idx != -1 ) cursor_idx = pointed_idx;
    cursor_move = false;
    break;
  case CC_Pointer:
    cursor_idx = pointed_idx == -1 ? cursor_idx : pointed_idx;
    cursor_move = false;
    break;
  case CC_Previous:
    cursor_move = true;
    cursor_idx = sd->idx_inrange(cursor_idx + cursor_move_amt);
    cursor_move_amt = 0;
    break;
  case CC_Next:
    cursor_move = true;
    cursor_idx = sd->idx_inrange(cursor_idx + cursor_move_amt);
    cursor_move_amt = 0;
    break;
  case CC_Init:
    cursor_move = true;
    cursor_idx = 0;
    cursor_move_amt = 0;
    break;
  case CC_Goto:
    if( cursor_idx != cursor_cmd_goto_idx )
      {
        cursor_move = true;
        cursor_idx = cursor_cmd_goto_idx;
        cursor_move_amt = 0;
      }
    else
      cursor_move = false;
    break;

  default:
    cursor_move = false;
    ASSERTS(false);
    break;
  }

  Insn_Dataset_Info* const cursor_di =
    cursor_idx == -1 ? NULL : idx_to_info[cursor_idx];

  if( cursor_move && view_last_idx
      && prev_cursor_idx >= view_first_idx && prev_cursor_idx <= view_last_idx
      && ( cursor_idx < view_first_idx || cursor_idx >= view_last_idx
           || cursor_di && cursor_di->yp == -1 ) )
    {
      if( plot_ped_clipped )
        {
          const int goal_maybe = cursor_idx - ( height_insn >> 1 );
          goal_idx = max(0, min( goal_maybe, sd->idx_max - height_insn ) );
        }
      else
        {
          const int goal_maybe =
            cursor_di->first_event_c - ( view_width_c >> 1 );
          goal_c = max(0, min( goal_maybe, sd->seg_duration_c-view_width_c ) );
        }
      user_cmd_redisplay(this,false);
    }

  cursor_cmd = CC_None;
  search_bttn_sensitivity_set(this); // Set based on cursor_idx.

  const bool cursor_is_locked = cursor_locked; // Avoid gtk_toggle... overhead.

  //
  // Determine the location of the disassembly window.
  //

  const bool dis_show_idx_is_first = drag_dis_window;

  // Unless locked scroll disassembly window to keep this idx visible.
  int dis_show_idx = -1;
  if( drag_dis_window )
    {
      if( pointed_idx == -1 )
        {
          int delta_insn =
            ( pointer_y_p - drag_dis_window_last_y_p ) / pixels_per_inst;
          dis_show_idx = dis_first_idx + delta_insn;
          int delta_y = delta_insn * pixels_per_inst;
          drag_dis_window_last_y_p += delta_y;
        }
      else
        {
          dis_show_idx = pointed_idx;
          drag_dis_window_last_y_p = pointer_y_p;
        }
    }
  else if( cursor_move )
    {
      if( pointed_idx == -1 ||
         cursor_idx_last >= dis_first_idx && cursor_idx_last <= dis_last_idx )
        dis_show_idx = cursor_idx;
    }
  else if( !dis_locked )
    {
      if( pointed_idx != -1 ) dis_show_idx = pointed_idx;
    }


  const int subtree_base = sd->insn_stream.subtree_base;
  const int dest_subtree = subtree_base + OR_rd;
  const int subtree_viamem_base = subtree_base + OR_num;
  // Maximum number of registers.  Doubles counted as 2, etc.
  sd->insn_stream.subtree_base += 2 * OR_num;
  if( subtree_base > sd->insn_stream.subtree_base )
    fatal("Internal Error: Try restarting, this shouldn't happen very often.");

  const char* const source_description = "";

  Insn_Dataset_Info* const pointed_di =
    pointed_idx == -1 ? NULL : idx_to_info[pointed_idx];
  const int pointed_pc = pointed_di ? pointed_di->pc : 0;
  Insn_Decode_Info* const cursor_si = cursor_di ? cursor_di->si : NULL;
  Insn_Decode_Info* const pointed_si = pointed_di ? pointed_di->si : NULL;

  const bool cursor_moved = cursor_idx != cursor_idx_last;
  const bool pointer_moved = pointed_idx != pointed_idx_last;
  cursor_move_more_recent =
    cursor_moved || !pointer_moved && cursor_move_more_recent;
  cursor_idx_last = cursor_idx;
  pointed_idx_last = pointed_idx;

  const bool follow_pointer = pointed_di && !cursor_move_more_recent;

  Insn_Dataset_Info* const pane_di =
    follow_pointer ? pointed_di : cursor_di;
  Insn_Decode_Info* const pane_si =
    follow_pointer ? pointed_si : cursor_si;
  const int pane_c =
    follow_pointer || !pane_di ? pointer_c :
    pane_di->rob_enqueue_c+((pane_di->rob_dequeue_c-pane_di->rob_enqueue_c)>>1);
  const int pane_state =
    follow_pointer || pointed_idx == cursor_idx ? pointed_state : -1;
  const int pane_pc = pane_di ? pane_di->pc : 0;
  Insn_Partition_Info* const pane_pi =
    pane_di ? pane_di->insn_partition_info : NULL;
  Insn_Dataset_Info* const highlight_di =
    shift_key_down && pointed_di ? pointed_di : cursor_di;
  Insn_Decode_Info* const highlight_si = highlight_di ? highlight_di->si : NULL;
  const int32_t highlight_pc = highlight_di ? highlight_di->pc : 0;
  const int32_t highlight_ea_line =
    highlight_di && highlight_di->ea_valid ? EA_LINE(highlight_di->ea) : 1;
  const int32_t highlight_ea =
    highlight_di && highlight_di->ea_valid ? highlight_di->ea : 0;

  Color_Info& label_bg =
    pane_di != cursor_di ? app.c_pointer_bg :
    cursor_is_locked ? app.c_cursor_locked_bg : app.c_cursor_unlocked_bg;

  const int search_tag =
    sd->tag_absolute_to_relative(search_tag_or_cycle_absolute,TAG_NEVER);

  PStack<GtkWidget*> labels_either;
  PStack<GtkWidget*> labels_neither;

  {
    const char *state_name =
      pane_state != -1 && ds->state_info[pane_state].name ?
      ds->state_info[pane_state].name : NULL;

    if( state_name )
      vp_ipc_line.printf(sta_label,"State: %s", state_name);
    else
      vp_ipc_line.printf(sta_label,"");
  }

  if( pane_di )
    {
      RString a(200);
      PSList<char*> messages;
      messages += pane_di->seg_plot_annotation_messages;
      if( pane_pi ) messages += pane_pi->seg_plot_annotation_messages;
      const int occ = messages.occ();
      int idx_upper = messages.find(pane_c);
      int idx_lower = idx_upper - 1;
      PQueue<char*> some_messages;

      for( int amt = min(100,occ); amt; )
        {
          if( idx_lower >= 0 )
            {
              some_messages.enqueue_at_head(messages[idx_lower--]);
              amt--;
            }
          if( idx_upper < occ )
            {
              some_messages += messages[idx_upper++];
              amt--;
            }
        }

      for( char* m; some_messages.dequeue(m); ) { a += m; a += "\n---\n"; }
      char *msg_plain;
      if( pango_parse_markup(a,-1,0,NULL,&msg_plain,NULL,NULL) )
        {
          gtk_label_set_markup(GTK_LABEL(seg_plot_annotation_label),a.s);
          gtk_label_set_markup(seg_plot_annotation_pane_label,a.s);
        }
      else
        {
          gtk_label_set_text(GTK_LABEL(seg_plot_annotation_label),a.s);
          gtk_label_set_text(seg_plot_annotation_pane_label,a.s);
        }

      gtk_widget_modify_bg
        (annotation_data->message_large_ebox, GTK_STATE_NORMAL, label_bg);
      gtk_widget_modify_bg
        (annotation_data->message_pane_ebox, GTK_STATE_NORMAL, label_bg);
    }

  if( pointed_di )
    {
      for( Annotation_Iterator ai(ds,sd,pointed_di); ai; ai++ )
        ai.def->pointed = true;
      for( Annotation_Iterator ai(ds,sd,pointed_di->insn_partition_info);
           ai; ai++ )
        ai.def->pointed = true;
    }
  if( cursor_di )
    {
      for( Annotation_Iterator ai(ds,sd,cursor_di); ai; ai++ )
        ai.def->cursor = true;
      for( Annotation_Iterator ai(ds,sd,cursor_di->insn_partition_info);
           ai; ai++ )
        ai.def->cursor = true;
    }

  {
    bool blink_message = false;

    for( Annotation_Iterator ai(ds,sd,pane_di); ai; ai++ )
      {
        blink_message |= ai.def->blink;
        ai.def->use_count_last_repaint_message++;
      }
    for( Annotation_Iterator ai(ds,sd,pane_pi); ai; ai++ )
      {
        blink_message |= ai.def->blink;
        ai.def->use_count_last_repaint_message++;
      }

    if( annotation_data->message_pane_height < 40 && pane_di
        && pane_di->seg_plot_annotation_messages.occ() )
      {
        PSplit first(pane_di->seg_plot_annotation_messages[0],'\n');
        char* msg_plain;
        pango_parse_markup(first[1],-1,0,NULL,&msg_plain,NULL,NULL);
        RString msg_ellip;
        if( first > 3 ) msg_ellip.sprintf("<b>[+%d lines]</b> ",first.occ()-3);
        msg_ellip.sprintf
          ("<span background=\"%s\">%s</span>",
           blink_message ? "gray" : label_bg.spec, msg_plain);
        gtk_label_set_markup(seg_plot_annotation_pane_label,msg_ellip);
      }
  }

  if( ds->readex && pane_si )
    {
      vp_ipc_line.printf(src_label,"%s%s",
                         source_description,
                         RX_addr_to_source(pane_pc));
      vp_ipc_line.printf(dis_label,"%s",
                         pane_si ? pane_si->get_dis_string() : "");
      labels_either += src_label;
      labels_either += dis_label;
    }
  else
    {
      vp_ipc_line.printf(src_label,"Seg start: %s",sd->source);
      vp_ipc_line.printf(dis_label,"");
      labels_neither += src_label;
      labels_neither += dis_label;
    }

  if( pointed_di && !ps )
    vp_ipc_line.printf(pos_label,"Time %s  Tag %s  PC 0x%08x  Insn %s",
                       comma(c_to_ac(pointer_c)),
                       comma(sd->seg_start_tag + pointed_di->tag),
                       pointed_pc,
                       comma(sd->seg_start_pcount+pointed_di->commit_idx-1));
  else
    vp_ipc_line.printf(pos_label,"Time %s", comma(c_to_ac(pointer_c)));


  while( labels_either ) pgtk_label_set_background(labels_either,label_bg);
  while( labels_neither ) pgtk_label_set_background(labels_neither,NULL);
  pgtk_label_set_background(sta_label,app.c_pointer_bg);
  pgtk_label_set_background(pos_label,app.c_pointer_bg);

  //
  // Mark dataflow tree rooted at highlighted instruction.
  //
  if( highlight_si && pref_plot_high_dep_limit )
    {
      // Mark highlighted instruction.
      //
      highlight_si->dataflow_distance = 1;
      highlight_si->dataflow_subtree = dest_subtree;
      const bool direct_only = pref_plot_high_dep_limit == 1;
      {
        for(Reg_Info *ri; highlight_si->dests_iterate(ri); )
          {
            ri->dataflow_distance = 1;
            ri->dataflow_subtree =
              subtree_base + ri->role + ( ri->role == OR_mem ? OR_num : 0 );
          }
      }

      // Mark predecessors of highlighted instruction.
      //
      PStack<Insn_Decode_Info*> todo(highlight_si);
      bool root = true;
      int subtree = dest_subtree;

      for(Insn_Decode_Info *si; todo.pop(si); )
        {
          int next_dep_distance;
          if( root )
            next_dep_distance = 1;
          else
            {
              subtree = si->dataflow_subtree;
              next_dep_distance = si->dataflow_distance + 1;
            }

          for(Reg_Info *ri; si->sources_iterate(ri); )
            {
              if( root ) subtree = subtree_base + ri->role;
              if( !pref_plot_dep_through_mem && ri->role == OR_mem ) continue;
              ri->dataflow_distance = next_dep_distance;
              const int modified_subtree =
                subtree +
                ( subtree < subtree_viamem_base && ri->role == OR_mem
                  ? OR_num : 0 );
              ri->dataflow_subtree = modified_subtree;

              if( !ri->writer_si ) continue;
              Insn_Decode_Info *wsi = ri->writer_si;
              ASSERTS( wsi != si );
              Reg_Info* const wri =
                ri->writer_ri ? ri->writer_ri - ri->writer_ri->ri_offset : NULL;

              if( wri &&
                  ( wri->dataflow_subtree < subtree_base
                    || wri->dataflow_distance < next_dep_distance ) )
                {
                  wri->dataflow_subtree = modified_subtree;
                  wri->dataflow_distance = next_dep_distance;
                }

              if( wsi->dataflow_subtree < subtree_base
                  || wsi->dataflow_distance < next_dep_distance )
                {
                  wsi->dataflow_distance = next_dep_distance;
                  wsi->dataflow_subtree = modified_subtree;
                  todo += wsi;
                }
            }
          if( direct_only ) break;
          root = false;
        }

      // Mark successors to highlighted instruction.
      //
      const int highlight_pdx = highlight_di->idxst;

      for( Insn_Dataset_Info* dii = highlight_di->next; dii; dii = dii->next )
        {
          Insn_Decode_Info* const si = dii->si;
          if( !si ) continue;
          Reg_Info *furthest_ri = NULL;
          int src_dep_distance = -1;

          for(Reg_Info *ri; si->sources_iterate(ri); )
            {
              if( !pref_plot_dep_through_mem && ri->role == OR_mem ) continue;
              Insn_Decode_Info* const wsi = ri->writer_si;
              if( !wsi || wsi->tag < 0 ) continue;
              Insn_Dataset_Info* const wdi = sd->tag_to_info[wsi->tag];
              if( wdi->idxst < highlight_pdx
                  || wsi->dataflow_subtree < subtree_base )
                continue;

              Reg_Info* const riref = ri - ri->ri_offset;
              riref->dataflow_subtree = ri->writer_ri->dataflow_subtree;
              riref->dataflow_distance = wsi->dataflow_distance;
              if( wsi->dataflow_distance > src_dep_distance )
                {
                  src_dep_distance = wsi->dataflow_distance;
                  furthest_ri = ri->writer_ri;
                }
            }
          if( !furthest_ri ) {si->dataflow_subtree = 0; continue;}
          si->dataflow_subtree = furthest_ri->dataflow_subtree;
          si->dataflow_distance = src_dep_distance + 1;
          if( direct_only ) continue;
          for(Reg_Info *ri; si->dests_iterate(ri); )
            {
              ri->dataflow_subtree = furthest_ri->dataflow_subtree
                + ( ri->role == OR_mem
                    && furthest_ri->dataflow_subtree < subtree_viamem_base
                    ? OR_num : 0 );
              ri->dataflow_distance = src_dep_distance + 1;
            }
        }
    }

  //
  // Draw disassembly pane.
  //
  if( !animating_insn && view_last_idx && plot_show_dis && ds->readex )
    {
      PangoLayout *pl = gtk_label_get_layout(GTK_LABEL(dis_lines_label));
      int lines = pango_layout_get_line_count(pl);
      int l_height, l_width;
      pango_layout_get_pixel_size(pl,&l_width,&l_height);
      const int text_height = lines && l_height
        ? int(0.99 + double(l_height) / lines) : 15;

      GdkRectangle area, inter;
      area.x = 0; area.y = 0; area.width = 10000; area.height = 10000;
      gtk_widget_intersect(dis_lines_label,&area,&inter);
      const int d_height = inter.height;

      // Determine first instruction in disassembly pane.
      //
      if( dis_show_idx_is_first )
        {
          dis_first_idx = sd->idx_inrange(dis_show_idx);
        }
      else if( dis_show_idx != -1 )
        {
          int last_pc = 0, window_pos = 0, idx, last_snip_serial = -1;

          for(idx = dis_first_idx;
              idx <= idx_max && window_pos < d_height; idx++ )
            {
              Insn_Dataset_Info* const di = idx_to_info[idx];
              int pc = di->pc;
              Insn_Partition_Info* const pi = di->insn_partition_info;
              if( pi && pi->snip_serial && pi->snip_serial != last_snip_serial )
                {
                  window_pos += text_height;
                  last_snip_serial = pi->snip_serial;
                }

              window_pos +=
                pc != last_pc + 4 && pc != last_pc + 8
                ? text_height << 1 : text_height;
              last_pc = pc;
            }
          int dis_last_idx_maybe = idx;
          int dis_idx_height = dis_last_idx_maybe - dis_first_idx;
          int dis_idx_height_h = dis_idx_height >> 1;
          int dis_idx_height_q = dis_idx_height >> 2;
          if( dis_show_idx < dis_first_idx )
            {
              dis_first_idx = sd->idx_inrange
                (dis_show_idx - dis_idx_height_h - dis_idx_height_q);
            }
          else if( dis_show_idx >= dis_last_idx_maybe )
            {
              dis_first_idx = sd->idx_inrange(dis_show_idx - dis_idx_height_q);
            }
        }

      dis_max_c = 0;
      dis_min_c = INT_MAX;

      // Construct disassembly text.
      //
      {
        RString dtext(300);
        int idx = dis_first_idx;
        int last_pc = 0;
        Insn_Partition_Info *pi_last = NULL;
        for(int window_pos = 0;
            window_pos < d_height && idx <= idx_max; idx++)
          {
            Insn_Dataset_Info* const di = idx_to_info[idx];
            Insn_Partition_Info* const pi = di->insn_partition_info;
            if( pi && pi != pi_last )
              {
                RString snip_label(100);

                snip_label.sprintf
                  ("<span foreground=\"brown\" weight=\"bold\">"
                   "Snip Serial %d</span>",
                   pi->snip_serial);
                if( pi->attributes & 7 )
                  snip_label += "<span foreground=\"black\"><b><i>";
                if( pi->attributes & 1 ) snip_label += "  Inject";
                if( pi->attributes & 2 ) snip_label += "  Merge";
                if( pi->attributes & 4 ) snip_label += "  Loop";
                if( pi->attributes & 7 ) snip_label += "</i></b></span>";

                if( !pi->seg_plot_annotation_assembly_verified )
                  {
                    char*const before = pi->seg_plot_annotation_assembly_before;
                    char*const after = pi->seg_plot_annotation_assembly_after;
                    RString am_snip_label = before + snip_label + after;
                    if( !pango_parse_markup
                        (am_snip_label,-1,0,NULL,NULL,NULL,NULL) )
                      {
                        pi->seg_plot_annotation_assembly_before =
                          g_markup_escape_text(before,strlen(before));
                        pi->seg_plot_annotation_assembly_after =
                          g_markup_escape_text(after,strlen(after));
                        free(before);  free(after);
                      }
                    pi->seg_plot_annotation_assembly_verified = true;
                  }

                dtext += pi->seg_plot_annotation_assembly_before;
                dtext += snip_label;
                dtext += pi->seg_plot_annotation_assembly_after;
                dtext += "\n";
                window_pos += text_height;
              }
            pi_last = pi;

            const int pc = di->pc;
            if( ( last_pc || pc ) && pc != last_pc + 4 && pc != last_pc + 8 )
              {
                dtext += "<span foreground=\"blue\">";
                const char* const source_info = RX_addr_to_source(pc);
                dtext += source_info && *source_info
                  ? source_info : "Source Line Unavailable";
                dtext += "</span>\n";
                window_pos += text_height << 1;
              }
            else
              window_pos += text_height;

            const bool highlighted = di == highlight_di;
            const bool cursor = idx == cursor_idx;
            const bool clocked = cursor && cursor_is_locked;
            bool pointed = idx == pointed_idx;
            bool same_sin = pc == highlight_pc;
            const bool same_cache_line =
              plot_high_same_line && di->ea_valid
              && highlight_ea_line == EA_LINE(di->ea);
            bool same_line_ref_inserted = false;
            const bool search_hit = pc == search_pc || search_tag == di->tag;
            Insn_Decode_Info *si = di->si;
            bool doomed = di->doomed;
            bool pre_ex = di->pre_ex;
            bool in_dataflow_tree = si && si->dataflow_subtree >= subtree_base;
            const bool hl_dests_unused_direct =
              pref_plot_high_dead_insn && si && si->dests_unused_direct;
            const bool hl_dests_unused_indirect =
              pref_plot_high_dead_insn && si && si->dests_unused_indirect;
            const bool visible = idx >= view_first_idx && idx <= view_last_idx;
            if( visible )
              {
                dis_max_c = max( dis_max_c, di->rob_dequeue_c );
                dis_min_c = min( dis_min_c, di->rob_enqueue_c );
              }
            dtext += "<b><i> </i></b>"; // Kludge for uniform baseline spacing.

            // Span entire line.
            //
            if( doomed && !pre_ex ) dtext += "<span foreground=\"#ff0000\">";
            if( pre_ex ) dtext += "<span foreground=\"#6f6fff\">";
            if( !visible ) dtext += "<i>";
            if( highlighted ) dtext += "<b>";
            if( hl_dests_unused_direct ) dtext += "<s>";
            if( pointed || cursor )
              {
                dtext += "<span background=\"";
                dtext +=
                  clocked ? app.c_cursor_locked_bg.spec :
                  cursor ? app.c_cursor_unlocked_bg.spec
                  : app.c_pointer_bg.spec;
                dtext += "\">";
              }

            if( search_hit )
              dtext.sprintf("<span foreground=\"%s\">",app.gdk_search_hit.spec);
            else if( same_sin )
              dtext += "<span foreground=\"#00af00\">";

            if( hl_dests_unused_indirect ) dtext += "<s>";
            dtext.sprintf("<tt>%08x</tt>",pc);
            if( hl_dests_unused_indirect ) dtext += "</s>";

            if( search_hit || same_sin ) dtext += "</span>";

            if( pointed && highlighted )
              {
                dtext += "</span><span background=\"";
                dtext += app.c_pointer_bg.spec;
                dtext += "\">";
              }

            dtext += "  ";
            RString insn_text(100);

            if( di->si )
              for(Dis_Piece *dp = NULL; (dp=di->si->dis_piece_iterate()); )
                switch( dp->type ){

                case DPT_Mnemnoic:
                  {
                    bool color_mn = !highlighted && in_dataflow_tree;
                    if( color_mn ) insn_text += "<span foreground=\"purple\">";
                    insn_text += dp->str;
                    if( color_mn ) insn_text += "</span>";
                  }
                  break;

                case DPT_Value:
                case DPT_Constant:
                case DPT_CTI_Displacement:
                case DPT_Punctuation:
                  insn_text += dp->str;
                  break;

                case DPT_Address_Symbol:
                case DPT_Address_Value:
                case DPT_Register:
                  {
                    const bool highl_same_line =
                      same_cache_line && dp->type != DPT_Register;
                    Reg_Info *ri = dp->reg_info;
                    int dep_distance = ri ? ri->dataflow_distance : 0;
                    int subtree_idx =
                      ri ? ri->dataflow_subtree - subtree_base : -1;
                    bool through_mem;
                    if( subtree_idx >= OR_num )
                      {
                        through_mem = true;
                        subtree_idx -= OR_num;
                      }
                    else
                      through_mem = false;

                    if ( highl_same_line && !same_line_ref_inserted )
                      {
                        same_line_ref_inserted = true;
                        insn_text.sprintf
                          ("<span foreground=\"%s\"><b>   Ref ",
                           app.c_same_line.spec);
                        if ( highlight_di == di )
                          insn_text += "is";
                        else
                          insn_text.sprintf("%+d =", di->ea - highlight_ea);
                        insn_text += "</b></span>";
                      }

                    bool color_reg = subtree_idx >= 0;
                    if( color_reg )
                      {
                        if( through_mem ) insn_text += "<i>";
                        insn_text += "<span foreground=\"";
                        insn_text += app.dependency_color[subtree_idx].spec;
                        insn_text += "\">";
                        if( dep_distance == 1 ) insn_text += "<b>";
                      }
                    else if ( highl_same_line )
                      insn_text.sprintf
                        ("<span foreground=\"%s\">", app.c_same_line.spec);
                    const bool final_use = ri && ri->final_use;
                    if ( final_use ) 
                      insn_text.sprintf
                        ("<span style=\"%s\">", visible ? "italic" : "normal");
                    insn_text += dp->str;
                    if ( final_use ) insn_text += "</span>";
                    if( color_reg )
                      {
                        if( dep_distance == 1 ) insn_text += "</b>";
                        insn_text += "</span>";
                        if( through_mem ) insn_text += "</i>";
                      }
                    else if ( highl_same_line )
                      insn_text += "</span>";
                  }
                  continue;
                default:
                  ASSERTS( false );
                }

            if( !di->seg_plot_annotation_assembly_verified )
              {
                char* const before = di->seg_plot_annotation_assembly_before;
                char* const after = di->seg_plot_annotation_assembly_after;
                RString am_insn_text = before + insn_text + after;
                if( !pango_parse_markup(am_insn_text,-1,0,NULL,NULL,NULL,NULL) )
                  {
                    di->seg_plot_annotation_assembly_before =
                      g_markup_escape_text(before,strlen(before));
                    di->seg_plot_annotation_assembly_after =
                      g_markup_escape_text(after,strlen(after));
                    free(before);  free(after);
                  }
                di->seg_plot_annotation_assembly_verified = true;
              }

            dtext += di->seg_plot_annotation_assembly_before;
            dtext += insn_text;
            dtext += di->seg_plot_annotation_assembly_after;

            if( pointed || cursor ) dtext += "</span>";
            if( hl_dests_unused_direct )
              {
                Insn_Dataset_Info* const wdi =
                  sd->tag_to_info[si->overwriter_tag_min];
                const int widx = info_to_idx(wdi);
                dtext += "</s>  <i>Dead by ";
                if( wdi->pc != di->pc )
                  dtext.sprintf("insn at %+d, 0x%x", widx - idx, wdi->pc);
                else
                  dtext.sprintf("this insn at %+d", widx - idx);
                dtext += "</i>";
              }
            if( highlighted ) dtext += "</b>";
            if( !visible ) dtext += "</i>";
            if( doomed || pre_ex ) dtext += "</span>";
            dtext += "\n";
            last_pc = pc;
          }
        gtk_label_set_markup(GTK_LABEL(dis_lines_label),dtext);
        dis_last_idx = idx - 1;
      }

      vpos.printf(dis_lines_label);
    }

  vpos.draw_pixmap();

  // Draw frame around plot area.
  //
  if( ps )
    {
      PGDK_Points<5> p(0,vpos.top_pos);
      p.x(vpos.width-1);  p.y(vpos.bottom_pos);  p.x(0);  p.y(vpos.top_pos);
      gdk_gc_set_foreground (gc, app.c_dis_outline_locked);
      vpos.gdk_draw_lines(ww,gc,p,p);
    }

  // If user is dragging out an area to zoom, draw a black rectangle
  // showing the boundaries of this area.
  //
  if( zoom_c_1 )
    {
      gint x_1 = gint(zoom_x_1);
      gint y_1 = gint(zoom_p_1);
      PGDK_Points<4> p(x_1,y_1);
      p.x(pointer_x_p);  p.y(pointer_y_p);  p.x(x_1);
      gdk_draw_polygon(ww,style->black_gc,0,p,p);
    }

# define idx_to_w(idx) \
    ( ( (idx) - view.top_idx + height_insn) / height_insn )


  const int gap = max(4, int(pixels_per_cycle * 1.5));

  // Draw a box around plot area instructions that also appear in the
  // disassembly window.
  //
  if( ped_view && ds->readex && plot_show_dis
      && dis_first_idx <= view_last_idx && dis_last_idx >= view_first_idx )
    {
      const int margin = min(gap << 3,70);
      const int grab_size = 10;
      const int grab_size_h = grab_size >> 1;
      const int x1 =
        max( 0, ( dis_min_c - view_start_c ) * pixels_per_cycle - margin);
      const int x2 =
        min( ( dis_max_c - view_start_c + 1 ) * pixels_per_cycle + margin,
             w_width-1);
      const int first_dis_insn_p =
        idx_to_p(max(view_first_idx,dis_first_idx)) - yfrac;
      const int last_dis_insn_p =
        idx_to_p(min(dis_last_idx,view_last_idx)+1) - yfrac;

      gdk_gc_set_foreground
        (gc, dis_locked && !ps
         ? app.c_dis_outline_locked : app.c_dis_outline);
      gdk_gc_set_line_attributes
        (gc,1,GDK_LINE_SOLID,GDK_CAP_ROUND,GDK_JOIN_ROUND);

      PGDK_Points<10> p;

      if( first_dis_insn_p < last_dis_insn_p )
        {
          p.reset(x1,first_dis_insn_p);
          p.x(x2);  p.y(last_dis_insn_p);  p.x(x1);  p.y(first_dis_insn_p);
          vpos.gdk_draw_lines(ww,gc,p,p);
        }
      else
        {
          p.reset(x2,vpos.top_pos);
          p.y(last_dis_insn_p);  p.x(x1);  p.y(vpos.top_pos);
          vpos.gdk_draw_lines(ww,gc,p,p);

          p.reset(x1,vpos.bottom_pos);
          p.y(first_dis_insn_p);  p.x(x2);  p.y(vpos.bottom_pos);
          vpos.gdk_draw_lines(ww,gc,p,p);
        }

      if( !ps )
        {
          if( drag_dis_window )
            {
              dis_drag_grab_rect.setxywh
                (pointer_x_p - grab_size_h, pointer_y_p - grab_size_h,
                 grab_size, grab_size);
              gdk_draw_line(ww,gc,pointer_x_p,pointer_y_p,x2,first_dis_insn_p);
            }
          else
            dis_drag_grab_rect.setxywh
              (x2 - grab_size_h, first_dis_insn_p - grab_size_h,
               grab_size, grab_size);

          gdk_draw_rectangle
            (ww,gc,true,
             dis_drag_grab_rect.x1, dis_drag_grab_rect.y1,
             grab_size, grab_size);
        }
    }

  // Update the envelope.
  //
  {
    int vpos = idx_to_vertical_position(view_last_idx);
    int idx = view_last_idx;
    while( idx >= view_first_idx )
      {
        int c = INT_MAX;
        while( idx >= view_first_idx && vpos-- )
          {
            Insn_Dataset_Info* const di = idx_to_info[idx--];
            di->rob_enqueue_c_envelope = c = min(c,di->extent_x_left);
          }
        vpos = view_height_insn;
      }
  }

  // Highlight instructions to show dependencies, etc.
  //
  vpos.ps_clip_to_plot_area_start();
  const int line_thickness =
    pixels_per_cycle < 12 ? 1 :
    pixels_per_cycle < 24 ? 2 : 3;
  gdk_gc_set_line_attributes
    (gc,line_thickness,GDK_LINE_SOLID,GDK_CAP_ROUND,GDK_JOIN_ROUND);
  const bool dep_obscure = plot_dep_obscure; // Avoid gtk overhead.
  if( ped_view && view_last_idx && ! seg_rob_ped_animating )
    for(int idx = view_first_idx; idx <= view_last_idx; idx++)
      {
        Insn_Dataset_Info* const di = idx_to_info[idx];
        Insn_Decode_Info* const si = di->si;
        if( !si ) continue;
        bool obscure = false;
        const bool hl_dead = pref_plot_high_dead_insn
          && si && ( si->dests_unused_direct || si->dests_unused_indirect );
        PStack<Feature> features;

        // Set color and other highlighting attributes for this instruction.
        //
        while( Feature* const f = di->seg_plot_annotation_markers.iterate() )
          features += *f;

        if ( idx == pointed_idx )
          // Instruction is under mouse pointer.
          features += Feature(&app.gdk_black,&app.gdk_white);

        if ( idx == cursor_idx )
          // Instruction is the cursor instruction.
          features += Feature
            ( &app.gdk_black,
              cursor_is_locked
              ? &app.c_cursor_locked_bg.gdkcolor
              : &app.c_cursor_unlocked_bg.gdkcolor );

        if( di && di == highlight_di )
          // This is the highlight reference instruction.
          features += Feature(&app.gdk_black);

        if ( plot_high_same_sin && di->pc == highlight_pc )
          // Insn is different dynamic instance (same addr) than cursor insn.
          features += Feature(&app.gdk_same_sin);

        if( di->pc == search_pc || di->tag == search_tag )
          features += Feature(&app.gdk_search_hit.gdkcolor);

        if ( plot_high_same_line && di->ea_valid
             && EA_LINE(di->ea) == highlight_ea_line )
          features += Feature(&app.gdk_same_sin,false);

        if( di && di != highlight_di && si->dataflow_subtree >= subtree_base )
          // Instruction uses output of or produces input of cursor insn.
          {
            const int subtree_idx_raw = si->dataflow_subtree - subtree_base;
            const bool through_mem = subtree_idx_raw >= OR_num;
            const int subtree_idx = subtree_idx_raw - (through_mem?OR_num:0);
            features += Feature(app.dependency_color[subtree_idx],!through_mem);
          }

        if ( !features && dep_obscure )
          // Instruction gets no highlighting, and with this option is obscured
          // (shown all gray).
          obscure = true;

        if( !features && !dep_obscure && !hl_dead ) continue;

        // Coordinates of instruction.
        //
        const int x1 =  // Leftmost extent of instruction.
          di->extent_x_left * pixels_per_cycle;
        const int x2 =  // Rightmost extent of instruction.
          di->extent_x_right * pixels_per_cycle;

        if ( x2 < x1 ) continue;

        const int y1 =  // Top of instruction.
          di->yp;
        if( y1 < 0 ) continue;

        if( obscure )
          {
            gdk_gc_set_foreground(gc,&app.gdk_obscure);
            vpos.gdk_draw_rectangle(ww,gc,true,x1,y1,x2-x1,pixels_per_inst);
            continue;
          }

        const int y2 = y1 + pixels_per_inst;
        const int ymid = y1 + ( pixels_per_inst >> 1 );

        // Coordinates of left-side highlighting arrow-like things.
        //
        int x1a = x1 - gap;   // Arrow start (flat side).
        int x1b = x1a - gap;  // Where tapering starts.
        int x1c = x1b - gap;  // End of arrow (pointy side).

        // Coordinates of right-side highlighting arrow-like things.
        //
        int x2a = x2 + gap;
        int x2b = x2a + gap;
        int x2c = x2b + gap;

        // If too small to show clearly plot a box instead of an arrow.
        //
        if( !ps && pixels_per_inst < 4 ) { x1b = x1c;  x2b = x2c; }
        const int offset = x1a - x1c;

        if( hl_dead )
          {
            gdk_gc_set_foreground(gc,app.c_ovr_plot_shadow);
            if( pixels_per_inst >= 4 )
              {
                const int pitch = 
                  si->dests_unused_direct
                  ? pixels_per_cycle : pixels_per_cycle << 1;
                const int sep = pixels_per_inst >> 2;
                for(int x = x1 - ( pixels_per_cycle>>1 ); x < x2;  x += pitch )
                  {
                    const int next_x = x + pixels_per_cycle - 1;
                    PGDK_Points<2> p(x,y1+sep); p.xy(next_x,y2-sep);
                    vpos.gdk_draw_lines(ww,gc,p,p);
                    PGDK_Points<2> p2(next_x,y1+sep); p2.xy(x,y2-sep);
                    vpos.gdk_draw_lines(ww,gc,p2,p2);
                  }
              }
            else
              vpos.gdk_draw_rectangle(ww,gc,true,x1,y1,x2-x1,pixels_per_inst);
          }

        for( Feature feature(NULL); features.pop(feature); )
          {
            // Trace outline of left-side arrow and plot.
            //
            PGDK_Points<10> p(x1c,ymid);
            p.xy(x1b,y1);  p.x(x1a);  p.y(y2);  p.x(x1b);  p.xy(x1c,ymid);
            if( feature.color_inside )
              {
                gdk_gc_set_foreground(gc,feature.color_inside);
                vpos.gdk_draw_polygon(ww,gc,true,p,p);
              }
            gdk_gc_set_foreground(gc,feature.color);
            vpos.gdk_draw_polygon(ww,gc,feature.fill,p,p);

            p.reset(x2c,ymid);
            p.xy(x2b,y1);  p.x(x2a);  p.y(y2);  p.x(x2b);  p.xy(x2c,ymid);
            if( feature.color_inside )
              {
                gdk_gc_set_foreground(gc,feature.color_inside);
                vpos.gdk_draw_polygon(ww,gc,true,p,p);
                gdk_gc_set_foreground(gc,feature.color);
              }
            vpos.gdk_draw_polygon(ww,gc,feature.fill,p,p);

            x1a -= offset; x1b -= offset; x1c -= offset;
            x2a += offset; x2b += offset; x2c += offset;
          }
      }

  Insn_Partition_Info* const cursor_pi =
    cursor_di ? cursor_di->insn_partition_info : NULL;

  // Highlight snip boundaries and type.
  //
  if( !animating_insn && ped_view && !seg_rob_ped_animating
      && ( plot_high_snips || plot_high_snip_boundaries )
      && cursor_pi && cursor_idx >= 0 && view_last_idx && sd->snip_count > 1 )
    {
      const int snip_x_offset = -6*gap;
      const int ss_current = cursor_pi->snip_serial;
      Insn_Partition_Info* const cursor_prev_pi = cursor_pi->snip_prev;
      Insn_Partition_Info* const cursor_next_pi = cursor_pi->snip_next;
      Insn_Dataset_Info *di_last = NULL;

      const int cursor_prev_ss =
        cursor_prev_pi ? cursor_prev_pi->snip_serial : ss_current - 1;
      const int cursor_next_ss =
        cursor_next_pi ? cursor_next_pi->snip_serial : ss_current + 1;

      int ss_last = 0;

      for( int idx = view_first_idx; idx <= view_last_idx; idx++ )
        {
          Insn_Dataset_Info* const di = idx_to_info[idx];
          Insn_Partition_Info* const pi = di->insn_partition_info;
          if( !pi ) continue;
          const int snip_serial = pi->snip_serial;
          const int x1 = di->rob_enqueue_c_envelope * pixels_per_cycle;
          const int y1 = di->yp - 1;

          if( plot_high_snips )
            {
              GdkColor *marker_color = NULL;

              if( ss_current == snip_serial )
                marker_color = &app.gdk_color_snip_pointed;
              else if( snip_serial == cursor_prev_ss )
                marker_color = &app.gdk_color_snip_pointed_pred_1;
              else if( snip_serial < cursor_prev_ss )
                marker_color = &app.gdk_color_snip_pointed_pred_x;
              else if( snip_serial == cursor_next_ss )
                marker_color = &app.gdk_color_snip_pointed_succ_1;
              else
                marker_color = &app.gdk_color_snip_pointed_succ_x;

              const int w = pi->attributes & 0x12 ? gap : gap >> 1;
              const bool fill = ! (pi->attributes & 9);

              gdk_gc_set_foreground(gc, marker_color);
              vpos.gdk_draw_rectangle
                (ww, gc, fill, x1+snip_x_offset, y1, w, pixels_per_inst);
            }

          if( plot_high_snip_boundaries && ss_last != snip_serial )
            {
              Insn_Partition_Info *si =
                 sd->partition_sequence_to_info[di->insn_partition_sequence];
              GdkColor *sep_color = NULL;
              const int sep_x1 = x1 + snip_x_offset - 2*gap;
              const int sep_x2 = x1 + snip_x_offset + gap - 1;

              if( si->snip_prev == NULL )
                sep_color = &app.gdk_black;
              else if( ss_last == si->snip_prev->snip_serial )
                sep_color = &app.gdk_color_snip_separator_in_order;
              else if( snip_serial > ss_last )
                sep_color = &app.gdk_color_snip_separator_gap;
              else
                sep_color = &app.gdk_color_snip_separator_out_of_order;

              {
                GdkPoint pts[2];
                pts[0].x = sep_x1;     pts[0].y = y1;
                pts[1].x = sep_x2;     pts[1].y = y1;
                gdk_gc_set_foreground(gc, sep_color);
                vpos.gdk_draw_lines(ww, gc, pts, 2);
              }

              if( di->tag == si->tag_lowest )
                {
                  gdk_gc_set_foreground(gc, sep_color);
                  vpos.gdk_draw_rectangle(ww, gc, true, sep_x1, y1,
                                          gap, pixels_per_inst);
                }

              if( si->snip_prev && di_last &&
                  di_last->tag == si->snip_prev->tag_highest )
                  vpos.gdk_draw_rectangle(ww, gc, true,
                                          sep_x1, y1 - pixels_per_inst,
                                          gap, pixels_per_inst);
            }
           ss_last = snip_serial;
           di_last = di;
        }
    }

  // Draw a line indicating the cycle search target.
  //
  if( search_target_type == STT_Cycle )
    {
      const int target_c = ac_to_c(search_tag_or_cycle_absolute);
      if( target_c >= view_start_c & target_c <= view_end_c )
        {
          gdk_gc_set_foreground(gc,&app.gdk_search_hit.gdkcolor);
          const int x1 = c_to_p(target_c);
          const int x2 = c_to_p(target_c+1);
          PGDK_Points<5> p(x1,vpos.top_pos);
          p.y(vpos.bottom_pos);
          if( pixels_per_cycle > 2 )
            {
              p.x(x2);  p.y(vpos.top_pos);  p.x(x1);
            }
          vpos.gdk_draw_lines(ww,gc,p,p);
        }
    }

  // Draw a cycle-width vertical box around instructions under the
  // mouse pointer. (These instructions are in the ROB.) The color of
  // the box is based on how much empty space there is in the
  // ROB. (RED means none or almost none, etc.)
  //
  if( ped_view ) do {
    int pointer_ridx_maybe = *sd->rri_idx[pointer_c];
    if( pointer_ridx_maybe == -1 ) break;
    while( pointer_ridx_maybe >= 0 )
      {
        ROB_Rect_Info *rri = sd->rob_rectangle_info[pointer_ridx_maybe];
        if( rri->y1 >= 0 ) break;
        pointer_ridx_maybe++;
      }
    const int pointer_ridx = pointer_ridx_maybe;

    ROB_Rect_Info *oldest_insn = sd->rob_rectangle_info[pointer_ridx];
    int next_c = pointer_c;
    int next_ridx = sd->rri_idx.next_used(next_c)[0];
    if( next_ridx == -1 ) break;
    ROB_Rect_Info *youngest_insn = sd->rob_rectangle_info[next_ridx-1];
    if( youngest_insn->x1 != oldest_insn->x1 ) break;
    int occ = 0;
    const int x1 = pointer_x_p / pixels_per_cycle * pixels_per_cycle;
    const int x2 = x1 + pixels_per_cycle;

    if( !ps ) {
      for(int i = pointer_ridx;  i < next_ridx;  i++)
        {
          ROB_Rect_Info *rri = sd->rob_rectangle_info[i];
          occ += rri->y2 - rri->y1;
        }
      GdkColor *color;
      if( occ >= ds->dsvv_rob_size )
        color = &app.gdk_red;
      else if ( occ >= ds->dsvv_rob_size - ds->dsvv_issue_width )
        color = &app.gdk_amber;
      else if ( occ >= ds->dsvv_rob_size >> 1 )
        color = &app.c_rob_half_full;
      else
        color = &app.c_rob_half_empty;
      gdk_gc_set_foreground(gc,color);

      PGDK_Points<5> p(x1,vpos.top_pos);
      p.y(vpos.bottom_pos);
      if( pixels_per_cycle > 2 )
        {
          p.x(x2);  p.y(vpos.top_pos);  p.x(x1);
        }
      vpos.gdk_draw_lines(ww,gc,p,p);
    }
  } while( 0 );

  vpos.ps_clip_to_plot_area_end();

  cursor_at_limit = cursor_idx == 0 || cursor_idx == idx_max;
  gtk_widget_set_sensitive(seg_bttn_prev_insn,cursor_idx > 0);
  gtk_widget_set_sensitive(seg_bttn_next_insn,cursor_idx < idx_max);

  // Update count of annotated instructions and partitions
  // appearing in disassembly pane.
  //
  if( cursor_idx >= 0 && view_last_idx )
    for(int idx = view_first_idx; idx <= view_last_idx; idx++)
      {
        const bool as = idx >= dis_first_idx && idx <= dis_last_idx;
        Insn_Dataset_Info* const di = idx_to_info[idx];
        for( Annotation_Iterator ai(ds,sd,di); ai; ai++ )
          {
            ai.def->use_count_last_repaint_plot++;
            if( as ) ai.def->use_count_last_repaint_assembler++;
          }
        Insn_Partition_Info* const pi = di->insn_partition_info;
        if( !pi ) continue;
        for( Annotation_Iterator ai(ds,sd,pi); ai; ai++ )
          {
            ai.def->use_count_last_repaint_plot++;
            if( as ) ai.def->use_count_last_repaint_assembler++;
          }
      }

#define SET_STATE(LOC)                                                        \
  if( ad->letter_##LOC )                                                      \
    {                                                                         \
      const bool sensitive = ad->use_count_last_repaint_##LOC;                \
      gtk_widget_set_sensitive(ad->letter_##LOC, sensitive);                  \
      gtk_widget_modify_bg                                                    \
        ( ad->letter_##LOC, GTK_STATE_NORMAL,                                 \
          cursor_is_locked && ad->pointed && ad->cursor                       \
          ? am->bg_letter_cursor_and_pointer                                  \
          : ad->pointed ? app.c_pointer_bg                                    \
          : cursor_is_locked && ad->cursor                                    \
          ? app.c_cursor_locked_bg : am->bg_letter_normal );                  \
    }

  while(Annotation_Definition* const ad = am->annotation_definitions.iterate())
    {
      SET_STATE(plot);
      SET_STATE(assembler);
      SET_STATE(message);
    }
# undef SET_STATE

  vpos.finish_ps();
  const int reference_idx = idx_reference_get();
  const int reference_pos = idx_to_vertical_position(reference_idx);
  view.top_idx = reference_idx - reference_pos;
  seg_plot_paint_variables_valid = true;
}

void
PSE_Window::paint_ovr(GtkWidget *w, GdkEventExpose *ee)
{
  GdkWindow *win = w->window;

  gint w_height, w_width;
  gdk_window_get_size(win,&w_width,&w_height);
  gdk_window_set_cursor(draw_area_ovr_win,app.cursor_arrow);
  gdk_window_clear_area
    (win, ee->area.x, ee->area.y, ee->area.width, ee->area.height);

  Auto_List<Segment_Data> &sdata = ds->sdata;

  VPos vpos;
  vpos.init(this,w,gc,win,ee);

  if( ovr_pointed_segment )
    {
      vpos.printf
        (rnk_label, "Segment %s, Rank %d",
         ovr_pointed_segment->segment_label, 1+ovr_pointed_segment->rank);

      ds->opm->pane_value_labels_update(this,ovr_pointed_segment);

      //CJM 2004-08-24 Tue 14:34:
      //  These labels will eventually only be visible in segment view.
      vpos.printf(ipc_label,"");

      vpos.printf(pos_label,"Time  %s to %s cyc  Insn %s to %s",
                  comma(ovr_pointed_segment->seg_start_ac),
                  comma(ovr_pointed_segment->seg_end_ac),
                  comma(ovr_pointed_segment->seg_start_pcount),
                  comma(ovr_pointed_segment->seg_start_pcount
                        +ovr_pointed_segment->icount));

      vpos.printf(grd_label,"Tag  %s to %s",
                  ovr_pointed_segment->seg_start_tag >= 0 ?
                  comma(ovr_pointed_segment->seg_start_tag) : "?",
                  ovr_pointed_segment->seg_end_tag >= 0 ?
                  comma(ovr_pointed_segment->seg_end_tag) : "?"
                  );

      vpos.printf(sta_label, "Data Size %s  Compression %.1fx",
                  comma(ovr_pointed_segment->data_uncompr_size),
                  double(ovr_pointed_segment->data_uncompr_size) /
                  ovr_pointed_segment->data_compr_size);
      if( ds->readex )
        {
          vpos.printf
            (src_label,"Top Insn: %s",
             ovr_pointed_segment->source_most_frequent_insn.s);
          if( ovr_pointed_segment->source_most_frequent_call_insn.len() )
            vpos.printf
              (frq_label,"Top Call: %s",
               ovr_pointed_segment->source_most_frequent_call_insn.s);
          else
            vpos.printf(frq_label,"");
        }
      else
        {
          vpos.printf(src_label,"First Insn: %s",ovr_pointed_segment->source);
          vpos.printf
            (frq_label,"Top Insn: %s",
             ovr_pointed_segment->source_most_frequent_insn.s);
        }
    }

  ds->ipc_plot_left_margin = 5;
  ds->ipc_plot_right_margin = 5;

  const int vertical_margin = w_height > 20 ? 5 : 0;
  const int left_margin     = ds->ipc_plot_left_margin;
  const int right_margin    = ds->ipc_plot_right_margin;
  const int plot_width      = w_width - left_margin - right_margin;
  const int plot_top        = vertical_margin;
  ipc_plot_top_margin       = plot_top;
  const int plot_height     = w_height - 2 * vertical_margin;
  const int plot_bottom     = w_height - vertical_margin;

  if( plot_width < 1 ) return;

  const double delta_x  = (double)plot_width / sdata.occ;
  const double delta_x_min = 4.0;
  const int dq = max(1,int(delta_x) >> 2);

  if( delta_x < delta_x_min )
    {
      int new_width = left_margin + right_margin + int(delta_x_min) * sdata.occ;
      gtk_widget_set_size_request(w, new_width, -1);
      ASSERTS( ovr_window_resize_count < 2 );
      ovr_window_resize_count++;
      return;
    }

  const gint radius  = delta_x < 2 ? 1 : ((gint)delta_x)>>1;
  const gint diameter = radius << 1;
  const gint offset_amount = radius < 8 ? 1 : radius >> 3;
  GdkColor* const bg_prelight = &w->style->bg[GTK_STATE_PRELIGHT];

  Segment_Data **sd_base = plot_ovr_by_ipc ? sdata.storage : ds->sdata_chron;

  const int num_ps = ds->opm->plot_series_num;
  const int first_seg = max( int((ee->area.x-left_margin)/delta_x ) - 1, 0);
  const double first_pos_x = left_margin + first_seg * delta_x;
  const double pos_x_limit = ee->area.x + ee->area.width;
  bool first_series_plotted = false;

  for(int i = 0; i < num_ps; i++)
    {
      Ovr_Plot_Series* const ops = &ds->opm->plot_series[i];
      if( !gtk_toggle_action_get_active(ops->display_toggle) ) continue;

      gint last_pos_x = -1, last_pos_y = 0;
      double pos_x = first_pos_x;  // The left edge of the plotted points.
      double r_min = ops->range_min_get();
      double r_max = ops->range_max_get();
      double ovr_delta_y = (r_max==r_min) ? 0 : plot_height/(r_max-r_min);
      bool shading = false;
      bool continuous_indexing = true;

      for(int j = first_seg; j < sdata.occ && pos_x < pos_x_limit; j++)
        {
          Segment_Data* const sd_i = sd_base[j];
          const gint pos_y = // The center of the plotted points.
            plot_bottom
            - gint((sd_i->ovr_plot_value[ops->value_index]-r_min)*ovr_delta_y);
          const int shadow =
             ovr_pointed_segment == sd_i || sd_i->rob_rectangle_info.size;
          const int offset = shadow ? offset_amount : 0;

          if( !first_series_plotted && ovr_pointed_segment == sd_i )
            {
              gdk_gc_set_foreground(gc, bg_prelight);
              gdk_draw_rectangle(win, gc, true, (int)pos_x, plot_top,
                                 (int)delta_x, plot_height);
            }
          if( !first_series_plotted
              && search_pc != PC_NEVER && !sd_i->search_indexed )
            {
              gdk_gc_set_foreground(gc, &app.gdk_red);
              gdk_draw_rectangle
                (win, gc, true,
                 int(pos_x) + dq, plot_top, dq << 1, plot_height);
            }

          if ( !sd_i->search_indexed ) continuous_indexing = false;
          if ( !first_series_plotted && continuous_indexing )
            {
              if ( j > first_seg 
                   && abs( sd_base[j-1]->source_most_frequent_pc
                           - sd_i->source_most_frequent_pc ) > 4 * 100 )
                shading = !shading;

              gdk_gc_set_foreground
                (gc, shading ? &app.c_ovr_bg_gray_0 : &app.c_ovr_bg_gray_1 );
              gdk_draw_rectangle
                (win, gc, true,
                 int(pos_x)- dq, plot_top, delta_x + 1, plot_height);
            }

          if( !first_series_plotted && search_segment_hit[sd_i->segment_num] )
            {
              gdk_gc_set_foreground(gc, &app.gdk_search_hit.gdkcolor);
              gdk_draw_rectangle
                (win, gc, true,
                 int(pos_x) + dq, plot_top, dq << 1, plot_height);
            }

          if( shadow )
            {
              gdk_gc_set_foreground(gc,app.c_ovr_plot_shadow);
              gdk_draw_arc
                (win, gc, sd_i != sd,
                 (gint)pos_x, pos_y-radius, diameter, diameter, 0, 360<<6);
            }

          if( sd_i->rob_rectangle_info.size )
             gdk_gc_set_foreground(gc, &ops->color_visited);
          else
             gdk_gc_set_foreground(gc, ops->color_default);

          if( gtk_toggle_action_get_active(ops->connect_toggle) )
            {
              if( last_pos_x >= 0 )
                gdk_draw_line
                  (win, gc, (gint) pos_x + radius,
                   pos_y, last_pos_x + radius, last_pos_y);

              last_pos_x = (gint) pos_x;
              last_pos_y = pos_y;
            }

          gdk_draw_arc(win, gc, sd_i != sd,
                       (gint) pos_x-offset, pos_y-radius-offset,
                       diameter, diameter, 0, 360<<6);

          pos_x += delta_x;
        }
      first_series_plotted = true;
    }
}
