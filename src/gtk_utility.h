//  -*- c++ -*-
/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2014 Louisiana State University

// $Id$

//
// GTK Support
//

#ifndef GTK_UTILITY_H
#define GTK_UTILITY_H

#include <gtk/gtk.h>
#include "misc.h"


///
/// GTK Object Pointer Classes
///

#if 0

// Types for GTK objects that don't require casts.  For example, do this:

PGtkLabelPtr label = gtk_label_new("Hello");
gtk_label_set_justify(label,GTK_JUSTIFY_LEFT);
PGtkVBoxPtr vbox = gtk_vbox_new(true,1);
gtk_box_pack_start(vbox,label,false,true,0);

// instead of

GtkWidget *label = gtk_label_new("Hello");
GtkWidget *vbox = gtk_vbox_new(true,1);
gtk_label_set_justify(GTK_LABEL(label),GTK_JUSTIFY_LEFT);
gtk_box_pack_start(GTK_BOX(vbox),label,false,true,0);

#endif

class PGtkWidgetPtr {
public:
  PGtkWidgetPtr(GtkWidget* w):w(w){};
  PGtkWidgetPtr():w(NULL){};
  GtkWidget* w;
  operator GtkWidget* () const { return w; }
  operator GTypeInstance* () const { return (GTypeInstance*)w; }
  operator void* () const { return (void*)w; }
  operator bool () const { return w; }
};

class PGObjectPtr {
public:
  PGObjectPtr(GObject* w):w(w){};
  PGObjectPtr():w(NULL){};
  GObject* w;
  operator GObject* () const { return w; }
  operator GTypeInstance* () const { return (GTypeInstance*)w; }
  operator void* () const { return (void*)w; }
  operator bool () const { return w; }
};

#define PGTKPTR_DECLARE(child_gtype,cast,parent_gtype)                        \
class P##child_gtype##Ptr: public P##parent_gtype##Ptr {                      \
public:                                                                       \
  P##child_gtype##Ptr(){ w = NULL; }                                          \
  P##child_gtype##Ptr(GtkWidget* a){ w = (GtkWidget*)cast(a); }               \
  P##child_gtype##Ptr(child_gtype* a){ w = (GtkWidget*)cast(a); }             \
  P##child_gtype##Ptr operator = (GtkWidget*a)                                \
  { w = (GtkWidget*) cast(a); return (P##child_gtype##Ptr) w; }               \
  operator child_gtype* () const { return (child_gtype*)w; }                  \
  child_gtype* operator -> () const { return (child_gtype*)w; }               \
};

#define PGTKoPTR_DECLARE(child_gtype,cast,parent_gtype)                       \
class P##child_gtype##Ptr: public P##parent_gtype##Ptr {                      \
public:                                                                       \
  P##child_gtype##Ptr(){ w = NULL; }                                          \
  P##child_gtype##Ptr(child_gtype* a){ w = (GObject*)cast(a); }               \
  P##child_gtype##Ptr(GObject* a){ w = (GObject*)cast(a); }                   \
  P##child_gtype##Ptr operator = (child_gtype*a)                              \
  { w = (GObject*) cast(a); return (P##child_gtype##Ptr) w; }                 \
  operator child_gtype* () const { return (child_gtype*)w; }                  \
  child_gtype* operator -> () const { return (child_gtype*)w; }               \
};

PGTKPTR_DECLARE(GtkMisc,GTK_MISC,GtkWidget);
PGTKPTR_DECLARE(GtkLabel,GTK_LABEL,GtkMisc);
PGTKPTR_DECLARE(GtkContainer,GTK_CONTAINER,GtkWidget);
PGTKPTR_DECLARE(GtkTable,GTK_TABLE,GtkContainer);
PGTKPTR_DECLARE(GtkBin,GTK_BIN,GtkContainer);
PGTKPTR_DECLARE(GtkAlignment,GTK_ALIGNMENT,GtkBin);
PGTKPTR_DECLARE(GtkEventBox,GTK_EVENT_BOX,GtkBin);
PGTKPTR_DECLARE(GtkScrolledWindow,GTK_SCROLLED_WINDOW,GtkBin);
PGTKPTR_DECLARE(GtkButton,GTK_BUTTON,GtkBin);
PGTKPTR_DECLARE(GtkToggleButton,GTK_TOGGLE_BUTTON,GtkButton);
PGTKPTR_DECLARE(GtkCheckButton,GTK_CHECK_BUTTON,GtkToggleButton);
PGTKPTR_DECLARE(GtkRadioButton,GTK_CHECK_BUTTON,GtkCheckButton);
PGTKPTR_DECLARE(GtkBox,GTK_BOX,GtkContainer);
PGTKPTR_DECLARE(GtkHBox,GTK_HBOX,GtkBox);
PGTKPTR_DECLARE(GtkVBox,GTK_VBOX,GtkBox);
PGTKPTR_DECLARE(GtkPaned,GTK_PANED,GtkContainer);
PGTKPTR_DECLARE(GtkHPaned,GTK_HPANED,GtkPaned);
PGTKPTR_DECLARE(GtkVPaned,GTK_VPANED,GtkPaned);
PGTKPTR_DECLARE(GtkWindow,GTK_WINDOW,GtkBin);
PGTKPTR_DECLARE(GtkDialog,GTK_DIALOG,GtkWindow);
PGTKPTR_DECLARE(GtkDrawingArea,GTK_DRAWING_AREA,GtkWidget);
PGTKPTR_DECLARE(GtkAspectFrame,GTK_ASPECT_FRAME,GtkContainer);

PGTKPTR_DECLARE(GtkItem,GTK_ITEM,GtkBin);
PGTKPTR_DECLARE(GtkMenuItem,GTK_MENU_ITEM,GtkItem);
PGTKPTR_DECLARE(GtkCheckMenuItem,GTK_CHECK_MENU_ITEM,GtkMenuItem);
PGTKPTR_DECLARE(GtkRadioMenuItem,GTK_RADIO_MENU_ITEM,GtkCheckMenuItem);

PGTKPTR_DECLARE(GtkFrame,GTK_FRAME,GtkBin);

PGTKPTR_DECLARE(GtkEntry,GTK_ENTRY,GtkWindow);

PGTKPTR_DECLARE(GtkToolbar,GTK_TOOLBAR,GtkContainer);
PGTKPTR_DECLARE(GtkImage,GTK_IMAGE,GtkMisc);

PGTKPTR_DECLARE(GtkToolItem,GTK_TOOL_ITEM,GtkBin);
PGTKPTR_DECLARE(GtkToolButton,GTK_TOOL_BUTTON,GtkToolItem);
PGTKPTR_DECLARE(GtkToggleToolButton,GTK_TOGGLE_TOOL_BUTTON,GtkToolButton);
PGTKPTR_DECLARE(GtkSeparatorToolItem,GTK_SEPARATOR_TOOL_ITEM,GtkToolItem);

PGTKoPTR_DECLARE(GtkAction,GTK_ACTION,GObject);
PGTKoPTR_DECLARE(GtkToggleAction,GTK_TOGGLE_ACTION,GtkAction);
PGTKoPTR_DECLARE(GtkRadioAction,GTK_RADIO_ACTION,GtkToggleAction);

///
/// Hook List Wrapper
///

class PGHookList
{
public:
  PGHookList()
  {
    g_hook_list_init(&hook_list,sizeof(GHook));
  }
  operator GHookList* () { return &hook_list; }
  void append(GHookFunc func, void *data)
  {
    GHook* const hook = g_hook_alloc(&hook_list);
    hook->func = (gpointer)func;
    hook->data = data;
    g_hook_append(&hook_list,hook);
  }
  void invoke()
  {
    g_hook_list_invoke(&hook_list,false);
  }
private:
  GHookList hook_list;
};


///
/// Label With Alignment and Event Box
///

struct PLabelEventBoxPtr {
  PLabelEventBoxPtr(const char* text = NULL, bool markup = false):
    label(gtk_label_new( markup ? NULL : text)),
    alignment(gtk_alignment_new(0,1,0,0)),
    event_box(gtk_event_box_new())
  {
    if( markup ) gtk_label_set_markup(label,text);
    gtk_container_add(alignment,label);
    gtk_container_add(event_box,alignment);
  }
  operator GtkAlignment* () const { return alignment; }
  operator GtkEventBox* () const { return event_box; }
  operator GtkWidget* () const { return event_box; }
  operator GtkLabel* () const { return label; }
  operator void* () const { return (void*) event_box; }
  const PGtkLabelPtr label;
  const PGtkAlignmentPtr alignment;
  const PGtkEventBoxPtr event_box;
};

///
/// Suppress Handler
///
//
// So, you want to set a toggle button to true but not run its handler.
// Don't happen to have it's signal ID?  No, problem, just instantiate
// one of these.
//
class PGtk_Suppress_Handler {
public:
  PGtk_Suppress_Handler(gpointer object, gpointer handler):
    object(object),
    signal_id(g_signal_handler_find
              (object,G_SIGNAL_MATCH_FUNC, 0, 0, 0, handler, NULL))
  {
    g_signal_handler_block(object,signal_id);
  }
  ~PGtk_Suppress_Handler()
  {
    g_signal_handler_unblock(object,signal_id);
  }
  const gpointer object;
  const gulong signal_id;
};


///
/// Other Functions
///

// Process gtk/gdk events until queue empty.
// Note: Higher numbers indicate lower priority.
// Default idle timer priority is 200.
void yield_to_gtk(int priority = 150);

// Check if keyboard modifier was down when event recorded.
//
#define META_IS_DOWN(e) meta_is_down((GdkEvent*)(e))
#define SHIFT_IS_DOWN(e) shift_is_down((GdkEvent*)(e))
#define CONTROL_IS_DOWN(e) control_is_down((GdkEvent*)(e))
// Any modifier.
#define AMODIFIER_IS_DOWN(e) amodifier_is_down((GdkEvent*)(e))

bool meta_is_down(GdkEvent *e);
bool shift_is_down(GdkEvent *e);
bool control_is_down(GdkEvent *e);
bool amodifier_is_down(GdkEvent *e);

//
// Warp Pointer -- Full access to XWarpPointer, warp only if
// pointer is within source rectangle
//
void
pgtk_XWarpPointer(GdkWindow *display, GdkWindow *src_w, GdkWindow *dest_w,
                  int src_x, int src_y,
                  unsigned src_width, unsigned src_height,
                  int dest_x, int dest_y);

//
// Intrawindow relative or absolute warp only
//
void
pgtk_XWarpPointer(GtkWidget *widget, int dest_x, int dest_y);

//
// Simple rectangle functions.
//
class p_rectangle {
public:
  int x1, x2, y1, y2;
  void setxywh(int x1p, int y1p, int wp, int hp)
  { x1=x1p; y1=y1p; x2=x1+wp; y2=y1+hp;}
  int wd(){return x2 - x1;}
  int ht(){return x2 - x1;}
  bool in(double x, double y){ return in(int(x),int(y)); }
  bool in(int x, int y)
  {return x >= x1 && x <= x2 && y >= y1 && y <= y2;}
};

//
// Label-Like Widget with ellipsis and tear-off grab.
//

class PGTK_Label_Ellipsomatic {
public:
  PGTK_Label_Ellipsomatic(){};
  void init(GtkWidget *label_mainp)
  {
    label_main = label_mainp;
    label_ellipsis = gtk_label_new("...");
    gint xpad, ypad;
    gtk_misc_get_padding(GTK_MISC(label_main),&xpad,&ypad);
    gtk_misc_set_padding(GTK_MISC(label_ellipsis),xpad,ypad);
    max_width = 0;  max_height = 0;

    hbox = gtk_hbox_new(false,0);
    handle_box = gtk_handle_box_new();
    gtk_handle_box_set_handle_position
      (GTK_HANDLE_BOX(handle_box),GTK_POS_LEFT);
    attached = true;
    gtk_box_pack_start(GTK_BOX(hbox), label_ellipsis, false, false, 3);
    gtk_box_pack_start(GTK_BOX(hbox), label_main, true, true, 3);
    gtk_label_set_markup
      (GTK_LABEL(label_ellipsis), "<span background=\"green\">+</span>");
    gtk_widget_hide(label_ellipsis);
    gtk_container_add(GTK_CONTAINER(handle_box), hbox);
    gtk_widget_set_size_request(hbox,-1,5);
    const gpointer thisp = gpointer(this);
    g_signal_connect
     (G_OBJECT(handle_box), "child-attached",
      G_CALLBACK(evnt_cb_handlebox_attach), thisp);
    g_signal_connect
     (G_OBJECT(handle_box), "child-detached",
      G_CALLBACK(evnt_cb_handlebox_detach), thisp);
    g_signal_connect
     (G_OBJECT(label_main), "size-request",
      G_CALLBACK(evnt_cb_size_request), thisp);
    g_signal_connect
     (G_OBJECT(label_main), "size-allocate",
      G_CALLBACK(evnt_cb_size_allocate), thisp);
  }

  operator GtkLabel*(){ return GTK_LABEL(label_main); }
  GtkLabel* label() { return GTK_LABEL(label_main); }
  operator GtkWidget*(){ return GTK_WIDGET(handle_box); }
  bool is_attached() const { return attached; }

private:

  static void
  evnt_cb_handlebox_attach
  (GtkHandleBox *handlebox, GtkWidget *child, gpointer data)
  {
    ((PGTK_Label_Ellipsomatic*)data)->attached = true;
    gtk_widget_set_size_request(child,-1,5);
  }

  static void
  evnt_cb_handlebox_detach
  (GtkHandleBox *handlebox, GtkWidget *child, gpointer data)
  {
    PGTK_Label_Ellipsomatic* const moi = (PGTK_Label_Ellipsomatic*)data;
    moi->max_width = 100;
    moi->max_height = 0;
    moi->attached = false;
    moi->ellipsis_update();
    gtk_widget_set_size_request(child,-1,-1);
   }

  static void
  evnt_cb_size_request(GtkWidget *w, GtkRequisition *req, gpointer data)
  {
    PGTK_Label_Ellipsomatic* const moi = (PGTK_Label_Ellipsomatic*)data;
    moi->requisition = *req;
    moi->ellipsis_update();
  }

  static void
  evnt_cb_size_allocate(GtkWidget *w, GtkAllocation *allo, gpointer data)
  {
    PGTK_Label_Ellipsomatic* const moi = (PGTK_Label_Ellipsomatic*)data;
    moi->allocation = *allo;
    moi->ellipsis_update();
  }

  void
  ellipsis_update()
  {
    if( attached &&
        ( allocation.width < requisition.width
          || allocation.height < requisition.height ) )
      gtk_widget_show(label_ellipsis);
    else
      gtk_widget_hide(label_ellipsis);

    if( !attached )
      {
        max_width = min(max(max_width,requisition.width),max_keep_width);
        max_height = min(max(max_height,requisition.height),max_keep_height);
        gtk_widget_set_size_request
          (hbox,
           max(requisition.width,max_width)+6,
           max(requisition.height,max_height));
      }
  }

  static const gint max_keep_width = 500;
  static const gint max_keep_height = 150;
  gint max_width;   // Maximum width requested (up to a limit) since detach.
  gint max_height;  // Likewise.
  GtkRequisition requisition;
  GtkAllocation allocation;
  GtkWidget* handle_box;
  GtkWidget* label_ellipsis;
  GtkWidget* label_main;
  GtkWidget* hbox;
  bool attached;

};

//
// GtkToggleButton that can be used like a Boolean variable.
//
class PGTK_Toggle_Button {
public:
  PGTK_Toggle_Button(){ b = NULL; }
  PGTK_Toggle_Button(GtkToggleButton *b):b(b){}
  GtkToggleButton *b;
  bool operator = (bool st){ gtk_toggle_button_set_active(b,st); return st;}
  GtkWidget* operator = (GtkWidget *bp){ b = GTK_TOGGLE_BUTTON(bp); return bp;}
  PGTK_Toggle_Button& operator = (PGTK_Toggle_Button& b2)
  {
    gtk_toggle_button_set_active(b,b2);
    return b2;
  }
  operator bool() const { return gtk_toggle_button_get_active(b); }
  operator GtkToggleButton*() const { return b; }
  operator GtkWidget*() const { return GTK_WIDGET(b); }
  operator GTypeInstance* () const { return (GTypeInstance*)b; }
  operator void* () const { return (void*)b; }
};

//
// GtkToggleButton that can be used like a Boolean variable.
//
class PGTK_Toggle_Tool_Button {
public:
  PGTK_Toggle_Tool_Button(){ b = NULL; }
  PGTK_Toggle_Tool_Button(GtkToggleToolButton *b):b(b){}
  PGtkToggleToolButtonPtr b;
  bool operator = (bool st)
  { gtk_toggle_tool_button_set_active(b,st); return st;}
  GtkWidget* operator = (GtkWidget *bp){ b = bp; return bp;}
  PGTK_Toggle_Tool_Button& operator = (PGTK_Toggle_Tool_Button& b2)
  {
    gtk_toggle_tool_button_set_active(b,b2);
    return b2;
  }
  operator bool() const { return gtk_toggle_tool_button_get_active(b); }
  operator GtkToggleToolButton*() const { return b; }
  operator GtkWidget*() const { return b; }
};

//
// Class for constructing a list of points WITH MINIMUM OVERHEAD.
//
template <int num> class PGDK_Points {
public:
  PGDK_Points(gint x, gint y){reset(x,y);}
  PGDK_Points(){pt=0;};
  void reset(gint x, gint y) {pts[0].x = x;  pts[0].y = y;  pt = 1;}
  void l(gint dx){pts[pt].x=pts[pt-1].x + dx;  pts[pt].y=pts[pt-1].y; pt++;}
  void x(gint x){pts[pt].x = x;  pts[pt].y = pts[pt-1].y; pt++;}
  void r(gint dx){pts[pt].x=pts[pt-1].x - dx;  pts[pt].y=pts[pt-1].y; pt++;}
  void d(gint dy){pts[pt].x=pts[pt-1].x;  pts[pt].y=pts[pt-1].y + dy; pt++;}
  void y(gint y){pts[pt].x = pts[pt-1].x;  pts[pt].y = y; pt++;}
  void xy(gint x, gint y){pts[pt].x = x;  pts[pt].y = y; pt++;}
  operator GdkPoint*(){ return pts; }
  operator int(){ return pt; }
private:
  GdkPoint pts[num];
  int pt;
};


// Discard any current attributes and set background on LABEL to LABEL_BG.
// If label_bg NULL, remove all attributes.
//
void pgtk_label_set_background(GtkLabel *label, const GdkColor *label_bg);
void pgtk_label_set_foreground(GtkLabel *label, const GdkColor *label_bg);
inline void
pgtk_label_set_background(GtkWidget *label, const GdkColor *label_bg)
{ pgtk_label_set_background(GTK_LABEL(label),label_bg); }
inline void
pgtk_label_set_foreground(GtkWidget *label, const GdkColor *label_bg)
{ pgtk_label_set_foreground(GTK_LABEL(label),label_bg); }

#endif
