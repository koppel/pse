/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2014 Louisiana State University

// $Id$

//
// PSE Setup
//

#include <gtk/gtk.h>
#include <strings.h>
#include <malloc.h>
#include <gdk/gdkkeysyms.h>
#include "pse_window.h"
#include "messages.h"
#include "handlers.h"
#include "annotation_manager.h"
#include "ovr_plot_manager.h"
#include "rti_declarations.h"
#include "search.h"

#define CB(f) GtkItemFactoryCallback(f)

struct pGtkItemFactoryEntry {
  const gchar *path, *accel;
  GtkItemFactoryCallback cb;
  guint callback_action;
  const gchar *item_type;
  gconstpointer extra_data;
};

pGtkItemFactoryEntry menu_entries[] =
{
  { "/_File", NULL, NULL, 0, "<Branch>" },
  { "/File/_Open", "<control>O", CB(menu_cb_open), 0,
    "<StockItem>", GTK_STOCK_OK },
  { "/File/_Close", "q", CB(menu_cb_close), 0, 
    "<StockItem>", GTK_STOCK_CLOSE },
  { "/File/_Clone", "<control>N", CB(menu_cb_clone), 0, NULL },
  { "/File/--2", NULL, NULL, 0, "<Separator>" },
  { "/File/E_xport...", NULL, CB(menu_cb_ps), 0, NULL },
  { "/File/---", NULL, NULL, 0, "<Separator>" },
  { "/File/_Quit", "<control>E", CB(menu_cb_close), 1,
    "<StockItem>", GTK_STOCK_QUIT },
  { "/_Edit", NULL, NULL, 0, "<Branch>" },
  { "/Edit/_Copy", "<control>c", CB(menu_cb_copy), 'c',
    "<StockItem>", GTK_STOCK_COPY },
  { "/Edit/_Preferences ...", NULL, CB(menu_cb_pref), 0,
    "<StockItem>", GTK_STOCK_PREFERENCES },
  { "/Zoom/", NULL, NULL, 0, "<Tearoff>" },
  { "/Zoom/_Unit Scale", "0", CB(menu_cb_zoom), '0',
    "<StockItem>", GTK_STOCK_ZOOM_100 },
  { "/Zoom/Decode _Width Scale", "1", CB(menu_cb_zoom),  '1', NULL },
  { "/Zoom/Clip-n-Zoom", "i", CB(menu_cb_zoom),'c', NULL },
  { "/Zoom/Zoom _In", "z", CB(menu_cb_zoom), 'z',
    "<StockItem>", GTK_STOCK_ZOOM_IN },
  { "/Zoom/Zoom _Out", "u", CB(menu_cb_zoom), 'u', 
    "<StockItem>", GTK_STOCK_ZOOM_OUT },
  { "/Zoom/Zoom In Inst", "<control>Up", CB(menu_cb_zoom), 'X', NULL },
  { "/Zoom/Zoom Out Inst","<control>Down", CB(menu_cb_zoom), 'Y', NULL },
  { "/Zoom/Zoom In Cyc", "<control>Right", CB(menu_cb_zoom), 'x', NULL },
  { "/Zoom/Zoom Out Cyc", "<control>Left", CB(menu_cb_zoom), 'y', NULL },
  { "/Zoom/Zoom To Disassembly Window", "w", CB(menu_cb_zoom), 'w', NULL },
  { "/_View", NULL, NULL, 0, "<Branch>" },
  { "/View/_Data ...", "d", CB(menu_cb_data), 0, NULL },
  { "/View/_State Key ...", NULL, CB(menu_cb_colorkey), 0, NULL },
  { "/View/_Annotation Preferences ...", NULL, CB(annotate_menu_callback),
    'a', NULL },
  { "/View/_Annotation Messages ...", NULL, CB(annotate_menu_callback),
    'm', NULL },
  { "/View/Overview _Plot Series Choose...", NULL,
    CB(ovr_plot_series_menu_callback), 0, NULL },
  { "/View/Overview Plot _Values Pane", NULL,CB(ovr_plot_values_menu_callback),
     0, "<CheckItem>" },
  { "/View/Dis_assembly Pane", "a", CB(menu_cb_dis_window), 0, "<CheckItem>" },
  { "/View/---", NULL, NULL, 0, "<Separator>"  },
  { "/View/Increase Text Size", "<control>equal", CB(menu_cb_font), '+', NULL },
  { "/View/Decrease Text Size", "<control>minus", CB(menu_cb_font), '-', NULL },
  { "/_Go", NULL, NULL, 0, "<Branch>" },
  { "/Go/Instruction Cursor Next", "n", CB(menu_cb_insn_cursor), 'n', NULL },
  { "/Go/Instruction Cursor Previous", "p", CB(menu_cb_insn_cursor), 'p', NULL },
  { "/Go/_Stop Scrolling", "KP_5", CB(menu_cb_animate), '5', NULL },
  { "/_Highlight", NULL, NULL, 0, "<Branch>" },
  { "/_Highlight/---", NULL, NULL, 0, "<Tearoff>" },
  { "/_Highlight/Dataflow _All", NULL, NULL, 'a', "<CheckItem>" },
  { "/_Highlight/Dataflow _Direct", NULL, NULL, 'd', "<CheckItem>" },
  { "/_Highlight/Dataflow _None", NULL, NULL, 'n', "<CheckItem>" },
  { "/_Highlight/Dataflow Through _Memory", NULL, NULL, 'm',
     "<CheckItem>" },
  { "/_Highlight/---", NULL, NULL, 0, "<Separator>"  },
  { "/_Highlight/D_ead Instructions", NULL,NULL, 'x', "<CheckItem>"},
  { "/_Highlight/---", NULL, NULL, 0, "<Separator>"  },
  { "/_Highlight/Same _Instruction", NULL, NULL, 'i', "<CheckItem>" },
  { "/_Highlight/Same _Cache Line", NULL, NULL, 'c', "<CheckItem>" },
  { "/_Highlight/_Snips", NULL, NULL, 's', "<CheckItem>" },
  { "/_Highlight/Snip _Boundaries", NULL, NULL, 'b', "<CheckItem>" },
  { "/_Highlight/---", NULL, NULL, 0, "<Separator>"  },
  { "/_Highlight/A_nnotations", NULL, NULL, 0, "<CheckItem>"},
  { "/_Help", NULL, NULL, 0, "<Branch>" },
  { "/Help/_About", NULL, CB(menu_cb_about), 0, 
    "<StockItem>", GTK_STOCK_ABOUT }
};

pGtkItemFactoryEntry context_menu_entries[] =
{
  { "/Clip", NULL, CB(menu_cb_clip), 0, "<CheckItem>" },
  { "/Clip-n-Zoom", "i", CB(menu_cb_zoom),'c', NULL },
  { "/---", NULL, NULL, 0, "<Separator>" },
  { "/Highlight Dataflow _All", NULL, NULL, 'a', "<CheckItem>" },
  { "/Highlight Dataflow _Direct", NULL, NULL, 'd', "<CheckItem>" },
  { "/Highlight Dataflow _None", NULL, NULL, 'n', "<CheckItem>" },
  { "/Highlight Dataflow Through _Memory", NULL, NULL, 'm', "<CheckItem>" },
  { "/---", NULL, NULL, 0, "<Separator>" },
  { "/_Highlight D_ead Instructions",NULL, NULL, 'x', "<CheckItem>"},
  { "/---", NULL, NULL, 0, "<Separator>" },
  { "/Highlight Same _Instruction", NULL, NULL, 'i', "<CheckItem>" },
  { "/Highlight Same _Cache Line", NULL, NULL, 'c', "<CheckItem>" },
  { "/Highlight _Snips", NULL, NULL, 's', "<CheckItem>"},
  { "/Highlight Snip _Boundaries", NULL, NULL, 'b', "<CheckItem>"},
  { "/Highlight A_nnotations",     NULL, NULL,            0,   "<CheckItem>"},
  { "/---", NULL, NULL, 0, "<Separator>" },
  { "/Zoom _In", "z", CB(menu_cb_zoom), 'z',
    "<StockItem>", GTK_STOCK_ZOOM_IN },
  { "/Zoom _Out", "u", CB(menu_cb_zoom), 'u', 
    "<StockItem>", GTK_STOCK_ZOOM_OUT },
  { "/_Zoom", NULL, NULL, 0, "<Branch>" },
  { "/Zoom/Zoom _Unit Scale", "0", CB(menu_cb_zoom), '0', 
    "<StockItem>", GTK_STOCK_ZOOM_100 },
  { "/Zoom/Zoom Decode _Width Scale", NULL, CB(menu_cb_zoom), '1', NULL },
  { "/Zoom/Zoom In Inst", "<control>Up", CB(menu_cb_zoom), 'X', NULL },
  { "/Zoom/Zoom Out Inst","<control>Down", CB(menu_cb_zoom), 'Y', NULL },
  { "/Zoom/Zoom In Cyc", "<control>Left", CB(menu_cb_zoom), 'x', NULL },
  { "/Zoom/Zoom Out Cyc", "<control>Right", CB(menu_cb_zoom), 'y', NULL },
  { "/Zoom/Zoom To Disassembly Window", "w", CB(menu_cb_zoom), 'w', NULL },
};

static GtkWidget *
insert_button(GtkWidget *bar, GtkWidget *button,
                  WCallback handler, gpointer handler_data,
                  const char *tip = NULL, WCallback handler_released = NULL)
{
  g_signal_connect(G_OBJECT(button),
                   handler_released ? "pressed" : "clicked",
                   G_CALLBACK(handler), handler_data);
  if( handler_released )
    g_signal_connect(G_OBJECT(button), "released",
                     G_CALLBACK(handler_released), handler_data);
  gtk_box_pack_start(GTK_BOX(bar), button, false, true, 0);
  if( tip ) gtk_tooltips_set_tip(GTK_TOOLTIPS(app.tips), button, tip, tip);
  return button;
}

static GtkWidget *
insert_new_toggle(GtkWidget *bar, const char *label,
                  WCallback handler, gpointer handler_data,
                  bool initial = false, const char *tip = NULL)
{
  GtkWidget *toggle = gtk_toggle_button_new_with_label(label);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(toggle),initial);
  insert_button(bar, toggle, handler, handler_data, tip);
  return toggle;
}

GtkWidget*
insert_new_button(GtkWidget *bar, const char *label,
                  WCallback handler, gpointer handler_data, const char *tip)
{
  return insert_button(bar, gtk_button_new_with_label(label),
                       handler, handler_data, tip);
}

#if 0
// Defined out to avoid "not used" warnings.  Uncomment if needed.
// 30 June 2003, 15:44:23 CDT
static GtkWidget *
insert_stock_button(GtkWidget *bar, char *stock_id,
                  WCallback handler, gpointer handler_data,
                    char *tip = NULL, WCallback handler_released = NULL)
{
  return insert_button(bar, gtk_button_new_from_stock(stock_id),
                       handler, handler_data, tip, handler_released);
}
#endif

GtkWidget*
insert_stock_icon_toggle
(GtkWidget *bar, const char *stock_id,
 WCallback handler, gpointer handler_data, bool initial, const char *tip)
{
  GtkWidget *b = gtk_toggle_button_new();
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b),initial);
  GtkWidget *w = gtk_image_new_from_stock(stock_id,
                                          GTK_ICON_SIZE_SMALL_TOOLBAR);
  gtk_container_add(GTK_CONTAINER(b),w);
  return insert_button(bar, b, handler, handler_data, tip);
}

GtkWidget *
insert_stock_icon_button(GtkWidget *bar, const char *stock_id,
                  WCallback handler, gpointer handler_data,
                  const char *tip, WCallback handler_released)
{
  GtkWidget *b = gtk_button_new();
  GtkWidget *w = gtk_image_new_from_stock(stock_id,
                                          GTK_ICON_SIZE_SMALL_TOOLBAR);
  gtk_container_add(GTK_CONTAINER(b),w);
  return insert_button(bar, b, handler, handler_data, tip, handler_released);
}

void
PSE_Window::make_title()
{
  g_free(title);

  if( ds->dataset_loaded )
    {
      RString benchmark_name;
      const char* const benchmark_id = ds->sim_var_value("benchmark_id");

      if ( benchmark_id && benchmark_id[0] )
        {
          PSplit parts(benchmark_id,'.');
          char* const suite = parts.dequeue();
          char* const rest = parts.joined_copy();
          if( rest ) benchmark_name.sprintf("%s,%s",rest,suite);
          else       benchmark_name = suite;
        }
      else if( const char* const bm_ds_name =
               ds->sim_var_value("exec_file_name") )
        {
          RString bm_rs( bm_ds_name ? bm_ds_name : "Not found");
          char *benchmark = bm_rs;
          char *b2 = &benchmark[bm_rs.len()];

          while ( b2 > benchmark && *b2 != '.' && *b2 != '/' ) b2--;
          if( *b2 == '.' )
            {
              *b2 = 0;
              while ( b2 > benchmark && *b2 != '/' ) b2--;
              if( *b2 == '/' ) b2++;
            }

          benchmark_name = b2;
        }
      else 
        {
          benchmark_name = "";
        }

      RString serial;

      char* const run_id = ds->sim_var_value("run_id");

      if( run_id && run_id[0] )
        {
          serial = run_id;
        }
      else
        {
          RString fn(ds->file_name);
          char *s2 = fn.s + fn.len() - 1;
          while ( s2 > fn.s && ( *s2 < '0' || *s2 > '9' ) ) *s2-- = 0;
          while ( s2 > fn.s && ( *s2 >= '0' && *s2 <= '9' ) ) s2--;
          if( s2 > fn.s )
            {
              serial = s2 + 1;
            }
          else if( const int32_t time_ue_ds = ds->sim_var_value("time_ue",0) )
            {
              const time_t time_ue = time_ue_ds;
              serial.strftime("%k:%M:%S %d %b %Y",time_ue);
            }
          else
            serial = "";
        }

      title = g_strdup_printf
        ("%s %s (%d) - PSE", benchmark_name.s, serial.s, window_num);
    }
  else
    {
      title = g_strdup_printf("No Dataset - PSE");
    }

  gtk_window_set_title (GTK_WINDOW(main_w), title);
}

void
PSE_Window::init_at_window_and_dataset_available()
{
  if( !ds->dataset_loaded ) return;
  if( !ds->sdata.occ ) return;
  sd = ds->sdata[ds->sdata.occ >> 1];
  view.pixels_per_cycle = view.pixels_per_inst = 4;

  ds->opm->window_and_dataset_init(this);
  search_window_and_dataset_init(this);
  gtk_widget_show(seg_labels_vbox);
}

static bool plot_label_link_cb_color
(GtkWidget *tb, GdkEventCrossing *e, GtkWidget *label)
{
  pgtk_label_set_foreground
    (label, e->type == GDK_ENTER_NOTIFY ? &app.gdk_blue : &app.gdk_black );
  return false;
}

static GtkWidget*
plot_label_link_new
(GtkWidget *box, GCallback callback, gpointer data, bool exp_fill = false)
{
  PGtkLabelPtr label(gtk_label_new(NULL));
  PGtkEventBoxPtr event_box(gtk_event_box_new());
  gtk_misc_set_alignment(label,0,0);
  gtk_misc_set_padding(label,2,2);
  gtk_label_set_selectable(label,false);
  gtk_container_add(event_box,label);
  if( box ) gtk_box_pack_start (GTK_BOX(box), event_box, exp_fill, exp_fill, 3);
  gtk_widget_add_events
    ( event_box, GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK
      | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK );
  g_signal_connect
    ( event_box, "enter-notify-event",
      G_CALLBACK(plot_label_link_cb_color), gpointer(label) );
  g_signal_connect
    ( event_box, "leave-notify-event",
      G_CALLBACK(plot_label_link_cb_color), gpointer(label) );
  g_signal_connect_swapped( event_box, "button-release-event", callback, data );
  return label;
}

static GtkWidget *
plot_label_new(GtkWidget *box, bool exp_fill = false)
{
  GtkWidget *label = gtk_label_new(NULL);
  gtk_misc_set_alignment(GTK_MISC(label),0,0);
  gtk_misc_set_padding(GTK_MISC(label),2,2);
  gtk_label_set_selectable(GTK_LABEL(label),true);
  if( box )
    gtk_box_pack_start (GTK_BOX(box), label, exp_fill, exp_fill, 3);
  return label;
}


void
PSE_Window::new_dataset()
{
  if( !ds->dataset_loaded ) FATAL;
  init_at_window_and_dataset_available();
  seg_plot_annotations_init(ds);
  make_title();

  ds->tree_widget =
    gtk_tree_view_new_with_model(GTK_TREE_MODEL(ds->data_tree));

  GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
  GtkTreeViewColumn *name_column = ds->name_column =
    gtk_tree_view_column_new_with_attributes
    ("Name",renderer,"text",0,"foreground",3,"weight",4,NULL);
  gtk_tree_view_column_set_sort_column_id(name_column,0);
  gtk_tree_view_column_set_sort_indicator(name_column,FALSE);
  gtk_tree_view_append_column(GTK_TREE_VIEW(ds->tree_widget), name_column);
  GtkTreeViewColumn *value_column =
    gtk_tree_view_column_new_with_attributes("Value", renderer,"text",1, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(ds->tree_widget), value_column);

  GtkTreeSelection *data_var_selected
    = gtk_tree_view_get_selection(GTK_TREE_VIEW(ds->tree_widget));
  g_signal_connect(G_OBJECT(data_var_selected),"changed",
                   G_CALLBACK(data_cb_selc), (gpointer) ds);
  g_signal_connect(G_OBJECT(ds->tree_widget), "button-press-event",
                   G_CALLBACK(evnt_cb_data_button_up), (gpointer) this);

  if( !ds->readex )
    {
      gtk_widget_set_sensitive(seg_menu_check_dis_pane,false);
      gtk_check_menu_item_set_active
        (GTK_CHECK_MENU_ITEM(seg_menu_check_dis_pane),false);
      gtk_widget_set_sensitive(seg_bttn_prev_insn,false);
      gtk_widget_set_sensitive(seg_bttn_next_insn,false);
      gtk_widget_hide(dis_pane_vbox);
    }


  gtk_widget_show(tool_bar_common);
  gtk_widget_show(tool_bar_ovr);
  gtk_widget_show(seg_labels_vbox);
  invalidate_draw_area();
}


static void
evnt_cb_dis_hbox_move_handle
(GtkPaned *paned, GParamSpec *param_spec, gpointer data)
{
  PSE_Window* const pw = (PSE_Window*) data;
  rtiv_disassembly_pane_width =
    gtk_paned_get_child2(pw->draw_dis_hpaned)->allocation.width;
};

static void
evnt_cb_annotation_pane_vbox_move_handle
(GtkPaned *paned, GParamSpec *param_spec, gpointer data)
{
  PSE_Window* const pw = (PSE_Window*) data;
  rtiv_annotation_message_pane_height =
    pw->annotation_data->message_pane_height =
    gtk_paned_get_child1(pw->draw_dis_vpaned)->allocation.height;
};

PSE_Window::PSE_Window(Dataset *ds):ds(ds)
{
  title = NULL;
  pref_dialog_info = NULL;

  ds->pse_windows += this;
  gpointer swptr = (gpointer) this;
  seg_plot_paint_variables_valid = false;

  ds->live_window_count++;
  window_num = ++ds->window_count;

  ds->am->setup_before_pse_window_initialized(this);
  ds->opm->setup_before_pse_window_initialized(this);

  GdkScreen* const screen = gdk_screen_get_default();
  const gint scr_height = gdk_screen_get_height(screen);

  // Fraction of vertical height for initial window.
  const double screen_height_frac = 0.5;
  const double aspect_ratio = 1.61803;

  const int height_initial = scr_height * screen_height_frac;
  const int width_initial = height_initial * aspect_ratio;
  // Scale factor applied to units given in pixels.
  const double pixel_scale = double(height_initial) / 550;

  //
  // View Mode and Settings
  //

  opt_grid_paced = rtiv_grid_paced;
  opt_grid_mult_decode_width = rtiv_grid_mult_decode_width;

  ovr_window_resize_count = 0;

  ovr_pointed_segment = NULL;
  plot_segment_as_ped = 1;
  plot_is_ovr = 1;
  plot_ovr_by_ipc = false;
  drag_dis_window = false;
  cursor_at_limit = false;
  restore_unlock_dis = false;
  plot_ped_clipped = rtiv_plot_ped_clipped;

  grid_spacing_inst = rtiv_grid_spacing_inst;
  grid_spacing_cyc = rtiv_grid_spacing_cyc;
  grid_spacing_inst_unit_threshold = rtiv_grid_spacing_inst_unit_threshold;
  grid_spacing_cyc_unit_threshold =  rtiv_grid_spacing_cyc_unit_threshold;

  animate_mode = Animate_Mode(rtiv_smooth_scroll_mode);
  base_accel = rtiv_smooth_scroll_accel;

  button3_up_ignore = false;
  button_down_motion = false;

  //
  // Labeling
  //

  show_labels = true;

  //
  // Graphics Support
  //

  pm = NULL;


  //
  // Window
  //

  main_w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  if( !ds->ref_window ) ds->ref_window = this->main_w;
  make_title();
  g_signal_connect(G_OBJECT(main_w), "delete_event",
                   G_CALLBACK(evnt_cb_delete), swptr);

  PGtkVBoxPtr vbox = gtk_vbox_new(false,0);
  gtk_container_add(GTK_CONTAINER(main_w),vbox);

  // Menu
  //
  accel_group = gtk_accel_group_new();

  GtkItemFactory *item_factory =
    gtk_item_factory_new(GTK_TYPE_MENU_BAR, "<main>", accel_group);

  gtk_box_pack_start (vbox,
                      gtk_item_factory_get_widget(item_factory,"<main>"),
                      false, true, 0);

  gtk_item_factory_create_items
    (item_factory, ASIZE(menu_entries),
     (GtkItemFactoryEntry*)menu_entries, swptr);

  gtk_window_add_accel_group(GTK_WINDOW(main_w),accel_group);

  GtkItemFactory *context_item_factory =
   gtk_item_factory_new(GTK_TYPE_MENU, "<main>", NULL);

  gtk_item_factory_create_items
   (context_item_factory, ASIZE(context_menu_entries),
    (GtkItemFactoryEntry*)context_menu_entries, swptr);

  context_menu_clip_item =
   gtk_item_factory_get_widget(context_item_factory, "/Clip");

  context_menu_popup =
   gtk_item_factory_get_widget(context_item_factory, "<main>");

  PGtkCheckMenuItemPtr enable_plot_area_check_menu =
    gtk_item_factory_get_item(context_item_factory,"/Highlight Annotations");
  gtk_action_connect_proxy
    (ds->am->enable_plot_area_action, enable_plot_area_check_menu);
  PGtkCheckMenuItemPtr enable_plot_area_check_menu2 =
    gtk_item_factory_get_item(item_factory,"/Highlight/Annotations");
  gtk_action_connect_proxy
    (ds->am->enable_plot_area_action, enable_plot_area_check_menu2);

  g_signal_connect(G_OBJECT(context_menu_popup), "selection_done",
                   G_CALLBACK(menu_cb_select_done), swptr);

  context_menu_zoom_submenu =
   gtk_item_factory_get_widget(context_item_factory, "/Zoom");

  g_signal_connect(G_OBJECT(context_menu_zoom_submenu), "selection_done",
                   G_CALLBACK(menu_cb_select_done), swptr);

  // Pref Toggle Init
# define PTI(obj,rti,name,tip,m1,m2) \
  obj.init(this, rti, name, tip); CONNECT2(obj,m1,m2);

# define CONNECT2(obj,m1,m2) \
  obj.connect(gtk_item_factory_get_item(item_factory,m1)); \
  obj.connect(gtk_item_factory_get_item(context_item_factory,m2));

  PTI(plot_high_same_sin, rtiv_plot_high_same_sin,
      "Highlight Same Instruction",
      "Highlight other occurrences of static instruction at cursor.",
      "/Highlight/Same Instruction", "/Highlight Same Instruction");

  PTI(plot_high_same_line, rtiv_plot_high_same_line,
      "Highlight Same Cache Line",
      "Highlight other instructions accessing same cache line as "
      "the instruction at cursor.",
      "/Highlight/Same Cache Line", "/Highlight Same Cache Line");

  PTI(plot_high_snips, rtiv_plot_high_snips,
      "Highlight Snips", "Highlight Snips",
      "/Highlight/Snips", "/Highlight Snips");

  PTI(plot_high_snip_boundaries, rtiv_plot_high_snip_boundaries,
      "Highlight Snip Boundaries", "Highlight Snip Boundaries",
      "/Highlight/Snip Boundaries", "/Highlight Snip Boundaries");

  PTI(pref_plot_dep_through_mem, rtiv_pref_plot_dep_through_mem,
      "Highlight Dataflow Through Memory",
      "Highlight data dependencies carried by memory locations.",
      "/Highlight/Dataflow Through Memory",
      "/Highlight Dataflow Through Memory");

  PTI(pref_plot_high_dead_insn, rtiv_plot_high_dead_insn,
      "Highlight Dead Instructions",
      "Highlight Dead Instructions",
      "/Highlight/Dead Instructions",
      "/Highlight Dead Instructions");
     
# undef CONNECT2
# undef PTI

# define CONNECT2(obj,val,name,tip,m1,m2) \
  obj.item_add(val,name,tip); \
  obj.connect(gtk_item_factory_get_item(item_factory,m1)); \
  {if(m2)obj.connect(gtk_item_factory_get_item(context_item_factory,m2));}

  pref_plot_high_dep_limit.init(this, rtiv_pref_plot_high_dep_limit);
  CONNECT2(pref_plot_high_dep_limit, 0, "Highlight Dataflow None",
           "Don't highlight items carrying dependencies.",
           "/Highlight/Dataflow None", "/Highlight Dataflow None");
  CONNECT2(pref_plot_high_dep_limit, 1, "Highlight Dataflow Direct",
           "Don't highlight items carrying dependencies.",
           "/Highlight/Dataflow Direct", "/Highlight Dataflow Direct");
  CONNECT2(pref_plot_high_dep_limit, 2, "Highlight Dataflow All",
           "Don't highlight items carrying dependencies.",
           "/Highlight/Dataflow All", "/Highlight Dataflow All");
#undef CONNECT2


  // Button Bar
  //
  GtkWidget* const tb_all = gtk_hbox_new(false,0);

  tool_bar_common = gtk_hbox_new(true,0); // Common: PED, ROB, and ovr.
  tool_bar_seg = gtk_hbox_new(true,0);    // PED and ROB
  tool_bar_ovr = gtk_hbox_new(true,0);
  tool_bar_ped = gtk_hbox_new(true,0);
  tool_bar_rob = gtk_hbox_new(true,0);
  tool_bars_seg = gtk_hbox_new(false,0);

  gtk_box_pack_start (GTK_BOX(tool_bars_seg), tool_bar_seg, false, true, 0);
  gtk_box_pack_start (GTK_BOX(tool_bars_seg), tool_bar_ped, false, true, 0);
  gtk_box_pack_start (GTK_BOX(tool_bars_seg), tool_bar_rob, false, true, 0);

  // expand, fill
  gtk_box_pack_start (GTK_BOX(tb_all), tool_bar_common, false, true, 0);
  gtk_box_pack_start (GTK_BOX(tb_all), tool_bar_ovr, false, true, 0);
  gtk_box_pack_start (GTK_BOX(tb_all), tool_bars_seg, false, true, 0);

  gtk_box_pack_start (vbox, tb_all, false, true, 0);
  gtk_box_pack_start (vbox, gtk_hseparator_new(), false, true, 0);

  dbutton = insert_stock_icon_toggle
    (tool_bar_common, GTK_STOCK_HOME, bttn_cb_display, swptr, plot_is_ovr,
     "Switch between Overview (that's supposed to be home) and Segment "
     "(PED and ROB) graphs.");

  pbutton = insert_new_toggle(tool_bar_seg, "PED", bttn_cb_ped, swptr,
                              plot_segment_as_ped,
                              "Switch between PED and ROB views of segment.");
  rbutton = insert_stock_icon_button
    (tool_bar_seg, GTK_STOCK_PRINT, bttn_cb_ps, swptr,
     "Export graph to PDF or EPS (Encapsulated PostScript).");
  cbutton = insert_new_toggle(tool_bar_ped, "Clip", bttn_cb_clip, swptr, false,
                              "When pressed do not wrap from top to bottom.");

  dpbutton = insert_new_toggle
    (tool_bar_rob, "DP", bttn_cb_decode_pos, swptr, false,
     "When not pressed scale ROB plot to ROB size, "
     "when pressed scale to fit present view.");

  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_GOTO_FIRST,bttn_cb_misc,
                           cb_data_new(GDK_Home),
                           "Scroll to beginning of segment.");
  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_GO_BACK,bttn_cb_misc,
                           cb_data_new(GDK_Left),"Scroll left.");
  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_GO_FORWARD,bttn_cb_misc,
                           cb_data_new(GDK_Right),"Scroll right.");
  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_GOTO_LAST,bttn_cb_misc,
                           cb_data_new(GDK_End),"Scroll to end of segment.");

  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_ZOOM_100,bttn_cb_zoom,
                           cb_data_new('0'),"Zoom out max.");
  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_ZOOM_IN,bttn_cb_zoom,
                           cb_data_new('z'),"Zoom in.");
  insert_stock_icon_button(tool_bar_seg,GTK_STOCK_ZOOM_OUT,bttn_cb_zoom,
                           cb_data_new('u'),"Zoom out.");
  seg_bttn_prev_view =
    insert_stock_icon_button(tool_bar_seg,GTK_STOCK_REDO,bttn_cb_zoom,
                             cb_data_new('l'),"Previous view (if avail).");

  plot_dep_obscure = insert_stock_icon_toggle
    (tool_bar_ped, GTK_STOCK_SELECT_COLOR, bttn_cb_insn, cb_data_new('o'), false,
     "Obscure uninteresting instructions.");

  seg_bttn_ped_order = insert_stock_icon_toggle
    (tool_bar_ped,GTK_STOCK_JUSTIFY_LEFT, bttn_cb_seg_insn_order, swptr,
     ped_plot_idxt=rtiv_ped_plot_idxt,
     "When pressed show instructions in decode order, otherwise "
     "in program order.");

  gtk_widget_set_sensitive(seg_bttn_prev_view,false);

  seg_bttn_data = insert_stock_icon_button
    (tool_bar_common, GTK_STOCK_PROPERTIES, bttn_cb_data,swptr,
     "View simulation data.");

  // Search
  //
  GtkWidget* const search_widget = search_bar_construct(this);
  gtk_box_pack_start(vbox, search_widget, false, false, 0);

  PGtkEventBoxPtr event_box = gtk_event_box_new();
  gtk_box_pack_start(vbox, event_box, true, true, 0);

  plot_vbox = gtk_vbox_new(false,0);
  gtk_container_add(event_box,plot_vbox);

  // Text labels above draw area.
  //
  seg_labels_vbox = gtk_vbox_new(false,0);
  gtk_box_pack_start(GTK_BOX(plot_vbox), seg_labels_vbox, false, false, 0);

  PGtkHBoxPtr row1_hbox = gtk_hbox_new(true,0);
  gtk_box_pack_start(GTK_BOX(seg_labels_vbox), row1_hbox, false, false, 0);

  PGtkHBoxPtr rank_follow_hbox = gtk_hbox_new(false,0);

  // Sort-by-execution-order Button
  //
  // Callback set in Ovr_Plot_Manager::pane_labels_build.
  ovr_sort_execution = gtk_radio_button_new_from_widget(NULL);
  gtk_tooltips_set_tip
    (app.tips, ovr_sort_execution, "Sort by execution order.", NULL);
  gtk_box_pack_start(rank_follow_hbox,ovr_sort_execution,false,false,0);

  gtk_box_pack_start(row1_hbox,rank_follow_hbox,true,true,0);
  label_set += rnk_label = plot_label_new(rank_follow_hbox,true);
  label_show_list_add(rnk_label,"Segment Position and Rank",false);

  seg_plot_src_labels_hbox = gtk_hbox_new(true,0);
  gtk_box_pack_start
    (GTK_BOX(seg_labels_vbox), seg_plot_src_labels_hbox, false, false, 0);

  label_set += ipc_label = plot_label_new(seg_plot_src_labels_hbox, true);
  label_set += dis_label = plot_label_new(seg_plot_src_labels_hbox,true);
  label_show_list_add(ipc_label,"IPC",false);
  label_show_list_add(dis_label,"Disassembled Instruction",false);

  PGtkHBoxPtr row3_hbox = gtk_hbox_new(true,0);
  gtk_box_pack_start(GTK_BOX(seg_labels_vbox), row3_hbox, false, false, 0);
  label_set += sta_label = plot_label_new(row3_hbox,true);
  label_show_list_add(sta_label,"State",false);
  label_set += frq_label = plot_label_new(row3_hbox,true);

  PGtkHBoxPtr src_label_hbox = gtk_hbox_new(false,0);
  gtk_box_pack_start(row1_hbox, src_label_hbox, true, true, 0);

  // Sort-by-static-instruction-order Button
  // Callback set in Ovr_Plot_Manager::pane_labels_build.
  ovr_sort_pc = gtk_radio_button_new_from_widget(ovr_sort_execution);
  gtk_tooltips_set_tip
    (app.tips, ovr_sort_pc, "Sort by most common instruction.", NULL);
  gtk_box_pack_start(src_label_hbox,ovr_sort_pc,false,false,0);

  label_set += src_label = plot_label_new(src_label_hbox, true);

  label_show_list_add(src_label,"Source",false);

  gtk_box_pack_start
    (GTK_BOX(seg_labels_vbox), gtk_hseparator_new(), false, true, 0);

  {
    GtkWidget *buttons_vbox = gtk_vbox_new(false, 0);

    insert_stock_icon_button(buttons_vbox, GTK_STOCK_CLOSE,
                             ovr_plot_values_hide_button_callback, swptr);

    menu_check_ovr_plot_values_pane =
      gtk_item_factory_get_item(item_factory,"/View/Overview Plot Values Pane");
    gtk_check_menu_item_set_active
      (GTK_CHECK_MENU_ITEM(menu_check_ovr_plot_values_pane),
       rtiv_ovr_show_ovr_plot_pane);

    insert_stock_icon_button(buttons_vbox, GTK_STOCK_ADD,
                             ovr_plot_series_button_callback, swptr,
                             "Add, remove, and modify plot series.");
    gtk_box_pack_start(ovr_plot_data->pane_hbox,buttons_vbox,
                       false, false, 0);
    gtk_box_pack_start(GTK_BOX(seg_labels_vbox),ovr_plot_data->pane_hbox,
                       false, false, 0);
  }

  // Annotation Messages, Plot Area, and Disassembly Window.
  //
  draw_dis_vpaned = gtk_vpaned_new();
  gtk_box_pack_start(GTK_BOX(plot_vbox), draw_dis_vpaned, true, true, 0);

  // Annotation Messages
  //
  gtk_paned_pack1
    (draw_dis_vpaned,ds->am->message_pane_construct(this),false,true);
  annotation_data->message_pane_height = rtiv_annotation_message_pane_height;
  gtk_paned_set_position
    (draw_dis_vpaned,annotation_data->message_pane_height);
  g_signal_connect(draw_dis_vpaned, "notify::position",
                   G_CALLBACK(evnt_cb_annotation_pane_vbox_move_handle),swptr);

  // Plot Area and Disassembly Window
  //
  draw_dis_hpaned = gtk_hpaned_new();
  gtk_paned_pack2(draw_dis_vpaned,draw_dis_hpaned,true,true);

  // Draw Areas (For overview and segment graph.)
  //
  draw_area = gtk_drawing_area_new();
  gtk_paned_pack1(draw_dis_hpaned, draw_area, true, true);
  g_object_set(G_OBJECT(draw_area),"can-focus",true,NULL);

  gtk_widget_set_size_request(draw_area, 500 * pixel_scale, 400 * pixel_scale);

  scroll_draw_area = gtk_scrolled_window_new(NULL,NULL);
  gtk_box_pack_start(GTK_BOX(plot_vbox),scroll_draw_area,true,true,0);

  draw_area_ovr = gtk_drawing_area_new();
  gtk_scrolled_window_add_with_viewport
    (GTK_SCROLLED_WINDOW(scroll_draw_area), GTK_WIDGET(draw_area_ovr));
  g_object_set(G_OBJECT(draw_area_ovr),"can-focus",true,NULL);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll_draw_area),
				 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);

  // DMK: Todo: Try out motion hints. 21 June 2002, 13:54:32 CDT
  gtk_widget_add_events (draw_area,
                         GDK_BUTTON_PRESS_MASK
                         | GDK_BUTTON_RELEASE_MASK
                         | GDK_EXPOSURE_MASK
                         | GDK_BUTTON1_MOTION_MASK
                         | GDK_POINTER_MOTION_MASK);

  g_signal_connect(G_OBJECT(draw_area), "expose_event",
                   G_CALLBACK(evnt_cb_expose),swptr);
  g_signal_connect(G_OBJECT(draw_area), "button_press_event",
                   G_CALLBACK(evnt_cb_button_down),swptr);
  g_signal_connect(G_OBJECT(draw_area), "button_release_event",
                   G_CALLBACK(evnt_cb_button_up),swptr);
  g_signal_connect(G_OBJECT(draw_area), "motion_notify_event",
                   G_CALLBACK(evnt_cb_motion),swptr);
  g_signal_connect(G_OBJECT(draw_area), "scroll-event",
                   G_CALLBACK(evnt_cb_scroll),swptr);

  gtk_widget_add_events (draw_area_ovr,
                         GDK_BUTTON_PRESS_MASK
                         | GDK_BUTTON_RELEASE_MASK
                         | GDK_EXPOSURE_MASK
                         | GDK_BUTTON1_MOTION_MASK
                         | GDK_POINTER_MOTION_MASK);

  g_signal_connect(G_OBJECT(draw_area_ovr), "expose_event",
                   G_CALLBACK(evnt_cb_expose),swptr);
  g_signal_connect(G_OBJECT(draw_area_ovr), "button_press_event",
                   G_CALLBACK(evnt_cb_button_down),swptr);
  g_signal_connect(G_OBJECT(draw_area_ovr), "button_release_event",
                   G_CALLBACK(evnt_cb_button_up),swptr);
  g_signal_connect(G_OBJECT(draw_area_ovr), "motion_notify_event",
                   G_CALLBACK(evnt_cb_motion),swptr);

  // Keyboard Handler
  //
  // DMK: Todo maybe: just use accelerators instead of key handler. 21 June 2002
  //
  g_signal_connect(G_OBJECT(main_w), "key_press_event",
                   G_CALLBACK(evnt_cb_key_press),swptr);
  g_signal_connect(G_OBJECT(main_w), "key_release_event",
                   G_CALLBACK(evnt_cb_key_press),swptr);

  key_handler_suppress = false;

  // Disassembly Area
  //

  dis_pane_vbox = gtk_vbox_new(false,0);
  gtk_paned_pack2(draw_dis_hpaned, dis_pane_vbox, false, true);

  tool_bar_dis = gtk_hbox_new(true,0);
  gtk_box_pack_start(GTK_BOX(dis_pane_vbox), tool_bar_dis, false, false, 0);

  PGtkEventBoxPtr dis_line_label_bg = gtk_event_box_new();
  gtk_box_pack_start(GTK_BOX(dis_pane_vbox), dis_line_label_bg, true, true, 0);
  gtk_widget_modify_bg
    (dis_line_label_bg,GTK_STATE_NORMAL,app.c_dis_window_locked);

  label_set += dis_lines_label = gtk_label_new("");
  gtk_container_add(dis_line_label_bg,dis_lines_label);
  gtk_misc_set_alignment(GTK_MISC(dis_lines_label),0,0);
  gtk_misc_set_padding(GTK_MISC(dis_lines_label),2,2);
  gtk_label_set_selectable(GTK_LABEL(dis_lines_label),true);

  cursor_locked =
    insert_new_toggle(tool_bar_dis," Lock ",bttn_cb_insn, cb_data_new('c'),
                      false, "Lock/unlock instruction cursor.");

  dis_locked =
    insert_new_toggle(tool_bar_dis,"LD",bttn_cb_insn, cb_data_new('d'),
                      false, "Lock/unlock disassembly window.");

  seg_bttn_next_insn =
    insert_stock_icon_button(tool_bar_dis,GTK_STOCK_GO_DOWN,bttn_cb_insn_pressed,
                             cb_data_new('n'),"Highlight next instruction.",
                             bttn_cb_insn_released);
  seg_bttn_prev_insn =
    insert_stock_icon_button(tool_bar_dis,GTK_STOCK_GO_UP,bttn_cb_insn_pressed,
                             cb_data_new('p'),"Highlight previous instruction.",
                             bttn_cb_insn_released);

  pgtk_label_set_background
    (GTK_LABEL(gtk_bin_get_child(GTK_BIN(cursor_locked.b))),
     app.c_cursor_unlocked_bg);

  seg_bttn_dis_pane_close =
    insert_stock_icon_button
    (tool_bar_dis,GTK_STOCK_CLOSE, bttn_cb_dis_window, swptr,
     "Hide disassembled code pane. (Use View menu to show again.)");

  seg_menu_check_dis_pane =
    gtk_item_factory_get_item(item_factory,"/View/Disassembly Pane");
  gtk_check_menu_item_set_active
    (GTK_CHECK_MENU_ITEM(seg_menu_check_dis_pane),true);

  gtk_window_set_default_size(GTK_WINDOW(main_w),width_initial,height_initial);
  {
    GdkGeometry g;
    g.min_width = 200;
    g.min_height = 50;
    gtk_window_set_geometry_hints
      (GTK_WINDOW(main_w), main_w, &g, GDK_HINT_MIN_SIZE);
  }

  gtk_paned_set_position
    (draw_dis_hpaned,width_initial-rtiv_disassembly_pane_width);
  g_signal_connect(draw_dis_hpaned, "notify::position",
                   G_CALLBACK(evnt_cb_dis_hbox_move_handle),swptr);

  // Text Labels below the draw area.
  //
  GtkWidget* const pos_grid_frame = gtk_frame_new(NULL);
  gtk_box_pack_start(GTK_BOX(plot_vbox), pos_grid_frame, false, false, 0);
  gtk_frame_set_shadow_type(GTK_FRAME(pos_grid_frame),GTK_SHADOW_IN);

  GtkWidget *pos_grid_hpane = gtk_hpaned_new();
  gtk_container_add(GTK_CONTAINER(pos_grid_frame),pos_grid_hpane);
  gtk_paned_set_position(GTK_PANED(pos_grid_hpane),350*pixel_scale);

  PGtkHBoxPtr pos_hbox = gtk_hbox_new(true,0);
  gtk_paned_pack1(GTK_PANED(pos_grid_hpane),pos_hbox,0,0);

  PGtkHBoxPtr grid_spc_hbox = gtk_hbox_new(true,0);
  gtk_paned_pack2(GTK_PANED(pos_grid_hpane),grid_spc_hbox,0,0);

  label_set += pos_label = plot_label_new(pos_hbox,true);
  label_set += grd_label =
    plot_label_link_new
    (grid_spc_hbox,G_CALLBACK(pref_show_gridlines),swptr,true);
  label_show_list_add(pos_label,"Time, Tag, and PC",true);
  label_show_list_add(grd_label,"Grid Spacing",true);

  // Voila
  //

  gtk_widget_show_all(main_w);

  draw_area_ovr_win = draw_area_ovr->window;
  draw_area_win = draw_area->window;
  gc = gdk_gc_new(draw_area->window);
  GtkStyle *style = gtk_widget_get_style(draw_area);
  gdk_gc_set_foreground(gc,&style->black);

  gtk_window_set_icon(GTK_WINDOW(main_w), app.icon_pixbuf);

  bttn_cb_display(dbutton,this);

  gtk_widget_hide(draw_dis_vpaned);
  gtk_widget_hide(tool_bars_seg);
  gtk_widget_hide(tool_bar_common);
  gtk_widget_hide(tool_bar_ovr);
  gtk_widget_hide(seg_labels_vbox);
  gtk_widget_hide(search_widget);
  seg_rob_ped_change(false);

  label_set.font_size_adjust(10000);
  init_at_window_and_dataset_available();
}
