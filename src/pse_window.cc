/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2010 Louisiana State University

// $Id$

#include <gtk/gtk.h>
#include "pse_window.h"
#include "handlers.h"
#include "rti_declarations.h"
#include "annotation_manager.h"

bool
View::operator == (View &v2)
{
  if( !valid || !v2.valid ) return false;

  return
    start_c == v2.start_c
    && top_idx == v2.top_idx
    && pixels_per_cycle == v2.pixels_per_cycle
    && pixels_per_inst == v2.pixels_per_inst;
}

bool
ViewEnv::operator == (ViewEnv &v)
{
  if( !valid || !v.valid ) return false;
  return
    width
    && sd == v.sd
    && pm_width >= v.width
    && height == v.height
    && plot_ped_clipped == v.plot_ped_clipped
    && plot_rob_decode_positioning == v.plot_rob_decode_positioning
    && plot_segment_as_ped == v.plot_segment_as_ped;
}

void
PSE_Label_Set::font_size_adjust(int millipoints)
{
  size += millipoints;
  size = max(4000,size);
  PangoFontDescription* const fd = pango_font_description_new();
  pango_font_description_set_size(fd,size);
  for(PGtkLabelPtr l; (l = labels.iterate()); )
    gtk_widget_modify_font(l,fd);
  pango_font_description_free(fd);
}

void 
Pref_Plot_AToggle::init
(PSE_Window *pwp, int &rti_varp, const char *name, const char *tooltip)
{
  pw = pwp;
  rti_var = &rti_varp;
  val = *rti_var;
  action = gtk_toggle_action_new(name, name, tooltip, NULL);
  gtk_toggle_action_set_active(action, val);
  g_signal_connect
    (action, "toggled",
     G_CALLBACK(Pref_Plot_AToggle::callback_toggled_wrapper), gpointer(this));
}

void
Pref_Plot_AToggle::callback_toggled(GtkToggleAction *w)
{
  *rti_var = val = gtk_toggle_action_get_active(w);
  user_cmd_redisplay(pw,false);
}

void 
Pref_Plot_ARadio::init(PSE_Window *pwp, int &rti_varp)
{
  pw = pwp;
  group = NULL;
  rti_var = &rti_varp;
  val = *rti_var;
}

void
Pref_Plot_ARadio::item_add(int item_val, const char *name, const char *tooltip)
{
  action_added_last = gtk_radio_action_new(name, name, tooltip, NULL, item_val);
  gtk_radio_action_set_group(action_added_last, group);
  group = gtk_radio_action_get_group(action_added_last);
  gtk_toggle_action_set_active(action_added_last, item_val == val );
  g_signal_connect
    (action_added_last, "changed",
     G_CALLBACK(Pref_Plot_ARadio::callback_changed_wrapper), gpointer(this));
}

void
Pref_Plot_ARadio::callback_changed(GtkRadioAction *w)
{
  const int new_val = gtk_radio_action_get_current_value(w);
  if( new_val == val ) return;
  *rti_var = val = gtk_radio_action_get_current_value(w);
  user_cmd_redisplay(pw,false);
}

int
PSE_Window::get_view_width_c()
{
  return ve.width / ( view.pixels_per_cycle ? view.pixels_per_cycle : 1 );
}

void
PSE_Window::invalidate_draw_area()
{
  GdkWindow* const win = plot_is_ovr ? draw_area_ovr_win : draw_area_win;
  GdkRectangle area;
  gdk_window_get_size(win,&ve.width,&ve.height);
  area.x = 0;  area.y = 0;
  area.width = ve.width;  area.height = ve.height;
  gdk_window_invalidate_rect(win,&area,true);
}

void
PSE_Window::push_current_view()
{
  last_view = view;
  last_plot_ped_clipped = plot_ped_clipped;
  gtk_widget_set_sensitive(seg_bttn_prev_view,true);
}

void
PSE_Window::restore_previous_view()
{
  if( !last_view.pixels_per_inst ) return;

  View soon_to_be_previous_view = view;
  view = last_view;
  last_view = soon_to_be_previous_view;
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbutton),
                               last_plot_ped_clipped);
  if( restore_unlock_dis ) dis_locked = false;
  restore_unlock_dis = false;
  turn_around_t = 0;
  arrival_t = 0;
  goal_c = view.start_c;
  goal_idx = view.top_idx;
  goal_d =
    plot_segment_as_ped && plot_ped_clipped ? view.top_idx : view.start_c;

  invalidate_draw_area();
}

int
PSE_Window::idx_reference_get()
{
  if( plot_show_dis && ds->readex )
    {
      if( cursor_idx >= dis_first_idx && cursor_idx <= dis_last_idx
          && idx_is_visible(cursor_idx) ) return cursor_idx;
      const int dis_window_mid = mid(dis_first_idx, dis_last_idx);
      if( dis_last_idx > dis_first_idx && idx_is_visible(dis_window_mid) )
        return dis_window_mid;
    }
  if( cursor_locked && idx_is_visible(cursor_idx) ) return cursor_idx;
  const int amt = view_last_idx - view_first_idx + 1;
  const int hh = view_height_insn >> 1;
  if( amt <= hh ) return view_last_idx;
  const int center_idx =
    view_first_idx + ( amt / (view_height_insn<<1) ) * view_height_insn + hh;
  if( idx_is_visible(center_idx) ) return center_idx;

  int idx_contiguous_largest_size = 0;
  int idx_contiguous_largest_start = view_first_idx;
  for( int idx = view_first_idx; idx <= view_last_idx; )
    {
      if( !idx_is_visible(idx) ) { idx++; continue; }
      const int idx_contiguous_start = idx;
      while( idx_is_visible(idx) ) if( idx++ == view_last_idx ) break;
      const int size = idx - idx_contiguous_start;
      if( size < idx_contiguous_largest_size ) continue;
      idx_contiguous_largest_size = size;
      idx_contiguous_largest_start = idx_contiguous_start;
    }
  const int idx_contiguous_mid =
    idx_contiguous_largest_start + ( idx_contiguous_largest_size >> 1 );
  const int idx_contiguous_center =
    idx_contiguous_mid - idx_to_vertical_position(idx_contiguous_mid) + hh;
  if( idx_is_visible(idx_contiguous_mid) ) return idx_contiguous_center;
  return idx_contiguous_mid;
}

static gboolean
indexing_change_animation_cb(gpointer swptr)
{
  return ((PSE_Window*)swptr)->indexing_change_timer_cb();
}

void
PSE_Window::insn_order_set()
{
  const int reference_idx = idx_reference_get();
  const int reference_pos = idx_to_vertical_position(reference_idx);
  Insn_Dataset_Info* const reference_di = idx_to_info(reference_idx);
  Insn_Dataset_Info* const cursor_di =
    cursor_idx == -1 ? NULL : idx_to_info(cursor_idx);
  const int dis_mid_idx = mid(dis_first_idx, dis_last_idx);
  Insn_Dataset_Info* const dis_di = idx_to_info(dis_mid_idx);
  const uint dis_cursor_offset = cursor_idx - dis_first_idx;

  rtiv_ped_plot_idxt = ped_plot_idxt = seg_bttn_ped_order;

  if( cursor_idx != -1 ) cursor_idx = info_to_idx(cursor_di);
  const int new_top_idx = info_to_idx(reference_di) - reference_pos;
  const int dis_size = dis_last_idx - dis_first_idx + 1;
  if( dis_cursor_offset < uint(dis_size) )
    {
      dis_first_idx = sd->idx_inrange(cursor_idx - dis_cursor_offset);
      dis_last_idx = sd->idx_inrange(dis_first_idx + dis_size);
    }
  else
    {
      const int dis_new_mid_idx = info_to_idx(dis_di);
      dis_first_idx = sd->idx_inrange(dis_new_mid_idx - (dis_size>>1));
      dis_last_idx = sd->idx_inrange(dis_first_idx+dis_size);
    }
  indexing_animation_finish_t = time_fp() + rtiv_indexing_animation_duration;
  indexing_animation_fraction = 0;
  indexing_animation_delta_idx = info_to_idx(reference_di) - reference_idx;
  indexing_animating = true;
  set_view_start(view.start_c,new_top_idx,true);
  g_timeout_add(33,::indexing_change_animation_cb,(gpointer)this);
}

void
PSE_Window::seg_rob_ped_change(bool animate)
{
  if ( indexing_animating ) return;
  indexing_animation_finish_t = time_fp()
    + ( animate ? rtiv_indexing_animation_duration : 0 );
  indexing_animation_fraction = 0;
  seg_rob_ped_animating = true;
  plot_segment_as_ped = true;
  if ( !animate )
    indexing_change_timer_cb();
  else
    g_timeout_add(33,::indexing_change_animation_cb,(gpointer)this);
}

bool
PSE_Window::indexing_change_timer_cb()
{
  if( !indexing_animating && !seg_rob_ped_animating ) return false;
  const double now = time_fp();
  const bool last_repaint = now >= indexing_animation_finish_t;
  if( last_repaint )
    {
      indexing_animation_fraction = 1.0;
      indexing_animating = false;
      if ( seg_rob_ped_animating )
        {
          seg_rob_ped_animating = false;
          plot_segment_as_ped =
            gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pbutton));
          if ( plot_segment_as_ped )
            {
              gtk_widget_show(tool_bar_ped);
              gtk_widget_hide(tool_bar_rob);
            }
          else
            {
              gtk_widget_hide(tool_bar_ped);
              gtk_widget_show(tool_bar_rob);
            }
        }
    }
  else
    {
      const double time_remaining = indexing_animation_finish_t - now;
      const double dur = rtiv_indexing_animation_duration;
      const double t1 = min(time_remaining, dur - time_remaining);
      const double distance = 2.0 * t1 * t1 / ( dur * dur );
      indexing_animation_fraction =
        time_remaining > t1 ? distance : 1.0 - distance;
    }

  set_view_start(view.start_c,view.top_idx,true);
  return !last_repaint;
}

void
PSE_Window::clip_mode_set()
{
  if ( idx_is_visible(pointed_idx_last) )
    {
      const int vp = idx_to_vertical_position(pointed_idx_last);
      set_view_start(view.start_c,pointed_idx_last-vp,true);
    }
  else
    set_view_start(view.start_c,view.top_idx,true);
}

void
PSE_Window::set_view_start(int start_c, int top_idx, bool force_update)
{
  int old_start_c = view.start_c;
  int old_top_idx = view.top_idx;
  const bool clipped = plot_segment_as_ped && plot_ped_clipped;
  view.start_c = start_c;
  view_start_d = clipped ? top_idx : start_c;
  goal_c = start_c;
  goal_d = clipped ? top_idx : start_c;
  goal_idx = top_idx;
  view.valid = 0;
  if( force_update || view.top_idx != top_idx ) pm_sd = NULL;
  view.top_idx = top_idx;
  offset_vc_do_update = true;
  if( force_update || old_start_c != start_c || old_top_idx && view.top_idx )
    invalidate_draw_area();
}

void
PSE_Window::segment_goto(int segment_idx)
{
  segment_goto(ds->get_sdata(segment_idx,plot_ovr_by_ipc));
}

void
PSE_Window::segment_goto(Segment_Data *sd_new)
{
  seg_plot_paint_variables_valid = false;
  const bool show_stopwatch = !sd->rob_rectangle_info.size;
  if( show_stopwatch )
    {
      gdk_window_set_cursor(main_w->window,app.cursor_watch);
      yield_to_gtk();
    }

  sd = sd_new;
  new_segment();
  gtk_widget_hide(tool_bar_ovr);
  gtk_widget_hide(scroll_draw_area);
  gtk_widget_show(tool_bars_seg);
  gtk_widget_show(draw_dis_vpaned);
  gtk_widget_show(seg_plot_src_labels_hbox);

  gtk_widget_show(seg_labels_vbox);
  seg_rob_ped_change(false);

  if( plot_is_ovr )
    {
      plot_is_ovr = false;
      PGtk_Suppress_Handler a(gpointer(dbutton),gpointer(bttn_cb_display));
      gtk_toggle_button_set_active
        (GTK_TOGGLE_BUTTON(dbutton),plot_is_ovr);
    }

  gtk_check_menu_item_set_active
    (GTK_CHECK_MENU_ITEM(menu_check_ovr_plot_values_pane),
     ovr_plot_data->seg_show_values);

  ds->opm->pane_value_labels_update(this,sd);

  if( show_stopwatch )
    gdk_window_set_cursor(main_w->window,app.cursor_arrow);

  invalidate_draw_area();
}

void
PSE_Window::overview_goto()
{
  if( !plot_is_ovr )
    {
      plot_is_ovr = true;
      PGtk_Suppress_Handler a(gpointer(dbutton),gpointer(bttn_cb_display));
      gtk_toggle_button_set_active
        (GTK_TOGGLE_BUTTON(dbutton),plot_is_ovr);
    }

  gtk_widget_hide(tool_bars_seg);
  gtk_widget_show(tool_bar_ovr);
  gtk_widget_hide(draw_dis_vpaned);
  gtk_widget_show(scroll_draw_area);
  gtk_widget_hide(seg_plot_src_labels_hbox);
  gtk_check_menu_item_set_active
    (GTK_CHECK_MENU_ITEM(menu_check_ovr_plot_values_pane),
     ovr_plot_data->ovr_show_values);
  search_at_overview(this);
  invalidate_draw_area();
}

void
PSE_Window::new_segment()
{
  cursor_cmd = CC_Init;
  cursor_move_amt = 0;
  cursor_locked = false;
  dis_locked = false;
  restore_unlock_dis = false;
  dis_first_idx = dis_last_idx = 0;
  pointed_idx = -1;
  cursor_idx = -1;
  drag_c = 0;
  zoom_c_1 = 0;
  goal_idx = 0;
  goal_c = 0;
  view_start_d = 0;
  velocity_d = 0;
  animating = false;
  indexing_animating = false;
  pm_height = 0;
  plot_rob_decode_positioning = PRDP_ROB_Size;
  dis_drag_grab_rect.setxywh(0,0,0,0);
  view.init(ds);
  gtk_widget_set_sensitive(seg_bttn_prev_view,false);
  gtk_toggle_button_set_active
    (GTK_TOGGLE_BUTTON(cbutton),rtiv_plot_ped_clipped);
  if( !sd->rob_rectangle_info.size ) prepare_rob_data(ds,sd);
  gtk_widget_set_sensitive(seg_bttn_ped_order,sd->snip_count > 1);
}

PSE_Window::~PSE_Window()
{
  ds->am->setup_before_pse_window_destroyed(this);
  if(ds) 
    {
      ds->live_window_count--;
      if( !ds->live_window_count )
        ds->ref_window = NULL;
      else if( ds->ref_window == this->main_w )
        ds->ref_window = ds->pw_get()->main_w;
    }
};
