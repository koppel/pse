/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2010 Louisiana State University

// $Id$

// Run-time initialized variable declarations.


#if (!defined(RTI_H) && !defined(rti_all_h))
#undef RTI
#define RTI(name,type,def,vmin,vmax,doc) extern type name;
#endif

RTI(rtiv_pse_exe_path, const char*, NULL, 0, 100000,
    "Path or paths in which to look for a benchmark executable. Paths "
    "are separated by a ':'.");

RTI(rtiv_rti_save, const char*, "overwrite-ask", 0, 100,
    "At PSE exit when settings file has changed\n"
    " if \"overwrite-always\", overwrite;\n"
    " if \"overwrite-never\", don't overwrite;\n"
    " if \"overwrite-ask\", overwrite if user answers yes.\n");

RTI(rtiv_pref_plot_high_dep_limit, int, 2, 0, 2, "How (or if) to "
    "highlight register dependencies. 0, none; 1, direct; 2, all.");

RTI(rtiv_plot_high_dead_insn, int, 1, 0, 1,
    "If 1, dead instructions (those writing registers having no effect) "
    "shown in disassembly pane crossed out.");

RTI(rtiv_plot_high_same_sin, int, 0, 0, 1, "Highlight other instances of "
    "the instruction under the cursor.");

RTI(rtiv_plot_high_same_line, int, 0, 0, 1, "Highlight other instructions "
    "accessing the same cache line as the instruction under the cursor.");

RTI(rtiv_plot_high_snips, int, 1, 0, 1, "Highlight separate nearby snips with "
    "different colors.");

RTI(rtiv_plot_high_snip_boundaries, int, 1, 0, 1, "Highlight snip boundaries "
    "with a line.");

RTI(rtiv_pref_plot_dep_through_mem, int, 1, 0, 1, "Highlight dependencies"
    " through memory.");

RTI(rtiv_seg_show_ovr_plot_pane, int, 0, 0, 1,
    "If 1, show values of segment series data in segment plot.");

RTI(rtiv_seg_zoom_factor, double, 1.5, 1.01, 10.0,
    "Amount by which to zoom in or zoom out.");

RTI(rtiv_ped_clip_n_zoom_pixels_per_cycle,int, 8, 1, 1000,
    "Horizontal resolution (pixels per cycle) set by clip-n-zoom.");
RTI(rtiv_ped_clip_n_zoom_pixels_per_inst,int, 8, 1, 1000,
    "Vertical resolution (pixels per instruction) set by clip-n-zoom.");

RTI(rtiv_ovr_show_ovr_plot_pane, int, 1, 0, 1,
    "If 1, show values of segment series data in overview plot.");

RTI(rtiv_ped_plot_idxt, int, 1, 0, 1,
    "If 1, show PED plot in decode order; if 0, show in program order.");

RTI(rtiv_plot_ped_clipped, int, 1, 0, 1,
    "If 0, instructions in PED plot wrap from bottom to top.");

RTI(rtiv_grid_paced, int, 0, 0, 1,
    "If 1, slant vertical gridlines so their slop matches maximum "
    "decode rate.");

RTI(rtiv_grid_mult_decode_width, int, 1, 0, 1,
    "If 1, spacing of horizontal gridlines is based on the decode width.");

RTI(rtiv_grid_spacing_inst, int, -10, -1000, 1000,
    "Distance between horizontal gridlines. "
    "If negative the approximate number of pixels between gridlines, "
    "if positive the exact number of instructions between gridlines.");

RTI(rtiv_grid_spacing_cyc, int, -10, -1000, 1000,
    "Distance between vertical gridlines. "
    "If negative the approximate number of pixels between gridlines, "
    "if positive the exact number of cycles between gridlines.");

RTI(rtiv_grid_spacing_inst_unit_threshold, int, 7, 1, 10000,
    "No gap between instructions in PED plot if less than VAL pixels apart.");

RTI(rtiv_grid_spacing_cyc_unit_threshold, int, 7, 1, 10000,
    "No gap between cycles in PED plot if less than VAL pixels apart.");

RTI(rtiv_smooth_scroll_mode, int, 3, 1, 3,
    "If 1, no smooth scrolling; if 2, smooth scroll short pans; "
    "if 3, always smooth scroll.");

RTI(rtiv_smooth_scroll_accel, double, 2000, 100, 20000,
    "Acceleration for areas being smooth scrolled.");

RTI(rtiv_indexing_animation_duration, double, 1.5, 0, 86400,
    "Number of seconds to switch from one instruction ordering to another.");

RTI(rtiv_ovr_series_settings, const char*, 
    "Branch Prediction Ratio*cD!IPC*CD!", 0, 100000,
    "Overview plot series options. Format \"NAME*OPTS!NAME*OPTS!...\", where "
    "NAME is the name of an overview series and OPTS is a string of option "
    "characters: D, display; d, don't display; C, connect; c don't connect.");

RTI(rtiv_ovr_series_settings_max, int, 50, 0, 100000,
    "The maximum number of plot series to store in rtiv_ovr_series_settings.");

RTI(rtiv_annotation_group_settings, const char*,
    "", 0, 10000,
    "Annotation group options. Format \"NAME*OPTS!NAME*OPTS!...\", where "
    "NAME is the name of an overview series and OPTS is a string of option "
    "characters: D, display; d, don't display.");
RTI(rtiv_annotation_dialog_width, int, 400, 1, 10000,
    "Width of annotation preferences dialog.");
RTI(rtiv_annotation_dialog_height, int, 800, 1, 10000,
    "Height of annotation preferences dialog.");
RTI(rtiv_disassembly_pane_width, int, 200, 0, 10000,
    "Width of disassembly pane.");
RTI(rtiv_annotation_message_window_width, int, 500, 1, 10000,
    "Width of annotation messages window.");
RTI(rtiv_annotation_message_window_height, int, 400, 1, 10000,
    "Height of annotation messages window.");
RTI(rtiv_annotation_message_pane_height, int, 40, 0, 10000,
    "Height of annotation message pane (on segment plots).");

RTI(rtiv_annotation_enable_plot_area, int, 1, 0, 1,
    "If 0, don't show any plot-area annotations.");
