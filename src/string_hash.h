/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2006 Louisiana State University

// $Id$

// String-indexed hash table.

// Explicit instantiations are at the end of the file.


#ifndef _STRING_HASH_H_
#define _STRING_HASH_H_

#include <string.h>
#include <stdlib.h>
#include <memory>


template <typename Data> class PString_Hash {
public:

  PString_Hash<Data>(int initial_size_lg = 5)
  {
    density = 0.6;
    size_lg = initial_size_lg - 1;
    size = occupancy = 0;
    storage = NULL;
    storage_sequential = NULL;
    iterate_ss_idx = 0;
    memset(&empty_rv,0,sizeof(empty_rv));
    grow();
  }

  ~PString_Hash<Data>()
  {
    for(int i=0; i<size; i++) if( storage[i].key ) free(storage[i].key);
    free(storage);
  }

  Data& operator [] (const char *key) { return get((char*)key,true); }
  // If only there was a way to tell if [] were on the lhs!!
  //  bool operator [] (const char *key) { return present(key); }

  // Non-allocating.
  bool lookup(const char* key, Data &d)
  {
    int idx;
    if( !find(key,idx) ) return false;
    d = storage[idx].data;
    return true;
  }

  // Return true if KEY is present.
  bool present(const char *key){ int i; return find(key,i); }

  // Return data, allocating a new element if necessary.
  Data& get(char *key, bool copy_key = true);

  // Set return value to use by function get when key not found.
  void set_empty_rv(Data d){ empty_rv = d; empty_rv_set = true; }

  // Return size of hash table.
  int get_size() { return size; }
  // Return number of items in hash table.
  int occ() { return occupancy; }

  char* iterate_keys()
  {
    if( iterate_ss_idx == occupancy ) {iterate_ss_idx = 0; return NULL;}
    return storage_sequential[iterate_ss_idx++]->key;
  }
  Data* iterate()
  {
    if( iterate_ss_idx == occupancy ) {iterate_ss_idx = 0; return NULL;}
    return &storage_sequential[iterate_ss_idx++]->data;
  }
  bool iterate(Data& d)
  {
    Data* const rv = iterate();
    if( !rv ) return false;
    d = *rv;
    return true;
  }

  // Used for tuning.
  int tries_sum;
  int tries_count;

private:
  int size_lg;
  double density;
  int grow_threshold;

  struct Storage {char *key; Data data;} *storage, **storage_sequential;
  Data dummy_data;
  Data empty_rv;   // Used as a return value when data not found.
  bool empty_rv_set;  // If not, allocate.
  int size, mask, occupancy, stride;
  int iterate_ss_idx;

  bool find(const char *key, int &idx, int &tries, int limit);
  bool find(const char *key, int &idx)
  { int tries; return find(key,idx,tries,occupancy); }
  int hash(const char *str);
  void grow();

};

template <typename Data> Data&
PString_Hash<Data>::get(char *key, bool cpy)
{
  int idx, tries;
  if( find(key,idx,tries,occupancy+1) ) return storage[idx].data;
  if( tries > 3 && occupancy > grow_threshold )
    {
      grow();
      find(key,idx,tries,occupancy+1);
    }
  const int last_occ = occupancy++;
  storage[idx].key = cpy ? strdup(key) : key;
  if( empty_rv_set )
    storage[idx].data = empty_rv;
  else
    new (&storage[idx].data) Data();
  storage_sequential[last_occ] = &storage[idx];
  return storage[idx].data;
}

template <typename Data> bool
PString_Hash<Data>::find(const char *key, int &idx, int& tries, int limit)
{
  tries = 0;
  tries_count++;
  idx = hash(key);

  while( 1 )
    {
      if( !storage[idx].key ) return tries_sum+=tries, false;
      if( !strcmp(storage[idx].key,key) ) return tries_sum+=tries, true;
      if( ++tries == limit ) return tries_sum+=tries, false;
      idx = mask & ( idx + stride );
    }
}

template <typename Data> void
PString_Hash<Data>::grow()
{
  const int old_size = size;
  Storage* const old_storage = storage;
  Storage** const old_storage_sequential = storage_sequential;
  size_lg++;
  size = 1 << size_lg;
  mask = size - 1;
  const static int strides[] =
    //1  2  4  8  16  32  64  128, 256, 512
    { 1, 1, 1, 3, 7 }; //,  13, 29,  71, 113, 197 };
  const static int table_size = sizeof(strides)/sizeof(strides[0]);
  stride = strides[ size_lg >= table_size ? table_size - 1 : size_lg ];
  grow_threshold = int( size * density );
  storage = (Storage*)calloc(size,sizeof(storage[0]));
  storage_sequential = (Storage**)calloc(size,sizeof(void*));
  occupancy = 0;
  for(int i=0; i<old_size; i++)
    if( old_storage[i].key )
      get(old_storage[i].key,true) = old_storage[i].data;
  tries_sum = 0;
  tries_count = 0;
  if( old_storage ) free( old_storage );
  if( old_storage_sequential ) free( old_storage_sequential );
}

template <typename Data> int
PString_Hash<Data>::hash(const char *str)
{
  const char *str_end = str;
  while(*str_end++);
  int size = str_end - str - 1;
  int size_sc = ( size >> 2 ) <<2;
  const int s1 = 8, s2 = 12, s3 = 24;

  int i;
  unsigned sum = 0;
  for(i=0; i<size_sc; i+=4)
    sum +=
      (str[i+3] + (str[i+2] << s1) + (str[i+1] << s2) + (str[i] << s3))
      ^((str[i+1] + (str[i] << s1) + (str[i+3] << s2) + (str[i+2] << s3))<<2)
      ^((str[i] + (str[i+1] << s1) + (str[i+2] << s2) + (str[i+3] << s3))<<4)
      ^((str[i+2] + (str[i+3] << s1) + (str[i] << s2) + (str[i+1] << s3))<<6);

  int remaining = size - i;

  if( remaining == 1 )
    sum += str[i];
  else if( remaining == 2 )
    sum +=
      ( str[i+1] + ( str[i] << s1 ) )
      ^(( str[i] + ( str[i+1] << s1 ) )<<4);
  else if( remaining == 3 )
    sum +=
      ( str[i+2] + ( str[i+1] << s1 ) + ( str[i] << s2 ) )
      ^ (( str[i+1] + ( str[i] << s1 ) + ( str[i+2] << s2 ) )<<2)
      ^ (( str[i] + ( str[i+2] << s1 ) + ( str[i+1] << s2 ) )<<4);

  unsigned int rv = 0;
  while( sum ) {rv ^= sum; sum >>= 9; }
  return rv & mask;
}


#ifdef SHTEST
#include <stdio.h>
int main(void)
{
  PString_Hash<int> vp_hash;
  int occ_shadow = 0;

  vp_hash["Larry/Moe/Curly"] = 1;  occ_shadow++;
  vp_hash["Larry/Moe"] = 2;  occ_shadow++;

  {
    int limit = 3000000;
    int stride = 7;
    for(int i=0; i<limit; i++)
      {
        const int n = i*stride + 120391232;
        char numstr[5000];
#undef HAVE_NUMBER_PROGRAM
#ifdef HAVE_NUMBER_PROGRAM
        char num[100];
        sprintf(num,"number %d",n);
        FILE *p = popen(num,"r");
        fgets(numstr,4999,p);
        fclose(p);
#else
        sprintf(numstr,"%d",n);
#endif
        vp_hash[numstr] = n;  occ_shadow++;
        if( i % 50000 == 0 )
          {
            int size = vp_hash.get_size();
            int occ = vp_hash.occ();
            printf(" size %6d  occ %6d = %6d used %.4f  search %6.3f  "
                   "tries %d\n",
                   size, occ, occ_shadow,
                   double(occ)/double(size),
                   double(vp_hash.tries_sum)/vp_hash.tries_count,
                   vp_hash.tries_sum
                   );
            if( occ != occ_shadow )
              {
                printf("Wrong number of items in hash.\n");
                exit(1);
              }
          }
      }
    for(int i=0; i<limit; i++)
      {
        const int n = i*stride + 120391232;
        char numstr[5000];
#ifdef HAVE_NUMBER_PROGRAM
        char num[100];
        sprintf(num,"number %d",n);
        FILE *p = popen(num,"r");
        fgets(numstr,4999,p);
        fclose(p);
#else
        sprintf(numstr,"%d",n);
#endif
        if( !vp_hash.present(numstr) )
          {
            printf("Expected to find %d (%d) (%s)\n",n,i,numstr);
            exit(1);
          }
        int d = vp_hash[numstr];
        if( d != n )
          {
            printf("Wrong number stored %d (correct) != %d\n",n,d);
            exit(1);
          }
      }
  }

  bool rv = vp_hash.present("Larry/Moe"); // will return false.
  printf("rv is %d\n",rv);
  return 0;

}
#endif


#endif
