/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2006 Louisiana State University

// $Id$

#define RTRC_STATE_BASE 100
#define RTRC_STATE_DECODE ( RTRC_STATE_BASE + 0 )
#define RTRC_STATE_FOUND  ( RTRC_STATE_BASE + 1 )
#define RTRC_STATE_DOOMED  ( RTRC_STATE_BASE + 5 )
#define RTRC_STATE_WAIT  ( RTRC_STATE_BASE + 10 )
#define RTRC_STATE_WAIT_LD  ( RTRC_STATE_BASE + 11 )
#define RTRC_STATE_WAIT_ST  ( RTRC_STATE_BASE + 12 )
#define RTRC_STATE_READY  ( RTRC_STATE_BASE + 20 )
#define RTRC_STATE_WAIT_FU  ( RTRC_STATE_BASE + 30 )
#define RTRC_STATE_PREREADY  ( RTRC_STATE_BASE + 50 )
#define RTRC_STATE_COMPLETE   ( RTRC_STATE_BASE + 80 )
#define RTRC_STATE_NOP    ( RTRC_STATE_BASE + 96 ) // Used to set tag.
#define RTRC_STATE_LEFT   ( RTRC_STATE_BASE + 97 )
#define RTRC_STATE_SQUASH ( RTRC_STATE_BASE + 98 )
#define RTRC_STATE_COMMIT ( RTRC_STATE_BASE + 99 )
#define RTRC_STATE_BLACK ( RTRC_STATE_BASE + 100 )
#define RTRC_STATE_WHITE ( RTRC_STATE_BASE + 101 )
#define RTRC_STATE_GRAY ( RTRC_STATE_BASE + 102 )
#define RTRC_STATE_GLACIER_BLUE ( RTRC_STATE_BASE + 103 )

#define DS_FMT_VARIABLE_PATH_DELIM '/'

#define DS_FMT_IDX_Segdata_Partition_info 1
#define DS_FMT_IDX_Segdata_Partition_info_Attributes 1
#define DS_FMT_IDX_Segdata_Partition_info_Snip_serial 2
#define DS_FMT_IDX_Segdata_Partition_info_Parent 3
