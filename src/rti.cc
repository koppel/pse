/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2010 Louisiana State University

// $Id$

/// Code needed to declare, save, and restore preferences.

#include <cstring>
#include <cctype>
#include <sys/stat.h>
#include "messages.h"
#include "rti.h"
#include "pse.h"

static const string var_type_ptr[] = {"int", "double", "const char*", ""};

void
rti(void *var_ptr, const char *var_name, const char *var_type_name,
    double min, double max, const char *msg)
{
  app.run_time_initialization->list_insert
      (var_ptr, var_name, var_type_name, min, max, msg);
}

RTI_Type
Run_Time_Initialization::to_rti_type(const char *type_name)
{
  string const *i = var_type_ptr;
  for(; *i != ""; ++i)
    if(*i == type_name) return (RTI_Type)(i - var_type_ptr);

  if(*i == "") fatal("Unknown parameter type: %s", type_name);
  return RTI_Error;
}

void
Run_Time_Initialization::list_insert(void *var_ptr, const char *var_name,
				     const char *var_type_name, double min,
				     double max, const char *msg)
{
  RTI_Type var_type = to_rti_type(var_type_name);

  if((list_ptr - list) >= MAXRTI)
    fatal("Too many variable initializations.");

  list_ptr->var_name = var_name;
  list_ptr->type = var_type;
  list_ptr->msg = msg;
  list_ptr->min = min;
  list_ptr->max = max;
  list_ptr->var_ptr = var_ptr;

  if(var_type == RTI_String && *((char**)var_ptr))
    *((char**)list_ptr->var_ptr) = strdup(*((char**)var_ptr));

  ++list_ptr;
}

void
Run_Time_Initialization::init_value_save()
{
  for(RTI_Info *an_rti = list; an_rti < list_ptr; ++an_rti)
    {
      switch(an_rti->type)
	{
	  case RTI_Int:
	    an_rti->init_val_ptr = (int*)malloc(sizeof(int));
	    *((int*)an_rti->init_val_ptr) = *((int*)an_rti->var_ptr);
	    break;

	  case RTI_Double:
	    an_rti->init_val_ptr = (double*)malloc(sizeof(double));
	    *((double*)an_rti->init_val_ptr) = *((double*)an_rti->var_ptr);
	    break;

	  case RTI_String:
            if(*((char**)an_rti->var_ptr))
              an_rti->init_val_ptr = strdup(*((char**)an_rti->var_ptr));
	    break;

	  default:
	    fatal("Internal error occurred while trying to retrieve type for "
		  "RTI variable %s", an_rti->var_name.c_str());
	}
    }
}

using std::ios;

bool
Run_Time_Initialization::parse_line(const char *filename,
				    istringstream &line, string &comments)
{
  string var_name;
  line >> var_name;

  RTI_Info *an_rti = list;
  while(an_rti < list_ptr && var_name != an_rti->var_name) ++an_rti;
  if(an_rti == list_ptr) return false;

  an_rti->comments = comments;
  pref_file_rti_ptr[pfr_cur_index++] = an_rti - list;

  string buffer, data_type;
  double value = 0;
  switch(an_rti->type)
    {
      case RTI_Int:
	data_type = "n integer";
	line >> value;
	*((int*)an_rti->var_ptr) = (int)value;
	break;

      case RTI_Double:
	data_type = " double";
	line >> value;
	*((double*)an_rti->var_ptr) = value;
	break;

      case RTI_String:
	data_type = " string";
	line.seekg(1, ios::cur);
	getline(line, buffer);
        if(*((char**)an_rti->var_ptr)) free(*((char**)an_rti->var_ptr));
	*((char**)an_rti->var_ptr) = strdup(buffer.c_str());
	value = buffer.length();
	break;

      default:
	fatal("Unrecognized variable type for RTI variable %s",
	      var_name.c_str());
    }

  if(!line && an_rti->type != RTI_String)
    fatal("Expected a%s value for variable \"%s\" in file \"%s\".\n",
	  data_type.c_str(), var_name.c_str(), filename);

  if(value < an_rti->min || value > an_rti->max)
    fatal("Value of RTI parameter %s, %.2f, not in range [%.2f, %.2f].",
	  var_name.c_str(), value, an_rti->min, an_rti->max);

  return true;
}

void
Run_Time_Initialization::option_examine_save(const char *opt_savep)
{
  opt_save = opt_savep;
  if( opt_save != "overwrite-ask"
      && opt_save != "overwrite-never"
      && opt_save != "overwrite-always" )
    fatal("Illegal value for rti rtiv_rti_save, \"%s\", should be "
          "overwrite-ask, overwrite-always, or overwrite-never.\n", opt_savep);
}

using std::ifstream;

static bool
get_line(ifstream &pf, string &line, int &first_nws_indx)
{
  if(!getline(pf, line)) return false;
  if(line.empty()) return true;

  using std::isspace;
  for(int i = 0; i < (int)line.length(); ++i)
    if(!isspace(line.at(i))) { first_nws_indx = i; break; }

  return true;
}

void
Run_Time_Initialization::file_parse(const char *filename)
{
  rti_register();

  ifstream pf(filename, ios::in);
  if(!pf) { init_value_save(); return; }

  struct stat stat_info;
  if(stat(filename, &stat_info) != -1)
    mod_time_at_read = stat_info.st_mtime;

  string line, comments;
  int first_nws_indx = 0;
  for(string line; get_line(pf, line, first_nws_indx);)
    {
      if(line.empty() || line.at(first_nws_indx) == '#')
	{
	  if(!line.empty()) comments.append(line);
	  comments.append("\n");
	  continue;
	}

      istringstream str(line);
      if(parse_line(filename, str, comments)) { comments = ""; continue; }

      str.seekg(0, ios::beg);
      str >> line;
      fatal("Unknown preference option \"%s\"\n", line.c_str());
    }

  final_comments = comments;
  init_value_save();
}

using std::ofstream;

void
Run_Time_Initialization::variable_value_write(ofstream &pf, const int &index)
{
  switch(list[index].type)
    {
      case RTI_Int: pf << *((int*)list[index].var_ptr) << "\n"; break;
      case RTI_Double: pf << *((double*)list[index].var_ptr) << "\n"; break;
      case RTI_String: pf << *((char**)list[index].var_ptr) << "\n"; break;
      default:
	ASSERTS(false);
    }
}

void
Run_Time_Initialization::file_write(const char *filename)
{
  bool modified = false;
  for(RTI_Info *ri = list; ri < list_ptr; ++ri)
    {
      switch(ri->type)
	{
	  case RTI_Int:
	    if(*((int*)ri->init_val_ptr) != *((int*)ri->var_ptr))
	      modified = ri->modified = true;
	    break;

	  case RTI_Double:
	    if(*((double*)ri->init_val_ptr) != *((double*)ri->var_ptr))
	      modified = ri->modified = true;
	    break;

	  case RTI_String:
            if(!(*((char**)ri->var_ptr))) break;
            if(!ri->init_val_ptr || 
               strcmp((char*)ri->init_val_ptr, *((char**)ri->var_ptr)))
	      modified = ri->modified = true;
	    break;

	  default:
	    fatal("Internal error occurred while writing the preferences"
		  " file.");
	}
    }

  if(!modified) return;

  struct stat stat_info;
  if( opt_save != "overwrite-always" && stat(filename, &stat_info) != -1)
    {
      time_t mod_time_at_write = stat_info.st_mtime;
      if(mod_time_at_write != mod_time_at_read)
        {
          if( opt_save == "overwrite-never" ) return;
          int response = 
            dialog_question
            (NULL,
             "The PSE settings file, \"%s\", has been modified, perhaps by "
             "another PSE session.  Replace those settings with the ones "
             "set in this session?  "
             "(Use rtiv_rti_save to avoid this question.)",
             filename);

          if(response != GTK_RESPONSE_YES) return;
        }
    }

  ofstream pf(filename);
  if(!pf)
    {
      dialog_warning(NULL, "Unable to save preferences because %s could "
		     "not be opened for writing.", filename);
      return;
    }

   for(int i = 0; i < pfr_cur_index; ++i)
     {
       int j = pref_file_rti_ptr[i];
       pf << list[j].comments << list[j].var_name << ' ';
       variable_value_write(pf, j);
       list[j].written = true;
     }

   if(final_comments != "\n") pf << final_comments;

   for(RTI_Info *ri = list; ri < list_ptr; ++ri)
     {
       if(ri->written || !ri->modified) continue;
       pf << "\n# " << ri->msg << "\n" << ri->var_name << ' ';
       int j = (int)(ri - list);
       variable_value_write(pf, j);
     }
}
