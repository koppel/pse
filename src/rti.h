/// PSE - A micro-architecture simulation dataset viewer. -*- c++ -*-
/// Copyright (c) 2010 Louisiana State University

// $Id$

/// Code needed to declare, save, and restore preferences.

#ifndef RTI_H
#define RTI_H


#include <string>
#include <sstream>
#include <fstream>
#include <malloc.h>

const int MAXRTI = 100;
enum RTI_Type{RTI_Int, RTI_Double, RTI_String, RTI_Error};

using std::string;

struct RTI_Info
{
  bool modified, written;
  double min,max;
  string var_name, msg, comments;
  void *var_ptr, *init_val_ptr;
  RTI_Type type;

  RTI_Info()
  {
    init_val_ptr = NULL; var_ptr = NULL; modified = false; written = false;
  }
  ~RTI_Info()
  {
    if(init_val_ptr) free(init_val_ptr);
  }
};

using std::istringstream;
using std::ofstream;

class Run_Time_Initialization {
public:
  Run_Time_Initialization() 
  { 
    list_ptr = list; pfr_cur_index = 0;
    mod_time_at_read = -1;
  }
  void list_insert(void *var_ptr, const char *var_name,
                   const char *var_type_name,
		   double min, double max, const char *msg);
  void file_parse(const char *filename);
  void option_examine_save(const char *save_option);
  void file_write(const char *filename);

private:
  RTI_Info list[MAXRTI], *list_ptr;
  int pref_file_rti_ptr[MAXRTI], pfr_cur_index;
  string final_comments;
  time_t mod_time_at_read;
  string opt_save;

  RTI_Type to_rti_type(const char *type_name);
  void init_value_save();
  bool parse_line(const char *filename, istringstream &line,
		  string &comments);
  void variable_value_write(ofstream &pf, const int &index);
};

void rti_register(void);
void rti(void *var_ptr, const char *var_name, const char *var_type_name,
	 double min_val, double max_val, const char *doc_str);

#endif
