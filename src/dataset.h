/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2014 Louisiana State University

// $Id$

#ifndef DATASET_H
#define DATASET_H

#include <gtk/gtk.h>
#include "pse.h"
#include "arrays.h"
#include "dataset_rfile.h"
#include "decode_sparc.h"
#include "gtk_utility.h"
#include "messages.h"
#include "misc.h"
#include "string_hash.h"
#include "ovr_plot_manager.h"

#define PLOT_SERIES_DISPLAYED 4
#define TAG_MAX INT_MAX
#define SEQUENCE_NUMBER_UNSET -9999

#define PC_NA -1 // Assigned to an insn when PC unknown or not applicable.
#define PC_NEVER -2  // Never assigned to an insn PC field, used for search.
#define TAG_NA -1  // Never assigned to an insn tag field, used for search.
#define TAG_NEVER -10  // Never assigned to an insn tag field, used for search.

typedef int64_t ds_num;

enum Dataset_Dispatch_Cmd {
  DS_Disp_State_in4 = DSF_CMD_FIRST_FREE,
  DS_Disp_State_in8,
  DS_Disp_PC_in4,
  DS_Disp_PC_in8,
  DS_Disp_EA_in4,   // Effective Address (of load or store).
  DS_Disp_EA_in8,
  DS_Disp_Text_in4, // Encoded Machine Instruction
  DS_Disp_Text_in8,
  DS_Disp_CWP_in4,  // Current window pointer before insn decoded.
  DS_Disp_CWP_in8,  // (CWP identifies SPARC register window.)
  DS_Disp_Time_in4,
  DS_Disp_Time_in8,
  DS_Disp_Partition_Sequence_in4,  // Program order of instruction parts.
  DS_Disp_Partition_Sequence_in8,
  DS_Disp_Annotation_Insn,  // Text annotation associated with most recent insn.
  // Text annotation associated with following insn, until next span.
  DS_Disp_Annotation_Insn_Span,
  DS_Disp_Annotation_Partition,
  DS_Disp_Error
};

inline char* strescape(const char *str) {return g_strescape(str,"\n\r\t\"");}

struct State_Info {
  const char *family, *color_spec, *name, *description;
  GdkColor gdk_color;
  const char *ps_color_code;
  bool in_rob;
};

// Simulation Variable Info
//
struct Sim_Var_Info {
  DSF_String name;            // Name. Does not include hierarchy.
  DSF_String val_str;         // Formatted value. (For string, plain value.)
  DSF_Primitive_Types data;   // Raw data. (Not used for strings.)
  char def_type;              // Type: DSF_CMD_IDX_IN4, etc.
  DSF_String doc_string;
};

struct Insn_Partition_Info {
  int count;  // Number of instructions in partition. (If ds well formed.)
  int rank;   // Rank of serial, 1 is smallest, 0 is never assigned.

  int sequence_number;   // Unique ID for partition.
  int program_order_idx; // Used to arrange in program order.
  int parent_sn;         // Sequence number of predecessor in program order.
  bool right_path;       // Path down oldest children.
  int snip_serial;   // Number assigned by simulator to groups of insn.
  int annotation_elt;
  bool seg_plot_annotation_assembly_verified; // Tested markup on Pango.
  char *seg_plot_annotation_assembly_before;
  char *seg_plot_annotation_assembly_after;
  PSList<char*> seg_plot_annotation_messages;
  int attributes;
  int tag_lowest; // The lowest instruction tag within the snip.
  int tag_highest;
  int commit_max_idx; // Index of latest commit.

  Insn_Partition_Info *prev, *next;
  Insn_Partition_Info *snip_prev, *snip_next;

  Insn_Partition_Info()
  {
    snip_serial = 0;  attributes = 0;  count = 0;
    parent_sn = SEQUENCE_NUMBER_UNSET;
    right_path = false;
    sequence_number = SEQUENCE_NUMBER_UNSET;
    prev = next = snip_prev = snip_next = NULL;
    tag_lowest = TAG_MAX; tag_highest = -1;
    commit_max_idx = 0;
    annotation_elt = -1;
    seg_plot_annotation_assembly_verified = false;
    seg_plot_annotation_assembly_before = NULL;
    seg_plot_annotation_assembly_after = NULL;
  }
};

struct Colorkey_Info {
  GtkWidget *colorkey_hbox;
  GtkWidget *draw_area;
  GtkWidget *align;
  GtkWidget *statename_label;
  GdkGC *gc;
};

struct ROB_Tags {
  int tag;
  int num;
};

struct Feature {
  GdkColor* color; bool fill;
  GdkColor* color_inside;
  Feature(GdkColor *color, bool fill = true)
    :color(color),fill(fill),color_inside(NULL){}
  Feature(GdkColor *color, GdkColor *color_inside)
    :color(color),fill(false),color_inside(color_inside){}
};

struct State_Change
{
  State_Change (int cp,int stp,int tagp,int idxp)
  {c=cp; st=stp; tag=tagp; sidx = idxp;  plot_state = -1; }
  int c;
  int limit_c;     // One cycle past end of state.
  int tag;         // Relative to beginning of segment. Position in fetch ord.
  int sidx;        // Unique number for state change.
  int st;          // State from dataset file.
  int plot_state;  // State used for plotting.
};

struct ROB_Rect_Info {
  int st, x1, x2, y1, y2;
  int y1_p;     // y1 in window coordinates. (For pointer matching.)
  int t1;       // Tag of first instruction covered by rectangle.
};

struct Insn_Dataset_Info {
  Insn_Dataset_Info()
  {
    pre_ex = doomed = false;
    rob_dequeue_c = -1;
    pc = text = ea = 0;
    ea_valid = false;
    cwp = CWP_Not_Provided;
    first_state_change = 0;
    commit_idx = 0;
    si = NULL;
    insn_partition_sequence = SEQUENCE_NUMBER_UNSET;
    insn_partition_info = NULL;
    annotation_elt = -1;
  }
  int idxst;  // Position in program order. (Sorted by partition seq, tag.)
  int rob_enqueue_c_envelope; // For plotting convenience, monotonic.
  int idxt;   // Position in decode order.  (Sorted by tag.)

  bool seg_plot_annotation_assembly_verified; // Tested markup on Pango.
  char* seg_plot_annotation_assembly_before;
  char* seg_plot_annotation_assembly_after;
  PStack<Feature> seg_plot_annotation_markers;
  PSList<char*> seg_plot_annotation_messages;

  static const int CWP_Not_Provided = -1;
  int32_t pc;   // Insn address, or zero if unknown or synthesized.
  int32_t ea;   // Effective address (of load or stores.)
  int32_t text; // Encoded instruction.
  int cwp;      // Current window pointer before insn decoded.
  int tag;
  Insn_Partition_Info *insn_partition_info;
  int insn_partition_sequence;
  bool ea_valid;
  int annotation_elt;
  Insn_Dataset_Info *prev, *next; // Program order.

  bool doomed;
  bool pre_ex;
  int first_event_c; // First event, may be before insn in rob.
  int rob_enqueue_c;
  int rob_dequeue_c; // Or last change if earlier.
  int last_event_end_c;
  int commit_idx;    // Number based on event order. First commit is 1.
  int yp; // Vertical pixel position at last paint of last window.
  int envelope_left_c;
  int envelope_right_c;
  int extent_x_left, extent_x_right; // As painted.
  int first_state_change;
  Insn_Decode_Info *si;
};

struct Segment_Data
{
  Segment_Data();

  // Initialized when dataset read;
  //
  long start_pos; // Offset in to file holding dataset.
  int icount;     // Number of instructions that commit during segment.
  int segment_num;
  char* segment_label;
  int rank;       // IPC rank.  (Zero is lowest IPC [worst].)
  ds_num seg_start_pcount; // Number of insn in prog, including skipped ones.
  ds_num seg_start_ac, seg_end_ac;
  int seg_duration_c;
  ds_num seg_start_tag, seg_end_tag;
  int idx_max;  // Number of instructions - 1.
  double ipc;
  char *source;   // Source information of first instruction in segment.
  RString source_most_frequent_insn;
  RString source_most_frequent_call_insn;
  long source_most_frequent_pc;

  int data_compr_size, data_uncompr_size;  // For tuning file format.
  int snip_count;

  int idx_inrange(int idx) const {return min(max(idx,0),idx_max); }
  int tag_absolute_to_relative(ds_num at, int out_of_range_rv)
  {
    if( at < seg_start_tag || at > seg_end_tag ) return out_of_range_rv;
    return int( at - seg_start_tag );
  }

  // Initialized by idle routine starting after dataset loads.
  //
  GTree* search_coverage_pc_tree;
  bool search_indexed; // Index created for data within segment.

  // Initialized when segment selected.
  //
  Auto_Array<ROB_Rect_Info> rob_rectangle_info;
  Auto_Array<ROB_Tags> rob_tags;
  Auto_Array<int> rri_idx;
  Auto_Array<int> rob_tags_index;
  Auto_List<State_Change> state_changes;
  Insn_Dataset_Info **idxst_to_info; // Sorted by partition Sequence, then Tag.
  Insn_Dataset_Info **idxt_to_info;  // Sorted by Tag.
  Auto_Array_V<Insn_Dataset_Info> tag_to_info;
  Auto_Hash<Insn_Partition_Info> partition_sequence_to_info;

  Insn_Stream insn_stream;

  //Overview Plot - A plot of specified data collected for each segment of the
  //                simulated code. Each point plotted using data samples
  //                gathered within the corresponding segment.
  double  *ovr_plot_value;
  int *ovr_plot_value_sample_count;
};

struct Dataset
{
  bool dataset_loaded;
  bool readex;                   // When true, executable file has been read.
  const bool gui;

  char *file_name;

  // Set using dataset variable value.
  int dsvv_rob_size, dsvv_issue_width;
  int32_t dsvv_exec_file_mod_time, dsvv_exec_file_size;
  int dsvv_linesize;
  int targ_line_mask;


  Auto_List<Segment_Data> sdata;
  Annotation_Manager *am;
  Ovr_Plot_Manager *opm;
  int search_segment_unindexed; // Next segment to index.

  int sdata_sort_idx;

  Segment_Data **sdata_chron;   // Chronological order.

  Segment_Data *get_sdata(int idx, bool by_current_sort)
  {
    ASSERTA( idx >= 0 && idx < sdata.occ );
    return by_current_sort ? sdata[idx] : sdata_chron[idx];
  }

  PString_Hash<Sim_Var_Info*> sim_var_info_hash;
  Sim_Var_Info* sim_var_info(const char *name)
  {
    Sim_Var_Info* info = NULL;
    sim_var_info_hash.lookup(name,info);
    return info;
  }
  int32_t sim_var_value(const char *name, int32_t nodata)
  {
    if( Sim_Var_Info* const info = sim_var_info(name) )
      {
        ASSERTS( info->def_type == DSF_CMD_IDX_IN4 );
        return info->data.i4;
      }
    return nodata;
  }
  int64_t sim_var_value_64(const char *name, int64_t nodata)
  {
    if( Sim_Var_Info* const info = sim_var_info(name) )
      {
        switch( info->def_type ){
        case DSF_CMD_IDX_IN4: return info->data.i4;
        case DSF_CMD_IDX_IN8: return info->data.i8;
        default: ASSERTS( false );
        }
      }
    return nodata;
  }
  char* sim_var_value(const char *name)
  {
    if( Sim_Var_Info* const info = sim_var_info(name) ) return info->val_str;
    return NULL;
  }

  GtkWidget *data_widget;       // Window for display of dataset variables.
  GtkWidget *color_key_widget;  // Window for display of color key
  GtkWidget *tree_widget;       // For display of dataset variables.
  GtkTreeStore *data_tree;      // Ditto.
  GtkTreeIter *tree_widget_last_active;
  GtkTreeViewColumn *name_column;
  GtkWidget *doc_message_label; // Label to display the docstring.
  GtkWidget *doc_label_frame;   // Frame around doc_message_label.
  GtkWidget *doc_status_bar;    // Statusbar below data_widget.
  PSE_Window* pw_get() const { return pse_windows.peek(); }
  PStack<PSE_Window*> pse_windows;
  PGtkWindowPtr ref_window;     // Reference window, intended for dialogs.
  int window_count;             // Number of child segment windows created.
  int live_window_count;        // Current number of child segment windows.

  // DMK: Make better use of variables below, or eliminate them. 21 June 2002
  int ipc_plot_left_margin, ipc_plot_right_margin;

  int cmd_dispatch_segment[256];
  State_Info state_info[512];  // pe kludge.  4 June 2003, 23:05:22 CDT
  Colorkey_Info colorkey_info[512]; // pe kludge  4 June 2003, 23:05:21 CDT
  Dataset_File df;

  Dataset(bool gui, char *path);  // Initialize structure.
  void file_first_pass();       // Load dataset.
  void new_dataset();         // Load dataset and construct segment window.
  void sortby_pc();
  void sortby_ovr_idx(int overview_plot_idx);
  ~Dataset();
  void invalidate_draw_areas();
private:
  void state_color_set(int st, char *cspec);
  void state_color_set_rgb(int st, double r, double g, double b);
  void state_color_set_defaults();
};


void prepare_rob_data(Dataset *ds, Segment_Data *sd);

#endif
