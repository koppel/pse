//  -*- c++ -*-
/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2007 Louisiana State University

// $Id$

// Data structures designed to support this package and are not complete
// enough for general purpose use.  Emphasis is usually on execution speed.

// An array with self-adjusting bounds and some iteration functions.
// Member functions prev_used and next_used probably make too many
// assumptions for general use.

#ifndef ARRAYS_H
#define ARRAYS_H

#include <memory.h>
#include <memory>
#include<stdlib.h>

template <class Data> class Auto_Hash {
public:
  Auto_Hash(Data *oob = NULL ){ init(oob); }
  ~Auto_Hash()
  {
    if( sorted_storage ) delete sorted_storage;
    for(Data* d = iterate_init(); iterate(d); ) delete d;
    g_hash_table_destroy(htable);
  }

  Data* put(long index)
  {
    Data* const elt = (Data*) g_hash_table_lookup(htable,(gpointer)index);
    if( elt ) return elt;
    Data* const nlt = new Data();
    g_hash_table_insert(htable,(gpointer)index,nlt);
    ss_updated = false;
    return nlt;
  }

  Data *operator [] (long index)
  {
    Data* const elt = (Data*) g_hash_table_lookup(htable,(gpointer)index);
    return elt ? elt : out_of_bounds_rv;
  }

  int occ(){ return g_hash_table_size(htable);}

  Data *iterate_init(){ iterate_setup(); return NULL; }

  long iterate_setup()
  {
    size = occ();
    iterator = 0;
    if( size && !ss_updated ) ss_update();
    return 0;
  }

  Data *iterate()
  {
    if( iterator == size ) { iterator = 0;  return NULL; }
    return operator [] (sorted_storage[iterator++]);
  }

  bool iterate(Data* &d)
  {
    d = iterate();
    return d;
  }

  bool iterate_keys(int &keyi)
  {
    long keyl;
    if( !iterate_keys(keyl) ) return false;
    keyi = (int) keyl;
    return true;
  }

  bool iterate_keys(long &key)
  {
    if( iterator == size ) { iterator = 0;  return false; }
    key = sorted_storage[iterator++];
    return true;
  }

private:
  void init(Data *oob)
  {
    htable = g_hash_table_new(g_direct_hash,g_direct_equal);
    out_of_bounds_rv = oob;
    sorted_storage = NULL;
    iterator = size = 0;
    ss_updated = false;
  }

  void ss_update()
  {
    const int n = occ();
    if( sorted_storage ) delete sorted_storage;
    sorted_storage = new long[n];
    long* ss_ptr = sorted_storage;
    g_hash_table_foreach(htable,ghfunc,(gpointer)&ss_ptr);
    qsort(sorted_storage,n,sizeof(*sorted_storage),comp);
    ss_updated = true;
  }

  static void ghfunc(gpointer key, gpointer value, gpointer user_data)
  {
    long** const ss_ptr = (long**) user_data;
    *ss_ptr[0]++ = (long)key;
  }

  static int comp(const void *a, const void *b)
  {
    return (*(long*)a)-(*(long*)b);
  }

  GHashTable* htable;
  Data* out_of_bounds_rv;
  long* sorted_storage;
  int iterator, size;
  bool ss_updated;
};

template <class Data> class Auto_Array {
public:
  Auto_Array(Data *oob = NULL ){ init(oob); }

  void init(Data *oob = NULL)
  {
    initialized = 0; storage = NULL; size = 0; next_free = 0;  iindex = 0;
    maxindex = 0;
    out_of_bounds_rv = oob;
  }
  ~Auto_Array(){ destruct(); }
  void destruct(){ if(storage)free(storage); storage = NULL; }
  void reset(){ destruct(); init(); }

  Data *operator [] (int index)
  {
    if( index < base || index >= base + size ) return out_of_bounds_rv;
    if( index > maxindex ) maxindex = index;
    return &storage[index - base];
  }

  int index_max() const { return maxindex; }
  int index_min() const { return base; }
  int occ() const { return size; }

  Data *next() {return put(next_free++);}

  Data *iterate_reset(){ iindex = 0; return NULL;}

  Data *iterate_set(int initial_index , int forwardp = 1)
  {
    forward = forwardp;
    iindex = !forward && initial_index == -1 ? size : initial_index;
    return iterate_bd();
  }

  Data *this_or_next_used(int &idxp)
  {
    if( idxp >= maxindex ) return out_of_bounds_rv;
    int idx = idxp <= base ? 0 : idxp - base;
    while ( idx < size && !storage[idx] ) idx++;
    idxp = idx + base;
    if( idx == size ) return out_of_bounds_rv;
    return &storage[idx];
  }

  Data *next_used(int &idxp){ idxp++; return this_or_next_used(idxp); }

  Data *prev_used(int idxp)
  {
    if( idxp <= base ) return out_of_bounds_rv;
    int idx = ( idxp > maxindex ? maxindex : idxp ) - base;
    do {idx--;} while ( idx >= 0 && !storage[idx] );
    if( idx < 0 ) return out_of_bounds_rv;
    return &storage[idx];
  }

  Data *iterate()
  {
    if( iindex > maxindex )
      {
        iindex = 0;  return NULL;
      }
    else
      {
        return &storage[iindex++];
      }
  }

  int iterator_index()
  {
    return iindex + base - 1;
  }

  Data *iterate_backward()
  {
    if( iindex == 0 )
      {
        iindex = size - 1;  return NULL;
      }
    else
      {
        return &storage[iindex--];
      }
  }

  Data *iterate_bd()
  {
    return forward ? iterate() : iterate_backward();
  }

  Data *put(int index)
  {
    if( index < base || index >= base + size )
      {
        if( !initialized ) {
          base = index; size = 1;
          storage = (Data*) malloc(sizeof(Data));
          new (storage) Data();
          initialized = 1;
        }

        if( index < base )
          {
            if( index < 0 ) fatal("Negative indices?");
            int new_base = index & ~( ( size >> 2 ) - 1 );

            int new_size_bound = maxindex - new_base + 1;
            int delta = base - new_base;

            Data *source = storage;
            const bool allo = size < new_size_bound;
            if( allo )
              {
                while( size < new_size_bound ) size <<= 1;
                storage = (Data*) malloc( size * sizeof(Data) );
                for(int i=maxindex-new_base; i<size; i++)
                  new (&storage[i]) Data();
              }

            memmove(&storage[delta],source,(maxindex-base+1) * sizeof(Data));
            if( allo ) free(source);
            for(int i=0; i<delta; i++) new (&storage[i]) Data();
            base = new_base;
          }
        else
          {
            int old_size = size;
            while( base + size <= index ) size <<= 1;
            storage = (Data*)realloc(storage,size * sizeof(Data));
            for(int i=old_size; i<size; i++) new (&storage[i]) Data();
          }
      }
    if( maxindex < index ) maxindex = index;
    return &storage[index-base];
  }

  int size;
  int next_free;

private:
  int initialized, forward;
  int base;
  int iindex, maxindex;
  Data *out_of_bounds_rv;
  Data *storage;
};

template <class D> class Elt
{
public:
  Elt(){ v = false; }
  bool operator ! () { return !v; }
  D d;
  bool v;
};

template <class Data> class Auto_Array_V : public Auto_Array<Elt<Data> >
{
  typedef Auto_Array<Elt<Data> > AE;
public:
  Auto_Array_V(){ AE::init(NULL); iindex=0; };
  Data* put(int idx)
  {
    Elt<Data>* const e = AE::put(idx);
    e->v = true;
    return &e->d;
  };

  Data *this_or_next_used(int &idxp)
  { return data_or_null(AE::this_or_next_used(idxp)); }
  Data *next_used(int &idxp) {return data_or_null(AE::next_used(idxp));}

  Data *iterate()
  {
    if( iindex == AE::size )
      {
        iindex = 0;  return NULL;
      }
    else
      {
        Data* const rv = this_or_next_used(iindex);
        iindex++;
        if( !rv ) iindex = 0;
        return rv;
      }
  }

  Data *operator [] (int idx) {return data_or_null(AE::operator [] (idx));}

private:
  Data* data_or_null(Elt<Data>* e){ return e ? &e->d : NULL; }
  int iindex;
};

template <class Data> class Auto_List {
public:
  void init(){
    occ = 0; size = 0; storage = NULL; iindex = 0;
    destructor_frees_elements = false; }
  void destruct()
  {
    Data *elt;
    if( destructor_frees_elements ) while( ( elt = iterate() ) ) free(elt);
    if(storage){free(storage); storage = NULL; }
  }
  void reset(){ destruct(); init(); }

  Auto_List(){ init(); }
  ~Auto_List(){ destruct(); }

  Data *operator [] (int index) {return storage[index];}
  void operator += (Data *e) { put(e); }

  void put(Data *e)
  {
    if( occ >= size )
      {
        if( size ) size <<= 1; else size = 4;
        storage = (Data**) realloc(storage, size * sizeof(Data*) );
      }
    storage[occ++] = e;
  }

  Data *iterate()
  {
    if( iindex == occ ) { iindex = 0; return NULL; }
    else return storage[iindex++];
  }

  int iterator_index() {return iindex - 1;}

  void iterate_set(int iindexp)
  {
    iindex = iindexp;
    if( iindex < 0 || iindex >= occ ) fatal("Internal Error.");
  }

  int occ;
  int base;
  int size;
  bool destructor_frees_elements;
  Data **storage;
private:
  int iindex;

};


template<class Data> class Stack {
  struct link
  {
    Data *data;
    link *next;
    link(Data *dat, link *nxt):
      data(dat), next(nxt) {}
  };
  link *top;
public:
  Stack():top(NULL) {}
  ~Stack() {while( top ) delete pop();}
  void push(Data *dat) {top = new link(dat,top);}
  Data *pop()
  {
    if( !top ) return NULL;
    Data *result = top->data;
    link *oldtop = top;
    top = top->next;
    delete oldtop;
    return result;
  }
};

#endif
