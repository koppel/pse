/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2014 Louisiana State University

// $Id$

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <memory.h>
#include "gtk_utility.h"
#include "messages.h"
#include "readex.h"
#include "enames.h"
#include "pse_window.h"
#include "dataset.h"
#include "dataset_rfile.h"
#include "string_hash.h"
#include "benchmark.h"
#include "handlers.h"
#include "rti_declarations.h"
#include "annotation_manager.h"
#include "ovr_plot_manager.h"

#define OVR_PLOT_SERIES_IPC 0

struct ROB_Entry
{
  int st;
  int c;
  int tag; // Relative to seg_start;
  ROB_Entry(int c,int st,int tag): st(st), c(c), tag(tag) {}
};

char* strescape(const char *str);

static bool
fatal_return_true(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);

  char *msg_part_1 = g_strdup_vprintf(fmt, ap);
  fatal("%s",msg_part_1);
  free(msg_part_1);
  va_end(ap);
  return true;  // All this hassle for a return value that's not used. (Yet.)
}

Segment_Data::Segment_Data():insn_stream()
{
  source_most_frequent_insn = "";
  source_most_frequent_call_insn = "";
  search_segment_init(this);
}

Dataset::Dataset(bool gui, char *path):gui(gui)
{
  df.set_error_handler(fatal_return_true);
  ref_window = NULL;
  tree_widget = color_key_widget = data_widget = NULL;

  dataset_loaded = false;
  window_count = live_window_count = 0;

  file_name = path;
  readex = false;  // Reassigned if executable read.

  for(unsigned int i=0; i<sizeof(state_info)/sizeof(state_info[0]); i++)
    {
      state_info[i].name = state_info[i].family = NULL;
      state_info[i].description = NULL;
      state_info[i].in_rob = true;
      state_info[i].color_spec = NULL;
      state_info[i].ps_color_code = NULL;
    }

  am = new Annotation_Manager(this);
  opm = new Ovr_Plot_Manager();
  if( gui ) new PSE_Window(this);

  if( path ) new_dataset();
}

Dataset::~Dataset()
{
}

void
Dataset::new_dataset()
{
  file_first_pass();
  dataset_loaded = true;
  search_dataset_init(this);
  for(PSE_Windows_Iterator pw(pse_windows);pw;pw++) pw->new_dataset();
  am->setup_after_dataset_load();
  opm->setup_after_dataset_load(this);
}

void
Dataset::state_color_set(int st, char *cspec)
{
  const char *fmt = "/st%d { %.2f %.2f %.2f setrgbcolor } def\n";
  GdkColormap *cmap = app.cmap;

  state_info[st].color_spec = cspec;

  GdkColor *cc = &state_info[st].gdk_color;

  gdk_color_parse(cspec,cc);
  if( !gdk_color_alloc(cmap,cc) ) fatal("Could not allocate color %s.",cspec);

  double r = double(cc->red) / 0xffff;
  double g = double(cc->green) / 0xffff;
  double b = double(cc->blue) / 0xffff;
  state_info[st].ps_color_code = g_strdup_printf(fmt,st,r,g,b);

  if( st > RTRC_STATE_COMMIT ) return;

  State_Info* si_doomed = &state_info[st-100];
  GdkColor *cc_doomed = &si_doomed->gdk_color;

# define toh(c) gushort( 0x9fff + (int(c) - 0x7fff) / 5 )

  cc_doomed->red = 0x4000 + toh(cc->red);
  cc_doomed->green = toh(cc->green);
  cc_doomed->blue = toh(cc->blue);

  if( !gdk_color_alloc(cmap,cc_doomed) ) fatal("Could not allocate color.");

  si_doomed->ps_color_code =
    g_strdup_printf(fmt,st-100,
                    double(cc_doomed->red) / 0xffff,
                    double(cc_doomed->green) / 0xffff,
                    double(cc_doomed->blue) / 0xffff);
# undef toh

  // DMK: Temporary kludge, to be replaced soon.  4 June 2003, 23:02:06 CDT

  State_Info* si_pe = &state_info[st+150];
  GdkColor *cc_pe = &si_pe->gdk_color;

# define toh(c) gushort( 0x9fff + (int(c) - 0x7fff) / 5 )

  cc_pe->red = toh(cc->red);
  cc_pe->green = toh(cc->green);
  cc_pe->blue = 0x4000 + toh(cc->blue);

  if( !gdk_color_alloc(cmap,cc_pe) ) fatal("Could not allocate color.");

  si_pe->ps_color_code =
    g_strdup_printf(fmt,st+150,
                    double(cc_pe->red) / 0xffff,
                    double(cc_pe->green) / 0xffff,
                    double(cc_pe->blue) / 0xffff);
# undef toh
}

void
Dataset::state_color_set_rgb(int st, double r, double g, double b)
{
  if( state_info[st].color_spec ) return;
  state_color_set
    (st,g_strdup_printf("#%02x%02x%02x",
                        int(r * 255.9), int(g * 255.9), int(b * 255.9)));
}

void
Dataset::state_color_set_defaults()
{
  state_color_set_rgb(RTRC_STATE_DECODE, 0.6275, 0.1255, 0.9412); // Purple
  state_color_set_rgb(RTRC_STATE_FOUND, 0.5, 0.5, 0.5);           // Gray?
  state_color_set_rgb(RTRC_STATE_PREREADY, 0.0, 1.0, 0.0);        // Green
  state_color_set_rgb(RTRC_STATE_GLACIER_BLUE, 0.7, 0.7, 1.0);  // Glacier Blue
  state_color_set_rgb(RTRC_STATE_READY, 1.0, 1.0, 0.0);            // Yellow
  state_color_set_rgb(RTRC_STATE_WAIT, 0.190,0.5,0.08);            // SapGreen
  state_color_set_rgb(RTRC_STATE_WAIT_LD,1, 0.980407, 0.803903);// LemonChiffon
  state_color_set_rgb(RTRC_STATE_WAIT_ST, 0.6, 0.6, 0.6);         // Gray
  state_color_set_rgb(RTRC_STATE_WAIT_FU, 1.0, 0.5, 0.0);
  state_color_set_rgb(RTRC_STATE_COMPLETE, 0.0, 0.0, 0.0);        // Black
  state_color_set_rgb(RTRC_STATE_LEFT, 0.0, 0.0, 0.0);            // Black
  state_color_set_rgb(RTRC_STATE_SQUASH, 1.0, 0.0, 0.0);          // Red
                                                   // LightCadmiumYellow
  state_color_set_rgb(RTRC_STATE_COMMIT, 1., 0.689993, 0.059999);
  state_color_set_rgb(RTRC_STATE_BLACK, 0,0,0);
  state_color_set_rgb(RTRC_STATE_WHITE, 1,1,1);
  state_color_set_rgb(RTRC_STATE_GRAY, 0.9,0.9,0.9);
}

void
Dataset::invalidate_draw_areas()
{
  for(PSE_Windows_Iterator pw(pse_windows);pw;pw++)pw->invalidate_draw_area();
}

extern "C" int
comp_ipc(const void *ap, const void *bp)
{
  Segment_Data **a = (Segment_Data**) ap;
  Segment_Data **b = (Segment_Data**) bp;

  return a[0]->ipc < b[0]->ipc ? -1 : a[0]->ipc > b[0]->ipc ? 1 : 0;
}

extern "C" int
comp_top_pc(const void *ap, const void *bp)
{
  Segment_Data **a = (Segment_Data**) ap;
  Segment_Data **b = (Segment_Data**) bp;

  return a[0]->source_most_frequent_pc < b[0]->source_most_frequent_pc 
    ? -1 : a[0]->source_most_frequent_pc > b[0]->source_most_frequent_pc 
    ? 1 : 0;
}

static int comp_ovr_sort_key_idx;  // :-(

extern "C" int
comp_ovr_sort_key(const void *ap, const void *bp)
{
  Segment_Data **a = (Segment_Data**) ap;
  Segment_Data **b = (Segment_Data**) bp;

  const int idx = comp_ovr_sort_key_idx;

  if ( a[0]->ovr_plot_value_sample_count[idx] == 0 
       && b[0]->ovr_plot_value_sample_count[idx] == 0 ) return 0;
  if ( a[0]->ovr_plot_value_sample_count[idx] == 0 ) return 1;
  if ( b[0]->ovr_plot_value_sample_count[idx] == 0 ) return -1;

  return a[0]->ovr_plot_value[idx] < b[0]->ovr_plot_value[idx]
    ? -1 : a[0]->ovr_plot_value[idx] > b[0]->ovr_plot_value[idx]
    ? 1 : 0;
}

static gboolean
bpd_delete_event_cb(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  PSE_Window *sw = (PSE_Window*)data;
  int response =
    dialog_question(sw->main_w, "Would you like to abort loading of the "
                    "dataset? Doing so will end PSE.");
  if(response == GTK_RESPONSE_YES)
  {
    gtk_widget_destroy(widget);
    close_and_maybe_quit(sw, 1);
    exit(1);
  }
  return TRUE;
}


void
Dataset::file_first_pass()
{
  GtkWidget *progress_bar = NULL;
  PGtkDialogPtr bp_dialog;
  gdouble uncompress_frac = 0.2;

  if( gui )
    {
      progress_bar = gtk_progress_bar_new();
      PSE_Window* const sw = pw_get();
      bp_dialog =
        gtk_dialog_new_with_buttons
        ("Loading Dataset", ref_window, GTK_DIALOG_DESTROY_WITH_PARENT, NULL);
      gtk_container_add(GTK_CONTAINER(bp_dialog->vbox), progress_bar);
      gtk_progress_bar_set_text((GtkProgressBar*)progress_bar,"Decompressing");
      gtk_window_set_default_size(bp_dialog,300,50);
      g_signal_connect
        (bp_dialog, "delete_event",
         G_CALLBACK(bpd_delete_event_cb), (gpointer)sw);
      gtk_widget_show_all(bp_dialog);
      yield_to_gtk();
      gtk_progress_bar_set_fraction((GtkProgressBar*)progress_bar, 0.01);
      yield_to_gtk();
    }
  struct stat stat_str;
  stat(file_name,&stat_str);
  off_t flen = stat_str.st_size;

  df.init(file_name);

  if( gui )
    {
      gtk_progress_bar_set_text((GtkProgressBar*)progress_bar,"Scanning");
      gtk_progress_bar_set_fraction
        ((GtkProgressBar*)progress_bar,uncompress_frac);
      yield_to_gtk();
    }

  int current_segment = 0;
  Segment_Data *sd = NULL;
  int event_attribute_unrecognized_count = 0;

  data_tree =
    gtk_tree_store_new(7,G_TYPE_STRING,G_TYPE_STRING,G_TYPE_STRING,
                       G_TYPE_STRING,G_TYPE_INT,G_TYPE_STRING,G_TYPE_STRING);

  GtkTreeIter ti, par_ti, *cur_par_ti = NULL;

  PString_Hash <GtkTreeIter> varpath_tree_hash(6);

  {
    int i;
    for(i = 0; i < DSF_CMD_FIRST_FREE; i++ )
      cmd_dispatch_segment[i] = i;
    for(i = DSF_CMD_FIRST_FREE; i < 256; i++ )
      cmd_dispatch_segment[i] = DS_Disp_Error;
  }

  double last_frac = uncompress_frac;
  DSF_Entry e;

  while( df.get(e) ) switch( cmd_dispatch_segment[ e.type ] ){

  case DSF_CMD_CODING_SYNC:
    break;

  case DS_Disp_Time_in4:
    ASSERTS( false );
    break;

  case DS_Disp_Time_in8:
    ASSERTS( false );
    break;

  case DSF_CMD_DEF_PRIMITIVE_STR:

#define DISP_SET(str,errmsg,c4,c8)                                            \
    ( e.key.s == str )                                                        \
      {                                                                       \
        if( e.def_type != DSF_CMD_IDX_IN4                                     \
            && e.def_type != DSF_CMD_IDX_IN8 )                                \
          fatal("Unexpected format for " errmsg);                             \
        cmd_dispatch_segment[e.data.u1] =                                     \
          e.def_type == DSF_CMD_IDX_IN4 ? (c4) : (c8);                        \
      }

    if( e.key.s == "annotation-insn" )
      {
        if( e.def_type != DSF_CMD_IDX_STR )
          fatal("Unexpected format for annotation.");
        cmd_dispatch_segment[e.data.u1] = DS_Disp_Annotation_Insn;
      }
    else if( e.key.s == "annotation-partition" )
      {
        if( e.def_type != DSF_CMD_IDX_STR )
          fatal("Unexpected format for annotation.");
        cmd_dispatch_segment[e.data.u1] = DS_Disp_Annotation_Partition;
      }
    else if( e.key.s == "annotation-insn-span" )
      {
        if( e.def_type != DSF_CMD_IDX_STR )
          fatal("Unexpected format for annotation.");
        cmd_dispatch_segment[e.data.u1] = DS_Disp_Annotation_Insn_Span;
      }
    else if( e.key.s == "time" )
      {
        if( e.def_type != DSF_CMD_IDX_IN4
            && e.def_type != DSF_CMD_IDX_IN8 )
          fatal("Unexpected format for time.");
        cmd_dispatch_segment[e.data.u1] =
          e.def_type == DSF_CMD_IDX_IN4 ? DS_Disp_Time_in4 : DS_Disp_Time_in8;
      }
    else if DISP_SET
      ("text"," the encoded instruction",DS_Disp_Text_in4,DS_Disp_Text_in8)
    else if DISP_SET
      ("cwp"," the current window pointer", DS_Disp_CWP_in4,DS_Disp_CWP_in8)
    else if DISP_SET("pc"," the program counter",DS_Disp_PC_in4,DS_Disp_PC_in8)
    else if DISP_SET
      ("ea"," the effective address",DS_Disp_EA_in4,DS_Disp_EA_in8)
    else if DISP_SET("partition_sequence"," partition sequence",
                     DS_Disp_Partition_Sequence_in4,
                     DS_Disp_Partition_Sequence_in8);

#undef DISP_SET

    break;

  case DS_Disp_State_in4:
  case DS_Disp_State_in8:
    if( e.type == RTRC_STATE_WAIT ) sd->icount++;
    break;

  case DSF_CMD_STR_STR:

    if( e.key.s == "named simulation variable path" )
      {
        char path_delim_as_str[2] = { DS_FMT_VARIABLE_PATH_DELIM, 0 };
        char *buf = strescape(e.data.s);
        RString path_entry(path_delim_as_str);

        cur_par_ti = NULL;

        for( char *tok = strtok( buf, path_delim_as_str );
              tok;
              tok = strtok( 0, path_delim_as_str ) )
          {
            path_entry += tok;

            if( varpath_tree_hash.present(path_entry) == false )
              {
                gtk_tree_store_append( data_tree, &ti, cur_par_ti );
                par_ti = ti;
                cur_par_ti = &par_ti;
                varpath_tree_hash[path_entry] = ti;
                gtk_tree_store_set( data_tree, &ti, 0, tok, 3, "grey13",
                                    4, PANGO_WEIGHT_BOLD, -1 );
              }
            else
              {
                par_ti = varpath_tree_hash[path_entry];
                cur_par_ti = &par_ti;
              }

            path_entry += path_delim_as_str;
          }
        free(buf);
      }
    break;

  case DSF_CMD_NAMESPACE_STR_BEGIN:
    if( e.key.s == "annotation define" )
      am->definition_read(df);
    else if( e.key.s == "named simulation variable" )
      {
        Sim_Var_Info* const info = new Sim_Var_Info;
        char *fmt = NULL;
        gtk_tree_store_append(data_tree,&ti,cur_par_ti);

        while( df.get( e ) )
          {
            if( e.type == DSF_CMD_NAMESPACE_END ) break;
            if( e.type != DSF_CMD_STR_STR
                && e.type != DSF_CMD_STR_IN8
                && e.type != DSF_CMD_STR_DBL
                && e.type != DSF_CMD_STR_IN4 ) FATAL;

            if( e.key.s == "name" )
              info->name.assign_take(strescape(e.data.s));
            else if( e.key.s == "value" )
              {
                ASSERTS( info->name );

                switch( e.type ){
                case DSF_CMD_STR_STR:
                  info->val_str.assign_take(strescape(e.data.s));
                  info->def_type = DSF_CMD_IDX_STR;
                  break;
                case DSF_CMD_STR_DBL:
                  info->val_str.assign_take
                    (psafe_sprintf_dup( fmt ? fmt : "%f", e.data.d));
                  info->data.d = e.data.d;
                  info->def_type = DSF_CMD_IDX_DBL;
                  break;
                case DSF_CMD_STR_IN4:
                  info->val_str.assign_take
                    (psafe_sprintf_dup( fmt ? fmt : "%d", e.data.i4));
                  info->data.i4 = e.data.i4;
                  info->def_type = DSF_CMD_IDX_IN4;
                  break;
                case DSF_CMD_STR_IN8:
                  info->val_str.assign_take
                    (psafe_sprintf_dup(fmt ? fmt : "%lld", e.data.i8));
                  info->data.i8 = e.data.i8;
                  info->def_type = DSF_CMD_IDX_IN8;
                  break;
                default:
                  FATAL;
                }
              }
            else if( e.key.s == "docstring" )
              info->doc_string = strescape(e.data.s);
            else if( e.key.s == "format" )
              fmt = strescape(e.data.s);    // Needs validity check.
            else
              FATAL;

          }

        if( sim_var_info_hash.present(info->name) ) { ASSERTS( false ); }
        else sim_var_info_hash[info->name] = info;

        char* const store_doc_string =
          info->doc_string ? info->doc_string.str() : (char*)"No docstring.";

        {
          //Only works well for Unix line endings.
          int lines = 0;
          char *vptr = info->val_str;
          while( *vptr ) if( *vptr++ == '\n' && lines++ == 4 ) break;

          if( lines > 4 )
            {
              char c = *vptr;  *vptr = 0;
              RString trimmed(info->val_str);  *vptr = c;
              trimmed += "...";
              gtk_tree_store_set
                (data_tree, &ti, 0, info->name.str(), 1, trimmed.s,
                 2, store_doc_string, 3, "grey23",
                 4, PANGO_WEIGHT_NORMAL, 5, info->val_str.str(), -1);
            }
          else
            {
              gtk_tree_store_set
                (data_tree, &ti, 0, info->name.str(), 1, info->val_str.str(),
                 2, store_doc_string, 3, "grey23",
                 4, PANGO_WEIGHT_NORMAL, 5, NULL, -1);
            }
        }

      }
    else if( e.key.s == "define state" )
      {
        int state_idx = 0;
        while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END ) switch(e.type ){
        case DSF_CMD_DEF_PRIMITIVE_STR:
          if( state_idx ) FATAL;
          state_idx = e.data.u1;
          cmd_dispatch_segment[state_idx] =
            e.def_type == DSF_CMD_IDX_IN4
            ? DS_Disp_State_in4 : DS_Disp_State_in8;

          if( state_info[state_idx].name )
            fatal("Second name, %s, defined for state %s with index %d.",
                  e.key.s.str(), state_info[state_idx].name, state_idx);
          state_info[state_idx].name = strescape(e.key.s);
          continue;
        case DSF_CMD_STR_STR:
          if( !state_idx ) FATAL;
          if( e.key.s == "family" )
            state_info[state_idx].family = strescape(e.data.s);
          else if( e.key.s == "color" )
            {
              if( gui )
                state_color_set(state_idx,e.data.s.dup()); // Needs validity ch.
            }
          else if( e.key.s == "description" )
            state_info[state_idx].description = strescape(e.data.s);
          else if ( !event_attribute_unrecognized_count++ )
            fprintf(stderr,"Unrecognized event attribute, \"%s\"\n"
                    "(Warning only issued once.)\n",e.key.s.str());
          continue;
        case DSF_CMD_STR_IN4:
          if( e.key.s == "in rob" )
            state_info[state_idx].in_rob = e.data.i4;
          else if ( !event_attribute_unrecognized_count++ )
            fprintf(stderr,"Unrecognized event attribute, \"%s\"\n"
                    "(Warning only issued once.)\n",e.key.s.str());
          continue;

        case DSF_CMD_STR_DBL:
          if ( !event_attribute_unrecognized_count++ )
            fprintf(stderr,"Unrecognized event attribute, \"%s\"\n"
                    "(Warning only issued once.)\n",e.key.s.str());
          continue;
        default:
          FATAL;
        }
      }
    else if( e.key.s == "segment start" )
      {
        sd = new Segment_Data;
        sd->insn_stream.set_addr_to_source(RX_addr_to_source);
        sd->icount = 0;
        sd->snip_count = 0;
        ds_num seg_start_dcount = 0;
        ds_num seg_end_dcount = 0;
        ds_num seg_start_ccount = 0;
        ds_num seg_end_ccount = 0;

        df.auto_assign.put("dcount",&seg_start_dcount);
        df.auto_assign.put("ccount",&seg_start_ccount);
        df.auto_assign.put("pcount",&sd->seg_start_pcount,0);
        df.auto_assign.put("time",&sd->seg_start_ac,-1);
        df.auto_assign.put("tag",&sd->seg_start_tag,-1);
        df.auto_assign.put("source",&sd->source,NULL);
        df.auto_assign.put("LABEL",&sd->segment_label,NULL);
        df.auto_assign.scan(e,DSF_CMD_NAMESPACE_STR_BEGIN);
        if( sd->source )
          {
            char* const old_copy = sd->source;
            sd->source = strescape(sd->source);
            free(old_copy);
          }

        if( df.eof() ) {delete sd; break;}

        if( e.key.s != "segment data" ) FATAL;

        long start_pos = df.tell();
        df.skip_block();
        if( ! df.get(e) ) {delete sd; break;}
        if( e.type != DSF_CMD_NAMESPACE_END ) FATAL;

        sd->data_uncompr_size = df.last_block_uncompressed_size;
        sd->data_compr_size = df.last_block_compressed_size;

        opm->data_read(df, sd);

        df.auto_assign.reset();
        df.auto_assign.put("time",&sd->seg_end_ac);
        df.auto_assign.put("dcount",&seg_end_dcount);
        df.auto_assign.put("ccount",&seg_end_ccount);
        df.auto_assign.put("tag",&sd->seg_end_tag,-1);

        df.auto_assign.scan();

        if( df.eof() ) {delete sd; break;}

        sd->icount = seg_end_ccount - seg_start_ccount;
        sd->start_pos = start_pos;
        if( !sd->seg_start_pcount ) sd->seg_start_pcount = seg_start_ccount;
        sdata.put(sd);
        current_segment++;

        double frac =
          uncompress_frac + (1 - uncompress_frac)* ((gdouble)start_pos) / flen;

        if( gui && ( frac - last_frac ) > 0.005 )
          {
            gtk_progress_bar_set_fraction
              ((GtkProgressBar*)progress_bar, min(1.0,frac));
            yield_to_gtk();
            last_frac = frac;
          }
      }
    else if( e.key.s == DS_FMT_OVR_PLOT_SERIES_INIT )
        opm->definition_read(df);
    else if ( df.eof() )
      break;
    else
      fatal("Unrecognized string in dataset, \"%s\"", e.key.s.str());
    }

  dsvv_rob_size = sim_var_value("rob_size",0);
  dsvv_issue_width = sim_var_value("issuewidth",0);
  dsvv_issue_width = sim_var_value("issue_width",dsvv_issue_width);
  dsvv_exec_file_mod_time = sim_var_value("exec_file_mod_time_ue",0);
  dsvv_exec_file_size = sim_var_value("exec_file_size",0);
  dsvv_linesize = sim_var_value("linesize",64);
  targ_line_mask = ~( ( 1 << lg(dsvv_linesize) ) - 1);

  {
    // DMK: Will be replaced by more general code. 29 July 2002, 17:25:49 CDT
    state_info[RTRC_STATE_PREREADY].name = "Pre-ready";

    for(int i=100; i<200; i++)
      if( state_info[i].name )
        {
          state_info[i-100].name =
            g_strdup_printf("%s (doomed)",state_info[i].name);
          state_info[i+150].name =
            g_strdup_printf("%s (pre-execute)",state_info[i].name);
        }

    if( gui ) state_color_set_defaults();
  }

  if( gui )
    {
      gtk_progress_bar_set_text((GtkProgressBar*)progress_bar,"Sorting");
      yield_to_gtk();
    }

  opm->series_new(OVR_PLOT_SERIES_IPC,"IPC","%.3f","%d instructions",
                  "Instructions per cycle.", dsvv_issue_width, 0, "black", 0);

  {
    Segment_Data *sd;
    int segment = 0;
    double sum_count = 0;
    double sum_cyc = 0;

    while( ( sd = sdata.iterate() ) )
      {
        sd->seg_duration_c = sd->seg_end_ac - sd->seg_start_ac;
        ASSERTS( sd->seg_duration_c >= 0 );
        double duration = sd->seg_duration_c;
        sum_cyc += duration;
        sum_count += sd->icount;
        sd->ipc = ((double)sd->icount) / duration;
        sd->segment_num = segment++;

        sd->ovr_plot_value[OVR_PLOT_SERIES_IPC] = sd->ipc;
        sd->ovr_plot_value_sample_count[OVR_PLOT_SERIES_IPC] = sd->icount;

        RStringF label("%d of %d",sd->segment_num+1,sdata.occ);
        if( sd->segment_label )
          { label += ": ";  label.append_take(sd->segment_label); }
        sd->segment_label = label.remove();

      }

    sdata_chron =
      (Segment_Data**)malloc(sdata.occ*sizeof(Segment_Data*));
    memcpy(sdata_chron,sdata.storage,
           sdata.occ*sizeof(Segment_Data*));

    qsort(sdata.storage,sdata.occ,sizeof(sdata.storage[0]),comp_ipc);

    int rank = 0;
    while( ( sd = sdata.iterate() ) ) sd->rank = rank++;

    if(0)printf("ipc (min,avg,max) (%.2f, %.2f, %.2f)  %d segments\n",
		sdata.storage[0]->ipc, sum_count / sum_cyc,
		sdata.storage[sdata.occ-1]->ipc,
		sdata.occ);
  }

  char* const bm_file_path = benchmark_path_get(this);
  if( bm_file_path )
    {
      readex = RX_init(bm_file_path);
      if(!readex)
        dialog_warning
          (ref_window, "Could not read benchmark executable in file %s.",
           bm_file_path);
      free(bm_file_path);
    }
  else
    readex = false;

  if( bp_dialog ) gtk_widget_destroy(bp_dialog);

  opm->series_auto_scale_all(sdata.storage, sdata.occ);
  opm->series_prioritize();
  if( rtiv_ovr_series_settings ) opm->series_rti_to_settings();

}


extern "C" int
rob_comp(const void *ap, const void *bp)
{
  ROB_Entry **a = (ROB_Entry**) ap;
  ROB_Entry **b = (ROB_Entry**) bp;
  int deltac = a[0]->c - b[0]->c;
  if( deltac ) return deltac;

  int deltai = a[0]->tag - b[0]->tag;
  return deltai;
}

extern "C" int
tag_comp(const void *ap, const void *bp)
{
  State_Change **a = (State_Change**) ap;
  State_Change **b = (State_Change**) bp;

  int deltai = a[0]->tag - b[0]->tag;
  if( deltai ) return deltai;

  int deltac = a[0]->c - b[0]->c;
  if( deltac ) return deltac;

  int deltax = a[0]->sidx - b[0]->sidx;
  return deltax;
}

extern "C" int
program_order_comp(const void *ap, const void *bp)
{
  Insn_Dataset_Info* const a = *(Insn_Dataset_Info**)ap;
  Insn_Dataset_Info* const b = *(Insn_Dataset_Info**)bp;
  const int part_diff =
    a->insn_partition_info->program_order_idx
    - b->insn_partition_info->program_order_idx;
  return part_diff ? part_diff : a->tag - b->tag;
}

inline static char*
annotation_prepend_time(char *a, ds_num t)
{
  RString at;
  at.sprintf("[%s] %s",comma(t),a);
  return at.remove();
}

void
prepare_rob_data(Dataset *ds, Segment_Data *sd)
{
  Dataset_File &df = ds->df;
  Annotation_Manager* const am = ds->am;
  Annotation_Segment_Data* const asd = am->allocate_sd(sd);

  df.reset_position_state();

  sd->rob_rectangle_info.reset();
  sd->rri_idx.init(new int(-1));

  df.seek(sd->start_pos);

  ds_num seg_start_ac = sd->seg_start_ac;
  int cyc = 0;
  Auto_List<State_Change> &state_changes = sd->state_changes;
  state_changes.destructor_frees_elements = true;
  Auto_List<ROB_Entry> rob_entries;
  Auto_Hash<Insn_Partition_Info>& ps_to_info = sd->partition_sequence_to_info;
  int tag = -1;
  ds_num full_tag;
  ds_num start_tag = sd->seg_start_tag;
  int first_done_tag = 0x7fffffff;
  int idx = 0;
  char *annotation_insn_span = NULL;
  int annotation_insn_span_ae_idx = -1;
  int commit_idx = 1;

  DSF_Entry e;

  int *cmd_dispatch_segment = ds->cmd_dispatch_segment;

  while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END )
    switch( cmd_dispatch_segment[e.type] ){

    case DSF_CMD_NAMESPACE_STR_BEGIN:
      {
        char* const s = e.key.s;
        ASSERTS( s[0] && !s[1] );
        switch( s[0] ){
        case 'a':  // Defined Annotation
          {
            int id;
            df.get(e);
            switch( e.type ){
            case DSF_CMD_UI1: case DSF_CMD_STR_UI1: id = e.data.u1; break;
            case DSF_CMD_UI2: case DSF_CMD_STR_UI2: id = e.data.u2; break;
            case DSF_CMD_UI4: case DSF_CMD_STR_UI4: id = e.data.u4; break;
            default: id = 0; ASSERTS( false );
            }
            Annotation_Definition* const ad =
              ds->am->annotation_definitions[id];
            ASSERTS( ad );
            const int idx = asd->annotations.next_free;
            Annotation_Elt* const ae = asd->annotations.next();
            int *ae_ptr;
            switch( ad->target ){
            case AT_Instruction:
              ae_ptr = &sd->tag_to_info.put(tag)->annotation_elt;
              break;
            case AT_Partition:
              df.get(e);
              ASSERTS( e.type == DSF_CMD_IDX_STR || e.type == DSF_CMD_IN4 );
              ae_ptr = &sd->partition_sequence_to_info.put
                ( e.type == DSF_CMD_IN4 ? e.data.i4 : e.key.i4 )
                ->annotation_elt;
              break;
            default:
              ae_ptr = NULL;
            }
            ASSERTS( ae_ptr );
            ae->id = id;
            ae->cyc = cyc;
            ae->link(ae_ptr,idx);
            ae->data_start = asd->annotation_data.next_free;
            while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END )
              {
                PVT_Scalar* const ad = asd->annotation_data.next();
                switch( e.type ){
                case DSF_CMD_IN1:case DSF_CMD_STR_IN1:  *ad=e.data.i1;     break;
                case DSF_CMD_UI1:case DSF_CMD_STR_UI1:  *ad=e.data.u1;     break;
                case DSF_CMD_IN2:case DSF_CMD_STR_IN2:  *ad=e.data.i2;     break;
                case DSF_CMD_UI2:case DSF_CMD_STR_UI2:  *ad=e.data.u2;     break;
                case DSF_CMD_IN4:case DSF_CMD_STR_IN4:  *ad=e.data.i4;     break;
                case DSF_CMD_UI4:case DSF_CMD_STR_UI4:  *ad=e.data.u4;     break;
                case DSF_CMD_IN8:case DSF_CMD_STR_IN8:  *ad=e.data.i8;     break;
                case DSF_CMD_DBL:case DSF_CMD_STR_DBL:  *ad=e.data.d;      break;
                case DSF_CMD_STR:case DSF_CMD_STR_STR:  *ad=e.data.s.dup();break;
                default:               ASSERTS( false );     break;
                }
              }
            ae->data_stop = asd->annotation_data.next_free;
          }
          break;
        }
      }
      break;
    case DSF_CMD_NAMESPACE_IDX_BEGIN:
      if( e.key.i4 == DS_FMT_IDX_Segdata_Partition_info )
        {
          df.get(e);
          ASSERTS( e.type == DSF_CMD_STR_IN4 );
          const int seq = e.data.i4;
          Insn_Partition_Info* const pi=sd->partition_sequence_to_info.put(seq);
          pi->sequence_number = seq;

          while( df.get(e) && e.type != DSF_CMD_NAMESPACE_END )
            switch( e.key.i4 ){
            case DS_FMT_IDX_Segdata_Partition_info_Attributes:
              // Deprecated.  Only the lower three bits should be set.
              pi->attributes = e.data.i4;
              ASSERTS( !(pi->attributes&~7) );
              break;
            case DS_FMT_IDX_Segdata_Partition_info_Snip_serial:
              pi->snip_serial = e.data.i4;
              break;
            case DS_FMT_IDX_Segdata_Partition_info_Parent:
              pi->parent_sn = e.data.i4;
              break;
            default:
              ASSERTS( false );
            }
        }
      else
        {
          ASSERTS( false );
        }
    break;

    case DSF_CMD_STR_IN4:
    case DSF_CMD_STR_STR:
      fatal("Unexpected command, %d",e.type);
      break;

    case DS_Disp_Time_in4:
      cyc = max(-1,e.data.i4 - seg_start_ac);
      break;

    case DS_Disp_Time_in8:
      cyc = max(-1,e.data.i8 - seg_start_ac);
      break;

    case DS_Disp_Partition_Sequence_in4:
      sd->tag_to_info.put(tag)->insn_partition_sequence = e.data.i4;
      break;

    case DS_Disp_PC_in4:
      sd->tag_to_info.put(tag)->pc = e.data.i4;
      break;

    case DS_Disp_PC_in8:
      sd->tag_to_info.put(tag)->pc = e.data.i8;
      break;

    case DS_Disp_EA_in4:
      {
        Insn_Dataset_Info *di = sd->tag_to_info.put(tag);
        di->ea = e.data.i4;
        di->ea_valid = true;
      }
      break;

    case DS_Disp_EA_in8:
      {
        Insn_Dataset_Info *di = sd->tag_to_info.put(tag);
        di->ea = e.data.i8;
        di->ea_valid = true;
      }
      break;

    case DS_Disp_Annotation_Partition:
      {
        char* const str = e.data.s.dup();
        const int idx = asd->annotations.next_free;
        Annotation_Elt* const ae = asd->annotations.next();
        df.get(e);
        int* const ae_ptr =
          &sd->partition_sequence_to_info.put(e.data.i4)->annotation_elt;
        ae->id = AID_Partition_Message;
        ae->cyc = cyc;
        ae->link(ae_ptr,idx);
        ae->data_start = asd->annotation_data.next_free;
        PVT_Scalar* const ad = asd->annotation_data.next();
        ad->type = PVTS_String;
        ad->s = str;
        ae->data_stop = asd->annotation_data.next_free;
      }
      break;

    case DS_Disp_Annotation_Insn:
      {
        const int idx = asd->annotations.next_free;
        Annotation_Elt* const ae = asd->annotations.next();
        int* const ae_ptr = &sd->tag_to_info.put(tag)->annotation_elt;
        ae->id = AID_Instruction_Message;
        ae->cyc = cyc;
        ae->link(ae_ptr,idx);
        ae->data_start = asd->annotation_data.next_free;
        PVT_Scalar* const ad = asd->annotation_data.next();
        ad->type = PVTS_String;
        ad->s = e.data.s.dup();
        ae->data_stop = asd->annotation_data.next_free;
      }
      break;

    case DS_Disp_Annotation_Insn_Span:
      if( e.data.s.str()[0] )
        {
          annotation_insn_span = e.data.s.dup();
          annotation_insn_span_ae_idx = -1;
        }
      else
        annotation_insn_span = NULL;

      break;

    case DS_Disp_Text_in4:
      sd->tag_to_info.put(tag)->text = e.data.i4;
      break;

    case DS_Disp_CWP_in4:
      sd->tag_to_info.put(tag)->cwp = e.data.i4;
      break;

    case DS_Disp_State_in4:
      full_tag = e.data.i4;
      goto DISP_STATE;
    case DS_Disp_State_in8:
      full_tag = e.data.i8;
    DISP_STATE:
      {
        if( start_tag == -1 ) sd->seg_start_tag = start_tag = full_tag;
        tag = full_tag >= start_tag ? full_tag - start_tag : -1;

        // DMK: This is a temporary fix.  Later add code to handle
        // situations where the smallest tag is not first.
        // 21 April 2003,  8:45:11 CDT
        // DMK: Since PSE now does not use tags for positioning, handling
        // negative indices should be easier.  Maybe just fixing Auto_Array.
        if( tag < 0 ) break;

        if( annotation_insn_span )
          {
            const int idx = asd->annotations.next_free;
            Annotation_Elt* const ae = asd->annotations.next();
            int* const ae_ptr = &sd->tag_to_info.put(tag)->annotation_elt;
            ae->id = AID_Squash_Group_Message;
            ae->cyc = cyc;
            ae->link(ae_ptr,idx);
            if( annotation_insn_span_ae_idx < 0 )
              {
                annotation_insn_span_ae_idx = asd->annotation_data.next_free;
                PVT_Scalar* const ad = asd->annotation_data.next();
                ad->type = PVTS_String;
                ad->s = annotation_insn_span;
              }
            ae->data_start = annotation_insn_span_ae_idx;
            ae->data_stop = annotation_insn_span_ae_idx + 1;
          }
        sd->tag_to_info.put(tag)->tag = tag;

        if( e.type == RTRC_STATE_NOP ) break;

        state_changes.put(new State_Change(cyc,e.type,tag,idx++));

        switch( e.type ){
        case RTRC_STATE_COMMIT:
          if( tag < first_done_tag ) first_done_tag = tag;
          sd->tag_to_info[tag]->commit_idx = commit_idx++;
          break;
        case RTRC_STATE_SQUASH:
          if( tag < first_done_tag ) first_done_tag = tag;
          sd->tag_to_info[tag]->doomed = true;
          break;
        case RTRC_STATE_DECODE:
          state_changes.put
            (new State_Change(cyc+1,RTRC_STATE_PREREADY, tag,idx++));
          break;
        default:
          break;
        }
      }
      break;

    default:
      fatal("Unknown command, %d",e.type);
    }

  // Find the maximum partition sequence read. (For use in assigning
  // partition sequences to instructions that lack them.)
  //
  int insn_ps_max = 1;
  for( Insn_Dataset_Info *di = NULL; (di = sd->tag_to_info.iterate()); )
    insn_ps_max = max(insn_ps_max,di->insn_partition_sequence);
  ps_to_info.iterate_setup();
  for( int sn; ps_to_info.iterate_keys(sn); ) insn_ps_max = max(insn_ps_max,sn);

  const int UNSET_SN = SEQUENCE_NUMBER_UNSET;

  for( Insn_Dataset_Info *di = NULL; (di = sd->tag_to_info.iterate()); )
    {
      if( di->insn_partition_sequence == UNSET_SN )
        di->insn_partition_sequence = ++insn_ps_max;
      Insn_Partition_Info* const pi =
        ps_to_info.put(di->insn_partition_sequence);
      if( pi->sequence_number == UNSET_SN )
        pi->sequence_number = di->insn_partition_sequence;
      di->insn_partition_info = pi;
      pi->count++;
      pi->commit_max_idx = max(pi->commit_max_idx,di->commit_idx);
      pi->tag_lowest = min(pi->tag_lowest, di->tag);
      pi->tag_highest = max(pi->tag_highest, di->tag);
    }

  if( ps_to_info.occ() )
    {
      ps_to_info.iterate_setup();
      int last_sequence_number = ++insn_ps_max;
      ASSERTS( !ps_to_info[last_sequence_number] );

      Auto_Hash< PSList<int> > children;
      Auto_Hash<bool> roots;
      PSList<Insn_Partition_Info*> pi_commit_order;

      for( int sn; ps_to_info.iterate_keys(sn); )
        {
          Insn_Partition_Info* const pi = ps_to_info[sn];
          if( pi->sequence_number == UNSET_SN ) pi->sequence_number = sn;
          if( pi->commit_max_idx )
            pi_commit_order.insert(pi->commit_max_idx, pi);
          if( pi->parent_sn == UNSET_SN ) // If sim did not specify parent.
            {
              pi->parent_sn = last_sequence_number;
              last_sequence_number = sn;
            }

          children.put(pi->parent_sn)->insert(-sn,sn);

          if( !ps_to_info[pi->parent_sn] ) roots.put(pi->parent_sn);
        }
      roots.iterate_setup();

      PStack<int> to_traverse;
      for( int root; roots.iterate_keys(root); ) to_traverse += root;

      for( int idx = 0; to_traverse; )
        {
          const int sn = to_traverse;
          if( Insn_Partition_Info *pi = ps_to_info[sn] )
            pi->program_order_idx = ++idx;
          int unused, snc;
          if( PSList<int>* const sl = children[sn] )
            while( sl->iterate(unused,snc) ) to_traverse += snc;
        }

      int ooo_count = 0;
      for( Insn_Partition_Info *pi, *piprev = pi_commit_order.iterate();
           ( pi = pi_commit_order.iterate() );
           piprev = pi )
        if( piprev->program_order_idx >= pi->program_order_idx )
          ooo_count++;
      if( ooo_count ) fprintf(stderr,"Order violations (%d).\n",ooo_count);

#ifdef STRICT
      for( int root; roots.iterate_keys(root); )
        {
          int sn = root;
          while( true )
            {
              if( Insn_Partition_Info* const pi = ps_to_info[sn] )
                pi->right_path = true;
              PSList<int>* const sl = children[sn];
              if( !sl ) break;
              sn = sl[0][0];
            }
        }
      while( Insn_Partition_Info* const pi = ps_to_info.iterate() )
        if( pi->commit_max_idx && !pi->right_path )
          {
            Insn_Partition_Info *pic = pi;
            while( pic )
              {
                printf("----\n");
                const int sn = pic->parent_sn;
                PSList<int>* const sl = children[sn];
                int unused, snc;
                while( sl->iterate(unused,snc) )
                  {
                    Insn_Partition_Info* const pisib = ps_to_info[snc];
                    printf(" %s s %d  idx %d  cmt %d\n",
                           pic == pisib ? "*" : " ",
                           pisib->sequence_number,
                           pisib->program_order_idx,
                           pisib->commit_max_idx
                           );
                  }
                pic = ps_to_info[sn];
              }
            ASSERTS( false );
            break;
          }
#endif
    }

  {
    const int occ1 = sd->tag_to_info.occ();

    // Prepare array holding pointers to instructions in program
    // order.  That is, program_order[3] points to instruction 3 (fourth)
    // in program order.
    //

    sd->idxt_to_info = (Insn_Dataset_Info**)malloc((occ1+1) * sizeof(void*));
    sd->idxst_to_info = (Insn_Dataset_Info**)malloc((occ1+1) * sizeof(void*));

    Insn_Dataset_Info **po = sd->idxst_to_info;
    Insn_Dataset_Info **to = sd->idxt_to_info;
    int occ = 0;
    for( Insn_Dataset_Info *di = NULL; (di = sd->tag_to_info.iterate()); )
      {*po++ = di; *to++ = di;  occ++;}
    *po = *to = NULL;
    sd->idxt_to_info =
      (Insn_Dataset_Info**) realloc(sd->idxt_to_info,occ*sizeof(void*));
    sd->idxst_to_info =
      (Insn_Dataset_Info**) realloc(sd->idxst_to_info,occ*sizeof(void*));
    sd->idx_max = occ - 1;
    qsort(sd->idxst_to_info,occ,sizeof(void*),program_order_comp);

    for(int i=0; i<occ; i++) sd->idxt_to_info[i]->idxt = i;

    Insn_Stream &is = sd->insn_stream;
    Insn_Dataset_Info** const program_order = sd->idxst_to_info;

    // Insn_Dataset_Info structures are held in tag order.  Link
    // them in program order and also call "next_insn" to set register
    // information such as dependencies.
    //
    for(int i=0; i<occ; i++)
      {
        Insn_Dataset_Info* const di = program_order[i];
        di->idxst = i;
        di->prev = i ? program_order[i-1] : NULL;
        di->next = i == occ - 1 ? NULL: program_order[i+1];
        di->si = NULL;
        if( !ds->readex || !di->text && !di->pc ) continue;
        uint32_t text = di->text; // Hey, what about nops!!!
        if( !text && !RX_get_text(di->pc,text) ) continue;
        di->si = is.next_insn(text,di->pc,di->tag,di->doomed,di->ea,di->cwp);
      }
  }

  qsort(state_changes.storage,state_changes.occ,
        sizeof(state_changes.storage[0]), tag_comp);

  {
    State_Change *e;
    State_Change *l = state_changes.iterate();
    bool squashed = false;
    bool post_commit = l->tag < first_done_tag;
    Insn_Dataset_Info *di = sd->tag_to_info[l->tag];
    bool doomed = di->doomed;
    bool pre_ex = false; // Well, not always.
    int seg_duration = sd->seg_end_ac - sd->seg_start_ac;
    di->rob_enqueue_c = l->c;
    di->first_event_c = l->c;
    di->pre_ex = pre_ex;
    di->rob_dequeue_c = seg_duration;
    di->first_state_change = state_changes.iterator_index();

    while( ( e = state_changes.iterate()) )
      {
        bool new_din = e->tag != l->tag;
        // DMK: This will be replaced by ordering rules read from dataset file.
        int overlap = !new_din && e->c == l->c;
        State_Info* const e_si = &ds->state_info[e->st]; // Current
        State_Info* const l_si = &ds->state_info[l->st]; // Last
        if( overlap && e->st == RTRC_STATE_COMMIT &&
            l->st != RTRC_STATE_READY && l->st != RTRC_STATE_COMPLETE )
          {
            State_Change *temp = e;  e = l;  l = temp;  e->c++;
          }

        if( new_din || e->c != l->c )
          {
            int stop_cyc =
              new_din
              || l->st == RTRC_STATE_COMMIT
              || pre_ex && e->st == RTRC_STATE_SQUASH
              ? l->c + 1 : e->c;

            int st = doomed && l->st != RTRC_STATE_SQUASH
              ? l->st - 100 :
              pre_ex ? l->st +150 : l->st;

            l->plot_state = st;
            l->limit_c = stop_cyc;
            di->last_event_end_c = stop_cyc;

            if( !squashed && l_si->in_rob && !post_commit )
              {
                for(int c = l->c; c < stop_cyc; c++)
                  rob_entries.put(new ROB_Entry(c,st,l->tag));
              }

            bool squash = l->st == RTRC_STATE_SQUASH;
            bool commit = l->st == RTRC_STATE_COMMIT;
            if ( squash || commit )
              di->rob_dequeue_c = l->c;

            if ( l->st == RTRC_STATE_DECODE )
              di->rob_enqueue_c = e->c;

            if( new_din )
              {
                di = sd->tag_to_info[e->tag]; // Warning, storage can move!
                pre_ex = e_si->family && e_si->family[0] == 'p';
                di->pre_ex = pre_ex;
                squashed = false;
                doomed = !pre_ex && di->doomed;
                post_commit = e->tag < first_done_tag;
                di->first_event_c = e->c;
                di->rob_enqueue_c = e->c;
                di->rob_dequeue_c = seg_duration;
                di->first_state_change = state_changes.iterator_index();
              }
            else
              {
                if ( squash ) squashed = true;
                if ( commit ) post_commit = true;
              }

            l = e;
            continue;
          }

        switch( l->st ){
        case RTRC_STATE_COMMIT:
          break;
        default:
          l = e;
        }
      }
  }

  qsort(rob_entries.storage,rob_entries.occ,sizeof(rob_entries.storage[0]),
        rob_comp);

  {
    int dsvv_rob_size = ds->dsvv_rob_size;

    int x1 = 0, x2 = x1 + 1, x1p = x1, x2p = x2;
    int x1_rri_idx = -1;
    int y1 = 0, y2 = 1, y1p = 0, y2p = 1;
    int last_cyc = 0;
    int last_state = -1;
    int stp = -1;
    int tp = -2;
    int last_decode_cyc = -1;
    int last_tag = -1;

    ROB_Entry *e;
    while( (e = rob_entries.iterate()) )
      {
        int delta_c = e->c - last_cyc;
        if( delta_c )
          {
            x1+=delta_c;  x2+=delta_c;  y1 = 0;  y2 = 1;  last_cyc = e->c;
          }
        else
          {
            y1++;  y2++;
          }

        bool gap = e->tag != last_tag + 1;
        last_tag = e->tag;

        if( (e->st == RTRC_STATE_DECODE || e->st == RTRC_STATE_DECODE - 100 )
            && last_decode_cyc < e->c )
          {
            y1 = dsvv_rob_size;  y2 = y1 + 1;  last_decode_cyc = e->c;
          }

        int curr_state = e->st;

        bool new_state = last_state != curr_state;
        last_state = curr_state;

        if( x1 != x1p || new_state || gap )
          {
            if( stp != -1 )
              {
                ROB_Rect_Info *rri = sd->rob_rectangle_info.next();

                rri->st = stp;  rri->t1 = tp;
                rri->x1 = x1p;  rri->x2 = x2p;
                rri->y1 = y1p;  rri->y2 = y2p;
              }

            x1p = x1;  x2p = x2;  y1p = y1;  y2p = y2;
            stp = curr_state;  tp = e->tag;
          }
        else
          {
            y2p = y2;
          }

        if( x1 != x1_rri_idx )
          {
            sd->rri_idx.put(x1)[0] = sd->rob_rectangle_info.next_free;
            x1_rri_idx = x1;
          }
      }
  }

  if( ps_to_info.occ() )
    {
      ps_to_info.iterate_init();
      int rank = 0;
      Insn_Partition_Info *prev = NULL;
      Insn_Partition_Info *first = NULL;
      PSList<Insn_Partition_Info*> pi_prog_order;

      while( Insn_Partition_Info* const pi = ps_to_info.iterate() )
        {
          if( !pi->count ) continue;
          sd->snip_count++;
          pi_prog_order.insert(pi->program_order_idx,pi);
        }

      while( Insn_Partition_Info* const pi = pi_prog_order.iterate() )
        {
          if( prev ) prev->next = pi; else first = pi;
          pi->rank = ++rank;
          pi->prev = prev;
          prev = pi;
        }

      Insn_Partition_Info *snip_prev = NULL;
      Insn_Partition_Info* pi = first;
      while( pi )
        {
          pi->snip_prev = snip_prev;
          if( !pi->next ) break;
          if( pi->snip_serial != pi->next->snip_serial ) snip_prev = pi;
          pi = pi->next;
        }
      Insn_Partition_Info *snip_next = NULL;
      while( pi )
        {
          pi->snip_next = snip_next;
          if( !pi->prev ) break;
          if( pi->snip_serial != pi->prev->snip_serial ) snip_next = pi;
          pi = pi->prev;
        }
    }
}

void
Dataset::sortby_pc()
{
  qsort(sdata.storage,sdata.occ,sizeof(sdata.storage[0]),comp_top_pc);
}

void
Dataset::sortby_ovr_idx(int idx)
{
  if ( idx == PSE_Window::OSK_Insn_Static_Order ) sortby_pc();
  else if ( idx == PSE_Window::OSK_Execution_Order )
    {
      ASSERTS( false );
    }
  else
    {
      ASSERTS( idx >= 0 && idx < opm->plot_series_num );
      // global static, used by sort func.
      comp_ovr_sort_key_idx = opm->plot_series[idx].value_index;
      qsort(sdata.storage,sdata.occ,sizeof(sdata.storage[0]),comp_ovr_sort_key);
    }
  int rank = 0;
  while ( Segment_Data* const sd = sdata.iterate() ) sd->rank = rank++;
}
