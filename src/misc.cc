/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2010 Louisiana State University

// $Id$

//
// Miscellaneous Support
//

#include <gtk/gtk.h>
#include<memory.h>
#include<malloc.h>
#include<time.h>
#include<stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "md5.h"

#include "misc.h"

#ifdef POSIX_TIMER
double
time_fp()
{
  struct timespec tp;
  clock_gettime(clock_realtime,&tp);
  return ((double)tp.tv_sec)+((double)tp.tv_nsec) * 0.000000001;
}
#else
double
time_fp()
{
  static GTimer *timer = NULL;
  if( !timer ) timer = g_timer_new();
  return g_timer_elapsed(timer,NULL);
}
#endif


bool
MD5_File::file(const char *path)
{
  v32 = 0;
  v64 = 0;
  const int fd = open(path, O_RDONLY);
  success = fd != -1;
  if( !success ) return false;
  const int size = sizeof(buf);
  md5_state_t md5s;
  md5_init(&md5s);
  while( true )
    {
      const int amt = read(fd, buf, size );
      if( amt <= 0 ) break;
      md5_append(&md5s, (md5_byte_t*)buf, amt);
    }
  close(fd);
  md5_finish(&md5s,(md5_byte_t*)vptr);
  v64 = digest64[0];
  v32 = *(uint32_t*)digest64;
#ifdef HOST_LITTLE_ENDIAN
  v64 = B8(v64);  v32 = B4(v32);
#endif
  return true;
}


char *
comma(double num)
{
 static char COMMA[COMMALEN],*CP=NULL,*CEND;
 char *tp,*to_caller;
 char *temp = g_strdup_printf("%.0f",num);
 int len = strlen(temp);
 int est_len = int(len * 1.4 + 1);
 if( !CP ){CEND = COMMA + COMMALEN;  CP=COMMA;}
 if( CP + est_len >= CEND ) CP = COMMA;
 *(to_caller=CP) = 0;  tp = temp;
 if( *tp == '-' ) {tp++;  *(CP++)='-';  len--; };
 int i = len % 3; if( i==0 ) len -= i=3;
 if( len >= 0 )
   {
     while(i--) *CP++ = *tp++;
     len/=3;
     for(i=0;i<len;i++){*(CP++)=','; *CP++=*tp++; *CP++=*tp++; *CP++=*tp++;}
     *(CP++)=0;
   }
 free(temp);
 return to_caller;
}

int
path_dots_remove(char *path_name)
{
  char *path_end = path_name + strlen(path_name);
  char *pptr = path_end;
  int passed = 0;
  char *nxt = path_end;
  int absolute = path_name[0] == '/';

  while( pptr > path_name )
    {
      while( pptr > path_name && *--pptr == '/' );
      if( *pptr == '/' ) break;
      char *end = pptr + 1;
      while( pptr >= path_name && *pptr != '/' ) pptr--;
      char *beg = pptr + 1;
      int len = end - beg;

      if ( len == 2 && beg[0] == '.' && beg[1] == '.' ) { passed++; continue; }
      if ( len == 1 && beg[0] == '.' ) continue;
      if ( passed ) { passed--;  continue; }
      if ( nxt > end + 1 ) { end[0] = 0;  end[1] = nxt - end; }
      nxt = beg;
    }

  if( passed && absolute ) return 0;

  char *from_ptr = nxt;
  char *to_ptr = path_name + absolute;
  while( passed-- ) { *to_ptr++ = '.';  *to_ptr++ = '.';  *to_ptr++ = '/'; }
  while( from_ptr < path_end )
    {
      if ( *from_ptr ) *to_ptr++ = *from_ptr++;
      else { *to_ptr++ = '/';  from_ptr += ((unsigned char*)from_ptr)[1]; }
    }
  if( to_ptr > path_name + 1 && to_ptr[-1] == '/' ) to_ptr--;
  *to_ptr = 0;
  return 1;
}

char*
pvt_sprintf_dup(const char *fmt, PVT_Scalar *adarray, int size)
{
  RString string(fmt ? strlen(fmt)<<1 : 1);
  int var_count = 0;
  while( *fmt )
    {
      const char* const start = fmt;
      while( *fmt && fmt[0] != '%' ) fmt++;
      string.append_substring(start,fmt);
      if( !*fmt ) break;
      if( fmt[1] == '%' ) {string += '%'; fmt += 2; continue;}
      const char* const whole_spec_start = fmt++;
      const int positional_arg_chars = strspn(fmt,"0123456789");
      const bool positional_arg =
        positional_arg_chars && fmt[positional_arg_chars] == '$';
      const int arg_position = positional_arg ? atoi(fmt) - 1 : var_count;
      if( arg_position < 0 || arg_position >= size )
        { string.append_substring(whole_spec_start,fmt);  continue; }
      if( positional_arg ) fmt += positional_arg_chars + 1; else var_count++;
      const char* const spec_start = fmt;
      fmt += strspn(fmt,"'#0- +");       // Flags                 (Use)
      fmt += strspn(fmt,"0123456789.");  // Width and precision.  (Use)
      RString fmt_clean(20);
      fmt_clean = "%";
      fmt_clean.append_substring(spec_start,fmt);
      fmt += strspn(fmt,"hlLqjzt");      // Length modifier.      (Discard)
      const char* const digits = fmt[0] == '{' ? ++fmt : NULL;
      if( digits )
        {
          const char* const next_pos_maybe = &fmt[strcspn(fmt,"}")];
          if( !*next_pos_maybe ) continue;
          fmt = next_pos_maybe + 1;
        }
      const uint digits_specified = digits ? fmt - digits - 1 : 0;
      const char conversion_specifier = *fmt++;     //            (Maybe Use)
      PVT_Scalar& ad = adarray[arg_position];
      const int len_before = string.len();
      bool could_use_digits_from_format = false;

      if( conversion_specifier == 's' )
        {
          RString rs;
          const char* const s =
            ad.type == PVTS_String ? ad.s :
            ad.type == PVTS_Int32 ? rs.sprintf("%d",ad.i4) :
            ad.type == PVTS_Double ? rs.sprintf("%f",ad.d) :
            ad.type == PVTS_UInt32 ? rs.sprintf("%#x",ad.u4) :
            ad.type == PVTS_Int64 ? rs.sprintf("%ld",ad.i8) : "?";
          fmt_clean += 's';
          string.sprintf(fmt_clean,s);
        }
      else if( ad.type == PVTS_String )
        {
          string += ad.s;
        }
      else if( conversion_specifier == 'T' ) // Non-standard format: Time/UE
        {
          const time_t t =
            ad.type == PVTS_Int32 ? ad.i4 :
            ad.type == PVTS_UInt32 ? ad.u4 :
            ad.type == PVTS_Int64 ? ad.i8 :
            ad.type == PVTS_Double ? (time_t) ad.d : 0;
          RStringFTime t_str("%d %b %Y %k:%M:%S %Z",t);
          fmt_clean += 's';
          string.sprintf(fmt_clean,t_str.s);
        }
      else if ( conversion_specifier == 'c' )
        {
          fmt_clean += 'c';
          string.sprintf(fmt_clean,ad.i4);
        }
      else if( conversion_specifier == 'b' ) // Non-standard format: Binary
        {
          char binary[65], *bptr = &binary[64];
          const char* const zo = "01";
          *bptr = 0;
          if( ((int64_t)ad) == 0 ) *--bptr = zo[0];
          else for( uint64_t i = ad; i; i >>= 1 ) *--bptr = zo[i & 1];
          fmt_clean += "s";
          string.sprintf(fmt_clean,bptr);
          could_use_digits_from_format = digits_specified;
        }
      else if( strchr("diouxX",conversion_specifier) )
        {
          switch( ad.type ){
          case PVTS_Int64:
            fmt_clean += "ll"; fmt_clean += conversion_specifier;
            string.sprintf(fmt_clean,ad.i8);
            break;
          case PVTS_Double:
            fmt_clean += ".0f";
            string.sprintf(fmt_clean,ad.d);
            break;
          case PVTS_Int32:
            fmt_clean += conversion_specifier;
            string.sprintf(fmt_clean,ad.i4);
            break;
          case PVTS_UInt32:
            fmt_clean += conversion_specifier;
            string.sprintf(fmt_clean,ad.u4);
            break;
          default:
            fmt_clean += conversion_specifier;
            string.sprintf(fmt_clean,ad.i4);
            break;
          }
          could_use_digits_from_format = digits_specified;
        }
      else if( strchr("aAeEfFgG",conversion_specifier) )
        {
          const double v = ad.is_number() ? (double) ad : 0.0;
          fmt_clean += conversion_specifier;
          string.sprintf(fmt_clean,v);
        }

      if( could_use_digits_from_format )
        for(char *s = string.s + len_before; *s; s++ )
          {
            const char c = *s;
            const unsigned int digit_val =
              unsigned( c - '0' ) < 10 ? c - '0' :
              unsigned( c - 'a' ) <  6 ? c - 'a' + 10 :
              unsigned( c - 'A' ) <  6 ? c - 'A' + 10 : -1;
            if( digit_val < digits_specified ) *s = digits[digit_val];
          }
    }
  return string.remove();
}

// Note: M_TOP_PAD is an ad-hoc feature test for the GNU LIBC __malloc_hook.
#ifdef M_TOP_PAD
#include "mcheck.h"
#else
void* (*__malloc_hook)(size_t size, const void *caller);
void* (*__realloc_hook)(void *ptr, size_t size, const void *caller);
#endif

static void* (*debug_malloced_malloc_hook_prev)
  (size_t size, const void *caller);

static void* (*debug_malloced_realloc_hook_prev)
  (void *ptr, size_t size, const void *caller);

static void*
debug_malloced_malloc(size_t size, const void *caller)
{
  __malloc_hook = debug_malloced_malloc_hook_prev;
  void* const str = malloc(size);
  __malloc_hook = debug_malloced_malloc;
  if( !str ) return NULL;
  memset(str,0xaa,size);
  return str;
}

static void*
debug_malloced_realloc(void *ptr, size_t size, const void *caller)
{
  __realloc_hook = debug_malloced_realloc_hook_prev;
  void* const str = realloc(ptr,size);
  __malloc_hook = debug_malloced_malloc;
  __realloc_hook = debug_malloced_realloc;
  return str;
}

void
debug_malloced_init(void)
{
  char* const mc = getenv("MALLOC_CHECK_");
  if( !mc ) return;
  const int mc_level = atoi(mc);
  if( mc_level < 1 ) return;
  fprintf(stderr,"Running with heap memory initialized to 0xaa.\n");
  debug_malloced_malloc_hook_prev = __malloc_hook;
  debug_malloced_realloc_hook_prev = __realloc_hook;
  __malloc_hook = debug_malloced_malloc;
  __realloc_hook = debug_malloced_realloc;
}
