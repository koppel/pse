/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2013 Louisiana State University

// $Id$

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "parse_table.h"
#include "ihash_data.h"

#include <instr.h>
#include <IHASH.h>
#include <ITYPES.h>
#include <amodes.h>
#include <reguse.h>

#define AM_Y 'y'
#define AM_SIMM3 '3'  // Three least significant bits
#define AM_RS3 't'

// Extra Formats

#define AM_PF 'P'  // Prefetch Function

#include "misc.h"
#include "decode_sparc.h"
#include "dataset.h"
#include "messages.h"
#include "readex.h"

static const char*(*addr_to_source)(t_addr);

static const char *regnames[] =
{"g0", "g1", "g2", "g3", "g4", "g5", "g6", "g7",
 "o0", "o1", "o2", "o3", "o4", "o5", "sp", "o7",
 "l0", "l1", "l2", "l3", "l4", "l5", "l6", "l7",
 "i0", "i1", "i2", "i3", "i4", "i5", "fp", "i7"};

#define MAXINSTR 0x7fffffff

#define NUMREGS 528


#define REG_UNIV_Y  1
#define REG_UNIV_FCC 8  // Layout of CC based on use of cc field in cond move.
#define REG_UNIV_ICC 12
#define REG_UNIV_XCC 14
#define REG_UNIV_FSR 16
#define REG_UNIV_LOAD 17
#define REG_UNIV_STORE 18
#define REG_UNIV_F0 19
#define REG_UNIV_G0 (REG_UNIV_F0+32)
#define REG_UNIV_O0 (REG_UNIV_G0+8)
#define REG_UNIV_I0 (REG_UNIV_O0+16)

#define FIELD_O0 8
#define FIELD_O7 (FIELD_O0+7)
#define FIELD_I0 (FIELD_O0+16)
#define FIELD_I7 (FIELD_I0+7)

#define getRS3(text) (((text)>>9)&0x1f)

int
Insn_Stream::reg_gpr_insn_to_univ(int cwp, int reg_addr)
{
  if( reg_addr < 8 ) return REG_UNIV_G0 + reg_addr;
  if( cwp == window_count - 1 && reg_addr > 23 )
    return REG_UNIV_O0 + reg_addr - 16;
  else
    return REG_UNIV_O0 + cwp * 16 + reg_addr;
}

static inline int
reg_fpr_insn_to_univ(int reg_addr)
{
  return REG_UNIV_F0 + reg_addr;
}

static inline int
reg_double_field_to_num(int rfield)
{
  return rfield & 1 ? rfield + 0x1f : rfield;
}

static inline int
reg_double_num_to_field(int rnum)
{
  return rnum < 32 ? rnum : rnum - 0x1f;
}

#define ASI_BLK_P 0xf0
#define ASI_FL8_P 0xd0

#define ih_ismem(ih) ( ihash_types[ih] \
                       & (IT_LOAD | IT_STORE | IT_ILOAD | IT_ISTORE ) )

#define ih_isanyload(ih) ( ihash_types[ih] & ( IT_LOAD | IT_ILOAD ) )
#define ih_isanystore(ih) ( ihash_types[ih] & ( IT_STORE | IT_ISTORE ) )

bool
insn_is_stda_block(int ih, uint32_t text)
{
  if ( ih != IH_STDFA ) return false;
  if ( getI(text) ) return false;
  return getASI(text) == ASI_BLK_P;
}

bool
insn_is_ldda_short(int ih, uint32_t text)
{
  if ( ih != IH_LDDFA ) return false;
  if ( getI(text) ) return false;
  return getASI(text) == ASI_FL8_P;
}

class IH_memory_access_size
{
public:
  int get(int ih, uint32_t text)
  {
    if ( insn_is_stda_block(ih,text) ) return 64;
    if ( insn_is_ldda_short(ih,text) ) return 1;
    return ih_to_data_size[ih];
  }
  IH_memory_access_size()
  {
    int i;

    for(i=0; i<NIHASH; i++)
      switch(i){
      case IH_LDSB:
      case IH_LDSBA:
      case IH_LDSTUB:
      case IH_LDSTUBA:
      case IH_LDUB:
      case IH_LDUBA:
      case IH_STB:
      case IH_STBA:
        ih_to_data_size[i] = 1; break;

      case IH_LDSH:
      case IH_LDSHA:
      case IH_LDUH:
      case IH_LDUHA:
      case IH_STH:
      case IH_STHA:
        ih_to_data_size[i] = 2; break;

      case IH_LDF:
      case IH_LDFA:
      case IH_LDFSR:
      case IH_LDSW:
      case IH_LDSWA:
      case IH_LDUW:
      case IH_LDUWA:
      case IH_STF:
      case IH_STFA:
      case IH_STFSR:
      case IH_STW:
      case IH_STWA:
        ih_to_data_size[i] = 4; break;

      case IH_LDD:
      case IH_LDDA:
      case IH_LDDF:
      case IH_LDDFA:
      case IH_LDX:
      case IH_LDXA:
      case IH_STD:
      case IH_STDA:
      case IH_STDF:
      case IH_STDFA:
      case IH_STX:
      case IH_STXA:
        ih_to_data_size[i] = 8; break;

      case IH_LDQF:
      case IH_LDQFA:
      case IH_STQF:
      case IH_STQFA:
        ih_to_data_size[i] = 16; break;

      default:
        ih_to_data_size[i] = 0; break;
      }
  }
  char ih_to_data_size[NIHASH];
};

IH_memory_access_size ih_memory_access_size;

static void
rfi_free(void *rfip)
{
  delete (Reg_File_Info *) rfip;
}

Insn_Stream::Insn_Stream()
{
  ea_to_info = g_hash_table_new_full(g_direct_hash,g_direct_equal,NULL,rfi_free);
  cwp = 0;
  int univ_regs_highest_num = reg_gpr_insn_to_univ(window_count,23);
  ureg_to_info_size = univ_regs_highest_num + 1;
  ureg_to_info =
    (Reg_File_Info*) calloc(ureg_to_info_size,sizeof(Reg_File_Info));
  subtree_base = 1;
}

void
Insn_Stream::set_addr_to_source
(const char*(*addr_to_source_p)(t_addr) = NULL)
{
  addr_to_source = addr_to_source_p;
}

Insn_Stream::~Insn_Stream()
{
  if( ureg_to_info )
    {
      for(int i=0; i<ureg_to_info_size; i++)
        if( ureg_to_info[i].pre_segment_si )
          delete ureg_to_info[i].pre_segment_si;
      free(ureg_to_info);
    }
  if( ea_to_info ) g_hash_table_destroy(ea_to_info);
}

bool
Insn_Stream::get_reg_val(Reg_Info *ri, int32_t &val)
{
  if( !ri ) {val = 0; return true;}
  Reg_File_Info *rfi = &ureg_to_info[ri->reg_num_univ];
  if( !rfi->value_valid ) {val = 0; return false; }
  val = rfi->value;
  return true;
}


Insn_Decode_Info *
Insn_Stream::next_insn(uint32_t text, uint32_t pc, int tag, bool doomed,
                       int32_t eap, int cwp_provided)
{
  if( cwp_provided != -1 ) cwp = cwp_provided;
  const int cwp_saved = cwp;
  
  Insn_Decode_Info* const si = new Insn_Decode_Info(this,text,pc,tag);
  Reg_Info *ri;

  Reg_Info* const ri_rd = si->by_role[OR_rd];
  Reg_Info* const ri_rs1 = si->by_role[OR_rs1];
  Reg_Info* const ri_rs2 = si->by_role[OR_rs2];

  if( doomed )
    {
      cwp = cwp_saved;
      if( ri_rd ) ri_rd->value_valid = false;
      si->ea_valid = false;
      si->ea_base_valid = false;
      while( si->sources_iterate(ri) )
        { ri->writer = -1;  ri->writer_ri = NULL;  ri->writer_si = NULL; }
      return si;
    }

  const bool imm = getI(text);
  const int32_t simm13 = getSIMM13(text);

  int32_t rs1value, rs2value;
  bool rs1valid = get_reg_val(si->by_role[OR_rs1],rs1value);
  bool rs2valid = get_reg_val(si->by_role[OR_rs2],rs2value);
  bool arith_valid = rs1valid && ( imm || rs2valid );
  int32_t op2value = imm ? simm13 : rs2value;
  si->ea_base_valid = 0;  // Reassigned.

  if( ih_ismem(si->ih) )
    {
      if( eap )
        {
          si->ea_valid = true;
          si->ea = eap;
        }
      else
        {
          si->ea_valid = arith_valid;
          if( arith_valid )
            {
              si->ea = rs1value + op2value;
            }
          else if( ri_rs1 && rs1valid && !imm )
            {
              si->ea = rs1value;
              si->ea_base_valid = 1;
            }
          else if( ri_rs2 && rs2valid && !imm )
            {
              si->ea = rs2value;
              si->ea_base_valid = 2;
            }
        }
    }
  else
    si->ea_valid = false;

  const int32_t ea = si->ea;

  si->finalize();

  if( ri_rd )
    {
      Reg_File_Info *rfi = &ureg_to_info[ri_rd->reg_num_univ];
      if( si->ih == IH_SETHI )
        {
          rfi->value = getIMM22(text) << 10;
          rfi->value_valid = true;
        }
      else if( arith_valid )
        {
          rfi->value_valid = true; // Modified in default case.
          switch( si->ih ){
          case IH_SLL: rfi->value = rs1value << ( op2value & 0x1f ); break;
          case IH_SRA: rfi->value = rs1value >> ( op2value & 0x1f ); break;
          case IH_SRL:
            rfi->value = uint32_t(rs1value) >> ( op2value & 0x1f ); break;
          case IH_SUBCC:
          case IH_SUB: rfi->value = rs1value - op2value; break;
          case IH_SAVE: case IH_RESTORE:
          case IH_ADDCC:
          case IH_ADD: rfi->value = rs1value + op2value; break;
          case IH_ORCC:
          case IH_OR: rfi->value = rs1value | op2value; break;
          case IH_ORNCC:
          case IH_ORN: rfi->value = rs1value | ~op2value; break;
          case IH_ANDCC:
          case IH_AND: rfi->value = rs1value & op2value; break;
          case IH_ANDNCC:
          case IH_ANDN: rfi->value = rs1value & ~op2value; break;
          case IH_XORCC:
          case IH_XOR: rfi->value = rs1value ^ op2value; break;
          case IH_XNORCC:
          case IH_XNOR: rfi->value = ~(rs1value ^ op2value); break;
          default: rfi->value_valid = false; break;
          }
        }
      else
        rfi->value_valid = false;

      ri_rd->value_valid = rfi->value_valid;
      ri_rd->value = rfi->value;
    }

  PStack<Insn_Decode_Info*> check(NULL);

  while( si->sources_iterate(ri) )
    {
      Reg_File_Info *rfi = NULL;

      if( ri->reg_num_univ == REG_UNIV_LOAD && si->ea_valid )
        rfi = (Reg_File_Info*)
          g_hash_table_lookup(ea_to_info,(gpointer)(size_t)(ea+ri->ri_offset));
      if( !rfi ) rfi = &ureg_to_info[ri->reg_num_univ];
      if( !rfi->si ) rfi->pre_segment_si = rfi->si = new Insn_Decode_Info(ri);

      ri->writer = rfi->tag;
      ri->writer_si = rfi->si;
      ri->writer_ri = rfi->ri;
      ri->final_use = false; // May be changed.
      rfi->reader_last_ri = ri;
      if( Reg_Info* const wri = ri->writer_ri )
        {wri->use_count++; wri->use_count_live++;}
    }

  while( si->dests_iterate(ri) )
    {
      ri->use_count_live = ri->use_count = 0;
      ri->overwrite_pending = true;
      Reg_File_Info* rfi = NULL;

      if( ri->reg_num_univ == REG_UNIV_STORE && si->ea_valid )
        {
          const gpointer ea_byte = (gpointer)(size_t)(ea+ri->ri_offset);
          rfi = (Reg_File_Info*) g_hash_table_lookup(ea_to_info,ea_byte);
          if( !rfi )
            {                    
              rfi = new Reg_File_Info;
              bzero(rfi,sizeof(*rfi));
              g_hash_table_insert(ea_to_info, ea_byte, rfi);
            }
        }
      else
        rfi = &ureg_to_info[ri->reg_num_univ];

      if ( Reg_Info* const rri = rfi->reader_last_ri )
        rri->final_use = true;

      if( Reg_Info* const wri = rfi->ri )
        {
          wri->overwrite_pending = false;
          rfi->si->overwriter_tag_min = min(rfi->si->overwriter_tag_min, tag );
          check += rfi->si;
        }
          
      rfi->tag = tag;
      rfi->ri = ri;
      rfi->si = si;
      rfi->reader_last_ri = NULL;
    }

  while( Insn_Decode_Info* const si = check )
    {
      if( si->dests_unlisted
          || si->dests_unused_indirect || si->dests_unused_direct ) continue;

      int use_count_total = 0, use_count_live_total = 0;
      bool live = false;
      while( si->dests_iterate(ri) )
        {
          use_count_total += ri->use_count;
          use_count_live_total += ri->use_count_live;
          live |= ri->overwrite_pending;
        }
      if( live || use_count_live_total ) continue;
      if( use_count_total ) si->dests_unused_indirect = true;
      else                  si->dests_unused_direct = true;

      while( si->sources_iterate(ri) )
        if( Reg_Info* const wri = ri->writer_ri )
          {
            ASSERTS( wri->use_count_live > 0 );
            if( --wri->use_count_live <= 0 ) check += ri->writer_si;
          }
    }

  return si;
}


static int
text_to_hash(unsigned text)
{
  bool debug = false;
  int *pt = &parse_table[0];
  int i = 0;
  int entry;

  unsigned nop_ih = 0x01000000;
  if( text == nop_ih ) return 537;

  while( i++ < 10 )
    {
      int left_shift = *pt++;
      int right_shift = *pt++;
      int field = ( text << left_shift ) >> right_shift;
      if( debug )
        printf(" Left %d, right %d, field 0x%x\n",left_shift,right_shift,field);
      entry = pt[field];
      if( entry <= 0 ) break;
      pt = &parse_table[entry];
    }

  ASSERTS( i != 10 );

  return -entry;
}


Dis_Piece*
Insn_Decode_Info::dis_piece_alloc()
{
  ASSERT( dp_count < dp_limit );
  return &dis_piece[dp_count++];
}

void
Insn_Decode_Info::dis_piece_unalloc(int num)
{
  ASSERT( dp_count >= num );
  while( num-- ) dis_piece[--dp_count].reset();
}

Dis_Piece*
Insn_Decode_Info::dis_piece_iterate()
{
  if( !dp_count ) disassemble();
  if( dp_iterator == dp_count )
    {
      dp_iterator = 0;
      return NULL;
    }
  return &dis_piece[dp_iterator++];
}

char *
Insn_Decode_Info::get_dis_string()
{
  if( !dis_string )
    for(Dis_Piece *dp = NULL; (dp = dis_piece_iterate()); )
      dis_string += dp->str;
  return dis_string.s;
}


void
Insn_Decode_Info::disassemble()
{
  bool pseudo = true;
  ASSERT( ih < 1000 );

  Dis_Piece *dpi = dis_piece_alloc();
  dpi->type = DPT_Mnemnoic;

  // Reassigned for some instructions.
  const char *name_str = ihash_names[ih];
  const char *format = ihash_amodes[ih];

  const int rs1 = getRS1(text);
  const int rs2 = getRS2(text);
  const int rs3 = getRS3(text);
  const int rd  = getRD(text);
  const int imm = getI(text);
  const int simm10 = getSIMM10(text);
  const int simm11 = getSIMM11(text);
  const int simm13 = getSIMM13(text);

  switch( ih ){
  case IH_TA:
  case IH_TCC:
  case IH_TCS:
  case IH_TE:
  case IH_TG:
  case IH_TGE:
  case IH_TGU:
  case IH_TL:
  case IH_TLE:
  case IH_TLEU:
  case IH_TN:
  case IH_TNE:
  case IH_TNEG:
  case IH_TPOSZ:
  case IH_TVC:
  case IH_TVS:
    if( imm && !rs1 ) format = "7I',Ss";
    else format = "7I',1r'+?Ss:2r";
    if( getCC12_11(text) == 0 ) format += 4;
    break;
  case IH_OR:
    if( !rs1 && !rs2 && !imm )
      {
        name_str = "clr";
        format = "dr";
        break;
      }
    if( !rs1 )
      {
        name_str = "mov";
        format = "?Ss:2r',dr";
        break;
      }
    break;
  case IH_XNOR:
    if( imm || rs2 ) break;
    name_str = "not";
    format = "1r',dr";
    break;
  case IH_SUB:
    if( rs1 || imm ) break;
    name_str = "not";
    format = "2r',dr";
    break;
  case IH_SUBCC:
    if( !rd )
      {
        name_str = "cmp";
        format = "1r',?Ss:2r";
      }
    break;
  case IH_JMPL:
    if( !rd )
      {
        bool off8 = imm && simm13 == 8;

        if( rs1 == FIELD_I7 && off8 )
          {
            name_str = "ret";
            format = "";
          }
        else if ( rs1 == FIELD_O7 && off8 )
          {
            name_str = "retl";
            format = "";
          }
        else
          {
            format = "1r'+?Ss:2r";
            name_str = "jmp";
          }
      }
    else if( rd == FIELD_O7 )
      {
        name_str = "call";
        format = "1r'+?Ss:2r";
      }
    break;
  case IH_RESTORE:
  case IH_SAVE:
    if( imm || rs1 || rs2 || rd ) break;
    format = "";
    break;
  case IH_STDFA:
    if ( !insn_is_stda_block(ih,text) ) break;
    name_str = "stda.block";
    format = "dd','[1r'+?Ss:2r']";
    break;
  case IH_LDDFA:
    if ( !insn_is_ldda_short(ih,text) ) break;
    name_str = "ldda.byte";
    format = "dd','[1r'+?Ss:2r']";
    break;
  case IH_PREFETCH:
    format = "'[1r'+?Ss:2r']',dP";
    break;
  }

  dpi->str = strdup(name_str);

  int val; // Register number or immediate.
  const int punct_limit = 100;
  char punct[punct_limit];
  char *puncti = punct;
  char punct_last_c = 0;

  *puncti++ = ' ';
  *puncti++ = ' ';

  while( *format )
    {
      Operand_Role role = OR_num; // Force an error if not re-assigned.

      switch( *format++ ){

        /* Literal character in format string. */
      case AM_CHAR:
        ASSERT( puncti < &punct[punct_limit-1] );
        punct_last_c = *format; // Zeroed at FORMAT.
        if( *format == '+' || *format == ']' ) *puncti++ = ' ';
        *puncti++ = *format;
        if( *format == ',' || *format == '[' || *format == '+') *puncti++ = ' ';
        format++;
        break;

        /* Skip following two format characters if instruction
           is immediate type. */
      case AM_I0:      if( imm ) format += 2;   break;

        /* Skip following two format characters if instruction
           is not immediate type. */
      case AM_I1:      if( !imm ) format += 2;  break;

        /* Operand location and types.  Extract value and
           go to code that interprets next format character as
           a format.  */
      case AM_DISP19:  val = getDISP19(text);                  goto FORMAT;
      case AM_DISP2_14:
        val = ( getDISP2(text) << 14 ) | getDISP14(text);      goto FORMAT;
      case AM_DISP22:  val = getDISP22(text);                  goto FORMAT;
      case AM_DISP30:  val = getDISP30(text);                  goto FORMAT;
      case AM_IMM22:   val = getIMM22(text);                   goto FORMAT;
      case AM_RD:      val = rd;               role = OR_rd;   goto FORMAT;
      case AM_RS1:     val = rs1;              role = OR_rs1;  goto FORMAT;
      case AM_RS2:     val = rs2;              role = OR_rs2;  goto FORMAT;
      case AM_RS3:     val = rs3;              role = OR_rs3;  goto FORMAT;
      case AM_Y:       val = 0;                role = OR_mult; goto FORMAT;
      case AM_ASI:     val = getASI(text);                     goto FORMAT;
      case AM_SIMM3:   val = text & 0x7;                       goto FORMAT;
      case AM_SIMM10:  val = simm10;                           goto FORMAT;
      case AM_SIMM11:  val = simm11;                           goto FORMAT;
      case AM_SIMM13:  val = simm13;                           goto FORMAT;
      case AM_SHCNT32: val = getSHCNT32(text);                 goto FORMAT;
      case AM_SHCNT64: val = getSHCNT64(text);                 goto FORMAT;
      case AM_CC26_25: val = getCC26_25(text); role = OR_cc;   goto FORMAT;
      case AM_CC12_11: val = getCC12_11(text); role = OR_cc;   goto FORMAT;
      case AM_CC21_20: val = getCC21_20(text); role = OR_cc;   goto FORMAT;
      default:         ASSERTS( false );                       break;
      }
      continue;

    FORMAT:
      char *punct_str;
      if( punct != puncti )
        {
          Dis_Piece *dpp = dis_piece_alloc();
          dpp->type = DPT_Punctuation;
          *puncti = 0;
          punct_str = dpp->str = strdup(punct);
          puncti = punct;
        }
      else
        punct_str = NULL;
      Dis_Piece *dpf = dis_piece_alloc();
      RString piece_str;

      switch( *format++ ){

        /* Immediates. */
      case AM_IMM:
        if( pseudo && !val && punct_last_c == '+' )
          {
            dis_piece_unalloc(2);
            break;
          }

        dpf->type = DPT_Constant;
        piece_str.sprintf("%u",val);
        dpf->str = piece_str.remove();
        break;

      case AM_SIMM:
        if( pseudo && !val && punct_last_c == '+' )
          {
            dis_piece_unalloc(2);
            break;
          }
        {
          int nval = val;
          if( val < 0 )
            for( char *ps = punct_str; *ps; ps++ )
              if( *ps == '+' ) { *ps = '-'; nval = -val; break; }
          dpf->type = DPT_Constant;
          piece_str.sprintf("%d",nval);
          dpf->str = piece_str.remove();
        }
        break;

      case AM_HI:
        dpf->type = DPT_Constant;
        piece_str.sprintf("%%hi(0x%x)",val<<10);
        dpf->str = piece_str.remove();
        break;

      case AM_FCC: // FP Condition Code Register
        dpf->type = DPT_Register;
        dpf->reg_info = by_role[role];
        piece_str.sprintf("%%fcc%d",val);
        dpf->str = piece_str.remove();
        break;

      case AM_ICC: // INTEGER Condition Code Register
        dpf->type = DPT_Register;
        dpf->reg_info = by_role[role];
        switch( val ){
        case 0: piece_str = "%icc"; break;
        case 2: piece_str = "%xcc"; break;
        default:
          ASSERTS( false );
          piece_str = "%??cc"; break;
        }
        dpf->str = piece_str.remove();
        break;

      case AM_Y:
        dpf->type = DPT_Register;
        dpf->reg_info = by_role[role];
        piece_str = "%y";
        dpf->str = piece_str.remove();
        break;

      case AM_REG: // GP Register
        if( pseudo && !val && punct_last_c == '+' )
          {
            dis_piece_unalloc(2);
            break;
          }

        dpf->type = DPT_Register;
        dpf->reg_info = by_role[role];
        piece_str.sprintf("%%%s",regnames[val]);
        dpf->str = piece_str.remove();
        break;

      case AM_FREGQ:
      case AM_FREGD:
        if ( val & 1 ) val += 0x1f;
        // Fall through...
      case AM_FREGS: 
        dpf->type = DPT_Register;
        dpf->reg_info = by_role[role];
        piece_str.sprintf("%%f%d",val);
        dpf->str = piece_str.remove();
        break;

        /* Displacement (branch or jump target). */
      case AM_PC:
        {
          dpf->type = DPT_CTI_Displacement;
          const t_addr addr = pc + (val<<2);
          piece_str.sprintf("%+di -> {0x%x %s}",val,addr,addr_to_source(addr));
          dpf->str = piece_str.remove();
        }
        break;

        //  Prefetch function.
      case AM_PF:
        dpf->type = DPT_Constant;
        switch ( val ) {
        case 0: piece_str += "#n_reads"; break;
        case 1: piece_str += "#one_read"; break;
        case 2: piece_str += "#n_writes"; break;
        case 3: piece_str += "#one_write"; break;
        case 4: piece_str += "#page"; break;
        case 20: piece_str += "#n_reads_strong"; break;
        case 21: piece_str += "#one_read_strong"; break;
        case 22: piece_str += "#n_writes_strong"; break;
        case 23: piece_str += "#one_write_strong"; break;
        default: piece_str.sprintf("%d",val); break;
        }
        dpf->str = piece_str.remove();
      break;

      default:
        ASSERTS( false );
        break;
      }
      punct_last_c = 0;
    }
  if( punct != puncti )
    {
      Dis_Piece *dpp = dis_piece_alloc();
      dpp->type = DPT_Punctuation;
      *puncti = 0;
      dpp->str = strdup(punct);
    }

  if( ea_valid )
    {
      const char* const sym =
        RX_addr_to_sym_expr(ea,ih_memory_access_size.get(ih,text));
      if( sym && *sym )
        {
          Dis_Piece *dpp = dis_piece_alloc();
          dpp->type = DPT_Address_Symbol;
          dpp->reg_info = by_role[OR_mem];
          RString sym_str; sym_str.sprintf("  {[%s]}",sym);
          dpp->str = sym_str.remove();
        }

      RString addr_str;  addr_str.sprintf("  {[0x%x]}",ea);
      Dis_Piece *dpp = dis_piece_alloc();
      dpp->type = DPT_Address_Value;
      dpp->reg_info = by_role[OR_mem];
      dpp->str = addr_str.remove();
    }

  Reg_Info *ri_rd = by_role[OR_rd];

  if( ri_rd && ri_rd->value_valid && ih != IH_SETHI )
    {
      Dis_Piece *dpp = dis_piece_alloc();
      dpp->type = DPT_Value;
      RString addr_str;
      addr_str.sprintf("  {0x%x}",ri_rd->value);
      dpp->str = addr_str.remove();
    }

}


Insn_Decode_Info::Insn_Decode_Info(Reg_Info *ri_reader)
{
  text = 0;
  pc = 0;
  tag = -1;
  ih = IH_NOP;
  dp_iterator = dp_count = 0;
  dataflow_subtree = 0;
  Reg_Info* const ri = reg_info.pushi();
  ri->reg_num_univ = ri_reader->reg_num_univ;
  ri->reg_num_insn = ri_reader->reg_num_insn;
  ri->desto = true;
  ri->value_valid = false;
  ri->writer = -1;
  ri->writer_si = NULL;
  ri->writer_ri = NULL;
  ri->ri_offset = 0;
  memset(by_role,0,sizeof(by_role));
  dests_unused_direct = dests_unused_indirect = false;
  dests_unlisted = false;
  overwriter_tag_min = TAG_MAX;
  reg_info.storage_unused_free();
}

Insn_Decode_Info::Insn_Decode_Info(Insn_Stream *is, uint32_t text,
                                   uint32_t pc, int tag)
  :text(text),pc(pc),tag(tag)
{
  dataflow_subtree = 0;
  dp_iterator = dp_count = 0;
  dests_unlisted = false;

  int cwp_source = is->cwp;
  int &cwp_desto = is->cwp;

  ih = text_to_hash(text);
  if( ih == IH_SAVE ) cwp_desto = cwp_source - 1;
  else if( ih == IH_RESTORE ) cwp_desto = cwp_source + 1;
  if( cwp_desto < 0 ) cwp_desto = is->window_count - 1;
  else if( cwp_desto == is->window_count ) cwp_desto = 0;
  dests_unlisted |= cwp_desto != cwp_source;
  dests_unlisted |= ih_isdcti(ih);

  {
#   define NEW_RI { ri = reg_info.pushi(); bzero(ri,sizeof(*ri)); }
    Reg_Info *ri = NULL;

    const int rs1 = getRS1(text);
    const int rs2 = getRS2(text);
    const int rs3 = getRS3(text);
    const int rd  = getRD(text);
    const int i   = getI(text);

#define RUFP2(rrole,field,is_dest)                                            \
    if ( field & 1 ) {                                                        \
      RUFPs(rrole,true,field+0x1f,1,is_dest);                                 \
    } else {                                                                  \
      RUFPs(rrole,true,field,1,is_dest);                                      \
      RUFPs(rrole,false,field+1,1,is_dest);                                   \
    }

#define RUFP(rrole,field,size,is_dest) RUFPs(rrole,true,field,size,is_dest)

#define RUFPs(rrole,ri_link,field,size,is_dest)                               \
    { NEW_RI;                                                                 \
      ri->reg_num_univ = reg_fpr_insn_to_univ(field);                         \
      ri->reg_num_insn = field;                                               \
      ri->role = rrole;                                                       \
      ri->ri_offset = ri_link ? 0 : 1;                                        \
      ri->desto = is_dest; }

#define RUMISC(rrole,univ_num,is_dest)                                        \
    { NEW_RI;                                                                 \
      ri->reg_num_univ = univ_num;                                            \
      ri->role = rrole;                                                       \
      ri->reg_num_insn = -1;                                                  \
      ri->ri_offset = 0;                                                      \
      ri->desto = is_dest; }

#define RUMEM(univ_num,is_dest,byte)                                          \
    { NEW_RI;                                                                 \
      ri->reg_num_univ = univ_num;                                            \
      ri->role = OR_mem;                                                      \
      ri->reg_num_insn = -1;                                                  \
      ri->ri_offset = byte;                                                   \
      ri->desto = is_dest; }

#define RUINTs(rrole,ri_link,field,size,is_dest)                              \
    if( field ) {                                                             \
      NEW_RI;                                                                 \
      ri->reg_num_univ =                                                      \
        is->reg_gpr_insn_to_univ(is_dest ? cwp_desto : cwp_source, field);    \
      ri->role = rrole;                                                       \
      ri->reg_num_insn = field;                                               \
      ri->ri_offset = ri_link ? 0 : 1;                                        \
      ri->desto = is_dest; }

#define RUINT(rrole,field,is_dest) RUINTs(rrole,true,field,0,is_dest)

#define RURDY RUMISC(OR_mult,REG_UNIV_Y,false)
#define RUWRY RUMISC(OR_mult,REG_UNIV_Y,true)

#define RUWRFSR RUMISC(OR_cc,REG_UNIV_FSR,true)
#define RURDFSR RUMISC(OR_cc,REG_UNIV_FSR,false)

    // Does not create ri for second register yet.
#define RUWR2RD RUINTs(OR_rd,true,rd,1,true)
#define RURD2RD RUINTs(OR_rd,true,rd,1,false)

#define RUWRREG15 RUMISC(OR_rd,is->reg_gpr_insn_to_univ(cwp_desto,15),true)
#define RUWRRD RUINT(OR_rd,rd,true)
#define RURDRD RUINT(OR_rd,rd,false)
#define RURDRS1 RUINT(OR_rs1,rs1,false)
#define RURDRS2 RUINT(OR_rs2,rs2,false)


#define RUWR2FRD RUFP2(OR_rd,rd,true)
#define RURD2FRD RUFP2(OR_rd,rd,false)
#define RURD2FRS1 RUFP2(OR_rs1,rs1,false)
#define RURD2FRS2 RUFP2(OR_rs2,rs2,false)
#define RURD2FRS3 RUFP2(OR_rs3,rs3,false)

#define RUWRFRD RUFP(OR_rd,rd,0,true)
#define RURDFRD RUFP(OR_rd,rd,0,false)
#define RURDFRS1 RUFP(OR_rs1,rs1,0,false)
#define RURDFRS2 RUFP(OR_rs2,rs2,0,false)
#define RURDFRS3 RUFP(OR_rs3,rs3,0,false)

#define getCC13_11(text) ((uint32(text)>>11)&0x7)

    // Note: Works for ICC, XCC, and FCC's.
#define RURDCC_13 RUMISC(OR_cc,REG_UNIV_FCC+getCC13_11(text),false)

#define RUNOTIMM (!i)

    // At some point, add these to the switch.
    if( ih_isuseccr(ih) ) {RUMISC(OR_cc,REG_UNIV_ICC,false)}
    if( ih_issetccr(ih) ) {RUMISC(OR_cc,REG_UNIV_ICC,true)}
    if( ih_issetfcc(ih) ) {RUMISC(OR_cc,REG_UNIV_FCC+getCC26_25(text),true)}
    if( ih_isfbpfcc(ih) ) {RUMISC(OR_cc,REG_UNIV_FCC+getCC21_20(text),false)}
    if( ih_isfbfcc(ih) )  {RUMISC(OR_cc,REG_UNIV_FCC,false)}

#include <reguse2.h>
    switch( ih ){
#include <REGUSE.h>
    case IH_FMADDS: case IH_FMSUBS: case IH_FNMSUBS: case IH_FNMADDS:
      RURDFRS1; RURDFRS2; RURDFRS3; RUWRFRD; break;
    case IH_FMADDD: case IH_FMSUBD: case IH_FNMSUBD: case IH_FNMADDD:
      RURD2FRS1; RURD2FRS2; RURD2FRS3; RUWR2FRD; break;
    }

    if ( insn_is_stda_block(ih,text) )
      {
        const int rdbase = reg_double_field_to_num(rd);
        for ( int i = 1;  i < 8;  i++ )
          {RUFP2(OR_rd,reg_double_num_to_field(rdbase+i*2),false);}
      }
  }

  by_role_update();
  dests_unused_direct = dests_unused_indirect = false;
  overwriter_tag_min = TAG_MAX;
}

void
Insn_Decode_Info::by_role_update()
{
  bzero(by_role,sizeof(by_role));
  while ( Reg_Info* const ri = reg_info.iterate() )
    if ( !ri->ri_offset ) by_role[int(ri->role)] = ri;
}

void
Insn_Decode_Info::finalize()
{
  const bool is_any_store = ih_isanystore(ih);
  if( ih_ismem(ih) && ea_valid )
    {
      const uint32_t mem_access_size = ih_memory_access_size.get(ih,text);
      Reg_Info *ri = NULL;
      for( uint byte = 0; byte < mem_access_size; byte++ )
        if( is_any_store ) {RUMEM(REG_UNIV_STORE,true,byte)}
        else               {RUMEM(REG_UNIV_LOAD,false,byte)}
    }
  if( is_any_store && !ea_valid) dests_unlisted = true;
  reg_info.storage_unused_free();
  by_role_update();
# undef NEW_RI
}

bool
Insn_Decode_Info::sources_iterate(Reg_Info* &ri)
{
  while( ( ri = reg_info.iterate() ) ) if( !ri->desto ) return true;
  return false;
}

bool
Insn_Decode_Info::dests_iterate(Reg_Info* &ri)
{
  while( ( ri = reg_info.iterate() ) ) if( ri->desto ) return true;
  return false;
}

//
// Instruction Classification - For External Use
//

bool
decode_insn_isa_call(int32_t text)
{
  const int hash = text_to_hash(text);
  if ( hash == IH_CALL ) return true;
  if ( hash != IH_JMPL ) return false;
  return getRD(text) == FIELD_O7;
}


#if 0
char *
Insn_Decode_Info::disassemble()
{
  char *str = ::disassemble(text);
  return str;
}
#endif

#ifdef MAIN
int
main(int argc, char **argv)
{
  // 10 0 1000 111010 00000 0 0 0000 0000 0000
  // 1001 0001 1101 0000 0000 0000 0000 0000
  //  unsigned text =0xae15e0c0; // or     %l7,192,%l7 ! 0x33c00,192,0x33cc0
  unsigned text = 0x90860000 ; // 7  0x1ffb09e0 addcc  %i0,%g0,%o0 ! 0x0,0x0,0x0
  disassemble(text);

  Insn_Stream is(20);
  Insn_Decode_Info *sins[5];
  Insn_Decode_Info **si = sins;
  int i = 0;
  *si++ = is.next_insn(text,i++);
  // 0x2f0000cf          5  0x1ffb09d8 sethi  %hi(207),%l7 ! %hi(207),0x33c00
  *si++ = is.next_insn(0x2f0000cf,i++);
  // 0xae15e0c0          6  0x1ffb09dc or     %l7,192,%l7 ! 0x33c00,192,0x33cc0
  *si++ = is.next_insn(0xae15e0c0,i++);
  *si = NULL;

  Reg_Info *ri;

  for(si = sins; *si; si++)
    {
      printf("%s  (%d)\n",
             si[0]->disassemble(),
             si[0]->ih);
      while( si[0]->sources_iterate(ri) )
        {
          printf("  Src: isa %d, univ, %d.\n",
                 ri->reg_num_insn,
                 ri->reg_num_univ);
        }
      while( si[0]->dests_iterate(ri) )
        {
          printf("  Dst: isa %d, univ, %d.\n",
                 ri->reg_num_insn,
                 ri->reg_num_univ);
        }
    }

  return 0;

}
#endif
