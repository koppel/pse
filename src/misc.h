/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2015 Louisiana State University

#ifndef MISC_H
#define MISC_H

#include "config.h"
#include "messages.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <malloc.h>
#include <time.h>

#define ASIZE(array) (sizeof(array)/sizeof(array[0]))

inline static int mid(int a, int b) {return a+(b-a)/2;}
inline int64_t max(int a, int64_t b){ return a > b ? a : b; }

template <typename T>
inline T max(T a, T b){ if( a > b ) return a; else return b; }
template <typename T>
inline T min(T a, T b){ if( a < b ) return a; else return b; }

template <typename T>
inline T setmax(T &a, T n){if ( n > a ) a = n; return a;}
template <typename T>
inline T setmin(T &a, T n){if ( n < a ) a = n; return a;}

//
// Machine-Instruction Routines
//

// Wrappers for gcc builtins that might be compiled as single machine
// instructions.

#define GCC_GE(maj,min) __GNUC__ \
  && ( __GNUC__ > (maj) || __GNUC__ == (maj) && __GNUC_MINOR__ >= (min) )

inline int w_popcount(unsigned int i)
{
# if GCC_GE(3,4)
  return __builtin_popcount(i);
# else
  int p = 0;
  while( i ) { p += i & 1; i >>= 1; }
  return p;
# endif
}

inline int w_clz(int i)
{
# if GCC_GE(3,4)
  return __builtin_clz(i);
# else
  if( i < 0 ) return 0;
  int lz = 31;
  while( i >>= 1 ) lz--;
  return lz;
# endif
}

inline int
lg(int i) // Doesn't work for 0.
{
  return 31-w_clz(i);
}

inline bool
is_power_of_2(int i)
{
  return w_popcount(i) == 1;
}


// Remove as many dots (current and parent directory) and slashes as
// possible from Unix PATH_NAME. Returns 1 if resulting path is
// invalid, 1 otherwise. The string at PATH_NAME is
// modified. Examples:
// "/home/smith/bin/../data/datafile" ->  "/home/smith/data/datafile", returns 1
// "//a/b/.//////c/d///.//.///.." ->  "/a/b/c", returns 1
// "bin/../data/datafile" ->  "data/datafile", returns 1
// "bin/../../data/datafile" ->  "../data/datafile", returns 1
// "/a/b/../.." ->  "/", returns 1
// "/a/b/../../.." ->  "/a/b/../../..", returns 0
// Other than detecting a reference to the root directory's parent does not
// check for valid file name syntax and does not check if files or directories
// exist.
int path_dots_remove(char *path_name);

// Return high-resolution time, in seconds.  If POSIX_TIMER defined
// time is number of seconds since the start of 1970 UTC (the Unix
// epoch), otherwise time is relative to first time_fp call.
#undef POSIX_TIMER
double time_fp();

#define COMMALEN 100 /* Buffer size. */
/* Return a pointer to a statically allocated buffer. Buffer is
   reused after filled and so a copy must be made if values are needed
   after a few calls. */
char *comma(double number);


class MD5_File {
public:
  MD5_File(const char *path):vptr((unsigned char*)digest64){file(path);}
  bool file(const char *path);
  inline operator bool(){ return success;}
  uint64_t v64; // MSB is first byte of digest.
  uint32_t v32; // Untested.
  unsigned char* const vptr;
private:
  bool success;
  uint64_t digest64[2];
  char buf[1024];
};

inline uint64_t md5_file_64(const char *path)
{MD5_File md5(path); return md5.v64;}

uint64_t md5_string(const char *str);

enum PVT_Scalar_Type {  // Variable type scalar.
  PVTS_Void,   // Not carrying data.
  // Integers must follow Void.
  PVTS_Int32,
  PVTS_UInt32,
  PVTS_Int64,
  // Single must follow last integer.
  PVTS_Single,
  PVTS_Double,
  // String must follow last float.
  PVTS_String // Char *, null terminated.
};

struct PVT_Scalar { // Variable Type Scalar
  PVT_Scalar(){};
  PVT_Scalar(double d):d(d),type(PVTS_Double){};
  PVT_Scalar(int32_t d):i4(d),type(PVTS_Int32){};
  PVT_Scalar(uint32_t d):u4(d),type(PVTS_UInt32){};
  PVT_Scalar(int64_t d):i8(d),type(PVTS_Int64){};
  PVT_Scalar(char *d):s(d),type(PVTS_String){};
  bool is_integer() const { return type > PVTS_Void && type < PVTS_Single; }
  bool is_number() const { return type > PVTS_Void && type < PVTS_String; }
  template<typename T> T operator = (T d) { *this = PVT_Scalar(d); return d; }
  operator char* () { return NULL; }
  template<typename T> operator T ()
  {
    switch(type){
    case PVTS_Int32: return T(i4);
    case PVTS_Int64: return T(i8);
    case PVTS_UInt32: return T(u4);
    case PVTS_Double: return T(d);
    case PVTS_String: return T(-1);
    default: return T(-1);
    }
  }
  union {
    char *s;
    int32_t i4;
    uint32_t u4;
    int64_t i8;
    double d;
  };
  PVT_Scalar_Type type;
};


//  Return a formated string as sprintf would do (almost), using data
//  in vtarray for arguments from elements 0 to size - 1. The memory
//  allocated for the formatted string, when no longer needed should
//  be freed using free. Here are some additional differences with
//  printf:
//
//  * An effort is made to format reasonably (and safely) when there
//  are errors in the format string. If a conversion specifier
//  specifies an argument that's not present (say the third one when
//  the size is two) the conversion specification is copied
//  verbatim. If a conversion specifier does not match the type of an
//  argument then the argument is converted to the correct type.
//
//  * A conversion specifier of 'T' indicates that the integer
//  argument should be converted to the time (using ctime_r).
//
//  * A conversion specifier of 'b' indicates that the integer
//  argument should be converted to binary.
//
//  * Characters appearing within braces just before the conversion
//  specifier are used as substitute digits, the first character
//  substituting for 0, the second character substituting for 1,
//  etc. This will only work for non-date (T) integer types. For
//  example, psafe_sprintf_dup("%{ny}b",10) would return "ynyn" (just
//  psafe_sprintf_dup("%b",10) would return "1010").
//
//  * Unlike printf, positional and non-positional arguments can be
//  mixed.
//
//  Most other printf conversion specifiers, flags, and widths work in
//  the usual way (since pvt_sprintf_dup calls the host sprintf to do
//  most of the formatting). 

char*
pvt_sprintf_dup(const char *fmt, PVT_Scalar *vtarray, int size);

// Wrapper functions for pvt_sprintf_dup.  These take ordinary arguments.
// So far, just defined for one argument, but more could be easily added.
template <typename T> inline char*
psafe_sprintf_dup(const char *fmt, T d)
{PVT_Scalar vt(d); return pvt_sprintf_dup(fmt,&vt,1);}

class RString {
private:
  void cpy(const char *sp)
  {
    if( !sp ) { s = NULL;  occ = size = 0;  return; }
    s = strdup(sp);
    occ = size = strlen(s);
  }
  void init_space(int initial_string_size)
  {
    size = initial_string_size;
    occ = 0;
    if( !size ) { s = NULL;  return; }
    s = (char*)malloc(size+1);
    s[0] = 0;
  }

public:
  // Make STRING the new string value without copying. Free STRING
  // later if necessary.
  char *assign_take(char *string)
  {
    if(s) free(s);
    s = string;
    occ = size = s ? strlen(s) : 0;
    return s;
  }
  void assign_substring(const char *start, const char *stop)
  {
    if(s){ s[0] = 0;  occ = 0; }
    append_substring(start,stop);
  }

  // If TAKE_POINTER false, initialize with a copy of STRING, otherwise
  // initialize with and if necessary free STRING.
  RString(char *string, bool take_pointer)
  {
    if(!take_pointer) cpy(string);
    else { cpy(NULL); assign_take(string); }
  }
  RString(const char *string){cpy(string);}
  RString(RString &str){ cpy(str.s); }
  RString(){ cpy(NULL); }
  RString(int initial_string_size){ init_space(initial_string_size); }
  ~RString(){ if(s) free(s); };
  char *pvt_sprintf(const char *fmt, PVT_Scalar *vtarray, int size)
  {
    char* const rv = pvt_sprintf_dup(fmt,vtarray,size);
    append(rv); free(rv);
    return s;
  }
  template <typename T> char *safe_sprintf(const char *fmt, T d)
  {
    PVT_Scalar vt(d);
    return pvt_sprintf(fmt,&vt,1);
  }
  char *strftime(const char *fmt, time_t time_ue)
  {
    struct tm t;
    localtime_r(&time_ue,&t);
    return strftime(fmt,&t);
  }
  char *strftime(const char *fmt, const struct tm *tm)
  {
    char maybe[500];
    const int buf_limit = sizeof(maybe) - 1;
    // Note: strftime returns zero if string doesn't fit.
    const int size = ::strftime(maybe, buf_limit, fmt, tm);
    if( !size && fmt[0] )
      fatal("RString::strftime can't create such a long string. Should it?");
    if( size ) operator += (maybe);
    return s;
  }
  char *sprintf(const char *fmt,...) __attribute__ ((format(printf,2,3)))
  {
    va_list ap;

    va_start(ap, fmt);
    return vsprintf(fmt,ap);
    va_end(ap);
  }
  char *vsprintf(const char *fmt, va_list ap)
  {
    va_list ap2;  va_copy(ap2,ap);
    char maybe[80];
    int buf_limit = sizeof(maybe) - 1;
    int size = ::vsnprintf(maybe,buf_limit,fmt,ap);

    if ( size < buf_limit ){ operator += (maybe);  return s; }

    char *buffer = new char[size+1];
    ::vsprintf(buffer,fmt,ap2);
    va_end(ap2);

    operator += (buffer);
    delete buffer;
    return s;
  }

  char *dup() {return s ? strdup(s) : NULL;}
  char *remove()
  {
    if ( !s ) return NULL;
    char *rv = (char*)realloc(s,occ+1);
    cpy(NULL);
    return rv;
  }
  inline bool operator != (RString &rs2) { return operator != (rs2.s); }
  inline bool operator == (RString &rs2) { return operator == (rs2.s); }
  inline bool operator == (const char *s2)
  { return !s || !s2 ? !s && !s2 : !strcmp(s,s2); }
  inline bool operator != (const char *s2){ return ! operator == (s2); }
  inline bool operator ! (){ return !s; }
  inline void operator = (RString &str)
  {
    if( !s ) { cpy(str.s); return; }
    s[0] = 0;  occ = 0;
    operator += (str.s);
  }
  inline void operator = (const char *c)
  {
    if( !s ) { cpy(c); return; }
    s[0] = 0;  occ = 0;
    operator += (c);
  }
  inline operator char*(){ return s;}
  int len(){ return occ; }
  int col()
  {
    int i = occ;
    while( i && s[i-1] != '\n' && s[i-1] != '\r' ) i--;
    return occ - i;
  }
  inline RString operator + (RString &s2){ return operator +(s2.s); }
  inline friend RString operator + (const char *c, RString &s2)
  { return RString(c) + s2; }
  RString operator + (const char *c) {RString rv(s); rv += c; return rv;}
  void operator += (char c)
  {
    if( occ == size )
      {
        if( size == 0 ) init_space(64);
        else
          {
            size <<= 1;
            s = (char*)realloc(s,size+1);
          }
      }
    s[occ++] = c;  s[occ] = 0;
  }
  void operator += (const char *c)
  {
    if( !c ) return;
    if( !s ) { cpy(c);  return; }
    int c_len = strlen(c);
    int new_occ = occ + c_len;
    if( new_occ > size )
      {
        size = new_occ;
        s = (char*)realloc(s,size+1);
      }
    memcpy(&s[occ],c,c_len+1);
    occ = new_occ;
  }
  inline void operator += (RString &str) { operator += (str.s); }
  void append(const char *c){ operator += (c); }
  void append_take(char *c)
  {
    if( occ ) { append(c);  free(c); }
    else { assign_take(c); }
  }
  void append_substring(const char *c, const char *stop)
  {
    if( c >= stop ) return;
    const char *ci = c;
    while( ci < stop && *ci ) ci++;
    const int c_len = ci - c;
    int new_occ = occ + c_len;
    if( new_occ > size )
      {
        if( !size ) init_space(new_occ);
        else s = (char*)realloc(s,new_occ+1);
        size = new_occ;
      }
    memcpy(&s[occ],c,c_len);
    occ = new_occ;
    s[occ] = 0;
  }

  char *s;
  int size; // Maximum string size that can fit in allocated space.
private:
  int occ;  // Current string size.
};

class RStringF: public RString {
public:
  RStringF(const char* fmt,...):RString()
  {va_list ap; va_start(ap,fmt); vsprintf(fmt,ap); va_end(ap);}
};

class RStringFTime: public RString {
public:
  RStringFTime(const char* fmt,time_t t):RString() {strftime(fmt,t);}
  RStringFTime(const char* fmt,struct tm *ts):RString() {strftime(fmt,ts);}
};

class RStringT: public RString {
public:
  RStringT(char* str):RString(){assign_take(str);}
};

class Lazy_String {
public:
  Lazy_String(const char *sep = "")
  {
    size = 1;
    occ = 0;
    storage = (char**)malloc(size*sizeof(void*));
    separator = sep;
  }
  ~Lazy_String(){ if(storage)free(storage); }
  void set_separator(const char *sep){ separator = sep; };
  inline void operator += (char *s)
  {
    if( occ == size )
      {
        size += 10;
        storage = (char**)realloc(storage,size*sizeof(void*));
      }
    storage[occ++] = s;
  }
  char *s(){ flatten(); return f.s;}
  void
  flatten()
  {
    if( !occ ) return;
    for(int i=0; ; )
      {
        f += storage[i];
        if( ++i == occ ) break;
        f += separator;
      }
    occ = 0;
  }
  inline operator char*(){ return s(); }

private:
  char **storage;
  const char *separator;
  int occ, size;
  RString f;
};


template <typename Data> class PQueue {
public:
  PQueue(Data first) {init(); enqueue(first);}
  PQueue() {init();}
  ~PQueue(){ if(storage) free(storage); }

  void operator += (Data i){enqueue(i);}

  operator bool (){ return occupancy; }
  operator Data (){ return dequeue(); }
  int occ() const { return occupancy; }

  Data head() {return storage[head_idx];}
  Data* get_storage() const { return storage; }
  Data* headi() {return occupancy ? &storage[head_idx] : NULL;}
  Data peek_tail(int i)
  {
    int idx = tail_idx - i - 1;
    if( idx < 0 ) idx += size;
    return storage[idx];
  }


  void enqueue_at_head(Data i)
  {
    if( occupancy == size ) alloc();
    if( !head_idx ) head_idx = size;
    storage[--head_idx] = i;
    occupancy++;
  }

  void enqueue(Data i)
  {
    if( occupancy == size ) alloc();
    storage[tail_idx++] = i;
    if( tail_idx == size ) tail_idx = 0;
    occupancy++;
  }

  Data* popi()
  {
    if( !occupancy ) return NULL;
    tail_idx = tail_idx ? tail_idx - 1 : size - 1;
    occupancy--;
    return &storage[tail_idx];
  }
  Data pop()
  {
    Data* const i = popi();
    Data rv = *i;
    i->~Data();
    return rv;
  }

  Data dequeue(){ static Data i; dequeue(i); return i; }

  Data* dequeuei()
  {
    if( !occupancy ) return NULL;
    Data* const i = &storage[head_idx++];
    if( head_idx == size ) head_idx = 0;
    occupancy--;
    return i;
  }

  bool dequeue(Data &i)
  {
    if( !occupancy ) return false;
    i = storage[head_idx++];
    if( head_idx == size ) head_idx = 0;
    occupancy--;
    return true;
  }

  bool iterate(Data &d)
  {
    if( iterator < 0 ) iterator = head_idx;
    if( iterator == tail_idx ) { iterator = -1;  return false; }
    d = storage[iterator];
    iterator++;
    if( iterator == size ) iterator = 0;
    return true;
  }

  void reset()
  {
    head_idx = 0;  tail_idx = 0;  occupancy = 0;  iterator = -1;
  }
  void operator = (Data i){reset();enqueue(i);}

private:

  void init()
  {
    head_idx = tail_idx = occupancy = size = 0;
    iterator = -1;
    storage = NULL;
  }

  void alloc()
  {
    if( size )
      {
        ASSERTS( occupancy );
        const int new_size = size << 1;
        Data* const new_storage = (Data*) malloc(new_size * sizeof(storage[0]));
        Data* sto_ptr = new_storage;
        const bool wrap = tail_idx <= head_idx;
        const int stop = wrap ? size : tail_idx;
        for(int i = head_idx; i < stop; i++) *sto_ptr++ = storage[i];
        if( wrap ) for(int i = 0; i < tail_idx; i++) *sto_ptr++ = storage[i];
        free(storage);
        storage = new_storage;
        size = new_size;
        head_idx = 0;
        tail_idx = occupancy;
      }
    else
      {
        size = 2; // DMK: Change to a larger number after testing.
        storage = (Data*) malloc(size * sizeof(storage[0]));
        head_idx = 0;  tail_idx = 0;
      }
  }

  Data *storage;
  int occupancy; // Number of items in queue.
  int head_idx;  // If non-empty, points to head entry.
  int tail_idx;  // Points to next entry to be written.
  int iterator;
  int size;      // Size of storage.
};

template <typename Data> class PStackIterator;

template <typename Data> class PStack {
public:
  PStack(Data first) {init(); push(first);}
  PStack() {init();}
  void reset() {first_free = 0;};
  ~PStack(){ if(storage) free(storage); }

  // Return true if the stack is not empty.
  operator bool () { return first_free; }

  // Return number of items in the stack.
  int occ() const { return first_free; }

  Data* get_storage() const { return storage; }
  void storage_unused_free()
  {
    if( !size ) return;
    size = first_free;
    if( size )
      storage = (Data*) realloc(storage, size * sizeof(storage[0]));
    else
      {
        free(storage);  storage = NULL;
      }
  }

  Data* iterate()
  {
    if( iterator == first_free ) { iterator = 0; return NULL; }
    return &storage[iterator++];
  }

  bool iterate(Data &d)
  {
    if( iterator == first_free ) { iterator = 0; return false; }
    d = storage[iterator++];
    return true;
  }

  Data* iterate_lifo()
  {
    fatal("Untested.");
    if( iterator_lifo < 0 ) iterator_lifo = first_free;
    return --iterator_lifo ? &storage[ iterator_lifo ] : NULL;
  }

  bool iterate_lifo(Data &d)
  {
    fatal("Untested.");
    if( iterator_lifo < 0 ) iterator_lifo = first_free;
    if( --iterator_lifo < 0 ) return false;
    d = storage[ iterator_lifo ];
    return true;
  }

  // Pop the stack and return the popped item.
  operator Data () { return pop(); }

  // Push I onto the stack.
  void operator += (Data i){push(i);}

  // Push I onto the stack.
  void push(Data i)
  {
    if( first_free == size ) alloc();
    storage[first_free++] = i;
  }

  // Allocate an element, push it, and return a pointer.
  Data* pushi()
  {
    if( first_free == size ) alloc();
    Data* const rv = &storage[first_free++];
    new (rv) Data;
    return rv;
  }

  // Return top of stack but don't pop.
  Data& peek() const { return storage[first_free-1]; }
  bool peek(Data &i) const
  {
    if( !first_free ) return false;
    i = peek();
    return true;
  }

  // Pop the stack and return popped item.
  Data pop() { return storage[--first_free]; }

  // Return true if stack non-empty just before call.  If non-empty,
  // pop the stack, assign popped item to I and return true.
  bool pop(Data &i)
  {
    if( !first_free ) return false;
    i = pop();
    return true;
  }

  void yank(Data &d)
  {
    for(int i=0; i<first_free; i++) if( d == storage[i] )
      {
        storage[i] = storage[--first_free];
        return;
      }
  }

private:
  void init() {
    first_free = 0;  size = 0;  storage = NULL;
    iterator = 0;  iterator_lifo = -1;
  }
  void alloc()
  {
    if( size )
      {
        size <<= 2;
        storage = (Data*) realloc(storage, size * sizeof(storage[0]));
      }
    else
      {
        size = 32;
        storage = (Data*) malloc(size * sizeof(storage[0]));
      }
  }

  Data *storage;
  int first_free;
  int size;
  int iterator, iterator_lifo;
  friend class PStackIterator<Data>;
};

template <typename Data>
class PStackIterator {
public:
  PStackIterator(PStack<Data>& pstack):pstack(pstack),index(0){}
  operator bool () { return index < pstack.first_free; }
  operator Data () { return pstack.storage[index]; }
  Data operator ++ () { fwd(); return pstack.storage[index]; }
  Data operator ++ (int) { fwd(); return pstack.storage[index-1]; }
  Data operator -> () { return pstack.storage[index]; }
private:
  PStack<Data>& pstack;
  int index;
  void fwd()
  {
    if( pstack.first_free == index ) return;
    index++;
  }
};

class PSplit {
public:
  PSplit(const char *string, char delimiter,
         int piece_limit = 0, int options = 0):
    queue(),free_at_destruct(!(options & DontFree)), delimiter(delimiter)
  {
    const char *start = string;
    if( start ) while( *start ) {
        const char *ptr = start;
        while( *ptr && *ptr != delimiter ) ptr++;
        if( start != ptr )
          {
            if( --piece_limit == 0 ) { queue += strdup(start); break; }
            RString piece;
            piece.assign_substring(start,ptr);
            queue += piece.remove();
          }
        if( !*ptr ) break;
        start = ++ptr;
      }
    storage = queue.get_storage();
    storage_occ = queue.occ();
  }
  ~PSplit()
  {
    if( !free_at_destruct ) return;
    for(int i=0; i<storage_occ; i++)free(storage[i]);
  }
  //  static const int NoCompress = 1;  // Implement when needed.
  static const int DontFree = 2;
  int occ() const { return queue.occ(); }
  char* operator [] (int idx) const { return storage[idx]; }
  operator int () const { return queue.occ(); }
  bool operator ! () const { return !queue.occ(); }
  char* pop()
  {
    return queue.occ() ? queue.pop() : NULL;
  }
  char* dequeue()
  {
    char* rv;
    if( queue.dequeue(rv) ) return rv;
    return NULL;
  }

  operator char* () { return dequeue(); }

  char* joined_copy()
  {
    if( !occ() ) return NULL;
    RString joined;
    for( char *piece = NULL; queue.iterate(piece); )
      {
        if( joined ) joined += delimiter;
        joined += piece;
      }
    return joined.remove();
  }

private:
  PQueue<char*> queue;
  const bool free_at_destruct;
  const char delimiter;
  char** storage;
  int storage_occ;
};

template <typename K, typename D> struct PSList_Elt_Generic
{
  K key;
  int insertion_idx;
  D data;
};

template <typename Data, typename Key = int> class PSList
{
  typedef PSList_Elt_Generic<Key,Data> Elt;
public:
  PSList(Data null_data):null_data(null_data),stack()
  {
    sorted = false;
    free_data = false;
    insertion_idx = 0;
  }
  PSList():stack()
  {
    sorted = false;
    free_data = false;
    insertion_idx = 0;
    memset(&null_data,0,sizeof(null_data));
  }

  Data* inserti(Key key)
  {
    Elt* const elt = stack.pushi();
    elt->key = key;
    elt->insertion_idx = insertion_idx++;
    sorted = false;
    return &elt->data;
  }

  void insert(Key key, Data data)
  {
    Data* const dataptr = inserti(key);
    *dataptr = data;
    sorted = false;
  }

  void operator += (PSList &l2)
  {
    for( Elt elt; l2.iterate(elt); )
      {
        elt.insertion_idx = insertion_idx++;
        stack += elt;
      }
    sorted = false;
  }

  Data operator [] (int idx) {return stack.get_storage()[idx].data;}

  int key_order(int32_t k1, int32_t k2){ return k1-k2; }
  int key_order(int64_t k1, int64_t k2){ return k1-k2; }
  int key_order(uint32_t k1, uint32_t k2)
  { return k1 > k2 ? 1 : k1 == k2 ? 0 : -1; }
  int key_order(uint64_t k1, uint64_t k2)
  { return k1 > k2 ? 1 : k1 == k2 ? 0 : -1; }
  int key_order(void *k1, void *k2)
  { return k1 > k2 ? 1 : k1 == k2 ? 0 : -1; }
  int key_order(const char *k1, const char *k2){ return strcmp(k1,k2); }

  Data* geti(Key key)
  {
    const int idx = find_eq(key);
    get_inserted = idx < 0;
    if ( !get_inserted ) return &stack.get_storage()[idx].data;
    Elt* const elt = stack.pushi();
    elt->key = key;
    sorted = false;
    return &elt->data;
  }

  bool new_item() { return get_inserted; }

  Elt* get_elt(int idx)
  {
    return &stack.get_storage()[idx];
  }

  Data get_eq(Key k)
  {
    const int idx = find_eq(k);
    return idx < 0 ? null_data : stack.get_storage()[idx].data;
  }

  Data get_ge(Key k)  // Smallest element >= k.
  {
    const int idx = find_ge(k);
    if ( idx >= 0 )
      {
        Elt* const storage = stack.get_storage();
        ASSERTS( key_order(storage[idx].key,k) >= 0 );
        ASSERTS( idx == 0 || key_order(storage[idx-1].key,k) <= 0 );
      }

    return idx < 0 ? null_data : stack.get_storage()[idx].data;
  }

  Data get_le(Key k)  // Largest element <= k.
  {
    const int idx = find_le(k);
    if ( idx >= 0 )
      {
        Elt* const storage = stack.get_storage();
        const int occ = stack.occ();
        ASSERTS( key_order(storage[idx].key,k) <= 0 );
        ASSERTS( idx + 1 == occ || key_order(storage[idx+1].key,k) >= 0 );
      }

    return idx < 0 ? null_data : stack.get_storage()[idx].data;
  }

  int find_eq(Key k)
  {
    int order;
    const int idx = find(k,order);
    return order || idx < 0 ? -1 : idx;
  }
  int find_le(Key k) // Largest element <= k.
  {
    int order;
    const int idx = find(k,order);
    if ( idx < 0 ) return -1;
    if ( order <= 0 ) return idx;
    return idx - 1;
  }
  int find_ge(Key k) // Smallest element >= k.
  {
    int order;
    int idx = find(k,order);
    if ( idx < 0 ) return -1;
    if ( order >= 0 ) return idx;
    idx++;
    return idx == stack.occ() ? -1 : idx;
  }

  // Deprecated
  int find(Key k)
  {
    const int idx = find_ge(k);
    return idx < 0 ? stack.occ() : idx;
  }

private:
  int find(Key k, bool exact)
  {
    int order;
    const int idx = find(k,order);
    return order ? -1 : idx;
  }
  int find(Key k, int &order)
  {
    sort();
    Elt* const storage = stack.get_storage();
    const int occ = stack.occ();
    if ( occ == 0 ) { order = 0;  return -1; }

    int index = 0;
    int delta = 1 << lg(occ);

    while ( true )
      {
        index += delta;
        if ( index >= occ )
          {
            order = key_order(storage[occ-1].key,k);
            if ( order <= 0 ) return occ - 1;
          }
        else
          {
            order = key_order(storage[index].key,k);
            if ( order == 0 ) return index;
          }

        if ( !delta ) return index;
        if ( order > 0 ) index ^= delta;

        delta >>= 1;
      }
  }


public:
  int occ() const { return stack.occ(); }

  static int comp_num(const void *a, const void *b)
  {
    return ((Elt*)a)->key == ((Elt*)b)->key
      ? ((Elt*)a)->insertion_idx - ((Elt*)b)->insertion_idx
      : ((Elt*)a)->key > ((Elt*)b)->key ? 1 : -1;
  }
  static int comp_str(const void *a, const void *b)
  {
    if ( const int ord = strcmp(((Elt*)a)->key,((Elt*)b)->key) ) return ord;
    return ((Elt*)a)->insertion_idx - ((Elt*)b)->insertion_idx;
  }

  typedef int (*Comp_Function)(const void*,const void*);
  Comp_Function get_comp(double *dummy){ return comp_num; }
  Comp_Function get_comp(uint32_t *dummy){ return comp_num; }
  Comp_Function get_comp(int32_t *dummy){ return comp_num; }
  Comp_Function get_comp(uint64_t *dummy){ return comp_num; }
  Comp_Function get_comp(int64_t *dummy){ return comp_num; }
  Comp_Function get_comp(char** ){ return comp_str; }
  Comp_Function get_comp(const char** ){ return comp_str; }
  void sort()
  {
    if( sorted ) return;
    sorted = true;
    Elt* storage = stack.get_storage();
    Key *dummy = NULL;
    qsort(storage,stack.occ(),sizeof(Elt),get_comp(dummy));
  }

  bool iterate(Elt& elt) { sort(); return stack.iterate(elt);}
  bool iterate(Data &data) { Key dummy; return iterate(dummy,data); }
  bool iterate(Key& key, Data& data)
  {
    sort();
    Elt* const elt = stack.iterate();
    if( !elt ) return false;
    key = elt->key;
    data = elt->data;
    return true;
  }

  Data iterate()
  {
    sort();
    Elt* const elt = stack.iterate();
    return elt ? elt->data : null_data;
  }

  void reset(bool next_free_data = false)
  {
    if( free_data ) for( Elt elt; stack.pop(elt); ) free((void*)elt.data);
    else stack.reset();
    free_data = next_free_data;
  }

private:
  bool free_data;
  bool sorted;
  bool get_inserted;
  PStack<Elt> stack;
  Data null_data;
  int insertion_idx;
};

template <typename Data> class PList
{
public:
  PList() { next = prev = NULL; }

  PList* remove(){ remove(NULL); return this; }
  PList* remove(PList *base)
  {
    if( next ) next->prev = prev;
    if( prev ) prev->next = next;
    return base == this ? next : base;
  }

  PList* before_insert(){ return before_insert(new PList<Data>()); }
  PList* before_insert(PList *elt)
  {
    elt->prev = prev;
    if( prev ) prev->next = elt;
    prev = elt;
    elt->next = this;
    return elt;
  }

  PList *next, *prev;
  Data data;
};

// Debug Uninitialized Storage
//
// When compiled with DEBUG_MALLOCED defined, each character allocated with
// malloc and sometimes realloc will be initialized to 0xaa.
//
void debug_malloced_init(void);

#endif
