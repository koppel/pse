/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2010 Louisiana State University

// $Id$

// Low-Level Dataset File Access Routines
//
// Dataset_File:      Performs low-level access.
// Decompress_Buffer: Used for buffering data for decompression.
// Auto_Assign:       Used by higher-level code to grab values from dataset file.

#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <stdio.h>
#include <bzlib.h>
#include <memory>
#include "dataset_rfile.h"

bool
dataset_rfile_default_error_handler(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);

  fprintf(stderr, "\nInternal Error: ");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\nExiting. \n");
  exit(-1);

  va_end(ap);
  return false;
}

// ============================================================================
/// Decompress Buffer

void
DSF_Decompress_Buffer::load(void *ptr, int size_compressed,
                            int size_uncompressed)
{
  if( buffer ) free(buffer);

  buffer = (char*) malloc( size_uncompressed );

  pos = 0;
  size = size_uncompressed;

  int small = 0;  // If 1 use less space but more time.
  int verbosity = 1; // 0 to 4.
  unsigned int destLen = size_uncompressed;

  int rv = BZ2_bzBuffToBuffDecompress(buffer, &destLen, (char*)ptr,
                                      size_compressed, small, verbosity);

  if( rv != BZ_OK ) (error_handler)("Corrupted data file.");
}


// ============================================================================
/// Auto_Assign

DSF_Auto_Assign::DSF_Auto_Assign()
{
  items = NULL;  size = 0;  occ = 0;  df = NULL;
  items_str = NULL;  size_str = 0;  occ_str = 0;
}

DSF_Auto_Assign::~DSF_Auto_Assign()
{
  if( items ) free(items);
  if( items_str ) free(items_str);
}

void
DSF_Auto_Assign::reset()
{
  occ = 0;  occ_str = 0;
  level = df->namespace_level;
}

void
DSF_Auto_Assign::assure_space(DSF_NS_Get_Item* &items, int &size, int &occ)
{
  if( occ < size ) return;
  const int oldsize = size;
  size += 10;
  int amt = size * sizeof(*items);
  items = (DSF_NS_Get_Item*)(items ? realloc(items,amt) : malloc(amt));
  for(int i = oldsize; i < size; i++) new (&items[i]) DSF_NS_Get_Item();
}

void
DSF_Auto_Assign::put(int idx, int32_t *vptr)
{
  assure_space(items,size,occ);
  DSF_NS_Get_Item *item = &items[occ++];
  item->idx = idx;
  item->type = DSF_CMD_IDX_IN4;
  item->ptr.i4 = vptr;
  item->encounters = 0;
}

void
DSF_Auto_Assign::scan(DSF_Entry &f, DSF_Command limit)
{
  while( df->get(f) && f.type != limit );
}

void DSF_Auto_Assign::put(const char *key, int32_t *vptr){ putt(key,vptr); }
void DSF_Auto_Assign::put(const char *key, int64_t *vptr){ putt(key,vptr); }
void DSF_Auto_Assign::put(const char *key, char **vptr){ putt(key,vptr); }


template <class T>
void
DSF_Auto_Assign::putt(const char *key, T *vptr)
{
  assure_space(items_str,size_str,occ_str);
  DSF_NS_Get_Item *item = &items_str[occ_str++];
  item->key = key;
  item->type = to_CMD_xxx(*vptr);
  item->ptr.i4 = (int32_t*)vptr;
  item->encounters = 0;
}

bool
DSF_Auto_Assign::check_idx(DSF_Entry &e)
{
  for(int i=0; i<occ; i++)
    {
      DSF_NS_Get_Item &item = items[i];
      if( item.idx != e.key.i4 ) continue;
      assign_var(item,e);
      return true;
    }
  return false;
}

bool
DSF_Auto_Assign::check_str(DSF_Entry &e)
{
  for(int i=0; i<occ_str; i++)
    {
      DSF_NS_Get_Item &item = items_str[i];
      if( item.key != e.key.s ) continue;
      assign_var(item,e);
      return true;
    }
  return false;
}

inline void
DSF_Auto_Assign::assign_var(DSF_NS_Get_Item &item, DSF_Entry &e)
{
  item.encounters++;
  int64_t i;

  switch( e.type ){
  case DSF_CMD_STR_UI1:
  case DSF_CMD_IDX_UI1: i = e.data.u1; break;
  case DSF_CMD_STR_IN1:
  case DSF_CMD_IDX_IN1: i = e.data.i1; break;
  case DSF_CMD_STR_IN4:
  case DSF_CMD_IDX_IN4: i = e.data.i4; break;
  case DSF_CMD_STR_IN8:
  case DSF_CMD_IDX_IN8: i = e.data.i8; break;
    // DMK 10 September 2002,  8:42:27 CDT
    // For now, convert doubles to integers.
  case DSF_CMD_STR_DBL:
  case DSF_CMD_IDX_DBL: i = int64_t(e.data.d);  break;
  case DSF_CMD_STR_STR:
  case DSF_CMD_IDX_STR: i = 0;         break; // Assign i to avoid warnings.
  default:
    (df->error_handler)("Cannot handle type.");
    i = 0;
  }
  switch( item.type ){
  case DSF_CMD_IDX_IN4: *item.ptr.i4 = i; break;
  case DSF_CMD_IDX_IN8: *item.ptr.i8 = i; break;
  case DSF_CMD_IDX_STR: *item.ptr.c = e.data.s.dup(); break;
  default: (df->error_handler)("Cannot handle type.");
  }
  if( level != df->namespace_level )
    (df->error_handler)("File problem.");
}

#if 0
// Not needed yet.  Uncomment when ready to test.
int
DSF_Auto_Assign::not_found_count()
{
  int nf = 0;
  for(int i=0; i<occ; i++) if( items[i].encounters ) nf++;
  return nf;
}

int
DSF_Auto_Assign::multiple_find_count()
{
  int nf = 0;
  for(int i=0; i<occ; i++) if( items[i].encounters > 1 ) nf++;
  return nf;
}
#endif

bool
DSF_Auto_Assign::each_found_once()
{
  for(int i=0; i<occ; i++) if( items[i].encounters != 1 ) return false;
  return true;
}


// ============================================================================
/// Dataset_File

bool
Dataset_File::init(char *file_namep)
{
  file_name = file_namep;
  memset(user_cmds,0,sizeof(user_cmds));
  return reopen();
}

bool
Dataset_File::reopen()
{
  tr = fopen(file_name,"r");
  if( !tr )
    return (error_handler)("Could not open file %s for input.", file_name);
  reset_position_state();


  if( eof() ) return false;

  file_big_endian = true; // DMK: Default, for now.  5 August 2002

  while( 1 ) {
    uint8_t pro_cmd;
    get_amt(&pro_cmd,1);
    switch( pro_cmd ){
    case DSF_CMD_PROLOG_BIG_ENDIAN:
      file_big_endian = true;
      continue;
    case DSF_CMD_PROLOG_LITTLE_ENDIAN:
      file_big_endian = false;
      continue;
    case DSF_CMD_PROLOG_END:
      break;
    default:
      fseek(tr,0,SEEK_SET);
      break;
    };
    break;
  }

#ifdef HOST_LITTLE_ENDIAN
  file_other_endian = file_big_endian;
#else
  file_other_endian = !file_big_endian;
#endif

  return true;
}

void
Dataset_File::reset_position_state()
{
  expect_sync = false;
  decompressing = false;
  skip_next_block = false;
  namespace_level = 0;
  dc_reset_position_state();
}

bool
Dataset_File::peek(DSF_Entry &e)
{
  if( !peeked )
    {
      peeked = true;
      peeked_rv = _get(peeked_e);
    }

  e = peeked_e;
  return peeked_rv;
}

bool
Dataset_File::get(DSF_Entry &e)
{
  if( peeked )
    {
      peeked = false;
      e = peeked_e;
      return peeked_rv;
    }

  return _get(e);
}

bool
Dataset_File::_get(DSF_Entry &e)
{
  DSF_Command &cmd = e.type;
  DSF_Command new_coding = DSF_CMD_EOL;

  while ( 1 ) switch( ({ e.serial = ++entry_serial; get_cmd(cmd);} ) ){

  case DSF_X_EOF:
    return false;

  case DSF_CMD_BLOCK_START:
    {
      int compression_format = get_ui1();
      int32_t csize = get_in4();
      int32_t dsize = get_in4();

      last_block_uncompressed_size = dsize;
      last_block_compressed_size = csize;

      if( skip_next_block )
        {
          fseek(tr,csize,SEEK_CUR);
          skip_next_block = false;
          break;
        }

      if( compression_format != 1 )
        (error_handler)("Compression format unrecognized.");

      void *buffer = malloc(csize);
      if( !buffer )
        (error_handler)("Could not allocate memory for decompress buffer.");
      get_amt(buffer,csize);
      decompress_buffer.load(buffer,csize,dsize);
      free(buffer);
      decompressing = true;
    }
    break;

  case DSF_CMD_CODING_SYNC:
    expect_sync = false;
    sync();
    dc_sync();
    break;

  case DSF_CMD_CODING_ABSOLUTE:
  case DSF_CMD_CODING_PLUS_0:
  case DSF_CMD_CODING_PLUS_1:
  case DSF_CMD_CODING_PLUS_IN1:
  case DSF_CMD_CODING_PLUS_IN2:
  case DSF_CMD_CODING_PLUS_IN3:
    if ( dc_ns->cmd_assumed_positional )
      {
        Difference_Coding_Info* const di = dc_hash_elt_get();
        const bool verify = cmd == DSF_CMD_CODING_ABSOLUTE;
        int64_t v_value = 0;
        if ( verify )
          {
            const int v_item_count = get_ui1();
            const int64_t v_hash_raw = get_in8();
            const int64_t v_l1_hint_key = get_in8();
            const int32_t v_l2_hint_key = get_in4();
            const int64_t v_last_value = get_in8();
            v_value = get_in8();
            ASSERTS( v_item_count == dc_ns->item_count - 1);
            ASSERTS( v_l1_hint_key == dc_ns->hint->l1_hint_key );
            ASSERTS( v_l2_hint_key == dc_ns->hint->l2_hint_key );
            ASSERTS( v_hash_raw == dc_ns->hash_raw );
            ASSERTS( v_last_value == di->last_value );
            get_cmd(cmd);
          }
        
        const DSF_Command coding = cmd;
        cmd = di->last_cmd_get();
        int64_t val;
        switch ( coding ) {
        case DSF_CMD_CODING_PLUS_0:  val = di->last_value;             break;
        case DSF_CMD_CODING_PLUS_1:  val = di->last_value + 1;         break;
        case DSF_CMD_CODING_PLUS_IN1:val = di->last_value + get_in1(); break;
        case DSF_CMD_CODING_PLUS_IN2:val = di->last_value + get_in2(); break;
        default: return (error_handler)("Unknown coding type, %d",coding);}

        ASSERTS( !verify || val == v_value );
        dc_read_idx_none(di,val);

        switch ( cmd ) {
        case DSF_CMD_IN1: case DSF_CMD_UI1: e.data.i1 = val; break;
        case DSF_CMD_IN2: case DSF_CMD_UI2: e.data.i2 = val; break;
        case DSF_CMD_IN4: case DSF_CMD_UI4: e.data.i4 = val; break;
        case DSF_CMD_IN8: case DSF_CMD_DBL: e.data.i8 = val; break;
        default: return (error_handler)("Unknown type, %d",cmd);}

        return true;
      }
    else
      {
        new_coding = cmd;
        break;
      }

  case DSF_CMD_NAMESPACE_END:
    dc_hash_at_ns_end();
    namespace_level--;
    return true;

  case DSF_CMD_NAMESPACE_IDX_BEGIN:
    namespace_level++;
    auto_assign.reset();
    get_in4(e.key);
    dc_hash_at_ns_begin(e.key.i4);
    if ( e.key.i4 == DSF_NS_IDX_DC_NS_Hints ) { dc_hints_read(); break; }
    if( e.key.i4 != DSF_NS_IDX_File_Format ) return true;
    auto_assign.put(DSF_NS_IDX_FF_Version_Major,&file_version_major);
    auto_assign.put(DSF_NS_IDX_FF_Version_Minor,&file_version_minor);
    auto_assign.put(DSF_NS_IDX_FF_Version_Micro,&file_version_micro);
    dc_auto_assign_prepare();
    auto_assign.scan();
    dc_setup_for_file_version();
    if( file_version_major != version_major
        || file_version_minor != version_minor )
      (error_handler)("Cannot handle file version %d.%d.%d.",
                      file_version_major, file_version_minor,
                      file_version_micro);
    break;

  case DSF_CMD_NAMESPACE_STR_BEGIN:
    namespace_level++;
    auto_assign.reset();
    e.key.s.assign_take(get_str_dup());
    dc_hash_at_ns_begin(e.key.s);
    return true;

  case DSF_CMD_STR_STR:
    e.key.s.assign_take(get_str_dup());
    e.data.s.assign_take(get_str_dup());
    if( auto_assign.check_str(e) ) break;
    return true;

#define CASE_STR_XXX(type,get) \
  case type: \
    e.key.s.assign_take(get_str_dup());  get(e.data); \
    if( auto_assign.check_str(e) ) break; else return true

 CASE_STR_XXX(DSF_CMD_STR_IN8,get_in8);
 CASE_STR_XXX(DSF_CMD_STR_IN4,get_in4);
 CASE_STR_XXX(DSF_CMD_STR_IN2,get_in2);
 CASE_STR_XXX(DSF_CMD_STR_IN1,get_in1);
 CASE_STR_XXX(DSF_CMD_STR_UI1,get_ui1);
 CASE_STR_XXX(DSF_CMD_STR_UI2,get_ui2);
 CASE_STR_XXX(DSF_CMD_STR_UI4,get_ui4);
 CASE_STR_XXX(DSF_CMD_STR_DBL,get_dbl);

  case DSF_CMD_IDX_STR:
    get_in4(e.key);
    e.data.s.assign_take(get_str_dup());
    if( auto_assign.check_idx(e) )break;
    return true;

  case DSF_CMD_STR:
    e.data.s.assign_take(get_str_dup());
    return true;

#define CASE_IDX_XXX(type,get,mem)                                            \
  case DSF_CMD_##type:                                                        \
    {get(e.data); dc_read_idx_none(DSF_CMD_##type,e.data.mem); return true;}  \
  case DSF_CMD_IDX_##type:                                                    \
    get_in4(e.key);  get(e.data);                                             \
    if( auto_assign.check_idx(e) ) break; else return true

  CASE_IDX_XXX(UI1,get_ui1,u1);
  CASE_IDX_XXX(IN1,get_in1,i1);
  CASE_IDX_XXX(UI2,get_ui2,u2);
  CASE_IDX_XXX(IN2,get_in2,i2);
  CASE_IDX_XXX(UI4,get_ui4,u4);
  CASE_IDX_XXX(IN4,get_in4,i4);
  CASE_IDX_XXX(IN8,get_in8,i8);
  CASE_IDX_XXX(DBL,get_dbl,d);

  case DSF_CMD_DEF_PRIMITIVE_STR:
    {
      int user_cmd = get_ui1();
      DSF_Command user_type_type = get_cmd();
      char *user_cmd_name = get_str_dup();
      e.key.s = user_cmd_name;
      e.data.c = user_cmd;
      e.def_type = user_type_type;
      if( user_cmds[user_cmd].primitive_type )
        return (error_handler)("User type %d, %s already defined.",
                               user_cmd, user_cmd_name);
      if( user_type_type != DSF_CMD_IDX_IN4
          && user_type_type != DSF_CMD_IDX_IN8
          && user_type_type != DSF_CMD_IDX_STR )
        return (error_handler)
          ("Cannot handle type. (cmd %d, type %d, name %s)",
           user_cmd, user_type_type, user_cmd_name);
      user_cmds[user_cmd].primitive_type = user_type_type;
      user_cmds[user_cmd].key = user_cmd_name;
    }
    return true;

  default:
    {
      if( cmd > 255 ) return (error_handler)("Internal Error");
      DSF_User_Type &ut = user_cmds[cmd];
      if( ut.primitive_type == DSF_CMD_EOL )
        return (error_handler)("Uninitialized type, %d.",cmd);
      if( expect_sync )
        return (error_handler)("Sync expected before data.", cmd);

      dc_ns->cmd_assumed_positional = false;

      if( new_coding ) ut.coding = new_coding;

      switch( ut.primitive_type ){

      case DSF_CMD_IDX_IN4:
        switch( ut.coding ){
        case DSF_CMD_CODING_ABSOLUTE:e.data.i4 = get_in4();              break;
        case DSF_CMD_CODING_PLUS_0:  e.data.i4 = ut.last.i4;             break;
        case DSF_CMD_CODING_PLUS_1:  e.data.i4 = ut.last.i4 + 1;         break;
        case DSF_CMD_CODING_PLUS_IN1:e.data.i4 = ut.last.i4 + get_in1(); break;
        case DSF_CMD_CODING_PLUS_IN2:e.data.i4 = ut.last.i4 + get_in2(); break;
        default: return (error_handler)("Unknown coding type, %d",ut.coding);
        }
        ut.last.i4 = e.data.i4;
        break;

      case DSF_CMD_IDX_IN8:
        switch( ut.coding ){
        case DSF_CMD_CODING_ABSOLUTE:e.data.i8 = get_in8();              break;
        case DSF_CMD_CODING_PLUS_0:  e.data.i8 = ut.last.i8;             break;
        case DSF_CMD_CODING_PLUS_1:  e.data.i8 = ut.last.i8 + 1;         break;
        case DSF_CMD_CODING_PLUS_IN1:e.data.i8 = ut.last.i8 + get_in1(); break;
        case DSF_CMD_CODING_PLUS_IN2:e.data.i8 = ut.last.i8 + get_in2(); break;
        default: return (error_handler)("Unknown coding type, %d",ut.coding);
        }
        ut.last.i8 = e.data.i8;
        break;

      case DSF_CMD_IDX_STR:
        e.data.s.assign_take(get_str_dup());
        break;

      default:
        return (error_handler)("Unknown primitive type.");
      }

      return true;
    }
  }
}

char *
Dataset_File::get_str_dup(bool tight)
{
  int size = 8;
  char *storage = (char*) malloc(size);
  int i = 0;

  while( 1 )
    {
      int c = get_ui1();
      if( eof() || c == 0 ) break;
      if( i + 1 == size ) storage = (char*) realloc(storage, size <<= 1 );
      storage[i++] = c;

    }
  storage[i++] = 0;
  return tight ? (char*) realloc(storage,i) : storage;
}

int64_t
Dataset_File::expect_int()
{
  DSF_Entry e;
  return expect_int(e);
}

int64_t
Dataset_File::expect_int(DSF_Entry& e)
{
  if ( !get(e) ) (error_handler)("Unexpected end of file, expected an int.\n");
  if ( !cmd_is_int(e.type) ) (error_handler)("Expected an integer.\n");
  return e.data_get_int();
}

void
Dataset_File::expect_ns_end()
{
  DSF_Entry e;
  if ( !get(e) ) (error_handler)("Expected namespace end\n");
  if ( e.type != DSF_CMD_NAMESPACE_END )
    (error_handler)("Expected namespace end.\n");
}

void
Dataset_File::sync()
{
  for ( int i=0; i<256; i++ )
    {
      DSF_User_Type* const ut = &user_cmds[i];
      ut->coding = DSF_CMD_CODING_ABSOLUTE;
      ut->last.i8 = 0; ut->last.i4 = 0; ut->last.i2 = 0; ut->last.i1 = 0;
    }
}

void
Dataset_File::dc_init()
{
  dc_f_uses_dc = false;
  dc_cookie = 0;
  dc_info = NULL;
  dc_ns_hint_default.l1_hint_key = -1;
  dc_ns_hint_default.l2_hint_key = -1;
  dc_ns_hint_default.corr_pos_vec = 1 << 1;
  dc_ns_hint_default.corr_user_cmd = -1;
  dc_reset_position_state();
}

void
Dataset_File::dc_reset_position_state()
{
  dc_compress_off_above_ns_level = 0;
  dc_compress_allowed_here = false;
  dc_ns_stack.reset();
  dc_hash_at_ns_begin_common(&dc_ns_hint_default,true);
  dc_ns->hash_raw = 0;
}

void
Dataset_File::dc_setup_for_file_version()
{
  dc_f_uses_dc = dc_f_version;
  if ( !dc_f_uses_dc ) return;
  dc_hash_size = 1 << dc_hash_bits;
  dc_hash_mask = dc_hash_size - 1;
  dc_info = (Difference_Coding_Info*)malloc(dc_hash_size*sizeof(*dc_info));
  for ( int i=0; i<dc_hash_size; i++ )
    new (&dc_info[i]) Difference_Coding_Info(this);
}

void
Dataset_File::dc_hints_read()
{
  DSF_Entry e;
  if ( !get(e) || e.type == DSF_CMD_NAMESPACE_END ) return;
  if ( e.type != DSF_CMD_STR ) (error_handler)("Badly formed hint.");
  RString hint_type(e.data.s);
  if ( hint_type == "name-l2_idx-user_cmd" || hint_type == "name-l2_idx-pos" )
    {
      static int serial = 0;
      serial++;
      DSF_Entry name; get(name);
      DSF_Entry l2,pc;
      const int l2_idx = expect_int(l2);
      const int pos_or_cmd = expect_int(pc);
      expect_ns_end();
      if ( hint_type == "name-l2_idx-user_cmd" )
        if ( name.type == DSF_CMD_STR )
          dc_namespace_correlation_hint_user_cmd
            (name.data.s.str(),l2_idx,pos_or_cmd);
        else
          dc_namespace_correlation_hint_user_cmd
            (name.data_get_int(),l2_idx,pos_or_cmd);
      else
        if ( name.type == DSF_CMD_STR )
          dc_namespace_correlation_hint_pos
            (name.data.s.str(),l2_idx,pos_or_cmd);
        else
          dc_namespace_correlation_hint_pos
            (name.data_get_int(),l2_idx,pos_or_cmd);
    }
  else
    (error_handler)("Badly formed hint.");
}

void
Dataset_File::dc_sync()
{
  dc_cookie++;
  if ( !dc_f_uses_dc ) return;
  dc_compress_off_above_ns_level = namespace_level;
  dc_compress_allowed_here = true;
}
