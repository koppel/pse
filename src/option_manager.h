/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2010 Louisiana State University

// $Id$

// Check and process command-line options.


#ifndef OPTION_MANAGER_H
#define OPTION_MANAGER_H

#include "string_hash.h"
#include "misc.h"

///
/// Option_Manager
///
//
// Check and process command-line options.
//
 /// Option_Manager::insert
// (const char *long_name, char short_name, char **val_ptr, int *occurrences)
//
// Tell option manager to look for a command-line option long_name or
// short_name when processing the command line (see example below for
// format). If val_ptr is non-NULL then the command-line option is
// expected to have an argument, write a pointer to that argument to
// *val_ptr. If the option appears multiple times then val_ptr points to
// the last value. If occurrences non-NULL, write *occurrences with the
// number of times the command line option appeared. Note that command
// line parsing occurs when Option_Manager::process it called, which
// should be after all calls to Option_Manager::insert are
// made. Execution will terminate immediately if long_name or short_name
// had been used previously. If long_name is NULL then only the
// short_name is used.
//
// Example:
//
//   app->option_manager.insert("variable-get",'v',&var_name,&count);
//
//   Tell Option_Manager to look for options in any of the forms:
//
//     pse -v something
//     pse -vsomething
//     pse --variable-get something
//     pse --variable-get=something

//   If such an option is found var_name will be written with a pointer
//   to "something" (regardless of which of the four styles above was
//   used) and count will be set to the number of times it appeared (1
//   for each of the four examples above).
//
 /// void Option_Manager::process(int argv, char **argc)
//
// Look at argv/argc (expected to be in the format passed to main) and
// set variables registered using Option_Manager::insert. Exit with a
// fatal error if an unexpected option appears, if an expected value did
// not appear or if an unexpected value does appear.


class Option_Manager
{
 public:
  struct Option_Info {
    RString long_name;
    char  short_name, **val_ptr;
    int *occurrences;
  };
  Option_Manager();
  void insert
  (const char *long_name, char short_name, char **val, int *occurrences = NULL);
  void process(int argv, char **argc);

  PStack<char*> orphans;
 private:
  PString_Hash<Option_Info*> option_hash;
};

#endif
