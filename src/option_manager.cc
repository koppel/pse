/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2010 Louisiana State University

// $Id$

// Check and process command-line options.


#include "option_manager.h"
#include "misc.h"

Option_Manager::Option_Manager(){}

void
Option_Manager::insert
(const char *long_name, char short_name, char **val, int *occurrences)
{
  Option_Info* const opt_info = new Option_Info;
  RStringF short_opt("-%c",short_name);
  opt_info->long_name = long_name;
  opt_info->short_name = short_name;
  opt_info->occurrences = occurrences;
  if( occurrences ) *opt_info->occurrences = 0;
  opt_info->val_ptr = val;
  if( long_name )
    {
      RStringF long_opt("--%s",long_name);
      ASSERTA( !option_hash.present(long_opt) );
      option_hash[long_opt] = opt_info;
    }
  ASSERTA( !option_hash.present(short_opt) );
  option_hash[short_opt] = opt_info;
}

void
Option_Manager::process(int argv, char **argc)
{
  PQueue<char*> unknown;
  for(int i=1; i<argv; i++)
    {
      char* const arg = argc[i];
      if( !arg[0] ) continue;
      if( arg[0] != '-' ) {orphans += arg; continue;}
      Option_Info *arg_info = NULL;
      char *val = NULL;
      if( arg[1] != '-' )
        {
          const char c = arg[2];
          arg[2] = 0;
          const bool present = option_hash.lookup(arg,arg_info);
          arg[2] = c;
          if( !present ) {unknown += arg; continue;}
          if( arg[2] ) val = &arg[2];
        }
      else
        {
          PSplit pieces(arg, '=', 2);
          char* const option = pieces;
          char* const val_maybe = pieces;
          if( val_maybe ) val = strdup(val_maybe);
          const bool present = option_hash.lookup(option,arg_info);
          if( !present ) {unknown += arg; continue;}
        }
      if( arg_info->val_ptr )
        {
          if( !val )
            {
              if( argv > i + 1 )
                val = argc[++i];
              else
                fatal("Missing argument for option %s\n",arg);
            }
          arg_info->val_ptr[0] = val;
        }
      else if( val )
        {
          fatal("Did not expect value for option %s\n",arg);
        }
      if( arg_info->occurrences ) arg_info->occurrences[0]++;
    }
  if( !unknown ) return;
  RStringF msg("Unexpected command-line switch%s: ",
               unknown.occ() > 1 ? "es" : "");
  while( unknown ) msg.sprintf("%s ",unknown.dequeue());
  fatal("%s\n",msg.s);
}
