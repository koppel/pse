/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2005 Louisiana State University

// $Id$

// Verify the version of the gtk headers and libraries that are found.


#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

int 
main(int argc, char **argv)
{
  bool errors = false;

  if( argc != 4 )
    {
      fprintf(stderr, "Usage of PSE GTK Version Checker:\n"
                      "%s <required_major> <required_minor> <required_micro>\n",
                      argv[0]);
      return 1;
    }

  gtk_init(&argc, &argv);
  
  const int major = atoi(argv[1]);
  const int minor = atoi(argv[2]);
  const int micro = atoi(argv[3]);

  if( gtk_check_version(major, minor, micro) != 0 )
    {
      fprintf(stderr, "*** Build Error: The version of the GTK libraries found "
                      "is not compatible:\n"
                      "GTK libraries found %d.%d.%d, need at least %d.%d.%d.\n",
                      gtk_major_version, gtk_minor_version, gtk_micro_version,
                      major, minor, micro);
      errors = true;
    }

  if( GTK_CHECK_VERSION(major, minor, micro) == 0 )
    {
      fprintf(stderr, "*** Build Error: The version of the GTK header files "
                      "found is not compatible:\n"
                      "GTK header files found %d.%d.%d, "
                      "need at least %d.%d.%d.\n",
                      GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION,
                      major, minor, micro);
      errors = true;
    }

  return errors ? 1 : 0;
}
