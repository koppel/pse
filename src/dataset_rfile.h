/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2010 Louisiana State University

// $Id$

// Low-Level Dataset File Access Routines
//
// Dataset_File:      Performs low-level access.
// Decompress_Buffer: Used for buffering data for decompression.
// Auto_Assign:       Used by higher-level code to grab values from dataset file.

#ifndef DATASET_RFILE_H
#define DATASET_RFILE_H

#include "config.h"
#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <malloc.h>
#include <string.h>
#include "dataset_ftypes.h"
#include "misc.h"

class Dataset_File;

bool dataset_rfile_default_error_handler(const char *fmt, ...);

inline double BD(double d)
{
  union { int64_t i; double d; } before, after;
  before.d = d;
  after.i = B8(before.i);
  return after.d;
}
inline float BS(float f)
{
  union { int32_t i; float f; } before, after;
  before.f = f;
  after.i = B4(before.i);
  return after.f;
}

typedef bool (*DSF_Error_Function)(const char *fmt, ...);

class DSF_String {
public:
  DSF_String():s(NULL){};
  ~DSF_String(){ if(s)free(s); }
  inline bool operator == (DSF_String &s2){ return !strcmp(s,s2.s); }
  inline bool operator == (const char *s2){ return !strcmp(s,s2); }
  inline bool operator != (DSF_String &s2){ return strcmp(s,s2.s); }
  inline bool operator != (const char *s2){ return strcmp(s,s2); }
  operator char*(){ return s; }
  char* str(){ return s; }
  char* assign_take(char *s2)
  {
    if( s ) free( s );
    s = s2;
    return s;
  }
  char* operator = (DSF_String &s2)
  { 
    return operator =(s2.s);
  }
  char* operator = (const char *s2)
  {
    if( s ) free( s );
    s = s2 ? strdup(s2) : NULL;
    return s;
  }
  inline bool operator ! (){ return !s; }
  char* dup() { return s ? strdup(s) : NULL; }
private:
  char *s;
};

typedef struct
{
  DSF_String s;
  union {
    char c;
    int8_t i1;
    uint8_t u1;
  };
  union {
    int16_t i2;
    uint16_t u2;
  };
  union {
    int32_t i4;
    uint32_t u4;
    float f;
  };
  union {
    int64_t i8;
    double d;
  };
  int64_t get_int(DSF_Command type)
  {
    switch ( type ) {
    case DSF_CMD_IDX_IN1: return i1;
    case DSF_CMD_IDX_IN2: return i2;
    case DSF_CMD_IDX_IN4: return i4;
    case DSF_CMD_IDX_IN8: return i8;
    case DSF_CMD_IDX_UI1: return u1;
    case DSF_CMD_IDX_UI2: return u2;
    case DSF_CMD_IDX_UI4: return u4;
    case DSF_CMD_IDX_DBL: return i8; // Really?

    case DSF_CMD_STR_IN1: return i1;
    case DSF_CMD_STR_IN2: return i2;
    case DSF_CMD_STR_IN4: return i4;
    case DSF_CMD_STR_IN8: return i8;
    case DSF_CMD_STR_UI1: return u1;
    case DSF_CMD_STR_UI2: return u2;
    case DSF_CMD_STR_UI4: return u4;
    case DSF_CMD_STR_DBL: return i8; // Really?

    case DSF_CMD_IN1: return i1;
    case DSF_CMD_IN2: return i2;
    case DSF_CMD_IN4: return i4;
    case DSF_CMD_IN8: return i8;
    case DSF_CMD_UI1: return u1;
    case DSF_CMD_UI2: return u2;
    case DSF_CMD_UI4: return u4;
    case DSF_CMD_DBL: return i8; // Really?

    default: return -1;
    }
  };
} DSF_Primitive_Types;

inline bool 
cmd_is_int(DSF_Command type)
{
  switch ( type ) {
  case DSF_CMD_IDX_IN1:
  case DSF_CMD_IDX_IN2:
  case DSF_CMD_IDX_IN4:
  case DSF_CMD_IDX_IN8:
  case DSF_CMD_IDX_UI1:
  case DSF_CMD_IDX_UI2:
  case DSF_CMD_IDX_UI4:

  case DSF_CMD_STR_IN1:
  case DSF_CMD_STR_IN2:
  case DSF_CMD_STR_IN4:
  case DSF_CMD_STR_IN8:
  case DSF_CMD_STR_UI1:
  case DSF_CMD_STR_UI2:
  case DSF_CMD_STR_UI4:

  case DSF_CMD_IN1:
  case DSF_CMD_IN2:
  case DSF_CMD_IN4:
  case DSF_CMD_IN8:
  case DSF_CMD_UI1:
  case DSF_CMD_UI2:
  case DSF_CMD_UI4:

return true;
  default: return false;
  }
}

struct DSF_Entry
{
  int64_t serial;
  DSF_Command type;
  DSF_Primitive_Types key, data;
  int64_t data_get_int() { return data.get_int(type); }
  char def_type;
};

struct DSF_User_Type
{
  DSF_Command primitive_type;
  DSF_Command coding;
  DSF_Primitive_Types last;
  int64_t last_get_int() { return last.get_int(primitive_type); }
  char *key;
};

// ----------------------------------------------------------------------------

class DSF_Decompress_Buffer
{
public:
  DSF_Decompress_Buffer()
  {
    buffer = NULL;  pos = 0;  size = 0;
    error_handler = dataset_rfile_default_error_handler;
  }

  void load(void *ptr, int size_compressed, int size_uncompressed);

  inline bool read(void *ptrp, int amt)
  {
    char *ptr = (char*) ptrp;
    char *b = &buffer[pos];
    pos += amt;

    switch ( amt ){
    case 1: *ptr = *b++;  break;
    case 2: *ptr++ = *b++;  *ptr = *b++;  break;
    case 4: *ptr++ = *b++;  *ptr++ = *b++; *ptr++ = *b++;  *ptr = *b++;  break;
    case 8: *ptr++ = *b++;  *ptr++ = *b++; *ptr++ = *b++;  *ptr++ = *b++;
            *ptr++ = *b++;  *ptr++ = *b++; *ptr++ = *b++;  *ptr = *b++;  break;
    default: memcpy(ptr,b,amt);  break;
    }

    return pos < size;
  }

  DSF_Error_Function error_handler;
  char *buffer;
  int pos, size;

};


struct DSF_Auto_Assign
{
  ~DSF_Auto_Assign();
  void reset();
  void put(const char *key, int64_t *v, int64_t def) { put(key,v); *v = def; }
  void put(const char *key, int64_t *v);
  void put(const char *key, int32_t *v, int32_t def) { put(key,v); *v = def; }
  void put(const char *key, int32_t *v);
  void put(const char *key, char **v, char *def) { put(key,v); *v = def; }
  void put(const char *key, char **v);
  void put(int idx, int32_t *vptr);
  void scan(){DSF_Entry e; scan(e); }
  void scan(DSF_Entry &e, DSF_Command limit = DSF_CMD_NAMESPACE_END);
#if 0
  // Uncomment when ready to test.
  int not_found_count();
  int multiple_find_count();
#endif
  bool each_found_once();
  friend class Dataset_File;

private:
  struct DSF_NS_Get_Item {
    int idx;
    DSF_String key;
    DSF_Command type;
    int encounters;
    union {
      char **c;
      int8_t *i1;
      int32_t *i4;
      int64_t *i8;
    } ptr;
  } *items, *items_str;

  DSF_Auto_Assign();
  template <class T> void putt(const char *key, T *vptr);
  void assign_var(DSF_NS_Get_Item &item, DSF_Entry &e);
  void assure_space(DSF_NS_Get_Item* &item, int &size, int &occ);
  bool check_idx(DSF_Entry &e);
  bool check_str(DSF_Entry &e);

  Dataset_File *df;
  int size, occ, size_str, occ_str;
  int level;
};

// ----------------------------------------------------------------------------

inline int64_t pun_dtoi64(double d)
{union { double d; int64_t i; } v; v.d = d; return v.i;}

struct DC_NS_Hint {
  uint32_t corr_pos_vec;
  int corr_user_cmd;
  int64_t l1_hint_key;
  int32_t l2_hint_key;
  PSList<DC_NS_Hint*,int32_t> hint_table_l2;
};

struct DC_NS_State {
  DC_NS_State(){};
  int hash_raw;
  int item_count;
  bool cmd_assumed_positional;
  DC_NS_Hint *hint;
};


class Dataset_File
{
  friend struct DSF_Auto_Assign;
 public:
  static const int version_major = 0;  // File version that can be handled.
  static const int version_minor = 1;
  static const int version_micro = 0;
  int file_version_major, file_version_minor, file_version_micro;
  bool file_big_endian, file_other_endian;

  Dataset_File()
  {
    error_handler = dataset_rfile_default_error_handler;
    auto_assign.df = this;
    peeked = false;
    entry_serial = 0;
    dc_init();
  }

  bool init(char *file_name);
  void sync(); // Reset difference coding state.
  bool reopen();
  void reset_position_state();
  void close(){ ::fclose(tr); }

  bool peek(DSF_Entry &e);
  bool get(DSF_Entry &e);

  int64_t expect_int(DSF_Entry& e);
  int64_t expect_int();
  void expect_ns_end();
  void namespace_finish()
  {
    int level = 1;
    for( DSF_Entry e; get(e); )
      if( e.type == DSF_CMD_NAMESPACE_END && level-- == 1 ) return;
      else if( e.type == DSF_CMD_NAMESPACE_STR_BEGIN
               || e.type == DSF_CMD_NAMESPACE_IDX_BEGIN ) level++;
  }
  void skip_block(){ skip_next_block = true; }

  inline bool eof(){ return feof(tr); }
  long tell(){
    if( decompressing )
      (error_handler)("Cannot provide file position within compressed region.");
    return ::ftell(tr);
  }
  void seek(off_t pos){
    decompressing = false;
    ::fseek(tr,pos,SEEK_SET);
    expect_sync = pos != 0;
  }

  void set_error_handler(DSF_Error_Function err)
  { error_handler = err; decompress_buffer.error_handler = err; }

  DSF_Auto_Assign auto_assign;

  int last_block_uncompressed_size;
  int last_block_compressed_size;
  int namespace_level;

 private:
  int64_t entry_serial;
  bool peeked, peeked_rv;
  DSF_Entry peeked_e;
  bool _get(DSF_Entry &e);
  void get_amt(void *ptr, int amt)
  {
    if( decompressing )
      {
        decompressing = decompress_buffer.read(ptr,amt);
        if( !decompressing )
          {
            if(decompress_buffer.pos > decompress_buffer.size )
              (error_handler)("Wrong amount read from decompression buffer.");
          }
        return;
      }
    fread(ptr,amt,1,tr);
  }

  inline void bo(int16_t &i) {if( file_other_endian ) i = B2(i);}
  inline void bo(uint16_t &i) {if( file_other_endian ) i = B2(i);}
  inline void bo(int32_t &i) {if( file_other_endian ) i = B4(i);}
  inline void bo(uint32_t &i) {if( file_other_endian ) i = B4(i);}
  inline void bo(int64_t &i) {if( file_other_endian ) i = B8(i);}
  inline void bo(double &i) {if( file_other_endian ) i = BD(i);}
  inline void bo(float &i) {if( file_other_endian ) i = BS(i);}

  inline int8_t  get_in1() {int8_t sh;   get_amt(&sh,1);  return sh;}
  inline bool get_in1(DSF_Primitive_Types &v)
  { v.i1 = get_in1();  return !eof();}

  inline int16_t get_in2() {int16_t sh;  get_amt(&sh,2);  bo(sh); return sh;}
  inline bool get_in2(DSF_Primitive_Types &v)
  { v.i2 = get_in2();  return !eof();}

  inline int32_t get_in4() {int32_t sh;  get_amt(&sh,4);  bo(sh); return sh;}
  inline bool get_in4(DSF_Primitive_Types &v)
  { v.i4 = get_in4();  return !eof();}

  inline uint8_t get_ui1() {uint8_t sh;  get_amt(&sh,1); return sh;}
  inline bool get_ui1(DSF_Primitive_Types &v)
  { v.u1 = get_ui1();  return !eof();}

  inline uint16_t  get_ui2() {uint16_t sh; get_amt(&sh,2);bo(sh);return sh;}
  inline bool get_ui2(DSF_Primitive_Types &v)
  { v.u2 = get_ui2();  return !eof();}

  inline uint32_t  get_ui4() {uint32_t sh; get_amt(&sh,4);bo(sh);return sh;}
  inline bool get_ui4(DSF_Primitive_Types &v)
  { v.u4 = get_ui4();  return !eof();}

  inline bool get_dbl(double &d){ get_amt(&d,8); bo(d); return !eof(); }
  inline bool get_dbl(DSF_Primitive_Types &v) { return get_dbl(v.d); }
  inline double get_dbl() { double d; get_dbl(d); return d; }

  inline bool get_in8(int64_t &sh) {get_amt(&sh,8);  bo(sh); return !eof();}
  inline bool get_in8(DSF_Primitive_Types &v) { return get_in8(v.i8); }
  inline int64_t get_in8(){ int64_t i; get_in8(i); return i; }

  inline DSF_Command get_cmd(DSF_Command &cmd) { return cmd = get_cmd(); }
  inline DSF_Command get_cmd()
  {unsigned char c; get_amt(&c,1); return eof() ? DSF_X_EOF : DSF_Command(c);}

  // Read null-terminated string from file into newly allocated storage.
  // If tight = 0 storage may exceed string length + 1, but will be quicker.
  char *get_str_dup(bool tight = true);

  DSF_Error_Function error_handler;

  DSF_Decompress_Buffer decompress_buffer;
  bool decompressing;

  DSF_User_Type user_cmds[256];
  bool expect_sync;
  bool skip_next_block;
  char *file_name;
  size_t flen;
  FILE *tr;

  struct Difference_Coding_Info {
    Difference_Coding_Info():ds_file(NULL){}
    Difference_Coding_Info(Dataset_File *ds_file):ds_file(ds_file)
    { cookie = ds_file->dc_cookie; }
    void cmd_set(DSF_Command cmd)
    { cookie = ds_file->dc_cookie; last_cmd = cmd; }
    void value_set(int64_t data) { last_value = data; }
    DSF_Command last_cmd_get()
    { ASSERTS( ds_file->dc_cookie == cookie );  return last_cmd; }

    Dataset_File* const ds_file;
    int64_t last_value;
  private:
    int cookie;
    DSF_Command last_cmd;
  } *dc_info;

  void dc_init();
  void dc_reset_position_state();

  void dc_hints_read();

  void dc_auto_assign_prepare()
  {
    auto_assign.put("dc-version",&dc_f_version,0);
    auto_assign.put("dc-hash-table-bits",&dc_hash_bits,0);
  }

  void dc_setup_for_file_version();

private:
  template<typename T> bool dc_namespace_correlation_hint_pos
  (T name, int32_t l2_idx, int pos)
  {
    DC_NS_Hint* const h1 = dc_ns_hint_get_l1(name);
    if ( !h1 ) return false;
    DC_NS_Hint* const h2 = dc_ns_hint_get_l2(h1,l2_idx);
    const int32_t mask = 1 << pos;
    h2->corr_pos_vec |= mask;
    return true;
  }

  template<typename T> bool dc_namespace_correlation_hint_user_cmd
  (T name, int32_t l2_idx, int user_cmd)
  {
    DC_NS_Hint* const h1 = dc_ns_hint_get_l1(name);
    if ( !h1 ) return false;
    DC_NS_Hint* const h2 = dc_ns_hint_get_l2(h1,l2_idx);
    ASSERTA( user_cmd == -1 || h2->corr_user_cmd == -1 );
    h2->corr_user_cmd = user_cmd;
    return true;
  }

  template<typename T>
  DC_NS_Hint* dc_ns_hint_get_l1(T name)
  {
    const int64_t key = dc_ns_hint_make_key(name);
    if ( !key ) return NULL;
    DC_NS_Hint*& h = *dc_ns_hint_table_l1.geti(key);
    if ( !dc_ns_hint_table_l1.new_item() ) return h;
    h = new DC_NS_Hint;
    h->l1_hint_key = key;
    h->l2_hint_key = -1;
    h->corr_pos_vec = 1 << 1;
    h->corr_user_cmd = -1;
    return h;
  }

  DC_NS_Hint* dc_ns_hint_get_l2(DC_NS_Hint* h1, int32_t key)
  {
    DC_NS_Hint*& h = *h1->hint_table_l2.geti(key);
    if ( !h1->hint_table_l2.new_item() ) return h;
    h = new DC_NS_Hint;
    h->l1_hint_key = h1->l1_hint_key;
    h->l2_hint_key = key;
    h->corr_pos_vec = 1 << 1;
    h->corr_user_cmd = -1;
    return h;
  }

  int64_t dc_ns_hint_make_key(const char *name)
  {
    if ( !name[0] || name[1] ) return 0;
    return name[0] << 4 | 1;
  }
  int64_t dc_ns_hint_make_key(int32_t idx)
  {
    const int64_t key = idx << 4 | 2;
    return idx == key >> 4 ? key : 0;
  }

  template<typename T>
  DC_NS_Hint* dc_ns_hint_lookup(T idx)
  {
    if ( const int64_t key = dc_ns_hint_make_key(idx) )
      return dc_ns_hint_table_l1.get_eq(key) ?: &dc_ns_hint_default;
    return &dc_ns_hint_default;
  }

  void dc_ns_hint_lookup_l2(int64_t data)
  {
    if ( !dc_ns->hint->hint_table_l2.occ() ) return;
    DC_NS_Hint* const h2 = dc_ns->hint->hint_table_l2.get_eq(data);
    if ( !h2 ) return;
    dc_ns->hint = h2;
  }

  void dc_sync();
  void dc_hash_at_ns_begin(const char *name)
  {
    if ( !dc_f_uses_dc ) return;
    DC_NS_Hint* const h = dc_ns_hint_lookup(name);
    dc_hash_at_ns_begin_common(h);
    dc_ns->hash_raw = name[0];
  }
  void dc_hash_at_ns_begin(int idx)
  {
    if ( !dc_f_uses_dc ) return;
    DC_NS_Hint* const h = dc_ns_hint_lookup(idx);
    dc_hash_at_ns_begin_common(h);
    dc_ns->hash_raw = idx;
  }
  void dc_hash_at_ns_begin_common( DC_NS_Hint *h, bool init = false )
  {
    if ( !init && !dc_f_uses_dc ) return;
    ASSERTS( init || dc_ns_stack.occ() == namespace_level );
    dc_ns = dc_ns_stack.pushi();
    dc_ns->hint = h;
    dc_ns->cmd_assumed_positional = dc_compress_allowed_here;
    dc_ns->item_count = 0;
  }

  Difference_Coding_Info* dc_hash_elt_get()
  {
    dc_ns->item_count++;
    const int dc_hash = dc_hash_mask & ( dc_ns->hash_raw + dc_ns->item_count );
    return &dc_info[dc_hash];
  }

  void dc_read_idx_none(DSF_Command cmd, double data)
  { dc_read_idx_none(cmd,pun_dtoi64(data)); }

  template<typename T>
  void dc_read_idx_none(DSF_Command cmd, T data_raw)
  {
    if ( !dc_f_uses_dc ) return;
    Difference_Coding_Info* const di = dc_hash_elt_get();
    di->cmd_set(cmd);
    dc_read_idx_none(di,int64_t(data_raw));
  }

  void dc_read_idx_none
  (Difference_Coding_Info *di, int64_t data)
  {
    di->value_set(data);

    const int item_mask = 1 << dc_ns->item_count;
    const bool hash = dc_ns->item_count < 30
      && dc_ns->hint->corr_pos_vec & item_mask;

    if ( hash ) dc_ns->hash_raw = dc_ns->hash_raw ^ data ^ data << 4;

    if ( dc_ns->item_count != 1 ) return;

    dc_ns_hint_lookup_l2(data);
    if ( dc_ns->hint->corr_user_cmd >= 0 )
      {
        DSF_User_Type* const ut = &user_cmds[dc_ns->hint->corr_user_cmd];
        const int64_t last_value = ut->last_get_int();
        dc_ns->hash_raw ^= last_value << 6 ^ last_value << 4 ^ last_value;
      }
  }

  void dc_hash_at_ns_end()
  {
    // Called before namespace decremented.
    if ( !dc_f_uses_dc ) return;
    ASSERTS( dc_ns_stack.occ() == namespace_level + 1 );
    dc_ns_stack.pop();
    dc_ns = &dc_ns_stack.peek();
    if ( !dc_compress_allowed_here
         || namespace_level > dc_compress_off_above_ns_level ) return;
    dc_compress_allowed_here = false;
  }

  int dc_f_version;
  bool dc_f_uses_dc;
  int dc_compress_off_above_ns_level;
  bool dc_compress_allowed_here;
  int dc_cookie;
  int dc_hash_bits, dc_hash_size, dc_hash_mask;
  DC_NS_Hint dc_ns_hint_default;
  PSList<DC_NS_Hint*,int64_t> dc_ns_hint_table_l1;
  PStack<DC_NS_State> dc_ns_stack;
  DC_NS_State *dc_ns;
};

#endif
