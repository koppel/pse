/* Parse table generated from Sun Shade IHASH.c using PSE's parse_ihash. */

 int parse_table[] = {

    /* Field: op,  MSB: 31,  LSB: 30  */
    0, 30,  /*  Left shift, Right Shift.  */
    /*  op = 00  */  140,
    /*  op = 01  */  -133,  /*  IH_CALL  */
    /*  op = 10  */  740,
    /*  op = 11  */  6,
    /* Field: op3,  MSB: 24,  LSB: 19  */
    7, 26,  /*  Left shift, Right Shift.  */
    /*  op3 = 000000  */  -491,  /*  IH_LDUW  */
    /*  op3 = 000001  */  -487,  /*  IH_LDUB  */
    /*  op3 = 000010  */  -489,  /*  IH_LDUH  */
    /*  op3 = 000011  */  -470,  /*  IH_LDD  */
    /*  op3 = 000100  */  -588,  /*  IH_STW  */
    /*  op3 = 000101  */  -575,  /*  IH_STB  */
    /*  op3 = 000110  */  -584,  /*  IH_STH  */
    /*  op3 = 000111  */  -577,  /*  IH_STD  */
    /*  op3 = 001000  */  -485,  /*  IH_LDSW  */
    /*  op3 = 001001  */  -479,  /*  IH_LDSB  */
    /*  op3 = 001010  */  -481,  /*  IH_LDSH  */
    /*  op3 = 001011  */  -493,  /*  IH_LDX  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 001101  */  -483,  /*  IH_LDSTUB  */
    /*  op3 = 001110  */  -590,  /*  IH_STX  */
    /*  op3 = 001111  */  -597,  /*  IH_SWAP  */
    /*  op3 = 010000  */  -492,  /*  IH_LDUWA  */
    /*  op3 = 010001  */  -488,  /*  IH_LDUBA  */
    /*  op3 = 010010  */  -490,  /*  IH_LDUHA  */
    /*  op3 = 010011  */  -471,  /*  IH_LDDA  */
    /*  op3 = 010100  */  -589,  /*  IH_STWA  */
    /*  op3 = 010101  */  -576,  /*  IH_STBA  */
    /*  op3 = 010110  */  -585,  /*  IH_STHA  */
    /*  op3 = 010111  */  -578,  /*  IH_STDA  */
    /*  op3 = 011000  */  -486,  /*  IH_LDSWA  */
    /*  op3 = 011001  */  -480,  /*  IH_LDSBA  */
    /*  op3 = 011010  */  -482,  /*  IH_LDSHA  */
    /*  op3 = 011011  */  -494,  /*  IH_LDXA  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 011101  */  -484,  /*  IH_LDSTUBA  */
    /*  op3 = 011110  */  -591,  /*  IH_STXA  */
    /*  op3 = 011111  */  -598,  /*  IH_SWAPA  */
    /*  op3 = 100000  */  -474,  /*  IH_LDF  */
    /*  op3 = 100001  */  106,
    /*  op3 = 100010  */  -477,  /*  IH_LDQF  */
    /*  op3 = 100011  */  -472,  /*  IH_LDDF  */
    /*  op3 = 100100  */  -581,  /*  IH_STF  */
    /*  op3 = 100101  */  72,
    /*  op3 = 100110  */  -586,  /*  IH_STQF  */
    /*  op3 = 100111  */  -579,  /*  IH_STDF  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 101101  */  -544,  /*  IH_PREFETCH  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 110000  */  -475,  /*  IH_LDFA  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 110010  */  -478,  /*  IH_LDQFA  */
    /*  op3 = 110011  */  -473,  /*  IH_LDDFA  */
    /*  op3 = 110100  */  -582,  /*  IH_STFA  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 110110  */  -587,  /*  IH_STQFA  */
    /*  op3 = 110111  */  -580,  /*  IH_STDFA  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 111100  */  -134,  /*  IH_CASA  */
    /*  op3 = 111101  */  -545,  /*  IH_PREFETCHA  */
    /*  op3 = 111110  */  -135,  /*  IH_CASXA  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -583,  /*  IH_STFSR  */
    /*  rd = 00001  */  -592,  /*  IH_STXFSR  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -476,  /*  IH_LDFSR  */
    /*  rd = 00001  */  -495,  /*  IH_LDXFSR  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: op2,  MSB: 24,  LSB: 22  */
    7, 29,  /*  Left shift, Right Shift.  */
    /*  op2 = 000  */  -466,  /*  IH_ILLTRAP  */
    /*  op2 = 001  */  490,
    /*  op2 = 010  */  240,
    /*  op2 = 011  */  150,
    /*  op2 = 100  */  -564,  /*  IH_SETHI  */
    /*  op2 = 101  */  280,
    /*  op2 = 110  */  700,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  cond = 0001  */  168,
    /*  cond = 0010  */  228,
    /*  cond = 0011  */  204,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  cond = 0101  */  180,
    /*  cond = 0110  */  216,
    /*  cond = 0111  */  192,
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  176,
    /*  a = 1  */  172,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -125,  /*  IH_BRZAPN  */
    /*  p = 1  */  -126,  /*  IH_BRZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -127,  /*  IH_BRZPN  */
    /*  p = 1  */  -128,  /*  IH_BRZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  188,
    /*  a = 1  */  184,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -121,  /*  IH_BRNZAPN  */
    /*  p = 1  */  -122,  /*  IH_BRNZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -123,  /*  IH_BRNZPN  */
    /*  p = 1  */  -124,  /*  IH_BRNZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  200,
    /*  a = 1  */  196,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -105,  /*  IH_BRGEZAPN  */
    /*  p = 1  */  -106,  /*  IH_BRGEZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -107,  /*  IH_BRGEZPN  */
    /*  p = 1  */  -108,  /*  IH_BRGEZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  212,
    /*  a = 1  */  208,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -117,  /*  IH_BRLZAPN  */
    /*  p = 1  */  -118,  /*  IH_BRLZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -119,  /*  IH_BRLZPN  */
    /*  p = 1  */  -120,  /*  IH_BRLZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  224,
    /*  a = 1  */  220,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -109,  /*  IH_BRGZAPN  */
    /*  p = 1  */  -110,  /*  IH_BRGZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -111,  /*  IH_BRGZPN  */
    /*  p = 1  */  -112,  /*  IH_BRGZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  236,
    /*  a = 1  */  232,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -113,  /*  IH_BRLEZAPN  */
    /*  p = 1  */  -114,  /*  IH_BRLEZAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -115,  /*  IH_BRLEZPN  */
    /*  p = 1  */  -116,  /*  IH_BRLEZPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  262,
    /*  a = 1  */  244,
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  -34,  /*  IH_BNA  */
    /*  cond = 0001  */  -20,  /*  IH_BEA  */
    /*  cond = 0010  */  -30,  /*  IH_BLEA  */
    /*  cond = 0011  */  -28,  /*  IH_BLA  */
    /*  cond = 0100  */  -32,  /*  IH_BLEUA  */
    /*  cond = 0101  */  -18,  /*  IH_BCSA  */
    /*  cond = 0110  */  -38,  /*  IH_BNEGA  */
    /*  cond = 0111  */  -132,  /*  IH_BVSA  */
    /*  cond = 1000  */  -14,  /*  IH_BAA  */
    /*  cond = 1001  */  -36,  /*  IH_BNEA  */
    /*  cond = 1010  */  -22,  /*  IH_BGA  */
    /*  cond = 1011  */  -24,  /*  IH_BGEA  */
    /*  cond = 1100  */  -26,  /*  IH_BGUA  */
    /*  cond = 1101  */  -16,  /*  IH_BCCA  */
    /*  cond = 1110  */  -92,  /*  IH_BPOSA  */
    /*  cond = 1111  */  -130,  /*  IH_BVCA  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  -33,  /*  IH_BN  */
    /*  cond = 0001  */  -19,  /*  IH_BE  */
    /*  cond = 0010  */  -29,  /*  IH_BLE  */
    /*  cond = 0011  */  -27,  /*  IH_BL  */
    /*  cond = 0100  */  -31,  /*  IH_BLEU  */
    /*  cond = 0101  */  -17,  /*  IH_BCS  */
    /*  cond = 0110  */  -37,  /*  IH_BNEG  */
    /*  cond = 0111  */  -131,  /*  IH_BVS  */
    /*  cond = 1000  */  -13,  /*  IH_BA  */
    /*  cond = 1001  */  -35,  /*  IH_BNE  */
    /*  cond = 1010  */  -21,  /*  IH_BG  */
    /*  cond = 1011  */  -23,  /*  IH_BGE  */
    /*  cond = 1100  */  -25,  /*  IH_BGU  */
    /*  cond = 1101  */  -15,  /*  IH_BCC  */
    /*  cond = 1110  */  -91,  /*  IH_BPOS  */
    /*  cond = 1111  */  -129,  /*  IH_BVC  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  346,
    /*  cond = 0001  */  454,
    /*  cond = 0010  */  382,
    /*  cond = 0011  */  418,
    /*  cond = 0100  */  430,
    /*  cond = 0101  */  298,
    /*  cond = 0110  */  406,
    /*  cond = 0111  */  310,
    /*  cond = 1000  */  478,
    /*  cond = 1001  */  442,
    /*  cond = 1010  */  334,
    /*  cond = 1011  */  466,
    /*  cond = 1100  */  370,
    /*  cond = 1101  */  394,
    /*  cond = 1110  */  358,
    /*  cond = 1111  */  322,
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  306,
    /*  a = 1  */  302,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -224,  /*  IH_FBPUGAPN  */
    /*  p = 1  */  -225,  /*  IH_FBPUGAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -226,  /*  IH_FBPUGPN  */
    /*  p = 1  */  -227,  /*  IH_FBPUGPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  318,
    /*  a = 1  */  314,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -216,  /*  IH_FBPUAPN  */
    /*  p = 1  */  -217,  /*  IH_FBPUAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -218,  /*  IH_FBPUPN  */
    /*  p = 1  */  -219,  /*  IH_FBPUPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  330,
    /*  a = 1  */  326,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -212,  /*  IH_FBPOAPN  */
    /*  p = 1  */  -213,  /*  IH_FBPOAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -214,  /*  IH_FBPOPN  */
    /*  p = 1  */  -215,  /*  IH_FBPOPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  342,
    /*  a = 1  */  338,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -220,  /*  IH_FBPUEAPN  */
    /*  p = 1  */  -221,  /*  IH_FBPUEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -222,  /*  IH_FBPUEPN  */
    /*  p = 1  */  -223,  /*  IH_FBPUEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  354,
    /*  a = 1  */  350,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -204,  /*  IH_FBPNAPN  */
    /*  p = 1  */  -205,  /*  IH_FBPNAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -206,  /*  IH_FBPNPN  */
    /*  p = 1  */  -207,  /*  IH_FBPNPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  366,
    /*  a = 1  */  362,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -236,  /*  IH_FBPULEAPN  */
    /*  p = 1  */  -237,  /*  IH_FBPULEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -238,  /*  IH_FBPULEPN  */
    /*  p = 1  */  -239,  /*  IH_FBPULEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  378,
    /*  a = 1  */  374,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -228,  /*  IH_FBPUGEAPN  */
    /*  p = 1  */  -229,  /*  IH_FBPUGEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -230,  /*  IH_FBPUGEPN  */
    /*  p = 1  */  -231,  /*  IH_FBPUGEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  390,
    /*  a = 1  */  386,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -200,  /*  IH_FBPLGAPN  */
    /*  p = 1  */  -201,  /*  IH_FBPLGAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -202,  /*  IH_FBPLGPN  */
    /*  p = 1  */  -203,  /*  IH_FBPLGPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  402,
    /*  a = 1  */  398,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -196,  /*  IH_FBPLEAPN  */
    /*  p = 1  */  -197,  /*  IH_FBPLEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -198,  /*  IH_FBPLEPN  */
    /*  p = 1  */  -199,  /*  IH_FBPLEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  414,
    /*  a = 1  */  410,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -184,  /*  IH_FBPGAPN  */
    /*  p = 1  */  -185,  /*  IH_FBPGAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -186,  /*  IH_FBPGPN  */
    /*  p = 1  */  -187,  /*  IH_FBPGPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  426,
    /*  a = 1  */  422,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -232,  /*  IH_FBPULAPN  */
    /*  p = 1  */  -233,  /*  IH_FBPULAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -234,  /*  IH_FBPULPN  */
    /*  p = 1  */  -235,  /*  IH_FBPULPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  438,
    /*  a = 1  */  434,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -192,  /*  IH_FBPLAPN  */
    /*  p = 1  */  -193,  /*  IH_FBPLAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -194,  /*  IH_FBPLPN  */
    /*  p = 1  */  -195,  /*  IH_FBPLPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  450,
    /*  a = 1  */  446,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -180,  /*  IH_FBPEAPN  */
    /*  p = 1  */  -181,  /*  IH_FBPEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -182,  /*  IH_FBPEPN  */
    /*  p = 1  */  -183,  /*  IH_FBPEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  462,
    /*  a = 1  */  458,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -208,  /*  IH_FBPNEAPN  */
    /*  p = 1  */  -209,  /*  IH_FBPNEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -210,  /*  IH_FBPNEPN  */
    /*  p = 1  */  -211,  /*  IH_FBPNEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  474,
    /*  a = 1  */  470,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -188,  /*  IH_FBPGEAPN  */
    /*  p = 1  */  -189,  /*  IH_FBPGEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -190,  /*  IH_FBPGEPN  */
    /*  p = 1  */  -191,  /*  IH_FBPGEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  486,
    /*  a = 1  */  482,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -176,  /*  IH_FBPAAPN  */
    /*  p = 1  */  -177,  /*  IH_FBPAAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -178,  /*  IH_FBPAPN  */
    /*  p = 1  */  -179,  /*  IH_FBPAPT  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  544,
    /*  cond = 0001  */  652,
    /*  cond = 0010  */  580,
    /*  cond = 0011  */  616,
    /*  cond = 0100  */  640,
    /*  cond = 0101  */  520,
    /*  cond = 0110  */  628,
    /*  cond = 0111  */  508,
    /*  cond = 1000  */  688,
    /*  cond = 1001  */  664,
    /*  cond = 1010  */  556,
    /*  cond = 1011  */  676,
    /*  cond = 1100  */  592,
    /*  cond = 1101  */  604,
    /*  cond = 1110  */  568,
    /*  cond = 1111  */  532,
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  516,
    /*  a = 1  */  512,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -101,  /*  IH_BPVSAPN  */
    /*  p = 1  */  -102,  /*  IH_BPVSAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -103,  /*  IH_BPVSPN  */
    /*  p = 1  */  -104,  /*  IH_BPVSPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  528,
    /*  a = 1  */  524,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -47,  /*  IH_BPCSAPN  */
    /*  p = 1  */  -48,  /*  IH_BPCSAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -49,  /*  IH_BPCSPN  */
    /*  p = 1  */  -50,  /*  IH_BPCSPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  540,
    /*  a = 1  */  536,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -97,  /*  IH_BPVCAPN  */
    /*  p = 1  */  -98,  /*  IH_BPVCAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -99,  /*  IH_BPVCPN  */
    /*  p = 1  */  -100,  /*  IH_BPVCPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  552,
    /*  a = 1  */  548,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -79,  /*  IH_BPNAPN  */
    /*  p = 1  */  -80,  /*  IH_BPNAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -81,  /*  IH_BPNPN  */
    /*  p = 1  */  -82,  /*  IH_BPNPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  564,
    /*  a = 1  */  560,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -55,  /*  IH_BPGAPN  */
    /*  p = 1  */  -56,  /*  IH_BPGAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -57,  /*  IH_BPGPN  */
    /*  p = 1  */  -58,  /*  IH_BPGPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  576,
    /*  a = 1  */  572,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -93,  /*  IH_BPPOSAPN  */
    /*  p = 1  */  -94,  /*  IH_BPPOSAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -95,  /*  IH_BPPOSPN  */
    /*  p = 1  */  -96,  /*  IH_BPPOSPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  588,
    /*  a = 1  */  584,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -71,  /*  IH_BPLEAPN  */
    /*  p = 1  */  -72,  /*  IH_BPLEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -73,  /*  IH_BPLEPN  */
    /*  p = 1  */  -74,  /*  IH_BPLEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  600,
    /*  a = 1  */  596,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -63,  /*  IH_BPGUAPN  */
    /*  p = 1  */  -64,  /*  IH_BPGUAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -65,  /*  IH_BPGUPN  */
    /*  p = 1  */  -66,  /*  IH_BPGUPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  612,
    /*  a = 1  */  608,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -43,  /*  IH_BPCCAPN  */
    /*  p = 1  */  -44,  /*  IH_BPCCAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -45,  /*  IH_BPCCPN  */
    /*  p = 1  */  -46,  /*  IH_BPCCPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  624,
    /*  a = 1  */  620,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -67,  /*  IH_BPLAPN  */
    /*  p = 1  */  -68,  /*  IH_BPLAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -69,  /*  IH_BPLPN  */
    /*  p = 1  */  -70,  /*  IH_BPLPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  636,
    /*  a = 1  */  632,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -87,  /*  IH_BPNEGAPN  */
    /*  p = 1  */  -88,  /*  IH_BPNEGAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -89,  /*  IH_BPNEGPN  */
    /*  p = 1  */  -90,  /*  IH_BPNEGPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  648,
    /*  a = 1  */  644,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -75,  /*  IH_BPLEUAPN  */
    /*  p = 1  */  -76,  /*  IH_BPLEUAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -77,  /*  IH_BPLEUPN  */
    /*  p = 1  */  -78,  /*  IH_BPLEUPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  660,
    /*  a = 1  */  656,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -51,  /*  IH_BPEAPN  */
    /*  p = 1  */  -52,  /*  IH_BPEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -53,  /*  IH_BPEPN  */
    /*  p = 1  */  -54,  /*  IH_BPEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  672,
    /*  a = 1  */  668,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -83,  /*  IH_BPNEAPN  */
    /*  p = 1  */  -84,  /*  IH_BPNEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -85,  /*  IH_BPNEPN  */
    /*  p = 1  */  -86,  /*  IH_BPNEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  684,
    /*  a = 1  */  680,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -59,  /*  IH_BPGEAPN  */
    /*  p = 1  */  -60,  /*  IH_BPGEAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -61,  /*  IH_BPGEPN  */
    /*  p = 1  */  -62,  /*  IH_BPGEPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  696,
    /*  a = 1  */  692,
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -39,  /*  IH_BPAAPN  */
    /*  p = 1  */  -40,  /*  IH_BPAAPT  */
    /* Field: p,  MSB: 19,  LSB: 19  */
    12, 31,  /*  Left shift, Right Shift.  */
    /*  p = 0  */  -41,  /*  IH_BPAPN  */
    /*  p = 1  */  -42,  /*  IH_BPAPT  */
    /* Field: a,  MSB: 29,  LSB: 29  */
    2, 31,  /*  Left shift, Right Shift.  */
    /*  a = 0  */  722,
    /*  a = 1  */  704,
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  -171,  /*  IH_FBNA  */
    /*  cond = 0001  */  -173,  /*  IH_FBNEA  */
    /*  cond = 0010  */  -169,  /*  IH_FBLGA  */
    /*  cond = 0011  */  -249,  /*  IH_FBULA  */
    /*  cond = 0100  */  -165,  /*  IH_FBLA  */
    /*  cond = 0101  */  -245,  /*  IH_FBUGA  */
    /*  cond = 0110  */  -161,  /*  IH_FBGA  */
    /*  cond = 0111  */  -241,  /*  IH_FBUA  */
    /*  cond = 1000  */  -157,  /*  IH_FBAA  */
    /*  cond = 1001  */  -159,  /*  IH_FBEA  */
    /*  cond = 1010  */  -243,  /*  IH_FBUEA  */
    /*  cond = 1011  */  -163,  /*  IH_FBGEA  */
    /*  cond = 1100  */  -247,  /*  IH_FBUGEA  */
    /*  cond = 1101  */  -167,  /*  IH_FBLEA  */
    /*  cond = 1110  */  -251,  /*  IH_FBULEA  */
    /*  cond = 1111  */  -175,  /*  IH_FBOA  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  -170,  /*  IH_FBN  */
    /*  cond = 0001  */  -172,  /*  IH_FBNE  */
    /*  cond = 0010  */  -168,  /*  IH_FBLG  */
    /*  cond = 0011  */  -248,  /*  IH_FBUL  */
    /*  cond = 0100  */  -164,  /*  IH_FBL  */
    /*  cond = 0101  */  -244,  /*  IH_FBUG  */
    /*  cond = 0110  */  -160,  /*  IH_FBG  */
    /*  cond = 0111  */  -240,  /*  IH_FBU  */
    /*  cond = 1000  */  -156,  /*  IH_FBA  */
    /*  cond = 1001  */  -158,  /*  IH_FBE  */
    /*  cond = 1010  */  -242,  /*  IH_FBUE  */
    /*  cond = 1011  */  -162,  /*  IH_FBGE  */
    /*  cond = 1100  */  -246,  /*  IH_FBUGE  */
    /*  cond = 1101  */  -166,  /*  IH_FBLE  */
    /*  cond = 1110  */  -250,  /*  IH_FBULE  */
    /*  cond = 1111  */  -174,  /*  IH_FBO  */
    /* Field: op3,  MSB: 24,  LSB: 19  */
    7, 26,  /*  Left shift, Right Shift.  */
    /*  op3 = 000000  */  -0,  /*  IH_ADD  */
    /*  op3 = 000001  */  -6,  /*  IH_AND  */
    /*  op3 = 000010  */  -538,  /*  IH_OR  */
    /*  op3 = 000011  */  -633,  /*  IH_XOR  */
    /*  op3 = 000100  */  -593,  /*  IH_SUB  */
    /*  op3 = 000101  */  -8,  /*  IH_ANDN  */
    /*  op3 = 000110  */  -540,  /*  IH_ORN  */
    /*  op3 = 000111  */  -631,  /*  IH_XNOR  */
    /*  op3 = 001000  */  -1,  /*  IH_ADDC  */
    /*  op3 = 001001  */  -536,  /*  IH_MULX  */
    /*  op3 = 001010  */  -622,  /*  IH_UMUL  */
    /*  op3 = 001011  */  -569,  /*  IH_SMUL  */
    /*  op3 = 001100  */  -594,  /*  IH_SUBC  */
    /*  op3 = 001101  */  -621,  /*  IH_UDIVX  */
    /*  op3 = 001110  */  -619,  /*  IH_UDIV  */
    /*  op3 = 001111  */  -560,  /*  IH_SDIV  */
    /*  op3 = 010000  */  -2,  /*  IH_ADDCC  */
    /*  op3 = 010001  */  -7,  /*  IH_ANDCC  */
    /*  op3 = 010010  */  -539,  /*  IH_ORCC  */
    /*  op3 = 010011  */  -634,  /*  IH_XORCC  */
    /*  op3 = 010100  */  -595,  /*  IH_SUBCC  */
    /*  op3 = 010101  */  -9,  /*  IH_ANDNCC  */
    /*  op3 = 010110  */  -541,  /*  IH_ORNCC  */
    /*  op3 = 010111  */  -632,  /*  IH_XNORCC  */
    /*  op3 = 011000  */  -3,  /*  IH_ADDCCC  */
    /*  op3 = 011001  */  -624,  /*  IH_UMULXCC  */
    /*  op3 = 011010  */  -623,  /*  IH_UMULCC  */
    /*  op3 = 011011  */  -570,  /*  IH_SMULCC  */
    /*  op3 = 011100  */  -596,  /*  IH_SUBCCC  */
    /*  op3 = 011101  */  -563,  /*  IH_SDIVXCC  */
    /*  op3 = 011110  */  -620,  /*  IH_UDIVCC  */
    /*  op3 = 011111  */  -561,  /*  IH_SDIVCC  */
    /*  op3 = 100000  */  -600,  /*  IH_TADDCC  */
    /*  op3 = 100001  */  -615,  /*  IH_TSUBCC  */
    /*  op3 = 100010  */  -601,  /*  IH_TADDCCTV  */
    /*  op3 = 100011  */  -616,  /*  IH_TSUBCCTV  */
    /*  op3 = 100100  */  -535,  /*  IH_MULSCC  */
    /*  op3 = 100101  */  806,
    /*  op3 = 100110  */  2524,
    /*  op3 = 100111  */  2452,
    /*  op3 = 101000  */  2456,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 101010  */  -551,  /*  IH_RDPR  */
    /*  op3 = 101011  */  -279,  /*  IH_FLUSHW  */
    /*  op3 = 101100  */  2528,
    /*  op3 = 101101  */  -562,  /*  IH_SDIVX  */
    /*  op3 = 101110  */  2418,
    /*  op3 = 101111  */  810,
    /*  op3 = 110000  */  2278,
    /*  op3 = 110001  */  2384,
    /*  op3 = 110010  */  -629,  /*  IH_WRPR  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  op3 = 110100  */  1764,
    /*  op3 = 110101  */  838,
    /*  op3 = 110110  */  1250,
    /*  op3 = 110111  */  1232,
    /*  op3 = 111000  */  -469,  /*  IH_JMPL  */
    /*  op3 = 111001  */  -557,  /*  IH_RETURN  */
    /*  op3 = 111010  */  820,
    /*  op3 = 111011  */  -278,  /*  IH_FLUSH  */
    /*  op3 = 111100  */  -558,  /*  IH_SAVE  */
    /*  op3 = 111101  */  -554,  /*  IH_RESTORE  */
    /*  op3 = 111110  */  2350,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: x_12,  MSB: 12,  LSB: 12  */
    19, 31,  /*  Left shift, Right Shift.  */
    /*  x_12 = 0  */  -567,  /*  IH_SLL  */
    /*  x_12 = 1  */  -568,  /*  IH_SLLX  */
    /* Field: rcond12_10,  MSB: 12,  LSB: 10  */
    19, 29,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 001  */  -532,  /*  IH_MOVRZ  */
    /*  rcond12_10 = 010  */  -529,  /*  IH_MOVRLEZ  */
    /*  rcond12_10 = 011  */  -530,  /*  IH_MOVRLZ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 101  */  -531,  /*  IH_MOVRNZ  */
    /*  rcond12_10 = 110  */  -528,  /*  IH_MOVRGZ  */
    /*  rcond12_10 = 111  */  -527,  /*  IH_MOVRGEZ  */
    /* Field: cond,  MSB: 28,  LSB: 25  */
    3, 28,  /*  Left shift, Right Shift.  */
    /*  cond = 0000  */  -611,  /*  IH_TN  */
    /*  cond = 0001  */  -604,  /*  IH_TE  */
    /*  cond = 0010  */  -609,  /*  IH_TLE  */
    /*  cond = 0011  */  -608,  /*  IH_TL  */
    /*  cond = 0100  */  -610,  /*  IH_TLEU  */
    /*  cond = 0101  */  -603,  /*  IH_TCS  */
    /*  cond = 0110  */  -613,  /*  IH_TNEG  */
    /*  cond = 0111  */  -618,  /*  IH_TVS  */
    /*  cond = 1000  */  -599,  /*  IH_TA  */
    /*  cond = 1001  */  -612,  /*  IH_TNE  */
    /*  cond = 1010  */  -605,  /*  IH_TG  */
    /*  cond = 1011  */  -606,  /*  IH_TGE  */
    /*  cond = 1100  */  -607,  /*  IH_TGU  */
    /*  cond = 1101  */  -602,  /*  IH_TCC  */
    /*  cond = 1110  */  -614,  /*  IH_TPOSZ  */
    /*  cond = 1111  */  -617,  /*  IH_TVC  */
    /* Field: opf9,  MSB: 9,  LSB: 9  */
    22, 31,  /*  Left shift, Right Shift.  */
    /*  opf9 = 0  */  938,
    /*  opf9 = 1  */  842,
    /* Field: opf13_10,  MSB: 13,  LSB: 10  */
    18, 28,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf13_10 = 0010  */  860,
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf8_5,  MSB: 8,  LSB: 5  */
    23, 28,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf8_5 = 0001  */  878,
    /*  opf8_5 = 0010  */  928,
    /*  opf8_5 = 0011  */  908,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf8_5 = 0101  */  888,
    /*  opf8_5 = 0110  */  918,
    /*  opf8_5 = 0111  */  898,
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -265,  /*  IH_FCMPS  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -257,  /*  IH_FCMPES  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -254,  /*  IH_FCMPEQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -264,  /*  IH_FCMPQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -253,  /*  IH_FCMPED  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf29_27,  MSB: 29,  LSB: 27  */
    2, 29,  /*  Left shift, Right Shift.  */
    /*  opf29_27 = 000  */  -252,  /*  IH_FCMPD  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: opf8_5,  MSB: 8,  LSB: 5  */
    23, 28,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf8_5 = 0001  */  956,
    /*  opf8_5 = 0010  */  1150,
    /*  opf8_5 = 0011  */  1068,
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf8_5 = 0101  */  1038,
    /*  opf8_5 = 0110  */  1058,
    /*  opf8_5 = 0111  */  1048,
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: cond17_14,  MSB: 17,  LSB: 14  */
    14, 28,  /*  Left shift, Right Shift.  */
    /*  cond17_14 = 0000  */  990,
    /*  cond17_14 = 0001  */  1026,
    /*  cond17_14 = 0010  */  1002,
    /*  cond17_14 = 0011  */  1014,
    /*  cond17_14 = 0100  */  1018,
    /*  cond17_14 = 0101  */  978,
    /*  cond17_14 = 0110  */  1010,
    /*  cond17_14 = 0111  */  974,
    /*  cond17_14 = 1000  */  1034,
    /*  cond17_14 = 1001  */  1022,
    /*  cond17_14 = 1010  */  986,
    /*  cond17_14 = 1011  */  1030,
    /*  cond17_14 = 1100  */  998,
    /*  cond17_14 = 1101  */  1006,
    /*  cond17_14 = 1110  */  994,
    /*  cond17_14 = 1111  */  982,
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -379,  /*  IH_FMOVSFU  */
    /*  if_13 = 1  */  -396,  /*  IH_FMOVSVS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -381,  /*  IH_FMOVSFUG  */
    /*  if_13 = 1  */  -367,  /*  IH_FMOVSCS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -378,  /*  IH_FMOVSFO  */
    /*  if_13 = 1  */  -395,  /*  IH_FMOVSVC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -380,  /*  IH_FMOVSFUE  */
    /*  if_13 = 1  */  -385,  /*  IH_FMOVSG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -376,  /*  IH_FMOVSFN  */
    /*  if_13 = 1  */  -391,  /*  IH_FMOVSN  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -384,  /*  IH_FMOVSFULE  */
    /*  if_13 = 1  */  -394,  /*  IH_FMOVSPOS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -382,  /*  IH_FMOVSFUGE  */
    /*  if_13 = 1  */  -387,  /*  IH_FMOVSGU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -375,  /*  IH_FMOVSFLG  */
    /*  if_13 = 1  */  -389,  /*  IH_FMOVSLE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -374,  /*  IH_FMOVSFLE  */
    /*  if_13 = 1  */  -366,  /*  IH_FMOVSCC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -371,  /*  IH_FMOVSFG  */
    /*  if_13 = 1  */  -393,  /*  IH_FMOVSNEG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -383,  /*  IH_FMOVSFUL  */
    /*  if_13 = 1  */  -388,  /*  IH_FMOVSL  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -373,  /*  IH_FMOVSFL  */
    /*  if_13 = 1  */  -390,  /*  IH_FMOVSLEU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -370,  /*  IH_FMOVSFE  */
    /*  if_13 = 1  */  -392,  /*  IH_FMOVSNE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -377,  /*  IH_FMOVSFNE  */
    /*  if_13 = 1  */  -368,  /*  IH_FMOVSE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -372,  /*  IH_FMOVSFGE  */
    /*  if_13 = 1  */  -386,  /*  IH_FMOVSGE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -369,  /*  IH_FMOVSFA  */
    /*  if_13 = 1  */  -365,  /*  IH_FMOVSA  */
    /* Field: rcond12_10,  MSB: 12,  LSB: 10  */
    19, 29,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 001  */  -363,  /*  IH_FMOVRSZ  */
    /*  rcond12_10 = 010  */  -360,  /*  IH_FMOVRSLEZ  */
    /*  rcond12_10 = 011  */  -361,  /*  IH_FMOVRSLZ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 101  */  -362,  /*  IH_FMOVRSNZ  */
    /*  rcond12_10 = 110  */  -359,  /*  IH_FMOVRSGZ  */
    /*  rcond12_10 = 111  */  -358,  /*  IH_FMOVRSGEZ  */
    /* Field: rcond12_10,  MSB: 12,  LSB: 10  */
    19, 29,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 001  */  -357,  /*  IH_FMOVRQZ  */
    /*  rcond12_10 = 010  */  -354,  /*  IH_FMOVRQLEZ  */
    /*  rcond12_10 = 011  */  -355,  /*  IH_FMOVRQLZ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 101  */  -356,  /*  IH_FMOVRQNZ  */
    /*  rcond12_10 = 110  */  -353,  /*  IH_FMOVRQGZ  */
    /*  rcond12_10 = 111  */  -352,  /*  IH_FMOVRQGEZ  */
    /* Field: rcond12_10,  MSB: 12,  LSB: 10  */
    19, 29,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 001  */  -351,  /*  IH_FMOVRDZ  */
    /*  rcond12_10 = 010  */  -348,  /*  IH_FMOVRDLEZ  */
    /*  rcond12_10 = 011  */  -349,  /*  IH_FMOVRDLZ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  rcond12_10 = 101  */  -350,  /*  IH_FMOVRDNZ  */
    /*  rcond12_10 = 110  */  -347,  /*  IH_FMOVRDGZ  */
    /*  rcond12_10 = 111  */  -346,  /*  IH_FMOVRDGEZ  */
    /* Field: cond17_14,  MSB: 17,  LSB: 14  */
    14, 28,  /*  Left shift, Right Shift.  */
    /*  cond17_14 = 0000  */  1102,
    /*  cond17_14 = 0001  */  1138,
    /*  cond17_14 = 0010  */  1114,
    /*  cond17_14 = 0011  */  1126,
    /*  cond17_14 = 0100  */  1130,
    /*  cond17_14 = 0101  */  1090,
    /*  cond17_14 = 0110  */  1122,
    /*  cond17_14 = 0111  */  1086,
    /*  cond17_14 = 1000  */  1146,
    /*  cond17_14 = 1001  */  1134,
    /*  cond17_14 = 1010  */  1098,
    /*  cond17_14 = 1011  */  1142,
    /*  cond17_14 = 1100  */  1110,
    /*  cond17_14 = 1101  */  1118,
    /*  cond17_14 = 1110  */  1106,
    /*  cond17_14 = 1111  */  1094,
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -328,  /*  IH_FMOVQFU  */
    /*  if_13 = 1  */  -345,  /*  IH_FMOVQVS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -330,  /*  IH_FMOVQFUG  */
    /*  if_13 = 1  */  -316,  /*  IH_FMOVQCS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -327,  /*  IH_FMOVQFO  */
    /*  if_13 = 1  */  -344,  /*  IH_FMOVQVC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -329,  /*  IH_FMOVQFUE  */
    /*  if_13 = 1  */  -334,  /*  IH_FMOVQG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -325,  /*  IH_FMOVQFN  */
    /*  if_13 = 1  */  -340,  /*  IH_FMOVQN  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -333,  /*  IH_FMOVQFULE  */
    /*  if_13 = 1  */  -343,  /*  IH_FMOVQPOS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -331,  /*  IH_FMOVQFUGE  */
    /*  if_13 = 1  */  -336,  /*  IH_FMOVQGU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -324,  /*  IH_FMOVQFLG  */
    /*  if_13 = 1  */  -338,  /*  IH_FMOVQLE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -323,  /*  IH_FMOVQFLE  */
    /*  if_13 = 1  */  -315,  /*  IH_FMOVQCC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -320,  /*  IH_FMOVQFG  */
    /*  if_13 = 1  */  -342,  /*  IH_FMOVQNEG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -332,  /*  IH_FMOVQFUL  */
    /*  if_13 = 1  */  -337,  /*  IH_FMOVQL  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -322,  /*  IH_FMOVQFL  */
    /*  if_13 = 1  */  -339,  /*  IH_FMOVQLEU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -319,  /*  IH_FMOVQFE  */
    /*  if_13 = 1  */  -341,  /*  IH_FMOVQNE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -326,  /*  IH_FMOVQFNE  */
    /*  if_13 = 1  */  -317,  /*  IH_FMOVQE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -321,  /*  IH_FMOVQFGE  */
    /*  if_13 = 1  */  -335,  /*  IH_FMOVQGE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -318,  /*  IH_FMOVQFA  */
    /*  if_13 = 1  */  -314,  /*  IH_FMOVQA  */
    /* Field: cond17_14,  MSB: 17,  LSB: 14  */
    14, 28,  /*  Left shift, Right Shift.  */
    /*  cond17_14 = 0000  */  1184,
    /*  cond17_14 = 0001  */  1220,
    /*  cond17_14 = 0010  */  1196,
    /*  cond17_14 = 0011  */  1208,
    /*  cond17_14 = 0100  */  1212,
    /*  cond17_14 = 0101  */  1172,
    /*  cond17_14 = 0110  */  1204,
    /*  cond17_14 = 0111  */  1168,
    /*  cond17_14 = 1000  */  1228,
    /*  cond17_14 = 1001  */  1216,
    /*  cond17_14 = 1010  */  1180,
    /*  cond17_14 = 1011  */  1224,
    /*  cond17_14 = 1100  */  1192,
    /*  cond17_14 = 1101  */  1200,
    /*  cond17_14 = 1110  */  1188,
    /*  cond17_14 = 1111  */  1176,
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -295,  /*  IH_FMOVDFU  */
    /*  if_13 = 1  */  -312,  /*  IH_FMOVDVS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -297,  /*  IH_FMOVDFUG  */
    /*  if_13 = 1  */  -283,  /*  IH_FMOVDCS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -294,  /*  IH_FMOVDFO  */
    /*  if_13 = 1  */  -311,  /*  IH_FMOVDVC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -296,  /*  IH_FMOVDFUE  */
    /*  if_13 = 1  */  -301,  /*  IH_FMOVDG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -292,  /*  IH_FMOVDFN  */
    /*  if_13 = 1  */  -307,  /*  IH_FMOVDN  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -300,  /*  IH_FMOVDFULE  */
    /*  if_13 = 1  */  -310,  /*  IH_FMOVDPOS  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -298,  /*  IH_FMOVDFUGE  */
    /*  if_13 = 1  */  -303,  /*  IH_FMOVDGU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -291,  /*  IH_FMOVDFLG  */
    /*  if_13 = 1  */  -305,  /*  IH_FMOVDLE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -290,  /*  IH_FMOVDFLE  */
    /*  if_13 = 1  */  -282,  /*  IH_FMOVDCC  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -287,  /*  IH_FMOVDFG  */
    /*  if_13 = 1  */  -309,  /*  IH_FMOVDNEG  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -299,  /*  IH_FMOVDFUL  */
    /*  if_13 = 1  */  -304,  /*  IH_FMOVDL  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -289,  /*  IH_FMOVDFL  */
    /*  if_13 = 1  */  -306,  /*  IH_FMOVDLEU  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -286,  /*  IH_FMOVDFE  */
    /*  if_13 = 1  */  -308,  /*  IH_FMOVDNE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -293,  /*  IH_FMOVDFNE  */
    /*  if_13 = 1  */  -284,  /*  IH_FMOVDE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -288,  /*  IH_FMOVDFGE  */
    /*  if_13 = 1  */  -302,  /*  IH_FMOVDGE  */
    /* Field: if_13,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
    /*  if_13 = 0  */  -285,  /*  IH_FMOVDFA  */
    /*  if_13 = 1  */  -281,  /*  IH_FMOVDA  */
    /* Field: opf8_5,  MSB: 8,  LSB: 5  */
    23, 28,  /*  Left shift, Right Shift.  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /*  opf8_5 = 0001  */  -636,  /*  IH_FMADDS  */
    /*  opf8_5 = 0010  */  -637,  /*  IH_FMADDD  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /*  opf8_5 = 0101  */  -638,  /*  IH_FMSUBS  */
    /*  opf8_5 = 0110  */  -639,  /*  IH_FMSUBD  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /*  opf8_5 = 1001  */  -640,  /*  IH_FNMSUBS  */
    /*  opf8_5 = 1010  */  -641,  /*  IH_FNMSUBD  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /*  opf8_5 = 1101  */  -642,  /*  IH_FNMADDS  */
    /*  opf8_5 = 1110  */  -643,  /*  IH_FNMADDD  */
    /* generic */  -468,  /*  IH_IMPDEP2  */
    /* Field: opf,  MSB: 13,  LSB: 5  */
    18, 23,  /*  Left shift, Right Shift.  */
    /*  opf = 000000000  */  -141,  /*  IH_EDGE8  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000000010  */  -142,  /*  IH_EDGE8L  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000000100  */  -137,  /*  IH_EDGE16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000000110  */  -138,  /*  IH_EDGE16L  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000001000  */  -139,  /*  IH_EDGE32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000001010  */  -140,  /*  IH_EDGE32L  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000010000  */  -12,  /*  IH_ARRAY8  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000010010  */  -10,  /*  IH_ARRAY16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000010100  */  -11,  /*  IH_ARRAY32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000011000  */  -4,  /*  IH_ALIGNADDR  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000011010  */  -5,  /*  IH_ALIGNADDRL  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000100000  */  -260,  /*  IH_FCMPLE16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000100010  */  -262,  /*  IH_FCMPNE16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000100100  */  -261,  /*  IH_FCMPLE32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000100110  */  -263,  /*  IH_FCMPNE32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000101000  */  -258,  /*  IH_FCMPGT16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000101010  */  -255,  /*  IH_FCMPEQ16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000101100  */  -259,  /*  IH_FCMPGT32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000101110  */  -256,  /*  IH_FCMPEQ32  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000110001  */  -399,  /*  IH_FMUL8X16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000110011  */  -401,  /*  IH_FMUL8X16AU  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000110101  */  -400,  /*  IH_FMUL8X16AL  */
    /*  opf = 000110110  */  -397,  /*  IH_FMUL8SUX16  */
    /*  opf = 000110111  */  -398,  /*  IH_FMUL8ULX16  */
    /*  opf = 000111000  */  -403,  /*  IH_FMULD8SUX16  */
    /*  opf = 000111001  */  -404,  /*  IH_FMULD8ULX16  */
    /*  opf = 000111010  */  -427,  /*  IH_FPACK32  */
    /*  opf = 000111011  */  -426,  /*  IH_FPACK16  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 000111101  */  -428,  /*  IH_FPACKFIX  */
    /*  opf = 000111110  */  -542,  /*  IH_PDIST  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 001001000  */  -149,  /*  IH_FALIGNDATA  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 001001011  */  -433,  /*  IH_FPMERGE  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 001001101  */  -274,  /*  IH_FEXPAND  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 001010000  */  -429,  /*  IH_FPADD16  */
    /*  opf = 001010001  */  -430,  /*  IH_FPADD16S  */
    /*  opf = 001010010  */  -431,  /*  IH_FPADD32  */
    /*  opf = 001010011  */  -432,  /*  IH_FPADD32S  */
    /*  opf = 001010100  */  -434,  /*  IH_FPSUB16  */
    /*  opf = 001010101  */  -435,  /*  IH_FPSUB16S  */
    /*  opf = 001010110  */  -436,  /*  IH_FPSUB32  */
    /*  opf = 001010111  */  -437,  /*  IH_FPSUB32S  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /*  opf = 001100000  */  -464,  /*  IH_FZERO  */
    /*  opf = 001100001  */  -465,  /*  IH_FZEROS  */
    /*  opf = 001100010  */  -412,  /*  IH_FNOR  */
    /*  opf = 001100011  */  -413,  /*  IH_FNORS  */
    /*  opf = 001100100  */  -153,  /*  IH_FANDNOT2  */
    /*  opf = 001100101  */  -154,  /*  IH_FANDNOT2S  */
    /*  opf = 001100110  */  -416,  /*  IH_FNOT2  */
    /*  opf = 001100111  */  -417,  /*  IH_FNOT2S  */
    /*  opf = 001101000  */  -151,  /*  IH_FANDNOT1  */
    /*  opf = 001101001  */  -152,  /*  IH_FANDNOT1S  */
    /*  opf = 001101010  */  -414,  /*  IH_FNOT1  */
    /*  opf = 001101011  */  -415,  /*  IH_FNOT1S  */
    /*  opf = 001101100  */  -459,  /*  IH_FXOR  */
    /*  opf = 001101101  */  -460,  /*  IH_FXORS  */
    /*  opf = 001101110  */  -407,  /*  IH_FNAND  */
    /*  opf = 001101111  */  -408,  /*  IH_FNANDS  */
    /*  opf = 001110000  */  -150,  /*  IH_FAND  */
    /*  opf = 001110001  */  -155,  /*  IH_FANDS  */
    /*  opf = 001110010  */  -457,  /*  IH_FXNOR  */
    /*  opf = 001110011  */  -458,  /*  IH_FXNORS  */
    /*  opf = 001110100  */  -446,  /*  IH_FSRC1  */
    /*  opf = 001110101  */  -447,  /*  IH_FSRC1S  */
    /*  opf = 001110110  */  -423,  /*  IH_FORNOT2  */
    /*  opf = 001110111  */  -424,  /*  IH_FORNOT2S  */
    /*  opf = 001111000  */  -448,  /*  IH_FSRC2  */
    /*  opf = 001111001  */  -449,  /*  IH_FSRC2S  */
    /*  opf = 001111010  */  -421,  /*  IH_FORNOT1  */
    /*  opf = 001111011  */  -422,  /*  IH_FORNOT1S  */
    /*  opf = 001111100  */  -420,  /*  IH_FOR  */
    /*  opf = 001111101  */  -425,  /*  IH_FORS  */
    /*  opf = 001111110  */  -418,  /*  IH_FONE  */
    /*  opf = 001111111  */  -419,  /*  IH_FONES  */
    /*  opf = 010000000  */  -565,  /*  IH_SHUTDOWN  */
    /*  opf = 010000001  */  -635,  /*  IH_SIAM  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* generic */  -467,  /*  IH_IMPDEP1  */
    /* Field: opf,  MSB: 13,  LSB: 5  */
    18, 23,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 000000001  */  -364,  /*  IH_FMOVS  */
    /*  opf = 000000010  */  -280,  /*  IH_FMOVD  */
    /*  opf = 000000011  */  -313,  /*  IH_FMOVQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 000000101  */  -411,  /*  IH_FNEGS  */
    /*  opf = 000000110  */  -409,  /*  IH_FNEGD  */
    /*  opf = 000000111  */  -410,  /*  IH_FNEGQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 000001001  */  -145,  /*  IH_FABSS  */
    /*  opf = 000001010  */  -143,  /*  IH_FABSD  */
    /*  opf = 000001011  */  -144,  /*  IH_FABSQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 000101001  */  -445,  /*  IH_FSQRTS  */
    /*  opf = 000101010  */  -443,  /*  IH_FSQRTD  */
    /*  opf = 000101011  */  -444,  /*  IH_FSQRTQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001000001  */  -148,  /*  IH_FADDS  */
    /*  opf = 001000010  */  -146,  /*  IH_FADDD  */
    /*  opf = 001000011  */  -147,  /*  IH_FADDQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001000101  */  -456,  /*  IH_FSUBS  */
    /*  opf = 001000110  */  -454,  /*  IH_FSUBD  */
    /*  opf = 001000111  */  -455,  /*  IH_FSUBQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001001001  */  -406,  /*  IH_FMULS  */
    /*  opf = 001001010  */  -402,  /*  IH_FMULD  */
    /*  opf = 001001011  */  -405,  /*  IH_FMULQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001001101  */  -268,  /*  IH_FDIVS  */
    /*  opf = 001001110  */  -266,  /*  IH_FDIVD  */
    /*  opf = 001001111  */  -267,  /*  IH_FDIVQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001101001  */  -442,  /*  IH_FSMULD  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 001101110  */  -269,  /*  IH_FDMULQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 010000001  */  -453,  /*  IH_FSTOX  */
    /*  opf = 010000010  */  -273,  /*  IH_FDTOX  */
    /*  opf = 010000011  */  -441,  /*  IH_FQTOX  */
    /*  opf = 010000100  */  -463,  /*  IH_FXTOS  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 010001000  */  -461,  /*  IH_FXTOD  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 010001100  */  -462,  /*  IH_FXTOQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 011000100  */  -277,  /*  IH_FITOS  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 011000110  */  -272,  /*  IH_FDTOS  */
    /*  opf = 011000111  */  -440,  /*  IH_FQTOS  */
    /*  opf = 011001000  */  -275,  /*  IH_FITOD  */
    /*  opf = 011001001  */  -450,  /*  IH_FSTOD  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 011001011  */  -438,  /*  IH_FQTOD  */
    /*  opf = 011001100  */  -276,  /*  IH_FITOQ  */
    /*  opf = 011001101  */  -452,  /*  IH_FSTOQ  */
    /*  opf = 011001110  */  -271,  /*  IH_FDTOQ  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  opf = 011010001  */  -451,  /*  IH_FSTOI  */
    /*  opf = 011010010  */  -270,  /*  IH_FDTOI  */
    /*  opf = 011010011  */  -439,  /*  IH_FQTOI  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -630,  /*  IH_WRY  */
    /* generic */  -626,  /*  IH_WRASR  */
    /*  rd = 00010  */  -627,  /*  IH_WRCCR  */
    /*  rd = 00011  */  -625,  /*  IH_WRASI  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /*  rd = 00110  */  -628,  /*  IH_WRFPRS  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /*  rd = 01111  */  2312,
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* generic */  -626,  /*  IH_WRASR  */
    /* Field: rs1,  MSB: 18,  LSB: 14  */
    13, 27,  /*  Left shift, Right Shift.  */
    /*  rs1 = 00000  */  2346,
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: i,  MSB: 13,  LSB: 13  */
    18, 31,  /*  Left shift, Right Shift.  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /*  i = 1  */  -566,  /*  IH_SIR  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -136,  /*  IH_DONE  */
    /*  rd = 00001  */  -556,  /*  IH_RETRY  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -559,  /*  IH_SAVED  */
    /*  rd = 00001  */  -555,  /*  IH_RESTORED  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: rs1,  MSB: 18,  LSB: 14  */
    13, 27,  /*  Left shift, Right Shift.  */
    /*  rs1 = 00000  */  -543,  /*  IH_POPC  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: x_12,  MSB: 12,  LSB: 12  */
    19, 31,  /*  Left shift, Right Shift.  */
    /*  x_12 = 0  */  -571,  /*  IH_SRA  */
    /*  x_12 = 1  */  -572,  /*  IH_SRAX  */
    /* Field: rs1,  MSB: 18,  LSB: 14  */
    13, 27,  /*  Left shift, Right Shift.  */
    /*  rs1 = 00000  */  -553,  /*  IH_RDY  */
    /* generic */  -547,  /*  IH_RDASR  */
    /*  rs1 = 00010  */  -548,  /*  IH_RDCCR  */
    /*  rs1 = 00011  */  -546,  /*  IH_RDASI  */
    /*  rs1 = 00100  */  -552,  /*  IH_RDTICK  */
    /*  rs1 = 00101  */  -550,  /*  IH_RDPC  */
    /*  rs1 = 00110  */  -549,  /*  IH_RDFPRS  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /*  rs1 = 01111  */  2490,
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* generic */  -547,  /*  IH_RDASR  */
    /* Field: rd,  MSB: 29,  LSB: 25  */
    2, 27,  /*  Left shift, Right Shift.  */
    /*  rd = 00000  */  -496,  /*  IH_MEMBAR  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
-1000,  /*  *** PARSE TABLE INVALID ***  */
    /* Field: x_12,  MSB: 12,  LSB: 12  */
    19, 31,  /*  Left shift, Right Shift.  */
    /*  x_12 = 0  */  -573,  /*  IH_SRL  */
    /*  x_12 = 1  */  -574,  /*  IH_SRLX  */
    /* Field: cond17_14,  MSB: 17,  LSB: 14  */
    14, 28,  /*  Left shift, Right Shift.  */
    /*  cond17_14 = 0000  */  2562,
    /*  cond17_14 = 0001  */  2598,
    /*  cond17_14 = 0010  */  2574,
    /*  cond17_14 = 0011  */  2586,
    /*  cond17_14 = 0100  */  2590,
    /*  cond17_14 = 0101  */  2550,
    /*  cond17_14 = 0110  */  2582,
    /*  cond17_14 = 0111  */  2546,
    /*  cond17_14 = 1000  */  2606,
    /*  cond17_14 = 1001  */  2594,
    /*  cond17_14 = 1010  */  2558,
    /*  cond17_14 = 1011  */  2602,
    /*  cond17_14 = 1100  */  2570,
    /*  cond17_14 = 1101  */  2578,
    /*  cond17_14 = 1110  */  2566,
    /*  cond17_14 = 1111  */  2554,
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -511,  /*  IH_MOVFU  */
    /*  if_18 = 1  */  -534,  /*  IH_MOVVS  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -513,  /*  IH_MOVFUG  */
    /*  if_18 = 1  */  -499,  /*  IH_MOVCS  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -510,  /*  IH_MOVFO  */
    /*  if_18 = 1  */  -533,  /*  IH_MOVVC  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -512,  /*  IH_MOVFUE  */
    /*  if_18 = 1  */  -517,  /*  IH_MOVG  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -508,  /*  IH_MOVFN  */
    /*  if_18 = 1  */  -523,  /*  IH_MOVN  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -516,  /*  IH_MOVFULE  */
    /*  if_18 = 1  */  -526,  /*  IH_MOVPOS  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -514,  /*  IH_MOVFUGE  */
    /*  if_18 = 1  */  -519,  /*  IH_MOVGU  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -507,  /*  IH_MOVFLG  */
    /*  if_18 = 1  */  -521,  /*  IH_MOVLE  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -506,  /*  IH_MOVFLE  */
    /*  if_18 = 1  */  -498,  /*  IH_MOVCC  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -503,  /*  IH_MOVFG  */
    /*  if_18 = 1  */  -525,  /*  IH_MOVNEG  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -515,  /*  IH_MOVFUL  */
    /*  if_18 = 1  */  -520,  /*  IH_MOVL  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -505,  /*  IH_MOVFL  */
    /*  if_18 = 1  */  -522,  /*  IH_MOVLEU  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -502,  /*  IH_MOVFE  */
    /*  if_18 = 1  */  -524,  /*  IH_MOVNE  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -509,  /*  IH_MOVFNE  */
    /*  if_18 = 1  */  -500,  /*  IH_MOVE  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -504,  /*  IH_MOVFGE  */
    /*  if_18 = 1  */  -518,  /*  IH_MOVGE  */
    /* Field: if_18,  MSB: 18,  LSB: 18  */
    13, 31,  /*  Left shift, Right Shift.  */
    /*  if_18 = 0  */  -501,  /*  IH_MOVFA  */
    /*  if_18 = 1  */  -497,  /*  IH_MOVA  */
   0 };
