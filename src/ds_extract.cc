/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2006 Louisiana State University

// $Id$

// Extract information from dataset file and send to stdout (so far).

#include <glib.h>
#include "pse.h"
#include "ds_extract.h"
#include "gtk_utility.h"
#include "dataset.h"

static void hook_after_args(DS_Extract *me);
static void hook_after_init(DS_Extract *me);


DS_Extract::DS_Extract(App *app):app(app)
{
  app->hooks_after_args.append((GHookFunc)&hook_after_args,this);
  app->option_manager.insert("variable-get",'v',&var_name);
  var_name = NULL;
}

static void
hook_after_args(DS_Extract *me)
{
  if( !me->var_name ) return;
  app.hooks_after_init.append((GHookFunc)&hook_after_init,me);
  app.gui = false;
}


static void
sim_var_print(Sim_Var_Info *info)
{
  printf(" %s = %s\n", info->name.str(), info->val_str.str());
}

static void
hook_after_init(DS_Extract *me)
{
  Dataset* const ds = me->app->ds;

  if( me->var_name[0] == '-' && !me->var_name[1] )
    {
      while( Sim_Var_Info** const info = ds->sim_var_info_hash.iterate() )
        sim_var_print(*info);
      return;
    }

  PSplit var_names(me->var_name,',');

  while( char* const var_name = var_names )
    {
      Sim_Var_Info *info;
      if( !ds->sim_var_info_hash.lookup(var_name,info) )
        printf("Simulation variable \"%s\" not found.\n", var_name);
      else
        sim_var_print(info);
    }
}
