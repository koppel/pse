/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2014 Louisiana State University

// $Id$

#ifndef PSE_WINDOW_H
#define PSE_WINDOW_H

#include "dataset.h"
#include "arrays.h"
#include "gtk_utility.h"
#include "search.h"

typedef void (*Func_vv)(void*); // Function returning void, with void* arg.

class Destruct_Hook
{
public:
  void insert(Func_vv func, void *ptr){ funcs += func; args += ptr; }
  ~Destruct_Hook(){ while ( funcs ) funcs.pop()(args.pop()); }
private:
  PStack<Func_vv> funcs;
  PStack<void*> args;
};

struct Pref_Dialog_Info;  // Defined in handlers.cc.

#define IDX_NEVER -2  // Impossible index, used for match targets.

enum Plot_ROB_Decode_Positioning
  {
    PRDP_Error, PRDP_ROB_Size, PRDP_Fit, PRDP_Fixed
  };

// Information on a segment plot to be, or that has been, displayed.
//
// Used to allow the user to undo zooms, etc.  With ViewEnv, used to
// determine whether pixel map should be updated.
//
struct View
{
  int start_c;                  // Cycle at beginning of view.
  int offset_vc;                // PED: Insn should start (rob enqueue) here.
  // PED: tag of instruction at top of plot.
  // May be out of range, out-of-range values used for positioning.
  int top_idx;
  int pixels_per_cycle;
  int pixels_per_inst;
  int valid;                    // If 1, elements above describe next view.

  void init(Dataset *ds);

  bool operator == (View &v2);
};

// Window configuration and options relevant to a View.
//
// With View, used to determine if pixel map should be updated.
//
struct ViewEnv
{
  Segment_Data *sd;
  gint width, height;
  int plot_ped_clipped, plot_segment_as_ped, plot_is_ovr;
  int pm_width;
  Plot_ROB_Decode_Positioning plot_rob_decode_positioning;
  bool valid;
  ViewEnv(){width = 0;}
  bool operator == (ViewEnv &v);
};

enum Animate_Mode { AM_Error, AM_None, AM_Short, AM_All };

struct Label_Show_Info
{
  GtkWidget *label;
  const char* const label_name; // Used in UI for visibility option.
  bool show_label_in_display;
  bool show_label_in_ps;

  Label_Show_Info
  (GtkWidget *label, const char *name,
   bool show_label_in_display, bool show_label_in_ps):
    label(label), label_name(name),
    show_label_in_display(show_label_in_display),
    show_label_in_ps(show_label_in_ps) {}

 };

class PSE_Label_Set {
public:
  PSE_Label_Set(){ size = 0; };
  ~PSE_Label_Set(){};
  void font_size_adjust(int milli_points);
  void operator += (PGtkLabelPtr label_ptr) { labels.put(label_ptr); }
  void operator += (GtkLabel *label) { labels.put(label); }
  void operator += (GtkWidget *label) { labels.put(GTK_LABEL(label)); }
  GtkLabel *iterate() { return labels.iterate(); }
private:
  int size;
  Auto_List<GtkLabel> labels;
};


class Call_Back_Data {
public:
  Call_Back_Data(PSE_Window *pw,char* c):sw(pw),s(c){timeout_handler_id=0;};
  Call_Back_Data(PSE_Window *sw,char c):sw(sw),c(c){timeout_handler_id=0;};
  Call_Back_Data(PSE_Window *sw,int i):sw(sw),i(i){timeout_handler_id=0;};
  Call_Back_Data():sw(NULL){timeout_handler_id=0;}
  union{ PSE_Window *sw,*pw; };
  char *s;
  char c;
  int i;
  gint timeout_handler_id;
};


class Pref_Plot_AToggle // Action Toggle
{
public:
  Pref_Plot_AToggle(){ rti_var = NULL; pw = NULL; };
  void init
  (PSE_Window *pw, int &rti_var, const char *name, const char *tooltip);
  void connect(GtkWidget *w){gtk_action_connect_proxy(action,w);}
  operator bool() const { return val; }
private:
  static void callback_toggled_wrapper(GtkToggleAction *w, gpointer moi)
  {((Pref_Plot_AToggle*)moi)->callback_toggled(w);}
  void callback_toggled(GtkToggleAction *w);
  PSE_Window *pw;
  PGtkToggleActionPtr action;
  int *rti_var;
  bool val;
};

class Pref_Plot_ARadio // Action Radio
{
public:
  Pref_Plot_ARadio(){ rti_var = NULL; pw = NULL; };
  void init(PSE_Window *pw, int &rti_var);
  void item_add(int item_val, const char *name, const char *tooltip);
  void connect(GtkWidget *w)
  {
    gtk_check_menu_item_set_draw_as_radio(GTK_CHECK_MENU_ITEM(w),true);
    gtk_action_connect_proxy(action_added_last,w);
  }
  operator int() const { return val; }
private:
  static void callback_changed_wrapper
  (GtkRadioAction *action, GtkRadioAction *current, gpointer moi)
  {if( current == action ) ((Pref_Plot_ARadio*)moi)->callback_changed(action);}
  void callback_changed(GtkRadioAction *w);
  PSE_Window *pw;
  PGtkRadioActionPtr action_added_last;
  GSList *group;
  int *rti_var;
  int val;
};


struct PSE_Window
{
  PSE_Window(Dataset *ds);
  ~PSE_Window();
  Destruct_Hook destruct_hook;

  Dataset* const ds;

  int window_num;
  char *title;

  Segment_Data *sd;
  FILE *ps_file_str;

  GtkAccelGroup *accel_group;
  bool key_handler_suppress;

  // View Settings
  //
  int plot_is_ovr;          // Plot overview graph.
  bool plot_segment_as_ped;  // Else, rob.
  bool seg_rob_ped_animating; // Set true for rob-to-ped and ped-to-rob. 
  bool plot_show_dis;
  bool last_plot_ped_clipped; // DMK: Will move to View or use one in ViewEnv.
  bool plot_ovr_by_ipc;       // Else, chronologically.
  int plot_ped_clipped;       // Else, wrap.

  bool seg_plot_paint_variables_valid;

  bool ovr_show_ovr_plot_values;
  bool seg_show_ovr_plot_values;

  Plot_ROB_Decode_Positioning plot_rob_decode_positioning;
  int plot_rob_decode_position_setting;

  Segment_Data *ovr_pointed_segment;

  // Grid (White lines drawn to group instructions.)
  //
  // If positive, draw grid lines between groups of GRID_SPACING_INST
  // instructions.  If negative, group instructions so that the number
  // of pixels per group is at least GRID_SPACING_INST.  GRID_SPACING_CYC
  // is defined similarly.
  //
  int grid_spacing_inst, grid_spacing_cyc;
  int opt_grid_mult_decode_width;
  int opt_grid_paced;

  //
  // Draw grid lines for individual instructions if they are more than
  // GRID_SPACING_INST_UNIT_THRESHOLD pixels apart.
  // GRID_SPACING_CYC_UNIT_THRESHOLD is defined similarly.
  //
  int grid_spacing_inst_unit_threshold, grid_spacing_cyc_unit_threshold;
  //
  // The number of instructions or cycles actually grouped together. A zero
  // indicates no grid lines.
  //
  int grid_spacing_used_inst;
  double grid_spacing_used_cyc;

  void grid_update_labels(){};

  // Clip Mode (When not in clip mode instructions wrap from bottom to top.)
  //
  GtkWidget *cbutton;
  GtkWidget *context_menu_clip_item;

  // Modification commands.
  //
  bool offset_vc_do_update;

  // Plot State
  //
  int ipc_plot_top_margin;
  int segment_plot_top, segment_plot_bottom;

  // Labeling
  //
  // DMK: show_labels will be generalized soon. 27 June 2002, 18:31:35 CDT
  bool show_labels;

  Pref_Plot_AToggle plot_high_same_sin;
  Pref_Plot_AToggle plot_high_same_line;
  Pref_Plot_AToggle plot_high_snips;
  Pref_Plot_AToggle plot_high_snip_boundaries;
  Pref_Plot_ARadio pref_plot_high_dep_limit;   // 0, none;  1, direct;  2, all.
  Pref_Plot_AToggle pref_plot_dep_through_mem;
  Pref_Plot_AToggle pref_plot_high_dead_insn;
  PGTK_Toggle_Button plot_dep_obscure;

  View view, last_view;

  // Plot Area Pixelmap  (To speed display updates.)
  //
  GdkPixmap *pm;
  // Todo: Replace pm_below with View and ViewEnv
  int pm_width, pm_height, pm_pixels_per_cycle, pm_view_start_c;
  View pm_view;
  ViewEnv ve;
  Segment_Data *pm_sd;
  int ovr_window_resize_count; // Number of times overview drawable resized.

  // Scrolling Animation Information
  //
  Animate_Mode animate_mode;
  double base_accel;
  bool animating;
  bool animating_insn_motion;  // If false, cycle (c) motion.
  int goal_c, goal_idx;
  double accel;
  double velocity_d, view_start_d, view_update_t, goal_d;
  double turn_around_t, arrival_t;

  // View Information -- Updated after each re-paint. (Other info in view, ve)
  // Refers to rob and ped plots.
  //
  int view_first_idx, view_last_idx;     // Refers to visible instructions.
  int view_end_c;
  int view_height_insn;
  int view_idx_adj, view_idx_wrap_base;  // Used to compute idx pixel positions.

  // Drag, Zoom
  //
  int drag_c;
  int drag_top_idx;
  gdouble drag_p;
  int zoom_c_1;
  int zoom_top_idx;
  gdouble zoom_p_1, zoom_x_1;

  // Pointer
  //
  int pointed_idx;
  int pointed_idx_last; // Updated each repaint.
  int cursor_pos_x_at_popup, cursor_pos_y_at_popup;

  // Overview Plot Sort
  typedef enum { OSK_Execution_Order = -1, OSK_Insn_Static_Order = -2 } 
    Ovr_Sort_Key_Order;
  //  int ovr_sort_key;
  PGtkRadioButtonPtr ovr_sort_execution;
  PGtkRadioButtonPtr ovr_sort_pc;

  // Instruction Ordering
  //
  bool indexing_change_timer_cb();
  Insn_Dataset_Info* idx_to_info(int idx) const { return idx_to_info()[idx];}
  Insn_Dataset_Info** idx_to_info() const
  { return ped_plot_idxt ? sd->idxt_to_info : sd->idxst_to_info; }
  int info_to_idx(Insn_Dataset_Info* di) const
  {return ped_plot_idxt ? di->idxt : di->idxst;}
  // If true in tag order, if false program (st) order.
  PGTK_Toggle_Button seg_bttn_ped_order;
  bool ped_plot_idxt;
  double indexing_animation_finish_t;
  double indexing_animation_fraction;
  int indexing_animation_delta_idx;
  bool indexing_animating;

  // Instruction Cursor (Instruction highlighted in plot and disassembly panes.)
  //
  int cursor_idx;
  int cursor_idx_last;
  bool cursor_move_more_recent; // More recent than pointer move.
  int cursor_move_amt;
  bool cursor_at_limit;  // Set to min or max idx, used to stop callback.
  PGTK_Toggle_Button cursor_locked;
  enum Cursor_Cmd
    { CC_Error, CC_Previous, CC_Next, CC_None, CC_Pointer,
      CC_Goto, CC_Init} cursor_cmd;
  int cursor_cmd_goto_idx;

  // Mouse Buttons
  //
  int last_button_up_time_no_motion;
  uint last_button_up_button;
  bool button_down_motion;
  int button3_up_ignore;

  // Disassembly Window and Source Information
  //
  PGTK_Toggle_Button dis_locked;
  p_rectangle dis_drag_grab_rect;
  bool drag_dis_window;
  int drag_dis_window_last_y_p;

  int dis_first_idx, dis_last_idx;
  int dis_max_c; // Largest dequeue time of dis window insns.
  int dis_min_c; // Smallest enqueue time of dis window insns.
  bool restore_unlock_dis;

  // Search
  //
  PGtkToolbarPtr search_toolbar;
  PGtkToolButtonPtr search_bttn_clear;
  PGtkToolButtonPtr search_bttn_first, search_bttn_prev;
  PGtkToolButtonPtr search_bttn_next, search_bttn_last, search_bttn_jump;
  PGtkEntryPtr search_entry;
  int32_t search_pc;
  int32_t search_hit_first_idx, search_hit_last_idx;
  Segment_Data *search_hit_first_segment;
  Segment_Data *search_hit_last_segment;
  bool *search_segment_hit;
  Insn_Dataset_Info** search_hit_ordering;
  ds_num search_tag_or_cycle_absolute;
  Search_Target_Type search_target_type;
  guint search_change_timeout_id;
  Entry_History *search_entry_history;

  // GTK/GDK Elements
  //
  GtkWidget *main_w;
  GtkWidget *plot_vbox;
  PGtkHPanedPtr draw_dis_hpaned;
  PGtkVPanedPtr draw_dis_vpaned;
  GtkWidget *dis_pane_vbox;
  GtkWidget *draw_area;
  GdkWindow *draw_area_win;
  GtkWidget *draw_area_ovr;
  GdkWindow *draw_area_ovr_win;
  GtkWidget *scroll_draw_area;
  GtkWidget *dbutton;
  GtkWidget *seg_bttn_dis_pane_close;
  GtkWidget *seg_menu_check_dis_pane;
  GtkWidget *rbutton;
  GtkWidget *pbutton;
  GtkWidget *dpbutton;
  GtkWidget *seg_bttn_prev_insn, *seg_bttn_next_insn, *seg_bttn_prev_view;
  GtkWidget *seg_bttn_data;
  GtkWidget *menu_check_ovr_plot_values_pane;
  GtkWidget *dis_lines_label;
  GdkCursor *cursor_draw;
  GtkWidget *tool_bars_seg;   // Tool bars used only in segment plots.
  GtkWidget *tool_bar_common; // Tool bar used in segment and overview plot.
  GtkWidget *tool_bar_ovr, *tool_bar_seg, *tool_bar_dis;
  GtkWidget *tool_bar_ped, *tool_bar_rob;
  GtkWidget *context_menu_popup;
  GtkWidget *context_menu_zoom_submenu;

  // Labels above plot area.
  //
  Auto_List <Label_Show_Info> label_show_list;
  void label_show_list_add
  (GtkWidget *label, const char *name, bool printed)
  { label_show_list.put( new Label_Show_Info(label,name,true,printed)); }

  PSE_Label_Set label_set;

  GtkWidget *seg_labels_vbox;  // Used in both overview and seg plots.
  GtkWidget *sta_label;
  GtkWidget *src_label;
  GtkWidget *ipc_label;
  GtkWidget *dis_label;
  PGtkLabelPtr frq_label; // Most frequently executed insn in segment.
  GtkWidget *pos_label;
  GtkWidget *rnk_label;
  GtkWidget *grd_label;
  GtkWidget *seg_plot_src_labels_hbox;
  PGtkLabelPtr seg_plot_annotation_pane_label;
  GtkWidget *seg_plot_annotation_label;
  GdkGC *gc;

  // Overview Plot Series
  //
  Ovr_Plot_PSE_Window_Data *ovr_plot_data;

  // Preferences Dialog
  //
  Pref_Dialog_Info *pref_dialog_info;

  // Annotations
  //
  Annotation_PSE_Window_Data *annotation_data;
  PGtkHBoxPtr annotation_button_and_label;

  // Miscellaneous
  //

  gpointer cb_data_new(char *c){return (gpointer)new Call_Back_Data(this,c); }
  gpointer cb_data_new(char c){return (gpointer)new Call_Back_Data(this,c); }
  gpointer cb_data_new(int i){return (gpointer)new Call_Back_Data(this,i); }

  int get_view_height_insn() const { return view_height_insn; }
  int get_view_width_c();

  // Coordinate Conversion
  //
  inline int p_to_c(int p)
  {
    return (int)( view.start_c + p / view.pixels_per_cycle );
  }

  inline int c_to_p(int c)
  {
    return ( c - view.start_c ) * view.pixels_per_cycle;
  }

  inline ds_num c_to_ac(int c)
  {
    return sd->seg_start_ac + c;
  }

  inline int ac_to_c(ds_num c)
  {
    return c - sd->seg_start_ac;
  }

  inline int c_valid_closest(int c)
  {return min(max(0,c),sd->seg_duration_c);}

  // Pointer Position
  //
  inline int pointer_c(GdkEventButton *be)
  {
    return pointer_c((GdkEventMotion*)be);
  }
  inline int pointer_c(GdkEventMotion *me)
  {
    return p_to_c( int(me->x) );
  }

  inline ds_num pointer_ac(GdkEventButton *be)
  {
    return c_to_ac(pointer_c((GdkEventMotion*)be));
  }
  inline ds_num pointer_ac(GdkEventMotion *be)
  {
    return c_to_ac(pointer_c(be));
  }

  inline int idx_to_vertical_position(int idx)
  {
    const uint idx1 = idx - view_idx_wrap_base;
    if( idx1 < uint(view_height_insn) )  return idx1;

    view_idx_wrap_base += view_height_insn;
    const uint idx2 = idx - view_idx_wrap_base;
    if( idx2 < uint(view_height_insn) ) return idx2;

    const int idx3 = ( idx + view_idx_adj ) % view_height_insn;
    view_idx_wrap_base = idx - idx3;
    return idx3;
  }
  inline int idx_to_p(int idx)
  {
    return idx_to_vertical_position(idx) * view.pixels_per_inst;
  }

  inline int idx_to_p(double idxf)
  {
    const int idx = (int)idxf;
    const int p1 = idx_to_vertical_position(idx) * view.pixels_per_inst;
    return view.pixels_per_inst == 1
      ? p1 : p1 + (int)((idxf-idx) * view.pixels_per_inst);
  }
  inline bool idx_is_visible(int idx) const
  {
    if ( !seg_plot_paint_variables_valid ) return false;
    if( idx < view_first_idx || idx > view_last_idx ) return false;
    Insn_Dataset_Info* const di = idx_to_info(idx);
    return
      di->first_event_c < view_end_c && di->rob_dequeue_c >= view.start_c;
  }

  int idx_reference_get();

  void invalidate_draw_area();
  void insn_order_set();
  void clip_mode_set();

  // Switch between ROB and PED plots.
  //
  void seg_rob_ped_change(bool animate);

  // View Save and Restore
  //
  void push_current_view();
  void restore_previous_view();

  // DMK: One of many ways to change a view, not part of some clean
  // interface...yet! (19 June 2002, 13:02:18 CDT)
  void set_view_start(int start_c, int top_idx, bool force_update = false);

  void segment_goto(Segment_Data *sd);
  void segment_goto(int segment_idx);
  void overview_goto();
  void new_segment();

  // Initialize window using dataset information.  Called once per window.
  void init_at_window_and_dataset_available();
  // Initialize window using new dataset.  Called once per dataset.
  void new_dataset();
  void make_title();

  void paint_ovr(GtkWidget *w, GdkEventExpose *ee);
  void paint_seg(GtkWidget *w, GdkEventExpose *ee);

};

typedef PStackIterator<PSE_Window*> PSE_Windows_Iterator;

typedef void (*WCallback)(GtkWidget*,gpointer);
GtkWidget* insert_stock_icon_button
(GtkWidget *bar, const char *stock_id, WCallback handler, gpointer handler_data,
 const char *tip = NULL, WCallback handler_released = NULL);
GtkWidget* insert_stock_icon_toggle
(GtkWidget *bar, const char *stock_id, WCallback handler, gpointer handler_data,
 bool initial = false, const char *tip = NULL);
GtkWidget* insert_new_button
(GtkWidget *bar, const char *label, WCallback handler, gpointer handler_data,
 const char *tip = NULL);

// DMK: Member function or stand-alone?  C++ takes getting used to. 21 June 2002
gint display_animation_step(gpointer swptr);
void seg_plot_annotations_init(Dataset *ds);

void user_cmd_redisplay(PSE_Window *pw, bool new_seg);

#endif
