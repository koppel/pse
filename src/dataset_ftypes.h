/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2010 Louisiana State University

// $Id$

// Types and constants used in the dataset file.

#ifndef DATASET_FTYPES_H
#define DATASET_FTYPES_H

/// Substantial and non-backward compatible changes will be made
/// in file format until version 1.0.

// Notes on Types:
//
// Byte order is indicated in file. (Not used 31 July 2002, 17:05:46 CDT)
// An IDX is a four-byte index, used to identify the following datum.
//    Some indices are defined in this file, others are user defined.
// A STR is a null terminated string of ordinary single-byte characters.

typedef enum
{
  DSF_Type_Error,
  DSF_Type_IN1,   // One byte signed integer.
  DSF_Type_IN2,   // Etc.
  DSF_Type_IN4,
  DSF_Type_IN8,
  DSF_Type_UI1,   // One byte unsigned integer.
  DSF_Type_UI2,   // Etc.
  DSF_Type_UI4,
  DSF_Type_UI8,
  DSF_Type_SNG,   // IEEE 754 single-precision floating point.
  DSF_Type_DBL,   // IEEE 754 double-precision floating point.
  DSF_Type_STR    // Null-terminated string of bytes.
} DSF_Types;

typedef enum
{
  // Used as a list terminator.
  DSF_CMD_EOL =0, // Must be coded as zero.

  DSF_X_EOF =-1,  // Should not be in dataset file.

  // Valid only at start of file.  Constants re-used after that.
  DSF_CMD_PROLOG_BIG_ENDIAN =1,
  DSF_CMD_PROLOG_LITTLE_ENDIAN =2,
  DSF_CMD_PROLOG_END =3,

  // Two items follow: An index and the indicated type.
  // The index is used to identify the value.
  DSF_CMD_IDX_IN1 =5,  // Also used to indicate type. (For now.)
  DSF_CMD_IDX_IN2,
  DSF_CMD_IDX_IN4,
  DSF_CMD_IDX_IN8,
  DSF_CMD_IDX_UI1,
  DSF_CMD_IDX_UI2,
  DSF_CMD_IDX_UI4,
  DSF_CMD_IDX_SNG,
  DSF_CMD_IDX_DBL,
  DSF_CMD_IDX_STR,

  // Two items follow: A string and the indicated type.
  // The string is used to identify the value.
  DSF_CMD_STR_IN1 =21,
  DSF_CMD_STR_IN2,
  DSF_CMD_STR_IN4,
  DSF_CMD_STR_IN8,
  DSF_CMD_STR_UI1,
  DSF_CMD_STR_UI2,
  DSF_CMD_STR_UI4,
  DSF_CMD_STR_SNG,
  DSF_CMD_STR_DBL,
  DSF_CMD_STR_STR,

  // One item follows: A value of the indicated type.
  // The value is identified by context.
  DSF_CMD_IN1 =31,
  DSF_CMD_IN2,
  DSF_CMD_IN4,
  DSF_CMD_IN8,
  DSF_CMD_UI1,
  DSF_CMD_UI2,
  DSF_CMD_UI4,
  DSF_CMD_SNG,
  DSF_CMD_DBL,
  DSF_CMD_STR,

#if 0
  // Possible extension.
  DSF_CMD_ARRAY_FIXED =41,
  DSF_CMD_ARRAY_VARIABLE,
  DSF_CMD_ARRAY_TERMINATED,
#endif

  // Used to indicate something about bracketed (between begin and end) items,
  // based on the STR or IDX.
  // Some IDX values are reserved for file-level data.
  // Used at the file level to specify file format.  Used at the dataset
  // level to indicate state definitions, simulation variables, etc.
  DSF_CMD_NAMESPACE_STR_BEGIN =50,
  DSF_CMD_NAMESPACE_IDX_BEGIN,
  DSF_CMD_NAMESPACE_END,

  // Used to define a user command.  A user command is intended
  // for specifying high-volume data.
  // CMD (user command number) CMD... (type) STR (name)
  DSF_CMD_DEF_PRIMITIVE_STR =60,
#if 0
  DSF_CMD_DEF_PRIMITIVE_IDX,
  DSF_CMD_DEF_AGGREGATE_STR,
  DSF_CMD_DEF_AGGREGATE_IDX,
  // DMK: Possible changes:  Add IDX type.  Take in to account namespace?
  // 30 July 2002, 13:00:57 CDT
#endif

  // Used to indicate how user command data is coded.  (So far only
  // difference coding used.)  Coding reduces the space needed for
  // slowly changing data and increases the compressibility of certain
  // data.
  DSF_CMD_CODING_SYNC =70,  // Reset coding info.  Used at possible seek target.
  DSF_CMD_CODING_ABSOLUTE,  // User command specifies complete value.
  DSF_CMD_CODING_PLUS_0,    // Value has not changed since last encountered.
  DSF_CMD_CODING_PLUS_1,    // New value is old value plus 1.
  DSF_CMD_CODING_PLUS_IN1,  // New value is old value plus 1 byte integer.
  DSF_CMD_CODING_PLUS_IN2,  // Etc.
  DSF_CMD_CODING_PLUS_IN3,
  // DMK: Possible extensions: FP difference coding. 30 July 2002, 13:01:54 CDT

  // Identify a block boundary.  For now this is used to specify a
  // compressed region but may later be used to specify file
  // structure.
  // FMT(un1) CSIZE(in4) DSIZE(in4):
  // FMT: 0, not compressed; 1, bzip2.
  // CSIZE: Size in file.   DSIZE: Uncompressed size.
  DSF_CMD_BLOCK_START =85,

#if 0
  // Possible extension.
  DSF_CMD_CMD2 =90,  // Two-character command follows.
  DSF_CMD_CMD4,      // Etc.
  DSF_CMD_CMD8,

  DSF_CMD_USR2 =95,  // Two-character user command follows.
  DSF_CMD_USR4,      // Etc.
  DSF_CMD_USR8,
#endif

  DSF_CMD_FIRST_FREE =100,
  DSF_CMD_USER_MAX =255
} _DSF_Command;

typedef int DSF_Command;


/// File-Level Namespace Indices

typedef enum {

  DSF_NS_IDX_File_Format = 0,  // Must be a palindrome.
  DSF_NS_IDX_DC_NS_Hints = 2,
  DSF_NS_IDX_First_Dataset_Index = 4096

} DSF_Namespace_Index;

typedef enum {

  DSF_NS_IDX_FF_Version_Major = 1,
  DSF_NS_IDX_FF_Version_Minor = 2,
  DSF_NS_IDX_FF_Version_Micro = 3

} DSF_NS_Index_File_Format;


// Compile-time conversion of type to command.

template <class T>
inline DSF_Command to_CMD_STR_xxx(T x)
{
  return DSF_Command(DSF_CMD_STR_IN1 - DSF_CMD_IDX_IN1 + to_CMD_xxx(x));
}

inline DSF_Command to_CMD_xxx(int64_t X){return DSF_CMD_IDX_IN8;}
inline DSF_Command to_CMD_xxx(int32_t X){return DSF_CMD_IDX_IN4;}
inline DSF_Command to_CMD_xxx(double X){return DSF_CMD_IDX_DBL;}
inline DSF_Command to_CMD_xxx(char *X){return DSF_CMD_IDX_STR;}

#endif
