/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright 2008 Louisiana State University

// $Id$

#ifndef DECODE_SPARC_H
#define DECODE_SPARC_H

#include "misc.h"
#include <gtk/gtk.h>

typedef uint32_t t_addr; // Address on target.

class Insn_Decode_Info;
class Insn_Stream;

enum Operand_Role {
  OR_rd = 0,
  OR_rs1,
  OR_rs2,
  OR_rs3,
  OR_cc,   // Condition code.
  OR_cb,   // Carry bit.
  OR_pred, // Predicate.
  OR_mult, // Multiply assist (%y in SPARC).
  OR_misc,
  OR_mem,  // Refers to the memory location at the effective address.
  OR_num   // Not an operand type.
};

struct Reg_Info {
  int writer;
  Reg_Info *writer_ri;
  Insn_Decode_Info *writer_si;
  char ri_offset; // Lowest-numbered register of group (pair, quad).
  char role;
  int16_t reg_num_univ;
  char reg_num_insn;
  bool desto; // Otherwise, source.
  int32_t value;
  bool value_valid;
  bool overwrite_pending;  // For registers, 0 or 1; for memory byte vector.
  bool final_use;          // Register value becomes dead.
  int16_t use_count_live;  // Number of uses that don't die out.
  int16_t use_count;  // Number of uses.
  int16_t dataflow_distance;
  int dataflow_subtree;
};

enum Dis_Piece_Type {
  DPT_Error,
  DPT_Register,
  DPT_Punctuation,
  DPT_Constant,
  DPT_CTI_Displacement,
  DPT_Value,
  DPT_Address_Symbol,
  DPT_Address_Value,
  DPT_Mnemnoic

};

struct Dis_Piece {
  Dis_Piece(){ str = NULL; }
  ~Dis_Piece(){ reset(); }
  char *str;
  Dis_Piece_Type type;
  Reg_Info *reg_info;
  void reset(){ if( str ) free(str); }
};


class Insn_Decode_Info {
public:
  friend class Insn_Stream;
  Insn_Decode_Info(Insn_Stream *is, uint32_t text, t_addr pc, int tag);
  Insn_Decode_Info(Reg_Info *ri);
  void finalize();
  bool sources_iterate(Reg_Info* &ri);
  bool dests_iterate(Reg_Info* &ri);
  Dis_Piece *dis_piece_iterate();
  char *get_dis_string();
  int ih;
  uint32_t text;
  t_addr pc;
  int32_t ea; // Effective address.
  bool ea_valid;
  bool dests_unused_direct;
  bool dests_unused_indirect;
  bool dests_unlisted; // Changes registers not listed in reg_info.
  int tag;
  int overwriter_tag_min; // Smallest tag of insn that overwrites dests.
  int ea_base_valid; // Register field with valid value.
  int dataflow_subtree;
  int dataflow_distance;
  Reg_Info *by_role[OR_num];
private:
  static const int dp_limit = 10;
  int dp_iterator, dp_count;
  Dis_Piece dis_piece[dp_limit];
  PStack<Reg_Info> reg_info;
  RString dis_string;

  void disassemble();
  Dis_Piece *dis_piece_alloc();
  void dis_piece_unalloc(int num);
  void by_role_update();
};

struct Reg_File_Info {
  int tag;
  Reg_Info *ri;
  Reg_Info *reader_last_ri;
  Insn_Decode_Info *si;
  Insn_Decode_Info *pre_segment_si;
  int32_t value;
  bool value_valid;
};

class Insn_Stream {
public:
  Insn_Stream();
  ~Insn_Stream();
  friend class Insn_Decode_Info;
  void set_addr_to_source(const char*(*addr_to_source_p)(t_addr addr));
  int cwp;
  Insn_Decode_Info *next_insn(uint32_t text, t_addr pc, int tag,
                              bool doomed, int32_t ea, int cwp);
  bool get_reg_val(Reg_Info *ri, int32_t &value);
  int subtree_base;
private:
  int reg_gpr_insn_to_univ(int cwp, int reg_addr);

  GHashTable *ea_to_info;
  Dis_Piece *dp_base;
  Reg_File_Info *ureg_to_info;
  int ureg_to_info_size;
  // DMK: Need to read window count from file, dependencies not shown correctly
  // if cwp turns over and window_count wrong. 15 July 2003, 12:15:01 CDT
  const static int window_count = 32;
};

bool decode_insn_isa_call(int32_t text);

#endif
