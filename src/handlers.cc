/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2015 Louisiana State University

// $Id$

//
// Handlers
//

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include "malloc.h"
#include "misc.h"
#include "gtk_utility.h"
#include "messages.h"
#include "pse_window.h"
#include "pse.h"
#include "handlers.h"
#include "enames.h"
#include "rti_declarations.h"
#include "annotation_manager.h"
#include "ovr_plot_manager.h"
#include "search.h"

/// Conventions:
//
// Callback Naming Convention:  xxxx_cb_yy_zzz
//
// xxxx: Approximate signal source, compressed to four letters.
//  evnt, bttn (Widget button), and menu.
//
// yy:   Type of signal. (Omitted if unnecessary, e.g., button pushes.)
// zzz:  Object emitting signal. (Omitted if unnecessary, e.g., mouse events.)


//
// Generic Handlers (Called by callbacks.)
//

void
export_postscript(PSE_Window *pw)
{
  if( pw->plot_is_ovr ) return;

  static RString last_dir;
  static RString last_name;
  if( !last_dir ) last_dir.assign_take(g_get_current_dir());

  PGtkDialogPtr chooser =
    gtk_file_chooser_dialog_new
    ("Export Graph To", GTK_WINDOW(pw->main_w),
     GTK_FILE_CHOOSER_ACTION_SAVE,
     GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     NULL);
  GtkFileChooser* const fchooser = GTK_FILE_CHOOSER(chooser);
     
  if( last_name ) gtk_file_chooser_set_current_name(fchooser, last_name);
  gtk_file_chooser_set_current_folder(fchooser, last_dir);
  gtk_dialog_set_default_response(chooser, GTK_RESPONSE_ACCEPT);

  PGtkVBoxPtr vbox = gtk_vbox_new(false,5);
  gtk_file_chooser_set_extra_widget(fchooser,vbox);
  gtk_container_add(vbox,gtk_hseparator_new());

  PGtkLabelPtr ps_label = gtk_label_new(NULL);
  gtk_container_add(vbox,ps_label);
  gtk_label_set_markup
    (ps_label,
     "Enter file name for exported graph above.\n"
     "Use extension \".pdf\" to write PDF,\n"
     "and extension \".eps\" to write EPS (encapsulated PostScript).\n");
  gtk_widget_show_all(vbox);

  RString export_file_format;
  RString export_file_path;

  while(1)
    {
      if( gtk_dialog_run(chooser) != GTK_RESPONSE_ACCEPT )
        {export_file_path = NULL;  break; }

      export_file_path.assign_take(gtk_file_chooser_get_filename(fchooser));

      PSplit parts(export_file_path,'.');
      if( parts == 1 ) export_file_format = "";
      else export_file_format.assign_take(g_ascii_strdown(parts.pop(),-1));

      if( export_file_format != "eps" && export_file_format != "pdf"
          && export_file_format != "png" )
        {
          dialog_warning
            (pw->main_w,
             "Extension %s unrecognized. Use \"eps\" or \"pdf\"",
             export_file_format.s);
          continue;
        }

      struct stat ps_file_stat;
      if( stat(export_file_path, &ps_file_stat) ) break;
      if( dialog_question
          (pw->main_w, "File \"%s\" exists.  Overwrite it?", export_file_path.s)
          == GTK_RESPONSE_YES)
        break;
    }

  gtk_widget_destroy(chooser);
  
  if( !export_file_path ) return;

  last_dir.assign_take(g_path_get_dirname(export_file_path));
  last_name.assign_take(g_path_get_basename(export_file_path));

  bool use_pclose = false;

  if( export_file_format == "eps" )
    {
      pw->ps_file_str = fopen(export_file_path,"w");
    }
  else if( export_file_format == "pdf" )
    {
      RStringF gs_cmd
        ("gs -q -dNOPAUSE -dBATCH -dEPSFitPage "
         "-dDEVICEWIDTHPOINTS=792 -dDEVICEHEIGHTPOINTS=612 "
         "-sDEVICE=pdfwrite -sOutputFile='%s' -",
         export_file_path.s);
      pw->ps_file_str = popen(gs_cmd,"w");
      use_pclose = true;
    }
  else if( export_file_format == "png" )
    {
      // Does not work properly yet. See Bug 376.
      RStringF gs_cmd
        ("gs -q -dNOPAUSE -dBATCH -dTextAlphaBits=4 "
         "-sDEVICE=pngalpha -sOutputFile='%s' -",
         export_file_path.s);
      pw->ps_file_str = popen(gs_cmd,"w");
      use_pclose = true;
    }
  else
    {
      ASSERTA( false );
    }

  if( !pw->ps_file_str )
    {
      dialog_warning
        (pw->main_w,
         "Could not open %s for output: %s.",
         export_file_path.s, strerror(errno));
      return;
    }

  while( Label_Show_Info* label_show_info = pw->label_show_list.iterate() )
    {
      if( label_show_info->show_label_in_ps )
        gtk_widget_show(label_show_info->label);
      else
        gtk_widget_hide(label_show_info->label);
    }

  gtk_widget_hide(pw->tool_bar_dis);
  GtkWidget* const message_pane = gtk_paned_get_child1(pw->draw_dis_vpaned);
  gtk_widget_hide(message_pane);
  yield_to_gtk();
  pw->paint_seg(NULL,NULL);
  gtk_widget_show(message_pane);
  gtk_widget_show(pw->tool_bar_dis);
  while( Label_Show_Info *label_show_info = pw->label_show_list.iterate() )
    {
      if( label_show_info->show_label_in_display )
        gtk_widget_show(label_show_info->label);
      else
        gtk_widget_hide(label_show_info->label);
    }

  if( use_pclose ) pclose(pw->ps_file_str); else fclose(pw->ps_file_str);
  pw->ps_file_str = NULL;
}

void
close_and_maybe_quit(PSE_Window *sw, int quit_all)
{
  Dataset *ds = sw->ds;

  if( !quit_all && --ds->window_count )
    {
      ds->pse_windows.yank(sw);
      gtk_widget_destroy(sw->context_menu_popup);
      gtk_widget_destroy(sw->main_w);
      delete sw;
      return;
    }

  GtkWidget *quit_dialog =
    gtk_dialog_new_with_buttons("Confirm Quit",
			       GTK_WINDOW(sw->main_w),
			       GTK_DIALOG_MODAL,
			       GTK_STOCK_YES,GTK_RESPONSE_YES,
			       GTK_STOCK_NO,GTK_RESPONSE_NO,
			       NULL);

  if( ds->window_count > 1 )
    {
      GtkWidget *quit_label = gtk_label_new(NULL);
      gtk_label_set_markup
        ( GTK_LABEL(quit_label),
          ds->window_count > 1
          ? "Close <i>all</i> windows--not just that one--and exit?"
          : "Do you <b>really</b> and <b>truly</b> want to exit?");

      gtk_container_add(GTK_CONTAINER(GTK_DIALOG(quit_dialog)->vbox),quit_label);
      gtk_window_set_default_size(GTK_WINDOW(quit_dialog),230,100);
      gtk_widget_show_all(quit_dialog);

      gint quit_result = gtk_dialog_run(GTK_DIALOG(quit_dialog));
      gtk_widget_destroy(quit_dialog);

      if( quit_result != GTK_RESPONSE_YES ) return;
    }

  if( rtiv_ovr_series_settings ) ds->opm->series_settings_to_rti();
  ds->am->rti_file_write_prepare();

  for( PSE_Window* pw; ds->pse_windows.pop(pw); )
    {
      gtk_widget_destroy(pw->context_menu_popup);
      gtk_widget_destroy(pw->main_w);
      delete pw;
    }
  gtk_main_quit();
}

gint
motion_handler_segment_plot(GtkWidget *w, GdkEventMotion *me, PSE_Window *sw)
{
  GdkWindow *win = w->window;
  gint w_height, w_width;
  gdk_window_get_size(win,&w_width,&w_height);

  int new_c = sw->pointer_c(me);

  if( sw->drag_c )
    {
      if( sw->animating ) return true;
      {
        int new_top_idx = sw->drag_top_idx
          + int( 0.5 + ( sw->drag_p - me->y ) / sw->view.pixels_per_inst );
        sw->set_view_start(sw->view.start_c - new_c + sw->drag_c, new_top_idx);
      }
      return true;
    }

  if( me->y < sw->segment_plot_top
      || me->y > sw->segment_plot_bottom ) return true;

  sw->invalidate_draw_area();

  return true;
}

gint
motion_handler_ovr_plot(GtkWidget *w, GdkEventMotion *me, PSE_Window *sw)
{
  Dataset *ds = sw->ds;
  if( !ds->dataset_loaded ) return false;
  GdkWindow *win = w->window;
  gint w_height, w_width;
  gdk_window_get_size(win,&w_width,&w_height);

  Auto_List<Segment_Data> &sdata = ds->sdata;

  int left_margin  = ds->ipc_plot_left_margin;
  int right_margin = ds->ipc_plot_right_margin;
  int plot_width   = w_width - left_margin - right_margin;
  int plot_top     = sw->ipc_plot_top_margin;
  int plot_bottom  = w_height;
  int plot_height  = plot_bottom - plot_top;

  if( plot_width < 1 ) return true;
  if( !sdata.occ ) return true;

  double delta_x = (double)plot_width / sdata.occ;
  gint radius = (gint) ( delta_x + 0.8 );
  if( radius < 3 ) radius = 3;

  Segment_Data *prev_sd = sw->ovr_pointed_segment;

  const int curr_iindex =
    min(max(int(( me->x - left_margin ) / delta_x ),0),sdata.occ-1);
  Segment_Data *curr_sd = ds->get_sdata(curr_iindex,sw->plot_ovr_by_ipc);

  if( prev_sd )
    {
      int prev_index =
        sw->plot_ovr_by_ipc ? prev_sd->rank : prev_sd->segment_num;

      if( prev_sd != curr_sd )
        {
          GdkRectangle area;
          area.x = (gint) ( ds->ipc_plot_left_margin
                            + (double)prev_index * delta_x ) - 3;
          area.y = plot_top;
          area.width = ( radius << 1 ) + 6;
          area.height = plot_height;

          sw->ovr_pointed_segment = NULL; // may be changed below.
          gdk_window_invalidate_rect(win,&area,true);
        }
    }
  if( prev_sd == curr_sd ) return true;

  if( curr_iindex < 0 || curr_iindex >= sdata.occ )
    {sw->ovr_pointed_segment = NULL;  return true;}

  GdkRectangle area,text_area;
  text_area.x = 0;
  text_area.y = 0;
  text_area.width = w_width;
  text_area.height = sw->ipc_plot_top_margin;
  gdk_window_invalidate_rect(win,&text_area,true);

  area.x =
    (gint) (ds->ipc_plot_left_margin + (double)curr_iindex * delta_x ) - 1;
  area.y = plot_top;
  area.width = ( radius << 1 ) + 2;
  area.height = plot_height;
  sw->ovr_pointed_segment = curr_sd;
  gdk_window_invalidate_rect(win,&area,true);

  return true;
}


//
// Preferences Dialog
//

struct Pref_Dialog_Info
{
  Pref_Dialog_Info(PSE_Window *pw)
  {
    dialog_construct(pw);
    pw->destruct_hook.insert((Func_vv)destruct,this);
  }
  ~Pref_Dialog_Info();
  static void destruct(Pref_Dialog_Info *moi){ delete moi; }
  Destruct_Hook destruct_hook;
  void dialog_construct(PSE_Window *pw);
  void category_select(int category);
  PGtkDialogPtr pref_dialog;
  GtkTreeSelection *pref_selected;
  GtkTreeIter iter_gridlines;
  GtkWidget *up_gridlines;
  GtkWidget *up_animation;
  GtkWidget *up_labels;
};

void pref_cb_selc
(GtkTreeSelection *pref_selected, Pref_Dialog_Info *user_pref_dialog_ptr);

struct Pref_Grid_Info
{
  PSE_Window* const pw;
  RString label_gridlines;
  int &grid_value;
  int* const rti;
  int rti_dummy;
  GtkWidget *grid_entry;
  GtkWidget *grid_radio_btn;
  bool entry_widget_exists;
  bool radio_button_exists;
  char const button_type;
  bool radio_button_state;
  RString label_gridlines2;
  const int initial_value;
  const bool invert;
  guint entry_change_timeout_id;

  Pref_Grid_Info(PSE_Window *pw, GtkWidget *container, GtkWidget *radio_grp,
                 const char *label_gridlines,
                 int &grid_value, int *rti,
		 bool entry_widget_exists, bool button_exists,
		 bool radio_button_state,const char *label_gridlines2,
		 int initial_value,
                 bool invert = false):
    pw(pw), label_gridlines(label_gridlines),
    grid_value(grid_value), rti( rti ? rti : &rti_dummy ),
    entry_widget_exists(entry_widget_exists),
    radio_button_exists(button_exists),
    button_type( !button_exists ? '0' : radio_grp ? 'r' : 'c' ),
    radio_button_state(radio_button_state),
    label_gridlines2(label_gridlines2),initial_value(initial_value),
    invert(invert)
  {
    entry_change_timeout_id = 0;
    create_widgets(container,radio_grp);
  }

  void create_widgets(GtkWidget *vbox, GtkWidget *radio_grp)
  {
    GtkWidget *hbox = gtk_hbox_new(0,5);
    gtk_box_pack_start(GTK_BOX(vbox),hbox,0,0,5);
    GtkWidget *label_desc = gtk_label_new(label_gridlines);
    const gpointer moi = gpointer(this);

    if( entry_widget_exists )
      {
	grid_entry = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(grid_entry),true);
        gtk_widget_set_size_request(grid_entry,50,-1);
        g_signal_connect
          (G_OBJECT(grid_entry),"focus-out-event",
           G_CALLBACK(callback_focus_out), moi);
        g_signal_connect
          (G_OBJECT(grid_entry),"key-press-event",
           G_CALLBACK(callback_key_press), moi);

        RStringF val_str("%d",initial_value);
        gtk_entry_set_text(GTK_ENTRY(grid_entry),val_str);
      }
    else
      grid_entry = NULL;

    if ( button_type == 'c' )
      {
        PGtkCheckButtonPtr check = grid_radio_btn =
          gtk_check_button_new_with_label(label_gridlines);
        gtk_toggle_button_set_active(check,radio_button_state);
        g_signal_connect(check,"toggled",G_CALLBACK(callback_toggle),moi);
        gtk_box_pack_start(GTK_BOX(hbox),check,0,0,5);
      }
    else if ( radio_button_exists && !entry_widget_exists )
      {
	grid_radio_btn =
	  gtk_radio_button_new_from_widget(GTK_RADIO_BUTTON(radio_grp));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(grid_radio_btn),
				     radio_button_state);

	g_signal_connect
          (G_OBJECT(grid_radio_btn),"toggled",G_CALLBACK(callback_toggle),moi);

	gtk_container_add(GTK_CONTAINER(grid_radio_btn),label_desc);
	gtk_box_pack_start(GTK_BOX(hbox),grid_radio_btn,0,0,5);
      }
    else if ( radio_button_exists && entry_widget_exists )
      {
	grid_radio_btn =
	  gtk_radio_button_new_from_widget(GTK_RADIO_BUTTON(radio_grp));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(grid_radio_btn),
				     radio_button_state);

	g_signal_connect(G_OBJECT(grid_radio_btn),"toggled",
	                 G_CALLBACK(callback_toggle), (gpointer)this);

	gtk_container_add(GTK_CONTAINER(grid_radio_btn),label_desc);
	gtk_box_pack_start(GTK_BOX(hbox),grid_radio_btn,0,0,5);
	gtk_box_pack_start(GTK_BOX(hbox),grid_entry,0,0,0);
      }
    if( !radio_button_exists )
      {
	gtk_box_pack_start(GTK_BOX(hbox),label_desc,0,0,5);
	gtk_box_pack_start(GTK_BOX(hbox),grid_entry,0,0,0);
      }
    if( label_gridlines2 )
      gtk_box_pack_start(GTK_BOX(hbox),gtk_label_new(label_gridlines2),0,0,5);
  }

  bool get_user_option()
  {
    if ( !radio_button_exists )
      {
        const int new_val = atoi(gtk_entry_get_text(GTK_ENTRY(grid_entry)));
        if( new_val == grid_value ) return false;
        *rti = grid_value = new_val;
        return true;
      }

    const bool button_state =
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(grid_radio_btn));

    if ( button_type == 'c' )
      {
        *rti = grid_value = button_state;
        return true;
      }
    if ( !button_state ) return false;

    const int old_val = grid_value;

    if ( grid_entry )
      {
        const int raw_val = atoi(gtk_entry_get_text(GTK_ENTRY(grid_entry)));
        *rti = grid_value = invert ? -raw_val : raw_val;
      }
    else
      {
        *rti = grid_value = initial_value;
      }
    return old_val != grid_value;
  }

  static void callback_toggle
  (GtkToggleButton *radio_button, Pref_Grid_Info *info)
  {
    info->callback_handle();
  } 

  static gboolean callback_focus_out
  (GtkToggleButton *radio_button, GdkEventFocus *e, Pref_Grid_Info *info)
  {
    info->callback_handle();
    return false;
  }
  static gboolean callback_key_press
  (GtkToggleButton *radio_button, GdkEventKey *e, Pref_Grid_Info *info)
  {
    info->callback_key_press_handle();
    return false;
  }

  void callback_handle()
  {
    timeout_cancel();
    if ( !get_user_option() ) return;
    pw->ve.valid = false;
    pw->invalidate_draw_area();
  }
  void callback_key_press_handle()
  {
    timeout_cancel();
    entry_change_timeout_id =
      g_timeout_add(750,(GSourceFunc)callback_timeout,(gpointer)this);
  }
  static gboolean callback_timeout(Pref_Grid_Info *info)
  { info->callback_timeout_handle(); return false; }
  void timeout_cancel()
  {
    if( !entry_change_timeout_id ) return;
    g_source_remove(entry_change_timeout_id);
    entry_change_timeout_id = 0;
  }
  void callback_timeout_handle()
  {
    entry_change_timeout_id = 0;
    callback_handle();
  }
};

struct Pref_Animation_Info
{
  PSE_Window* const pw;
  GtkWidget *animation_radio_btn;
  GtkWidget *hscale;
  RString label_animation;
  bool radio_button_exists;
  Animate_Mode animation_type;

  Pref_Animation_Info(PSE_Window *pw, GtkWidget *vbox, GtkWidget *radio_grp,
                      const char *label_animation, bool radio_button_exists,
		      Animate_Mode animation_type):
    pw(pw),
    label_animation(label_animation),radio_button_exists(radio_button_exists),
    animation_type(animation_type)
  {
    create_widgets(vbox,radio_grp);
  }

  void create_widgets(GtkWidget *vbox,GtkWidget *radio_grp)
  {
    GtkWidget *hbox = gtk_hbox_new(0,5);
    GtkWidget *animation_label = gtk_label_new(label_animation);
    if( radio_button_exists )
      {
	animation_radio_btn =
	  gtk_radio_button_new_from_widget(GTK_RADIO_BUTTON(radio_grp));
	gtk_container_add(GTK_CONTAINER(animation_radio_btn),animation_label);
	gtk_box_pack_start(GTK_BOX(hbox),animation_radio_btn,0,0,5);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,0,0,0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(animation_radio_btn),
                                     rtiv_smooth_scroll_mode == animation_type);
        g_signal_connect
          (animation_radio_btn,"toggled",
           G_CALLBACK(callback_radio),gpointer(this));
      }
    else
      {
	hscale = gtk_hscale_new_with_range(100.0,20000.0,500.0);
	gtk_scale_set_draw_value(GTK_SCALE(hscale),1);
	gtk_scale_set_value_pos(GTK_SCALE(hscale),GTK_POS_TOP);
	gtk_range_set_value(GTK_RANGE(hscale),pw->base_accel);
	gtk_box_pack_start(GTK_BOX(hbox),animation_label,0,0,5);
	gtk_box_pack_start(GTK_BOX(hbox),hscale,1,1,5);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,0,0,0);
        g_signal_connect
          (hscale,"value-changed", G_CALLBACK(callback_hscale),gpointer(this));
      }
  }

  static void callback_radio
  (GtkToggleButton *radio_button, Pref_Animation_Info *info)
  {
    if ( !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio_button)) )
      return;
    rtiv_smooth_scroll_mode = info->pw->animate_mode = info->animation_type;
  }

  static void callback_hscale(GtkRange *range, Pref_Animation_Info *info)
  {
    rtiv_smooth_scroll_accel = info->pw->base_accel =
      gtk_range_get_value(range);
  }
};

struct Pref_Label_Info
{
  Label_Show_Info* const info;
  GtkWidget *print_button;
  GtkWidget *plot_button;

  Pref_Label_Info(GtkWidget *vbox, Label_Show_Info *info):info(info)
  { create_widgets(vbox); }

  void create_widgets(GtkWidget *vbox)
  {
    GtkWidget *hbox = gtk_hbox_new(0,5);
    print_button = gtk_check_button_new();
    plot_button = gtk_check_button_new();
    PGtkLabelPtr pref_btn_label = gtk_label_new(info->label_name);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(plot_button),
				 info->show_label_in_display);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(print_button),
				 info->show_label_in_ps);
    g_signal_connect
      (plot_button,"toggled", G_CALLBACK(callback_toggled),gpointer(this));
    g_signal_connect
      (print_button,"toggled", G_CALLBACK(callback_toggled),gpointer(this));

    gtk_box_pack_start(GTK_BOX(hbox),plot_button,0,0,5);
    gtk_box_pack_start(GTK_BOX(hbox),print_button,0,0,10);
    gtk_box_pack_start(GTK_BOX(hbox),pref_btn_label,0,0,35);
    gtk_container_add(GTK_CONTAINER(vbox),hbox);
  }

  static void callback_toggled
  (GtkToggleButton *toggle_button, Pref_Label_Info *li)
  {
    Label_Show_Info* const si = li->info;

    si->show_label_in_display =
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(li->plot_button));
    si->show_label_in_ps =
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(li->print_button));

    if( si->show_label_in_display ) gtk_widget_show(si->label);
    else                            gtk_widget_hide(si->label);
  }
};

void
menu_cb_pref(gpointer pwptr, int action, GtkWidget *w)
{
  PSE_Window* const pw = (PSE_Window*) pwptr;
  if( !pw->pref_dialog_info ) pw->pref_dialog_info = new Pref_Dialog_Info(pw);
  if( action ) return;
  gtk_window_present(pw->pref_dialog_info->pref_dialog);
}

void
Pref_Dialog_Info::dialog_construct(PSE_Window *sw)
{
  pref_dialog = gtk_dialog_new_with_buttons
    ("Preferences", NULL, GTK_DIALOG_DESTROY_WITH_PARENT, NULL);

  g_signal_connect
    (pref_dialog, "delete_event",
     G_CALLBACK(gtk_widget_hide_on_delete), pref_dialog);
  g_signal_connect
    (pref_dialog, "response",
     G_CALLBACK(gtk_widget_hide), pref_dialog);

  GtkListStore *pref_list = gtk_list_store_new(1,G_TYPE_STRING);

  gtk_list_store_append(pref_list,&iter_gridlines);
  gtk_list_store_set(pref_list,&iter_gridlines,0,"Gridlines",-1);
  GtkTreeIter iter;
  gtk_list_store_append(pref_list,&iter);
  gtk_list_store_set(pref_list,&iter,0,"Scrolling",-1);
  gtk_list_store_append(pref_list,&iter);
  gtk_list_store_set(pref_list,&iter,0,"Labels",-1);

  GtkWidget *pref_list_view =
    gtk_tree_view_new_with_model(GTK_TREE_MODEL(pref_list));

  GtkCellRenderer *rer= gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(rer),"foreground","blue",NULL);
  GtkTreeViewColumn *column1 =
    gtk_tree_view_column_new_with_attributes("",rer,"text",0,NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(pref_list_view),column1);

  GtkWidget *animation_vbox = gtk_vbox_new(1,0);
  GtkWidget *animation_vbox_align = gtk_alignment_new(0,0,1,0.2);
  gtk_container_add(GTK_CONTAINER(animation_vbox_align),animation_vbox);
  GtkWidget *radio_animation_grp = gtk_radio_button_new(NULL);

  new Pref_Animation_Info
    (sw, animation_vbox, radio_animation_grp, "Never Scroll Smoothly",
     true, AM_None);

  new Pref_Animation_Info
    (sw, animation_vbox, radio_animation_grp, "Always Scroll Smoothly",
     true, AM_All);

  new Pref_Animation_Info
    (sw, animation_vbox, radio_animation_grp, "Scroll Smoothly on Short Pans",
     true, AM_Short);

  new Pref_Animation_Info
    (sw, animation_vbox, radio_animation_grp, "Smooth Scroll Acceleration",
     false, AM_None);

  GtkWidget* const grid_inst_vbox = gtk_vbox_new(false,5);
  GtkWidget *radio_inst_grp = gtk_radio_button_new(NULL);

  new Pref_Grid_Info
    (sw,grid_inst_vbox, NULL, "Spacing is multiple of decode width.",
     sw->opt_grid_mult_decode_width, &rtiv_grid_mult_decode_width,
     false, true, sw->opt_grid_mult_decode_width, NULL, 0);
  const bool fixed_insn_spacing = sw->grid_spacing_inst > 0;
  new Pref_Grid_Info
    (sw, grid_inst_vbox, radio_inst_grp, "Manual:",
     sw->grid_spacing_inst, &rtiv_grid_spacing_inst,
     true, true, fixed_insn_spacing,
     "instructions between gridlines.",
     fixed_insn_spacing ? sw->grid_spacing_inst : 10);
  new Pref_Grid_Info
    (sw,grid_inst_vbox,radio_inst_grp,"Auto: dense.",
     sw->grid_spacing_inst, &rtiv_grid_spacing_inst,
     false,true,false,NULL, -10);
  //                        RTI,  Ent exists, radio ex, radio state.
  new Pref_Grid_Info
    (sw,grid_inst_vbox,radio_inst_grp,"Auto: medium.",
     sw->grid_spacing_inst, &rtiv_grid_spacing_inst,
     false,true,false,NULL, -50);
  new Pref_Grid_Info
    (sw,grid_inst_vbox,radio_inst_grp,"Auto: sparse.",
     sw->grid_spacing_inst, &rtiv_grid_spacing_inst,
     false,true,false,NULL, -200);
  new Pref_Grid_Info
    (sw,grid_inst_vbox,radio_inst_grp,"Auto:",
     sw->grid_spacing_inst, &rtiv_grid_spacing_inst,
     true, true, !fixed_insn_spacing,
     "minimum pixels between gridlines.",
     fixed_insn_spacing ? 15 : -sw->grid_spacing_inst, true);
  new Pref_Grid_Info
    (sw,grid_inst_vbox,NULL,
     "No gap between instructions if less than",
     sw->grid_spacing_inst_unit_threshold,
     &rtiv_grid_spacing_inst_unit_threshold, 
     true,false,false,
     "pixels apart.",
     sw->grid_spacing_inst_unit_threshold);

  GtkWidget* const grid_cyc_vbox = gtk_vbox_new(false,5);
  GtkWidget *radio_cyc_grp = gtk_radio_button_new(NULL);
  const bool fixed_cyc_spacing = sw->grid_spacing_cyc > 0;

  new Pref_Grid_Info
    (sw,grid_cyc_vbox, NULL, "Paced",
     sw->opt_grid_paced, &rtiv_grid_paced, false, true, sw->opt_grid_paced,
     NULL, 0);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,"Manual:",
     sw->grid_spacing_cyc, &rtiv_grid_spacing_cyc,
     true, true, fixed_cyc_spacing,
     "cycles between gridlines.",
     fixed_cyc_spacing ? sw->grid_spacing_cyc : 10);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,"Auto: dense.",
     sw->grid_spacing_cyc, &rtiv_grid_spacing_cyc, false,true,false, NULL,-10);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,"Auto: medium.",
     sw->grid_spacing_cyc, &rtiv_grid_spacing_cyc, false,true,false, NULL,-50);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,"Auto: sparse.",
     sw->grid_spacing_cyc, &rtiv_grid_spacing_cyc, false,true,false, NULL,-200);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,"Auto:",
     sw->grid_spacing_cyc, &rtiv_grid_spacing_cyc,
     true, true, !fixed_cyc_spacing,
     "pixels between gridlines.",
     fixed_cyc_spacing ? 20 : -sw->grid_spacing_cyc, true);
  new Pref_Grid_Info
    (sw,grid_cyc_vbox,radio_cyc_grp,
     "No gap between cycles if less than",
     sw->grid_spacing_cyc_unit_threshold,
     &rtiv_grid_spacing_cyc_unit_threshold,
     true,false,false,
     "pixels apart.",
     sw->grid_spacing_cyc_unit_threshold);

  GtkWidget *label_vbox = gtk_vbox_new(1,5);
  GtkWidget *label_vbox_align = gtk_alignment_new(0,0,0,0.3);
  gtk_container_add(GTK_CONTAINER(label_vbox_align),label_vbox);
  GtkWidget *plot_label = gtk_label_new(NULL);
  GtkWidget *ps_label = gtk_label_new(NULL);
  GtkWidget *label_box = gtk_hbox_new(0,0);
  gtk_box_pack_start(GTK_BOX(label_box),plot_label,0,0,5);
  gtk_box_pack_start(GTK_BOX(label_box),ps_label,0,0,5);
  gtk_box_pack_start(GTK_BOX(label_vbox),label_box,0,0,5);

  const char* const cplot_m =
    "<span weight=\"bold\" size=\"13000\">Displayed</span>";
  gtk_label_set_markup(GTK_LABEL(plot_label), cplot_m);

  const char* const cps_m =
    "<span weight=\"bold\" size=\"13000\">Printed</span>";
  gtk_label_set_markup(GTK_LABEL(ps_label), cps_m);

  while( Label_Show_Info* const li = sw->label_show_list.iterate() )
    new Pref_Label_Info(label_vbox, li);

  GtkWidget *frame_labels_sett = gtk_frame_new(NULL);
  GtkWidget *labels_frame_label = gtk_label_new(NULL);
  const char* const labels_frame_m ="<span weight=\"bold\" size=\"15000\">"
    "Visible Labels</span>";

  gtk_label_set_markup(GTK_LABEL(labels_frame_label), labels_frame_m);

  gtk_frame_set_label_widget(GTK_FRAME(frame_labels_sett),
			     labels_frame_label);
  gtk_frame_set_shadow_type(GTK_FRAME(frame_labels_sett),GTK_SHADOW_ETCHED_IN);

  GtkWidget *frame_animation_sett = gtk_frame_new(NULL);
  GtkWidget *labels_animation_label = gtk_label_new(NULL);
  const char* const animation_frame_m ="<span weight=\"bold\" size=\"15000\">"
    "Scroll Settings</span>";

  gtk_label_set_markup(GTK_LABEL(labels_animation_label), animation_frame_m);

  gtk_frame_set_label_widget(GTK_FRAME(frame_animation_sett),
			     labels_animation_label);
  gtk_frame_set_shadow_type(GTK_FRAME(frame_animation_sett),
			    GTK_SHADOW_ETCHED_IN);

  GtkWidget *frame_gridlines_sett = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame_gridlines_sett),
			    GTK_SHADOW_NONE);
  GtkWidget *pref_inst_frame = gtk_frame_new(NULL);
  GtkWidget *pref_inst_frame_label = gtk_label_new(NULL);
  const char* const pref_inst_frame_m ="<span weight=\"bold\" size=\"13000\">"
    "Horizontal Gridlines</span>";
  gtk_label_set_markup(GTK_LABEL(pref_inst_frame_label), pref_inst_frame_m);
  gtk_frame_set_label_widget(GTK_FRAME(pref_inst_frame),
			     pref_inst_frame_label);

  GtkWidget *pref_cyc_frame = gtk_frame_new(NULL);
  GtkWidget *pref_cyc_frame_label = gtk_label_new(NULL);
  const char* const pref_cyc_frame_m ="<span weight=\"bold\" size=\"13000\">"
    "Vertical Gridlines</span>";
  gtk_label_set_markup(GTK_LABEL(pref_cyc_frame_label), pref_cyc_frame_m);
  gtk_frame_set_label_widget(GTK_FRAME(pref_cyc_frame),
			     pref_cyc_frame_label);
  GtkWidget *inst_cyc_vbox = gtk_vbox_new(0,10);

  gtk_container_add(GTK_CONTAINER(pref_inst_frame),grid_inst_vbox);
  gtk_container_add(GTK_CONTAINER(pref_cyc_frame),grid_cyc_vbox);
  gtk_box_pack_start(GTK_BOX(inst_cyc_vbox),pref_inst_frame,false,false,5);
  gtk_box_pack_start(GTK_BOX(inst_cyc_vbox),pref_cyc_frame,false,false,5);

  gtk_container_add(GTK_CONTAINER(frame_labels_sett),label_vbox_align);
  gtk_container_add(GTK_CONTAINER(frame_gridlines_sett),inst_cyc_vbox);
  gtk_container_add(GTK_CONTAINER(frame_animation_sett),animation_vbox_align);
  up_labels = frame_labels_sett;
  up_gridlines = frame_gridlines_sett;
  up_animation = frame_animation_sett;

  PGtkVBoxPtr main_vbox = gtk_vbox_new(0,0);

  gtk_box_pack_start(main_vbox, frame_labels_sett, false, false, 0);
  gtk_box_pack_start(main_vbox, frame_animation_sett, false, false, 0);
  gtk_box_pack_start(main_vbox, frame_gridlines_sett, false, false, 0);

  gtk_widget_show_all(main_vbox);

  gtk_widget_hide(frame_animation_sett);
  gtk_widget_hide(frame_labels_sett);

  PGtkHBoxPtr main_hbox = gtk_hbox_new(0,0);
  gtk_box_pack_start(main_hbox,pref_list_view, false, false, 0);

  GtkContainer *vbox = GTK_CONTAINER(GTK_DIALOG(pref_dialog)->vbox);
  gtk_container_add(vbox,main_hbox);

  pref_selected = gtk_tree_view_get_selection(GTK_TREE_VIEW(pref_list_view));
  g_signal_connect(G_OBJECT(pref_selected),"changed",
		   G_CALLBACK(pref_cb_selc), (gpointer)this);

  gtk_widget_show_all(pref_dialog);
  gtk_box_pack_start(main_hbox,main_vbox, true, true, 5);
}

Pref_Dialog_Info::~Pref_Dialog_Info()
{
  if( !pref_dialog ) return;
  gtk_widget_destroy(pref_dialog);
}

void pref_show_gridlines(PSE_Window *pw)
{
  menu_cb_pref(pw,1,NULL);
  Pref_Dialog_Info* const pdi = pw->pref_dialog_info;
  gtk_tree_selection_select_iter(pdi->pref_selected,&pdi->iter_gridlines);
  menu_cb_pref(pw,0,NULL);
}

void
pref_cb_selc
(GtkTreeSelection *pref_selected, Pref_Dialog_Info *user_pref_dialog_ptr)
{
  GtkTreeIter itr;
  GtkTreeModel *model;
  gchar *preference;

  if( gtk_tree_selection_get_selected(pref_selected,&model,&itr) )
    {
      gtk_tree_model_get(model,&itr,0,&preference,-1);

      if( !strcmp(preference,"Gridlines") )
	{
	  gtk_widget_show(user_pref_dialog_ptr->up_gridlines);
	  gtk_widget_hide(user_pref_dialog_ptr->up_labels);
	  gtk_widget_hide(user_pref_dialog_ptr->up_animation);
	}
      else if( !strcmp(preference,"Scrolling") )
	{
	  gtk_widget_show(user_pref_dialog_ptr->up_animation);
	  gtk_widget_hide(user_pref_dialog_ptr->up_labels);
	  gtk_widget_hide(user_pref_dialog_ptr->up_gridlines);
	}
      else if( !strcmp(preference,"Labels") )
	{
	  gtk_widget_show(user_pref_dialog_ptr->up_labels);
	  gtk_widget_hide(user_pref_dialog_ptr->up_animation);
	  gtk_widget_hide(user_pref_dialog_ptr->up_gridlines);
	}
    }
}


//
// Menu Item Handlers
//

void
menu_cb_open(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  GtkWidget *parent = sw->main_w;
  Dataset *ds = sw->ds;

  if(ds->file_name)
    {
      GtkWidget *sorry = gtk_message_dialog_new
        (GTK_WINDOW(parent), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO,
         GTK_BUTTONS_OK,
         "Loading of multiple datasets is not yet implemented.");
      gtk_dialog_run(GTK_DIALOG(sorry));
      gtk_widget_destroy(sorry);
      return;
    }

  char *env_dir = getenv("PSE_DIR");
  gchar *start_dir = env_dir ? g_strdup(env_dir) : g_get_current_dir();

  char *open_file_path =
        file_browse_dialog(parent, "Open Dataset", start_dir, ".ds");
  g_free(start_dir);
  if(!open_file_path) return;

  if(app.last_file_loaded_dir) free(app.last_file_loaded_dir);
  app.last_file_loaded_dir = g_path_get_dirname(open_file_path);
  ds->file_name = open_file_path;
  ds->new_dataset();
}


void
menu_cb_close(gpointer swptr, int quit_all, GtkWidget *w)
{
  close_and_maybe_quit((PSE_Window*)swptr,quit_all);
}

void
menu_cb_clone(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window* const pw = (PSE_Window*) swptr;
  PSE_Window* const pw_new = new PSE_Window(pw->ds);
  gtk_widget_show(pw_new->tool_bar_common);

  if( pw->plot_is_ovr )
    {
      pw_new->overview_goto();
    }
  else
    {
      // This isn't complete.
      pw_new->segment_goto(pw->sd);
      pw_new->view = pw->view;
      pw_new->ve = pw->ve;
      pw_new->ve.pm_width = 0;
      pw_new->cursor_idx = pw->cursor_idx;
      pw_new->view_start_d = pw->view_start_d;
      pw_new->goal_d = pw->goal_d;
      pw_new->goal_c = pw->goal_c;
      pw_new->plot_ped_clipped = pw->plot_ped_clipped;
    }

  PGtk_Suppress_Handler b(gpointer(pw_new->cbutton),gpointer(bttn_cb_clip));
  gtk_toggle_button_set_active
    (GTK_TOGGLE_BUTTON(pw_new->cbutton),pw_new->plot_ped_clipped);
}


void
colorkey_assign_colors(Dataset *ds, int i)
{
  Colorkey_Info &cki = ds->colorkey_info[i];
  GdkWindow *colorwin = cki.draw_area->window;
  cki.gc = gdk_gc_new(GDK_DRAWABLE(colorwin));
  gdk_gc_set_foreground(cki.gc,&ds->state_info[i].gdk_color);
}

void
evnt_cb_expose_colorkey(GtkWidget *w, GdkEventExpose *ee, gpointer swptr)
{
  Colorkey_Info* ciptr = (Colorkey_Info*)swptr;
  gint w_height, w_width;
  GdkWindow *win = w->window;
  gdk_window_get_size(win,&w_width,&w_height);
  int margin = 6;
  gdk_draw_rectangle
    (GDK_DRAWABLE(win),ciptr->gc,TRUE,margin,1,w_width-(margin<<1),w_height-1);
}

GtkWidget*
colorkey_build_hbox(Dataset *ds, int i)
{
  Colorkey_Info &cki = ds->colorkey_info[i];
  cki.colorkey_hbox = gtk_hbox_new(false,0);
  cki.align = gtk_alignment_new(0,0,0,0);
  cki.draw_area = gtk_drawing_area_new();
  gtk_widget_set_size_request(cki.draw_area,25,-1);
  g_signal_connect(G_OBJECT(cki.draw_area), "expose_event",
		   G_CALLBACK(evnt_cb_expose_colorkey), (gpointer)&cki);

  gtk_box_pack_start(GTK_BOX(cki.colorkey_hbox),cki.draw_area,false,true,0);
  gtk_box_pack_start(GTK_BOX(cki.colorkey_hbox), cki.align, false, true, 0);
  gtk_widget_add_events(cki.draw_area, GDK_EXPOSURE_MASK );
  const char* const state_name = ds->state_info[i].name;
  cki.statename_label = gtk_label_new(state_name);
  gtk_container_add(GTK_CONTAINER(cki.align), cki.statename_label);
  return cki.colorkey_hbox;
}

void
menu_cb_colorkey(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window *) swptr;
  Dataset *ds = sw->ds;

  if( ds->color_key_widget )
    {
      gtk_widget_show (ds->color_key_widget);
      return;
    }

  GtkWidget *color_key_w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW(color_key_w),
                        "Instruction State/Event Key - PSE");
  ds->color_key_widget = color_key_w;

  GtkWidget *colorkey_vbox = gtk_vbox_new(true,0);

  for( int i = 100; i<256; i++ ) if( ds->state_info[i].name )
    gtk_box_pack_start(GTK_BOX(colorkey_vbox),
                       colorkey_build_hbox(ds,i),false,false,3);

  gtk_container_add(GTK_CONTAINER(color_key_w),colorkey_vbox);
  gtk_widget_show_all(color_key_w);

  for( int i = 100; i<256; i++ ) if( ds->state_info[i].name )
    colorkey_assign_colors(ds,i);

  g_signal_connect(G_OBJECT(color_key_w), "delete_event",
                   G_CALLBACK(gtk_widget_hide_on_delete),
                   (gpointer)color_key_w);
}

gint
evnt_cb_data_button_up(GtkWidget *w, GdkEventButton *be, gpointer swptr)
{
  GtkTreeView* const tree_view = GTK_TREE_VIEW(w);
  PSE_Window* const sw = (PSE_Window *)swptr;
  GtkTreeModel* const model = GTK_TREE_MODEL(sw->ds->data_tree);
  GtkTreeIter iter;
  GtkTreePath *path;
  const gint pos_x = (gint)be->x, pos_y = (gint)be->y;
  const bool is_row = gtk_tree_view_get_path_at_pos(tree_view,pos_x,pos_y,
                                                    &path, NULL, NULL, NULL);
  bool ret = false;

  if( be->type == GDK_2BUTTON_PRESS && is_row )
    {
      gchar *variable_name, *val_full;

      gtk_tree_model_get_iter(model, &iter, path);
      gtk_tree_model_get(model, &iter, 0, &variable_name, 5, &val_full, -1);

      if( val_full )
        {
          RString title(variable_name);
          title += " - ";
          title += sw->title;

          GtkWidget* const label = gtk_label_new(val_full);
          GtkWidget* const scrolled_w = gtk_scrolled_window_new(NULL, NULL);
          GtkWidget* const dialog =
             gtk_dialog_new_with_buttons(title, NULL,
                                         GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_STOCK_OK,
                                         GTK_RESPONSE_OK,
                                         NULL);
          gtk_window_set_type_hint(GTK_WINDOW(dialog),
                                   GDK_WINDOW_TYPE_HINT_NORMAL);

          gtk_window_set_decorated(GTK_WINDOW(dialog), true);
          gtk_window_set_default_size(GTK_WINDOW(dialog), 475, 500);
          gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_w),
                                         GTK_POLICY_AUTOMATIC,
                                         GTK_POLICY_AUTOMATIC);

          g_signal_connect_swapped(G_OBJECT(dialog), "response",
                                   G_CALLBACK(gtk_widget_destroy),
                                   (gpointer)dialog);

          gtk_label_set_selectable(GTK_LABEL(label), true);
          gtk_scrolled_window_add_with_viewport
                                       (GTK_SCROLLED_WINDOW(scrolled_w), label);
          gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),scrolled_w);

          gtk_widget_show_all(dialog);
        }

      ret = true;
    }

  if( path ) gtk_tree_path_free(path);
  return ret;
}

void
data_cb_selc(GtkTreeSelection *data_var_selected,gpointer data)
{
  GtkTreeModel *model;
  GtkTreeIter itr;
  if( !gtk_tree_selection_get_selected(data_var_selected,&model,&itr) ) return;

  gchar *doc_string;
  gtk_tree_model_get(model,&itr,2,&doc_string,-1);
  if( !doc_string || ! *doc_string ) return;

  Dataset *ds = (Dataset *)data;

  gtk_label_set_text(GTK_LABEL(ds->doc_message_label),doc_string);
  gtk_widget_show_all(ds->doc_label_frame);
}

void
menu_cb_data(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window *) swptr;
  Dataset *ds = sw->ds;

  if( ds->data_widget )
    {
      gtk_widget_show (ds->data_widget);
    }
  else
    {
      GtkWidget *data_w = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      ds->data_widget = data_w;
      GtkWidget *data_vbox = gtk_vbox_new(false,0);
      GtkWidget *data_sw = gtk_scrolled_window_new(NULL,NULL);
      gtk_window_set_default_size(GTK_WINDOW(data_w),500,700);
      gtk_window_set_title (GTK_WINDOW(data_w), "Dataset Variables");

      ds->doc_message_label = gtk_label_new(NULL);

      gtk_misc_set_alignment(GTK_MISC(ds->doc_message_label),0,0);
      gtk_label_set_line_wrap(GTK_LABEL(ds->doc_message_label),true);

      ds->doc_label_frame = gtk_frame_new(NULL);
      gtk_container_add(GTK_CONTAINER(ds->doc_label_frame),
                        ds->doc_message_label);
      gtk_frame_set_shadow_type(GTK_FRAME(ds->doc_label_frame),GTK_SHADOW_IN);

      gtk_box_pack_start (GTK_BOX(data_vbox),data_sw,true,true,0);
      gtk_box_pack_start (GTK_BOX(data_vbox),ds->doc_label_frame,false,true,0);
      gtk_container_add (GTK_CONTAINER(data_w), data_vbox);
      gtk_container_add (GTK_CONTAINER(data_sw), ds->tree_widget);

      gtk_widget_show_all(data_w);

      g_signal_connect(G_OBJECT(data_w), "delete_event",
                       G_CALLBACK(gtk_widget_hide_on_delete),
                       (gpointer)data_w);
      gtk_tree_view_column_clicked(ds->name_column);
    }
}

void
menu_cb_ps(gpointer swptr, int action, GtkWidget *w)
{
  export_postscript((PSE_Window*)swptr);
}

void
menu_cb_about(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  GtkWidget *dialog =
    gtk_dialog_new_with_buttons("About",
                                GTK_WINDOW(sw->main_w),
                                GTK_DIALOG_DESTROY_WITH_PARENT,
                                GTK_STOCK_OK,
                                GTK_RESPONSE_NONE,
                                NULL);

  char *file_format =
    sw->ds->dataset_loaded
    ? g_strdup_printf(";  Loaded dataset version %d.%d.%d\n",
                      sw->ds->df.file_version_major,
                      sw->ds->df.file_version_minor,
                      sw->ds->df.file_version_micro)
    : strdup("\n"); // Avoid compiler warning.

  char *msg =
    g_strdup_printf("<span foreground=\"#580da6\">"
                    "<span size=\"20000\" weight=\"bold\">PSE</span>\n"
                    "Processor Simulation Elucidator\n"
                    "Viewer version %d.%d.%d;  Reader version %d.%d.%d%s"
                    "GTK Compiled %d.%d.%d,  Linked %d.%d.%d\n"
                    "Division of Electrical and Computer Engineering\n"
                    "Louisiana State University\n"
                    "Copyright (c) 2015 Louisiana State University</span>",
                    app.version_major,app.version_minor,app.version_micro,
                    sw->ds->df.version_major,
                    sw->ds->df.version_minor,
                    sw->ds->df.version_micro,
                    file_format,
                    GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION,
                    gtk_major_version, gtk_minor_version, gtk_micro_version
                    );

  free(file_format);

  GtkWidget *label = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(label),msg);
  free(msg);

  GtkWidget *vbox = GTK_DIALOG(dialog)->vbox;
  gtk_box_pack_start(GTK_BOX(vbox), label, false, false, 0);

  gtk_misc_set_padding(GTK_MISC(label),10,10);

  GdkColor gold;
  gold.red = 0xfb80;
  gold.green = 0x9f80;
  gold.blue = 0;

  gtk_widget_modify_bg(dialog, GTK_STATE_NORMAL, &gold);

#if 0
  // DMK: Need to figure out why this doesn't work.
  GdkColor purple;
  purple.red = 0xa0a3;
  purple.green = 0x2021;
  purple.blue = 0xf0f1;
  gtk_widget_modify_base(dialog, GTK_STATE_NORMAL, &purple);
  gtk_widget_modify_text(label, GTK_STATE_NORMAL, &purple);
#endif

  gtk_widget_show_all(dialog);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
}


//
// Widget Button Handlers
//

void
bttn_cb_data(GtkWidget *w, gpointer swptr)
{
  menu_cb_data((PSE_Window*)swptr,0,w);
}

void
bttn_cb_decode_pos(GtkWidget *w, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  sw->plot_rob_decode_positioning =
   gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w)) ? PRDP_Fit : PRDP_ROB_Size;
  sw->invalidate_draw_area();
}

void
bttn_cb_clip(GtkWidget *w, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  int bttn_clip_active = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w));
  gtk_check_menu_item_set_active
   (GTK_CHECK_MENU_ITEM(sw->context_menu_clip_item), bttn_clip_active);
  rtiv_plot_ped_clipped = sw->plot_ped_clipped = bttn_clip_active;
  sw->clip_mode_set();
}

void
bttn_cb_seg_insn_order(GtkWidget *w, gpointer swptr)
{((PSE_Window*) swptr)->insn_order_set();}

void
bttn_cb_ped(GtkWidget *w, gpointer ptr)
{
  ((PSE_Window*)ptr)->seg_rob_ped_change(true);
}

void
bttn_cb_ps(GtkWidget *w, gpointer swptr)
{
  export_postscript((PSE_Window*) swptr);
}

static gboolean
time_cb_insn(gpointer cdptr)
{
  Call_Back_Data *cd = (Call_Back_Data*) cdptr;
  if( cd->sw->cursor_at_limit && cd->timeout_handler_id )
    {
      g_source_remove(cd->timeout_handler_id);
      cd->timeout_handler_id = 0;
      return false;
    }
  menu_cb_insn_cursor(cd->sw,cd->c,NULL);
  const int delay = cd->timeout_handler_id ? 100 : 500;
  cd->timeout_handler_id = g_timeout_add(delay,time_cb_insn,cdptr);
  return false;
}
void
bttn_cb_insn(GtkWidget *w, gpointer cdptr)
{
  Call_Back_Data *cd = (Call_Back_Data*) cdptr;
  menu_cb_insn_cursor(cd->sw,cd->c,w);
}
void
bttn_cb_insn_pressed(GtkWidget *w, gpointer cdptr)
{
  time_cb_insn(cdptr);
}
void
bttn_cb_insn_released(GtkWidget *w, gpointer cdptr)
{
  Call_Back_Data *cd = (Call_Back_Data*) cdptr;
  if( !cd->timeout_handler_id ) return;
  g_source_remove(cd->timeout_handler_id);
  cd->timeout_handler_id = 0;
}

void
bttn_cb_zoom(GtkWidget *w, gpointer cdptr)
{
  Call_Back_Data *cd = (Call_Back_Data*) cdptr;
  menu_cb_zoom(cd->sw,cd->c,w);
}

void
bttn_cb_dis_window(GtkWidget *w, gpointer swptr)
{
  PSE_Window* const sw = (PSE_Window*) swptr;
  gtk_check_menu_item_set_active
    (GTK_CHECK_MENU_ITEM(sw->seg_menu_check_dis_pane),false);
}

void
bttn_cb_display(GtkWidget *w, gpointer pwptr)
{
  PSE_Window* pw = (PSE_Window*) pwptr;
  const bool plot_to_overview =
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w));
  if( plot_to_overview ) pw->overview_goto(); else pw->segment_goto(pw->sd);
}


//
// Mouse and Keyboard Events
//

gint
evnt_cb_scroll(GtkWidget *w, GdkEventScroll *e, gpointer swptr)
{
  PSE_Window* const pw = (PSE_Window*) swptr;
  if( e->state & GDK_BUTTON3_MASK )
    {
      pw->button3_up_ignore = true;
      menu_cb_zoom(pw, e->direction ? 'z' : 'u', NULL);
      return true;
    }
  menu_cb_insn_cursor(pw, e->direction ? 'n' : 'p', w);
  return true;
}

gint
evnt_cb_motion(GtkWidget *w, GdkEventMotion *ee, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;

  sw->button_down_motion |= ee->state & GDK_BUTTON1_MASK;
  sw->last_button_up_time_no_motion = 0;

  if( sw->plot_is_ovr ) return motion_handler_ovr_plot(w,ee,sw);
  else return motion_handler_segment_plot(w,ee,sw);
}

gint
evnt_cb_button_down(GtkWidget *w, GdkEventButton *be, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;

  sw->button_down_motion = false;
  gtk_widget_grab_focus(w);

  if(sw->context_menu_popup)
    gtk_menu_shell_deactivate(GTK_MENU_SHELL(sw->context_menu_popup));

  if( be->button == 3 || be->state & GDK_BUTTON3_MASK ) return true;

  if( !sw->plot_is_ovr )
    {
      if( !sw->animating )
        {
          if( SHIFT_IS_DOWN(be) )
            {
              gdk_window_set_cursor(sw->draw_area_win,app.cursor_crosshair);
              sw->zoom_c_1 = sw->pointer_c(be);
              sw->zoom_p_1 = be->y;
              sw->zoom_x_1 = be->x;
            }
          else if( sw->dis_drag_grab_rect.in( be->x, be->y ) )
            {
              gdk_window_set_cursor(sw->draw_area_win,app.cursor_dis_drag);
              sw->drag_dis_window = true;
              sw->drag_dis_window_last_y_p = (int)be->y;
            }
          else
            {
              gdk_window_set_cursor(sw->draw_area_win,app.cursor_fleur);
              sw->drag_c = sw->pointer_c(be);
              sw->drag_p = be->y;
              sw->drag_top_idx = sw->view.top_idx;
            }
        }
    }

  return true;
}

gint
evnt_cb_button_up(GtkWidget *w, GdkEventButton *be, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  const bool dragged = sw->button_down_motion;
  bool double_click =
    be->button == sw->last_button_up_button
    && be->time - sw->last_button_up_time_no_motion < 300;
  sw->last_button_up_button = be->button;
  sw->last_button_up_time_no_motion = be->time;
  sw->button_down_motion = false;

  if( sw->button3_up_ignore && be->button == 3 )
    {
      sw->button3_up_ignore = false;
      return true;
    }

  if( sw->plot_is_ovr )
    {
      if( sw->ovr_pointed_segment && !dragged )
        {
          sw->sd = sw->ovr_pointed_segment;
          gtk_button_clicked(GTK_BUTTON(sw->dbutton));
        }
    }
  else
    {
      if( be->button == 1 && be->state & GDK_BUTTON3_MASK )
        {
          gtk_toggle_button_set_active
            (GTK_TOGGLE_BUTTON(sw->cbutton),
             !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sw->cbutton)));
          sw->button3_up_ignore = true;
          return true;
        }
      if(be->button == 3)
        {
          if(sw->context_menu_popup && !dragged)
           {
             gtk_grab_add(sw->context_menu_popup);
             gtk_menu_popup(GTK_MENU(sw->context_menu_popup), NULL,
                            NULL, NULL, NULL, 0, be->time);
             gtk_grab_remove(sw->context_menu_popup);
             sw->cursor_pos_x_at_popup = (int)be->x;
             sw->cursor_pos_y_at_popup = (int)be->y;
           }
          return true;
        }

      gdk_window_set_cursor(sw->draw_area_win,app.cursor_corners);
      if( sw->drag_dis_window && dragged )
        {
          sw->drag_dis_window = false;
          sw->invalidate_draw_area();
          return true;
        }
      else if( sw->zoom_c_1 )
        {
          int zoom_c_1 = sw->zoom_c_1;
          sw->zoom_c_1 = 0;

          if( !dragged ) return true;

          if( sw->animating || !SHIFT_IS_DOWN(be) ) return true;

          GdkWindow *win = sw->draw_area_win;
          gint view_width_p, w_height;
          gdk_window_get_size(win,&view_width_p,&w_height);

          int zoom_c_2 = sw->pointer_c(be);
          int zoom_start_c = min(zoom_c_1,zoom_c_2);
          int zoom_end_c = max(zoom_c_1,zoom_c_2);
          int zoom_width_c = zoom_end_c - zoom_start_c;
          if( zoom_width_c < 5 ) return true;

          // zoom below.
          sw->push_current_view();
          sw->view.pixels_per_inst =
            max(1, int( sw->view.pixels_per_inst *
                        ( sw->segment_plot_bottom - sw->segment_plot_top )
                        / fabs(sw->zoom_p_1 - be->y) ));
          sw->view.pixels_per_cycle = view_width_p / zoom_width_c;
          int end_c = zoom_start_c + sw->view.pixels_per_cycle * zoom_width_c;
          if( end_c < zoom_end_c ) sw->view.pixels_per_cycle++;
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sw->cbutton),true);
          sw->set_view_start(zoom_start_c,sw->zoom_top_idx);
          return true;
        }
      else
        {
          if( !dragged )
            {
              sw->drag_dis_window = false;

              if( double_click && sw->pointed_idx == -1 )
                menu_cb_zoom(sw,'w',NULL);
              else if( be->button == 2 )
                menu_cb_zoom(sw,'c',NULL);
              else if( sw->pointed_idx != -1 )
                {
                  sw->cursor_cmd = PSE_Window::CC_Pointer;
                  sw->cursor_move_amt = 0;
                  const bool cursor_was_locked = sw->cursor_locked;
                  sw->cursor_locked =
                    sw->cursor_idx==sw->pointed_idx ? !sw->cursor_locked : true;

                  if( sw->cursor_locked != cursor_was_locked )
                    sw->dis_locked = sw->cursor_locked;

                  sw->cursor_idx = sw->pointed_idx; // Tentative.
                }
              else sw->dis_locked = !sw->dis_locked;

              sw->invalidate_draw_area();
            }
          sw->drag_c = 0;
          return true;
        }
    }
  return true;
}

void
user_cmd_redisplay(PSE_Window *sw, bool new_seg)
{
  if( !sw->seg_plot_paint_variables_valid ) return;
  double &goal_d = sw->goal_d;
  double &view_start_d = sw->view_start_d;
  const bool clipped = sw->plot_ped_clipped && sw->plot_segment_as_ped;
  goal_d = clipped ? sw->goal_idx : sw->goal_c;

  if( sw->animating_insn_motion != clipped )
    {
      sw->animating = false;
      sw->velocity_d = 0.0;
    }

  if( !sw->animating )
    sw->view_start_d = clipped ? sw->view.top_idx : sw->view.start_c;

  if( !new_seg && ( clipped ?
      sw->view.top_idx != sw->goal_idx : sw->view.start_c != sw->goal_c) )
    {
      double now = time_fp();
      sw->view_update_t = now;

      bool animate;
      switch ( sw->animate_mode ){
      case AM_None: animate = false;  break;
      case AM_All: animate = true; break;
      case AM_Short:
        {
          const int view_dimension =
            clipped ? sw->get_view_height_insn() : sw->get_view_width_c();
          animate =
            abs( clipped ? sw->goal_idx - sw->view.top_idx :
                 sw->goal_c - sw->view.start_c) <= 1 + view_dimension * 2;
        }
        break;
      default:
        animate = false;
        FATAL;
      }

      if( animate && sw->base_accel )
        {
          // v_0: current velocity.
          // t_1: time until turn around.
          // t_2: time from turn around until desto.
          // s_g: distance from current location to goal.
          // s_m: distance from current location to turn around.
          // a:   acceleration.
          // a t_1^2 + 2v_0t_1 + v_0^2/{2a} - s_g + s_0 = 0;

          double &velocity_d = sw->velocity_d;
          int direction = clipped ?
            ( sw->goal_idx > sw->view.top_idx ? 1 : 0 ) :
            ( sw->goal_c > sw->view.start_c ? 1 : 0 );

          double a_base = sw->base_accel /
             (clipped ? sw->view.pixels_per_inst : sw->view.pixels_per_cycle);
          double distance = goal_d - view_start_d;
          double a_dir_goal = direction ? a_base : -a_base;
          double v_sq = velocity_d * velocity_d;
          double a = ( ( velocity_d >= 0 ? distance : -distance ) > 0
                       && fabs(distance) < v_sq / ( 2 * a_base ) )
            ? -a_dir_goal : a_dir_goal;

          sw->accel = a;

          double two_a_inv = 1.0 / ( 2.0 * a );
          double b = 2.0 * velocity_d;
          double sqt = sqrt(2 * v_sq + 4 * a * distance);
          double t1a = (-b + sqt) * two_a_inv;
          double t1b = (-b - sqt) * two_a_inv;
          double t1 = t1a < t1b ? t1b : t1a;
          if( !( t1 >= 0 ) ) fatal("You did pass high school physics, right?");

          sw->turn_around_t = now + t1;
          sw->arrival_t = now + t1 + t1 + velocity_d / a;
        }
      else
        {
          sw->turn_around_t = 0;
          sw->arrival_t = 0;
        }

      if( sw->animating ) return;
      sw->animating = true;
      sw->animating_insn_motion = clipped;
      gtk_timeout_add(33,display_animation_step,(gpointer)sw);
    }
  else
    {
      sw->animating = false;
      sw->invalidate_draw_area();
    }
}

void
zoom_in(int &i)
{
  const int new_i = int( i * rtiv_seg_zoom_factor + 0.5 );
  i = new_i == i ? i + 1 : new_i;
}

void
zoom_out(int &i)
{
  const int new_i = int( i / rtiv_seg_zoom_factor + 0.5 );
  const int new_i_diff = new_i == i ? i - 1 : new_i;
  i = max(1,new_i_diff);
}

void
menu_cb_copy(gpointer pwptr, int action, GtkWidget *w)
{
  switch ( action ){
  case 'c':
    {
      GdkWindow* const window = gdk_selection_owner_get(GDK_SELECTION_PRIMARY);
      if ( !window ) return;
      GtkClipboard* const se = gtk_clipboard_get(GDK_SELECTION_PRIMARY);
      const gchar* const txt = gtk_clipboard_wait_for_text(se);
      if ( !txt ) return;
      GtkClipboard* const cb = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
      gtk_clipboard_set_text(cb,txt,-1);
    }
    break;
  default : ASSERTS(false);
  }
}

void
menu_cb_zoom(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window* const sw = (PSE_Window*) swptr;
  if( !sw->seg_plot_paint_variables_valid ) return;
  Dataset* const ds = sw->ds;
  bool clip = false;
  bool no_clip = false;
  bool skip_set_view = false;
  const int reference_idx =
    sw->pointed_idx >= 0 ? sw->pointed_idx : sw->idx_reference_get();
  const int reference_pos = sw->idx_to_vertical_position(reference_idx);
  Insn_Dataset_Info* const reference_di = sw->idx_to_info(reference_idx);
  const int reference_c =
    sw->plot_segment_as_ped
    ? mid(reference_di->rob_enqueue_c,reference_di->rob_dequeue_c)
    : sw->view.start_c + sw->ve.width / (2*sw->view.pixels_per_cycle);
  const int prev_pixels_per_inst = sw->view.pixels_per_inst;
  const int prev_pixels_per_cycle = sw->view.pixels_per_cycle;

  const bool dont_push_view =
    action == 'c'
    && sw->view.pixels_per_cycle == rtiv_ped_clip_n_zoom_pixels_per_cycle
    && sw->view.pixels_per_inst == rtiv_ped_clip_n_zoom_pixels_per_inst
    || action == 'l';

  if( !dont_push_view ) sw->push_current_view();

  switch( action ){
  case '0': // Zoom out max.
    sw->view.pixels_per_cycle = 1;
    sw->view.pixels_per_inst = 1;
    no_clip = true;
    break;

  case '1':
    sw->view.pixels_per_cycle = ds->dsvv_issue_width;
    sw->view.pixels_per_inst = 1;
    no_clip = true;
    break;

  case 'u': // Zoom out.
    zoom_out(sw->view.pixels_per_inst);
    zoom_out(sw->view.pixels_per_cycle);
    break;

  case 'z': // Zoom in.
    zoom_in(sw->view.pixels_per_inst);
    zoom_in(sw->view.pixels_per_cycle);
    break;

  case 'c': // Clip-n-Zoom
    if( dont_push_view )
      {
        sw->restore_previous_view();
        skip_set_view = true;
      }
    else
      {
        sw->view.pixels_per_cycle = rtiv_ped_clip_n_zoom_pixels_per_cycle;
        sw->view.pixels_per_inst = rtiv_ped_clip_n_zoom_pixels_per_inst;
        clip = true;
      }
    break;

  case 'w': // Disassembly Window
    {
      if( sw->dis_last_idx <= sw->dis_first_idx ) break;
      const int first_c = sw->dis_min_c - 6;
      const int last_c = sw->dis_max_c + 1 + 3;
      sw->view.pixels_per_inst =
        sw->ve.height / ( sw->dis_last_idx - sw->dis_first_idx );
      if( last_c <= first_c ) break;
      int ppc_maybe = sw->ve.width / ( last_c - first_c );
      sw->view.pixels_per_cycle = ppc_maybe ? ppc_maybe : 1;
      sw->set_view_start(first_c,sw->dis_first_idx,true);
      sw->restore_unlock_dis = sw->dis_locked;
      sw->dis_locked = true;
      clip = true;
      skip_set_view = true;
    }
    break;

  case 'l':
    sw->restore_previous_view();
    skip_set_view = true;
    break;

  case 'Y':
    zoom_out(sw->view.pixels_per_inst);
    break;

  case 'y':
    zoom_out(sw->view.pixels_per_cycle);
    break;

  case 'X':
    zoom_in(sw->view.pixels_per_inst);
    break;

  case 'x':
    zoom_in(sw->view.pixels_per_cycle);
    break;

  default:
    FATAL;
  }

  if( clip )
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sw->cbutton),true);
  if( no_clip )
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sw->cbutton),false);

  if( skip_set_view ) return;

  const int new_ref_pos =
    reference_pos * prev_pixels_per_inst / sw->view.pixels_per_inst;
  const int new_top_idx =
    sw->plot_segment_as_ped ? reference_idx - new_ref_pos : sw->view.top_idx;
  const int new_start_c =
    reference_c - ( reference_c - sw->view.start_c )
      * prev_pixels_per_cycle / sw->view.pixels_per_cycle;

  sw->set_view_start(new_start_c,new_top_idx,true);
}


void
menu_cb_animate(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;

  sw->push_current_view();

  switch( action ){
  case '5':
    if( sw->animating )
      {
        sw->goal_d = sw->view_start_d;
        if( sw->plot_ped_clipped && sw->plot_segment_as_ped )
          sw->goal_idx = sw->view.top_idx;
        else
          sw->goal_c = sw->view.start_c;
      }
    break;
  }
}

void
menu_cb_dis_window(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window* const sw = (PSE_Window*) swptr;
  sw->plot_show_dis =
    gtk_check_menu_item_get_active
    (GTK_CHECK_MENU_ITEM(sw->seg_menu_check_dis_pane));
  const bool show = sw->ds->readex && sw->plot_show_dis;
  if( show )
    gtk_widget_show(sw->dis_pane_vbox);
  else
    gtk_widget_hide(sw->dis_pane_vbox);
}

void
menu_cb_insn_cursor(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;

  switch( action ){
  case 'c': // Unlock instruction cursor.
    break;

  case 'o': case 'd':  // d, unlock disassembly window;  o, obscure.
    break;

  case 'n':
    sw->cursor_cmd = PSE_Window::CC_Next;
    sw->cursor_move_amt++;
    sw->cursor_locked = true;
    break;

  case 'p':
    sw->cursor_cmd = PSE_Window::CC_Previous;
    sw->cursor_move_amt--;
    sw->cursor_locked = true;
    break;

  case 'A': // Annotations in disassembly pane.
    break;

  default:
    ASSERTS(false);
  }

  user_cmd_redisplay(sw,false);
}

void
menu_cb_select_done(GtkWidget *context_menu, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*)swptr;

  GdkRectangle a;
  gtk_widget_get_pointer(context_menu, &a.x, &a.y);
  a.height = a.width = 1;
  if(!gtk_widget_intersect(context_menu, &a, NULL)) return;
  pgtk_XWarpPointer(sw->draw_area, sw->cursor_pos_x_at_popup,
                    sw->cursor_pos_y_at_popup);
}

void
menu_cb_clip(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  gtk_toggle_button_set_active
   (GTK_TOGGLE_BUTTON(sw->cbutton),
     gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(w)));
}

void
menu_cb_font(gpointer swptr, int action, GtkWidget *w)
{
  PSE_Window *sw = (PSE_Window*) swptr;

  switch( action ){
  case '+': sw->label_set.font_size_adjust(1000); break;
  case '-': sw->label_set.font_size_adjust(-1000); break;
  default : ASSERTS(false);
  }

  if(w)user_cmd_redisplay(sw,false);
}

// DMK: Eventually re-write to even_cb_key_press and this routine call
// some function to perform the actions. 31 March 2003,  9:31:06 CST
void
bttn_cb_misc(GtkWidget *w, gpointer cdptr)
{
  Call_Back_Data *cd = (Call_Back_Data*) cdptr;
  GdkEventKey e;
  e.type = GDK_KEY_PRESS;
  e.keyval = cd->i;
  e.state = 0;
  evnt_cb_key_press(w,&e,cd->sw);
}

gint
evnt_cb_key_press(GtkWidget *w, GdkEventKey *event, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  Dataset *ds = sw->ds;
  GtkWidget *draw = sw->draw_area;

  if( sw->key_handler_suppress ) return false;
  if( !ds->dataset_loaded ) return false;
  if( !draw ) return false;

  if( event->type == GDK_KEY_RELEASE )
    {
      switch( event->keyval ){
      case GDK_Shift_L: sw->invalidate_draw_area(); break;
      default: break;
      }
      return false;
    }

  if( event->type != GDK_KEY_PRESS ) return false;

  int redisplay = 0;

  Segment_Data *sd = sw->sd;
  int sd_idx = sw->plot_ovr_by_ipc ? sd->rank : sd->segment_num;
  int before_idx = sd_idx;
  const bool clipped = sw->plot_ped_clipped && sw->plot_segment_as_ped;
  const int segm_dimension =
    clipped ? sd->idx_max : sd->seg_end_ac - sd->seg_start_ac;
  const int view_dimension =
    clipped ? sw->get_view_height_insn() : sw->get_view_width_c();
  int* const scroll_goal = clipped ? &sw->goal_idx : &sw->goal_c;

  switch( event->keyval ){

  case GDK_ISO_Left_Tab:
    search_pw_cursor_move(sw,SSA_Insn_Prev);
    return true;

  case GDK_Tab:
    search_pw_cursor_move
      (sw, event->state & GDK_SHIFT_MASK ? SSA_Insn_Prev : SSA_Insn_Next);
    return true;

  case GDK_S: case GDK_s:
    if( !CONTROL_IS_DOWN(event) ) return false;
    gtk_widget_grab_focus(sw->search_entry);
    return true;

  case GDK_Shift_L: case GDK_Shift_R:
    sw->invalidate_draw_area();
    return false;

  case GDK_Home: case GDK_Right: case GDK_Left: case GDK_End:
    {
      if( CONTROL_IS_DOWN(event) ) return false;
      double frac = SHIFT_IS_DOWN(event) ? 0.2 : 0.8;
      int limit = segm_dimension - view_dimension;
      *scroll_goal =
        event->keyval == GDK_Home
        ? 0 :
        event->keyval == GDK_End
        ? limit :
        event->keyval == GDK_Right
        ? min(limit,*scroll_goal + int(double(view_dimension)*frac))
        : max(0,*scroll_goal - int(double(view_dimension) * frac));

      redisplay = clipped
        ? sw->goal_idx != sw->view.top_idx : sw->goal_c != sw->view.start_c;
    }
    break;

  case GDK_C: case GDK_c:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sw->cbutton),!clipped);
    return 1;

  case GDK_L: case GDK_l:
    sw->restore_previous_view();
    redisplay = 1;
    break;

  case GDK_F31: case GDK_KP_5:
    if( sw->animating )
      {
        sw->goal_d = sw->view_start_d;
        *scroll_goal = clipped ? sw->view.top_idx : sw->view.start_c;
      }
    // Fall through.

  case GDK_KP_Enter:
  case GDK_Return:
    if( sw->animating )
      {
        sw->turn_around_t = 0;
        sw->arrival_t = 0;
      }
    break;

  case GDK_Prior:
    sd_idx = CONTROL_IS_DOWN(event) ? 0 : sd_idx > 0 ? sd_idx-1 : sd_idx;
    break;

  case GDK_Next:
    {
      int limit = ds->sdata.occ - 1;
      sd_idx = CONTROL_IS_DOWN(event) ? limit :
               sd_idx < limit ? sd_idx+1 : sd_idx;
    }
    break;

  default:
    return false;
  }

  bool new_seg = before_idx != sd_idx;

  if( new_seg ) sw->segment_goto(sd_idx);
  if( redisplay ) user_cmd_redisplay(sw,new_seg);
  return true;
}


//
// Other Events
//

void
evnt_cb_expose(GtkWidget *w, GdkEventExpose *ee, gpointer swptr)
{
  PSE_Window *sw = (PSE_Window*) swptr;
  if( !sw->ds->dataset_loaded ) return;
  if( sw->plot_is_ovr ) sw->paint_ovr(w,ee); else sw->paint_seg(w,ee);
}

void
evnt_cb_delete(GtkWidget *main_w, GdkEvent *event, gpointer swptr)
{
  close_and_maybe_quit((PSE_Window*)swptr,0);
}
