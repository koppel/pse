/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright (c) 2013 Louisiana State University

// $Id$

// Code for locating and archiving the benchmark files needed to
// display assembly code, dependencies, etc.

#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <utime.h>
#include <gtk/gtk.h>
#include "misc.h"
#include "dataset.h"
#include "pse_window.h"
#include "messages.h"
#include "rti_declarations.h"
#include "gtk_utility.h"

class BM_Archive {
public:
  BM_Archive(Dataset *ds, bool arch_exists, char *arch_dir, char *bm_xname);
  void deposit_benchmark(const char *filepath,
                         bool offer_to_create = false);
  bool create_archive();

private:
  Dataset* const ds;
  const bool archive_exists;
  char* const parent_dir_path;
  RString bm_xname;             // Name using file md5 or size.
  RStringF benchmark_dir;
  RStringF benchmark_path;
};

class Benchmark_Match;

static void benchmark_browse_dialog(Dataset *ds, Benchmark_Match *bm);

enum BM_Need
  { BM_Archived_Location = 1, BM_Existing_Arch_Dir = 2,
    BM_Non_Archived_Location = 4,
    BM_Create_Arch_Dir = 8
  };

enum BM_Match_Flaw
  {
    // Magnitude comparisons made so renumber with care.
    BMF_None = 0,
    BMF_Wrong_Size = 1,
    BMF_Wrong_MD5 = 2,
    BMF_Not_Found = 3,
  };

static const char* const archive_dir_name = ".Benchmark_Archive";

class Benchmark_Match {
public:
  Benchmark_Match(Dataset *ds):ds(ds)
  {
    need =
      BM_Archived_Location | BM_Existing_Arch_Dir |
      BM_Non_Archived_Location | BM_Create_Arch_Dir;
    user_wants_flawed = false;
  }

  char *get();

  bool try_at(char *try_path)
  {
    RStringF try_name("%s/%s",try_path,bm_name.s);
    RStringF try_arch("%s/%s",try_path,archive_dir_name);
    RStringF try_arch_mname("%s/%s",try_arch.s,bm_xname.s);

    if( verbose ) fprintf(stderr,"try_at %s\n",try_path);

    if( need & BM_Non_Archived_Location && match_content(try_name) )
      {
        bm_non_archived_location = try_name;
        need &= ~BM_Non_Archived_Location;
      }

    if( need & BM_Archived_Location && match_name(try_arch_mname) )
      {
        bm_existing_arch_dir = try_path;
        bm_archived_location = try_arch_mname;
        need = 0;
      }

    if( dir_exists(try_arch) )
      {
        bm_archives += try_arch.dup();
      }

    if( need & BM_Existing_Arch_Dir && dir_writeable(try_arch) )
      {
        bm_existing_arch_dir = try_path;
        need &= ~( BM_Existing_Arch_Dir | BM_Create_Arch_Dir );
      }

    if( need & BM_Create_Arch_Dir && dir_writeable(try_path) )
      {
        bm_create_arch_dir = try_path;
        need ^= BM_Create_Arch_Dir;
      }

    return need ? false : true;
  }

  BM_Match_Flaw try_this_from_user(char *try_name)
  {
    const BM_Match_Flaw flaw = match_flaw_maybe(try_name,false);
    switch( flaw ) {
    case BMF_None:
      bm_flawed_location = NULL;
      bm_non_archived_location = try_name; break;
    case BMF_Wrong_MD5: case BMF_Wrong_Size: 
      bm_flawed_location = try_name;
      bm_non_archived_location = NULL; break;
    default:
      bm_flawed_location = NULL;
      bm_non_archived_location = NULL; break;
    }
    return flaw;
  }

  bool dir_writeable(char *path) {return dir_property(path,true);}
  bool dir_exists(char *path) {return dir_property(path,false);}
  bool dir_property(char *path,bool writeable)
  {
    struct stat stat_info;
    if((stat(path, &stat_info)) == -1) return false;
    if( !S_ISDIR(stat_info.st_mode) ) return false;
    return !access(path,W_OK);
  }

  bool match_content(char *t)
  { return match_flaw_maybe(t,false) == BMF_None; };

  BM_Match_Flaw match_flaw(char *try_path)
  {return match_flaw_maybe(try_path,false);}

  bool match_name(char *try_path)
  {return match_flaw_maybe(try_path,true) == BMF_None;}

  BM_Match_Flaw match_flaw_maybe(char *try_path, bool just_name)
  {
    struct stat stat_info;
    if( stat(try_path, &stat_info) == -1 ) return BMF_Not_Found;
    if( just_name ) return BMF_None;
    const off_t match_flaw_size = stat_info.st_size;
    if( match_flaw_size != bm_size ) return BMF_Wrong_Size;
    if( !bm_md5 ) return BMF_None;
    return bm_md5 == md5_file_64(try_path) ? BMF_None : BMF_Wrong_MD5;
  }

  const static bool verbose = false;

  uid_t file_owner_get(const char *path)
  {
    struct stat s;
    if( stat(path, &s) == -1 ) return 0;
    return s.st_uid;
  }

  bool try_above(char *startp)
  {
    const double time_start = time_fp();
    RString start(startp); path_dots_remove(start);
    const uid_t owner_start = file_owner_get(start);
    if( !owner_start )
      {
        if( verbose )
          fprintf(stderr,"Benchmark search, try above can't read \"%s\"\n",
                  start.s);
        return false;
      }
    if( verbose )
      fprintf(stderr,"Benchmark search, try above \"%s\" ...",start.s);
    PSplit pieces(start,'/');
    RString so_far("/"), dirs[pieces.occ()], *dp = &dirs[0];
    bool rv = false;
    while( char* const piece = pieces ) *dp++ += so_far.sprintf("%s/",piece);
    for( RString *dpd = dp-1; dpd >= &dirs[0]; dpd-- )
      if( file_owner_get(dpd->s) != owner_start || ( rv = try_at(dpd->s) ) )
        break;
    if( verbose )
      fprintf(stderr,"%s  t = %.1f s\n",
              rv ? "found" : "not found", time_fp() - time_start);
    return rv;
  }

  bool try_below(char *startp)
  {
    const double time_start = time_fp();
    RString start(startp); path_dots_remove(start);
    if( verbose )
      fprintf(stderr,"Benchmark search, try below \"%s\" need %x ...",
              start.s, need);
    PStack<char*> dirs(strdup(start));
    RString dir_path;
    RString skip_dir(".svn");
    while( dirs && dir_path.assign_take(dirs) )
      {
        if( !need || try_at(dir_path) ) continue; // Free strings.
        GDir* const dir = g_dir_open(dir_path,0,NULL);
        if( !dir ) continue;
        while( const gchar* const entry = g_dir_read_name(dir) )
          {
            if( skip_dir == entry ) continue;
            RStringF path("%s/%s",dir_path.s,entry);
            if( g_file_test(path,G_FILE_TEST_IS_DIR) ) dirs += path.remove();
          }
        g_dir_close(dir);
      }
    if( verbose )
      fprintf(stderr,"finish need %x.  t = %.1f s\n",
              need, time_fp() - time_start);
    return !need;
  }

  char* dir_search(char *dir_path)
  {
    if( verbose )
      fprintf(stderr,"Benchmark search, dir_search trying \"%s\" ...",dir_path);
    GDir* const dir = g_dir_open(dir_path,0,NULL);
    if( !dir ) return NULL;
    char *rv = NULL;
    while( const gchar* const entry = g_dir_read_name(dir) )
      {
        if( strncmp(entry,bm_name_lhs,bm_name_lhs.len()) ) continue;
        RStringF path("%s/%s",dir_path,entry);
        if( !match_flaw(path) ){ rv = path.remove();  break; }
      }
    g_dir_close(dir);
    if( verbose ) fprintf(stderr,"%s.\n", rv ? "found" : "not found");
    return rv;
  }

  char* bm_location()
  {
    if( bm_archived_location ) return bm_archived_location;
    if( bm_non_archived_location ) return bm_non_archived_location;
    if( bm_flawed_location ) return bm_flawed_location;
    return NULL;
  }

  Dataset* const ds;

  int need;
  bool user_wants_flawed;

  RString bm_name_lhs; // Will be prefix of archive name.
  RString bm_name;     // Name used by simulator.
  RString bm_xname;    // Archive name containing md5 hash or file size.
  RString pse_cwd;  // Current directory (of this process).
  char *bm_path_sim_observed;
  off_t bm_size;
  uint64_t bm_md5;

  RString bm_archived_location;
  RString bm_non_archived_location;
  RString bm_flawed_location;
  RString bm_existing_arch_dir;         // Does not include .Benchmark_Archive
  RString bm_create_arch_dir;           // Does not include .Benchmark_Archive
  PStack<char*> bm_archives;
};

char*
benchmark_path_get(Dataset *ds)
{
  Benchmark_Match benchmark_match(ds);
  return benchmark_match.get();
}

char*
Benchmark_Match::get()
{
  bm_path_sim_observed = ds->sim_var_value("exec_file_name");

  if( !bm_path_sim_observed )
    {
      dialog_warning
        (ds->ref_window,
         "Benchmark executable file name not found in dataset.");
      return NULL;
    }

  bm_name.assign_take(g_path_get_basename(bm_path_sim_observed));
  char* const sim_cwd = ds->sim_var_value("pwd");
  bm_size = ds->dsvv_exec_file_size;
  bm_md5 = ds->sim_var_value_64("exec_file_md5",0ll);
  pse_cwd.assign_take(g_get_current_dir());

  PSplit bm_pieces(bm_name,'.');
  RString bm_name_ext("");
  if( bm_pieces > 1 ) bm_name_ext.sprintf(".%s",bm_pieces.pop());
  bm_name_lhs = bm_pieces.joined_copy();

  if( bm_md5 ) 
    bm_xname.sprintf("%s.%lx%s",bm_name_lhs.s,bm_md5,bm_name_ext.s);
  else
    bm_xname.sprintf("%s.s%ld%s",bm_name_lhs.s,bm_size,bm_name_ext.s);

  RStringT bm_dir_sim_observed(g_path_get_dirname(bm_path_sim_observed));
  RStringT ds_dir(g_path_get_dirname(ds->file_name));

  while( true )
    {
      if( try_above(ds_dir) ) break;
      if( try_above(sim_cwd) ) break;
      if( try_above(pse_cwd) ) break;
      if( try_above(bm_dir_sim_observed) ) break;
      PSplit paths(rtiv_pse_exe_path,':');
      while( char* const p = paths ) if( try_below(p) ) break;
      if( !need ) break;
      for( char *archive_dir = NULL; bm_archives.iterate(archive_dir); )
        if( char* const match = dir_search(archive_dir) )
          {
            bm_non_archived_location = match;
            break;
          }
      break;
    }

  while( bm_archives ) free(bm_archives);

  if( !bm_location() ) benchmark_browse_dialog(ds, this);
  if( !user_wants_flawed ) bm_flawed_location = NULL;

  RString bm_path(bm_location());
  if( bm_path.len() == 0 ) return NULL;
  if( bm_archived_location || user_wants_flawed ) return bm_path.remove();

  const bool archive_exists = bm_existing_arch_dir;
  char* const bm_archive_dir =
    archive_exists ? bm_existing_arch_dir : bm_create_arch_dir;

  BM_Archive bm_archive(ds, archive_exists, bm_archive_dir, bm_xname);

  bm_archive.deposit_benchmark(bm_path, true);

  return bm_path.remove();
}

struct BCD_Bttn_Click_CB_Info
{
 GtkDialog *dialog;
 GtkLabel *bcd_msg_label;
 GtkEntry *entry_ptr;
 gchar *start_dir;
};

static void
bcd_browse_bttn_click_cb(GtkWidget *browse_bttn, gpointer data)
{
  BCD_Bttn_Click_CB_Info *data_ptr = (BCD_Bttn_Click_CB_Info *)data;
  RStringT entered_file
    (gtk_editable_get_chars(GTK_EDITABLE(data_ptr->entry_ptr), 0, -1));
  RStringT dir(g_path_get_dirname(entered_file));
  char* const start_dir =
    g_file_test(dir,G_FILE_TEST_IS_DIR) ? dir.s : data_ptr->start_dir;

  RStringT file_to_open_path
    (file_browse_dialog(NULL, "Locate Executable", start_dir, ".core"));

  if( file_to_open_path )
    gtk_entry_set_text(data_ptr->entry_ptr, file_to_open_path);

  gtk_dialog_response(data_ptr->dialog,GTK_RESPONSE_OK);
}

static void
bcd_entry_change_cb(GtkWidget *entry, gpointer data)
{
  GtkDialog *dialog = GTK_DIALOG(data);

  gtk_dialog_set_response_sensitive(dialog,
                                    GTK_RESPONSE_OK,
                                    TRUE);
  gtk_dialog_set_response_sensitive(dialog,
                                    GTK_RESPONSE_ACCEPT,
                                    FALSE);
  gtk_dialog_set_default_response(dialog,
                                  GTK_RESPONSE_OK);
}

static void
benchmark_browse_dialog(Dataset *ds, Benchmark_Match *bm)
{
  if( !app.gui )
    {
      fprintf(stderr,"Problem with benchmark executable, not loading\n");
      return;
    }

  PGtkDialogPtr bc_dialog =
    gtk_dialog_new_with_buttons
    ("Benchmark File Problem", ds->ref_window, GTK_DIALOG_MODAL,
     "Use Anyway", GTK_RESPONSE_ACCEPT,
     "Skip", GTK_RESPONSE_REJECT,
     GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);

  gtk_window_set_default_size(bc_dialog, 500, 150);
  gtk_dialog_set_response_sensitive(bc_dialog, GTK_RESPONSE_OK, FALSE);

  GtkWidget *dialog_msg_label = gtk_label_new(NULL);
  gtk_label_set_justify(GTK_LABEL(dialog_msg_label), GTK_JUSTIFY_FILL);
  gtk_label_set_line_wrap(GTK_LABEL(dialog_msg_label), TRUE);

  PGtkAlignmentPtr msg_align = gtk_alignment_new(0,0,0,0);
  gtk_container_add(msg_align, dialog_msg_label);
  gtk_alignment_set_padding(msg_align,5,5,5,5);

  GtkWidget *file_path_label = gtk_label_new("File Path");
  GtkWidget *file_path_label_align = gtk_alignment_new(0, 0.5, 0, 0);
  gtk_container_add(GTK_CONTAINER(file_path_label_align), file_path_label);

  PGtkEntryPtr file_path_entry = gtk_entry_new();
  GtkEditable* const file_path_entry_e = GTK_EDITABLE(file_path_entry);
  gtk_editable_set_editable(file_path_entry_e, TRUE);
  gtk_entry_set_position(file_path_entry, -1);

  gtk_entry_set_text(file_path_entry, bm->bm_name.s);

  gtk_entry_set_activates_default(file_path_entry, TRUE);
  g_signal_connect(file_path_entry, "changed",
                   G_CALLBACK(bcd_entry_change_cb),
                   (gpointer)bc_dialog);

  BCD_Bttn_Click_CB_Info bcd_bttn_click_data;
  bcd_bttn_click_data.dialog = bc_dialog;
  bcd_bttn_click_data.entry_ptr = file_path_entry;
  bcd_bttn_click_data.start_dir = bm->pse_cwd;
  bcd_bttn_click_data.bcd_msg_label = GTK_LABEL(dialog_msg_label);

  GtkWidget *browse_bttn = gtk_button_new_with_label("Browse");
  g_signal_connect(G_OBJECT(browse_bttn), "clicked",
                   G_CALLBACK(bcd_browse_bttn_click_cb),
                   (gpointer)&bcd_bttn_click_data);

  PGtkHBoxPtr hbox2 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(hbox2, file_path_label_align, FALSE, FALSE, 5);
  gtk_box_pack_start(hbox2, file_path_entry, TRUE, TRUE, 0);
  gtk_box_pack_start(hbox2, browse_bttn, FALSE, FALSE, 5);

  GtkWidget *separator = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(bc_dialog->vbox), msg_align, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(bc_dialog->vbox), separator, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(bc_dialog->vbox), hbox2, FALSE, FALSE, 5);

  while( true )
    {
      RStringT entered_file(gtk_editable_get_chars(file_path_entry_e, 0, -1));
      RString dialog_msg;

      const BM_Match_Flaw flaw = bm->try_this_from_user(entered_file);

      switch( flaw ){

      case BMF_None: break;

      case BMF_Not_Found:
        dialog_msg.sprintf
          ("<span size=\"x-large\">Benchmark "
           "File Error \n\n</span>"
           "<span size=\"medium\">Could not "
           "obtain information on file %s. "
           "(Details: %s) </span>",
           entered_file.s, strerror(errno));
        break;

      case BMF_Wrong_Size:
        dialog_msg.sprintf
          ("<span size=\"x-large\">Benchmark Size Wrong</span>\n\n"
           "<span size=\"medium\">The benchmark "
           "file that was found is not the correct "
           "size, %ld bytes, and cannot be used.\n\n"
           "Click Browse to specify a benchmark file location, Skip "
           "to proceed without one, or enter a file name and click OK.</span>",
           bm->bm_size);
        break;

      case BMF_Wrong_MD5:
        dialog_msg.sprintf
          ("<span size=\"x-large\">Benchmark Content Wrong</span>\n\n"
           "<span size=\"medium\">The benchmark "
           "file that was found does not have the correct "
           "content based on an MD5 hash, %lx, and so cannot be used.\n\n"
           "Click Browse to specify a benchmark file location, Skip "
           "to proceed without one, or enter a file name and click OK.</span>",
           bm->bm_md5);
        break;

      default: ASSERTS( false );
      }

      if( !dialog_msg ) break;

      gtk_dialog_set_response_sensitive
        (bc_dialog, GTK_RESPONSE_ACCEPT, flaw != BMF_Not_Found );
      gtk_dialog_set_default_response(bc_dialog, GTK_RESPONSE_REJECT);
      gtk_dialog_set_response_sensitive(bc_dialog, GTK_RESPONSE_OK, FALSE);
      gtk_dialog_set_response_sensitive(bc_dialog, GTK_RESPONSE_REJECT, TRUE);

      gtk_label_set_markup(GTK_LABEL(dialog_msg_label), dialog_msg);
      gtk_widget_show_all(bc_dialog);
      const int response = gtk_dialog_run(bc_dialog);
      gtk_widget_hide(bc_dialog);

      bm->user_wants_flawed = response == GTK_RESPONSE_ACCEPT;

      switch( response ){
      case GTK_RESPONSE_ACCEPT:
      case GTK_RESPONSE_REJECT: case GTK_RESPONSE_DELETE_EVENT: break;
      case GTK_RESPONSE_OK: continue;
      default: ASSERTS( false );
      }
      break;
    }
  gtk_widget_destroy(bc_dialog);
}

BM_Archive::BM_Archive
(Dataset *ds, bool archive_exists, char *bm_archive_parent_dir, char *bm_xname)
  : ds(ds), archive_exists(archive_exists),
    parent_dir_path(bm_archive_parent_dir), bm_xname(bm_xname),
    benchmark_dir("%s/%s",bm_archive_parent_dir,archive_dir_name),
    benchmark_path("%s/%s",benchmark_dir.s,bm_xname) {}

void
BM_Archive::deposit_benchmark(const char *org_filepath,
                              bool offer_to_create)
{
  GtkWidget* const parent_w = ds->ref_window;
  if( g_file_test(benchmark_path,G_FILE_TEST_EXISTS) ) return;
  if(!archive_exists)
    {
      if(!offer_to_create) return;
      if(!create_archive()) return;
    }
  else
    {
      int response =
          dialog_question(parent_w, "Copy \"%s\" "
                        "to benchmark archive at %s?",
                          org_filepath, benchmark_dir.s);
      if(response != GTK_RESPONSE_YES) return;
    }

  {
    using namespace std;
    ifstream org_benchmark(org_filepath, ios::binary);
    ofstream archive_copy(benchmark_path, ios::binary);
    if(!org_benchmark || !archive_copy) return;
    archive_copy << org_benchmark.rdbuf();
  }

  {
    struct stat dataset_info;
    if(stat(ds->file_name, &dataset_info) == -1)
      {
        dialog_warning(parent_w, "%s could not be copied to the "
                       "archive because of an error occurring while "
                       "obtaining information on the dataset.",
                       benchmark_path.s);
        remove(benchmark_path);
        return;
      }

    if(chmod(benchmark_path, dataset_info.st_mode) == -1)
      {
        dialog_warning(parent_w, "The following error occurred while "
                       "attempting to change the permissions of the "
                       "archived benchmark copy:\n\n %s\n\n"
                       "As a result, %s will not be copied to the "
                       "archive.", strerror(errno), benchmark_path.s);
        remove(benchmark_path);
        return;
      }
  }

  {
    struct utimbuf archive_copy_utimes;
    {
      int32_t mod_time = ds->dsvv_exec_file_mod_time;
      archive_copy_utimes.modtime = mod_time;
      archive_copy_utimes.actime = mod_time;
    }
    if(!utime(benchmark_path, &archive_copy_utimes)) return;
  }
}

bool
BM_Archive::create_archive()
{
  if(access(parent_dir_path, W_OK) == -1) return false;

  GtkWidget* const parent_w = ds->ref_window;
  {
    const int response =
      dialog_question
      (parent_w,
       "An archive doesn't presently "
       "exist. Would you like to create one? If "
       "created it would be located in:\n %s\n\n"
       "(If this location is not suitable create a directory named "
       "%s and, if necessary, use rtiv_pse_exe_path to specify its location.)"
       "",
       benchmark_dir.s,archive_dir_name);
    if(response != GTK_RESPONSE_YES) return false;
  }

  {
    struct stat parent_dir_info;
    if(stat(parent_dir_path, &parent_dir_info) == -1) return false;
    if(!mkdir(benchmark_dir, parent_dir_info.st_mode))
      {
        chmod(benchmark_dir, parent_dir_info.st_mode);
        return true;
      }
  }

  dialog_warning(parent_w, "The archive could not be "
                 "created due to the following reason:\n\n%s",
                 strerror(errno));
  return false;
}
