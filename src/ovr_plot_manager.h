/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2010 Louisiana State University

// $Id$

#ifndef OVR_PLOT_MANAGER_H
#define OVR_PLOT_MANAGER_H

#include <gtk/gtk.h>
#include "dataset.h"
#include "misc.h"
#include "gtk_utility.h"

#define PLOT_SERIES_DISPLAYED 4

#define DS_FMT_OVR_PLOT_SERIES_INIT "overview plot series init"
#define DS_FMT_OVR_PLOT_SERIES_NAME 1
#define DS_FMT_OVR_PLOT_SERIES_IDX 2
#define DS_FMT_OVR_PLOT_SERIES_DOCSTRING 3
#define DS_FMT_OVR_PLOT_SERIES_COLOR 4
#define DS_FMT_OVR_PLOT_SERIES_MAX 5
#define DS_FMT_OVR_PLOT_SERIES_MIN 6
#define DS_FMT_OVR_PLOT_SERIES_PRIORITY 7
#define DS_FMT_OVR_PLOT_SERIES_FORMAT 8
#define DS_FMT_OVR_PLOT_SERIES_SAMPLE_FORMAT 9
#define DS_FMT_OVR_PLOT_SERIES "overview plot series"

#define DS_FMT_OVR_PLOT_DATA_ID 1
#define DS_FMT_OVR_PLOT_DATA_VALUE 2
#define DS_FMT_OVR_PLOT_DATA_SAMPLE_COUNT 3

class Ovr_Plot_Series
{
public:
   RString name, value_format, sample_format, docstring;
   double range_max_auto, range_min_auto;
   double range_max_dsf, range_min_dsf;
   double range_max_usr, range_min_usr;
   GtkWidget *range_max_w, *range_min_w;
   PGtkLabelPtr value_w;
   PGtkToggleActionPtr display_toggle;
   PGtkToggleActionPtr connect_toggle;
   Color_Info color_default;
   GdkColor color_visited;
   int value_index, priority;
   bool from_settings_rti, display_initial, connect_initial;
   int modified_count;

   Ovr_Plot_Series() { name=""; from_settings_rti=false; modified_count=0;}

   double range_min_get()
   {
     return range_max_usr <= range_min_usr ? range_min_auto : range_min_usr;
   }

   double range_max_get()
   {
     return range_max_usr <= range_min_usr ? range_max_auto : range_max_usr;
   }

   bool is_modified()
   {
     return ( gtk_toggle_action_get_active(display_toggle) != display_initial ||
              gtk_toggle_action_get_active(connect_toggle) != connect_initial );
   }
};


struct Ovr_Plot_PSE_Window_Data {
    PGtkLabelPtr *pane_value_label;
    PGtkVBoxPtr pane_vbox;
    PGtkHBoxPtr pane_hbox;
    bool ovr_show_values;
    bool seg_show_values;
};

class Ovr_Plot_Manager
{
public:
   int plot_series_num, plot_value_id_max;
   Ovr_Plot_Series *plot_series, **plot_series_pri;
   PQueue <char *> ovr_rti_settings_unused;
   PGtkDialogPtr preferences_dialog;
   PGtkLabelPtr prefs_segment_label;

   Ovr_Plot_Manager()
   {
     plot_series_allocated = plot_series_num = plot_value_id_max = 0;
     plot_series = NULL;
     plot_series_pri = NULL;
     preferences_dialog = NULL;
   }

   ~Ovr_Plot_Manager()
   {
     if(plot_series != NULL) free(plot_series);
     if(plot_series_pri != NULL) free(plot_series_pri);
   }

   void data_read(Dataset_File& df, Segment_Data* const sd);
   void definition_read(Dataset_File& df);
   void series_new
   (int index, const char *name, const char *format, const char *sample_format,
    const char *docstring, double r_max, double r_min, const char *color,
    int priority);
   void series_auto_scale(int series_index, Segment_Data **sd_base, int occ);
   void series_auto_scale_all(Segment_Data **sd_base, int occ);
   void series_prioritize();
   void series_rti_to_settings();
   void series_settings_to_rti();
   void setup_before_pse_window_initialized(PSE_Window* const pw);
   void setup_after_dataset_load(Dataset* const ds);
   void pane_value_labels_update(PSE_Window* const pw, Segment_Data* const sd);
   void pane_labels_build_all(Dataset* const ds);
   void pane_labels_build(PSE_Window* const pw);
   void preferences_dialog_launch(PSE_Window* const pw);
   void window_and_dataset_init(PSE_Window* const pw);

private:
   int plot_series_allocated;

   void plot_series_realloc(int index)
   {
     const int previous_allocated = plot_series_allocated;
     plot_series_allocated +=
        index > plot_series_allocated + 16 ? index : 16;

     plot_series = (Ovr_Plot_Series *) realloc( plot_series,
           sizeof(Ovr_Plot_Series) * plot_series_allocated );

     memset(&plot_series[previous_allocated],0,
             (plot_series_allocated-previous_allocated)*sizeof(plot_series[0]));

   }

   static int compare_priority(Ovr_Plot_Series **a, Ovr_Plot_Series **b)
   {
     Ovr_Plot_Series *ops_a = *a, *ops_b = *b;
     return(ops_a->priority - ops_b->priority);
   }
   
   void preferences_dialog_construct(PSE_Window* const pw);
};

void ovr_plot_series_button_callback(GtkWidget *w, gpointer pwptr);
void ovr_plot_values_hide_button_callback(GtkWidget *w, gpointer pwptr);
void ovr_plot_values_menu_callback(gpointer pwptr, int action, GtkWidget *w);
void ovr_plot_series_menu_callback(gpointer pwptr, int action, GtkWidget *w);

#endif
