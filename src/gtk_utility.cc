/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2010 Louisiana State University

// $Id$

#include <gdk/gdkkeysyms.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include "gtk_utility.h"
#include "messages.h"

static gboolean
yield_to_gtk_handler(gpointer data) { return *((bool*)data) = false; }

void
yield_to_gtk(int priority)
{
  bool wait = true;
  const int id = g_idle_add(yield_to_gtk_handler,gpointer(&wait));
  GSource* const source = g_main_context_find_source_by_id(NULL,id);
  // Note: Lower numbers are higher priority.
  g_source_set_priority(source,priority);
  while( g_main_context_iteration(NULL,false) && wait );
}

// DMK: There must be a gxk function for this! 21 June 2002, 13:35:07 CDT

bool
meta_is_down(GdkEvent *e)
{
  GdkModifierType s;
  if( !gdk_event_get_state(e,&s) ) FATAL;
  return ( s & ( GDK_MOD1_MASK | GDK_MOD2_MASK | GDK_MOD3_MASK ) ) != 0;
}

bool
shift_is_down(GdkEvent *e)
{
  GdkModifierType s;
  if( !gdk_event_get_state(e,&s) ) FATAL;
  return ( s & GDK_SHIFT_MASK ) != 0;
}

bool
control_is_down(GdkEvent *e)
{
  GdkModifierType s;
  if( !gdk_event_get_state(e,&s) ) FATAL;
  return ( s & GDK_CONTROL_MASK ) != 0;
}

bool
amodifier_is_down(GdkEvent *e)
{
  GdkModifierType s;
  if( !gdk_event_get_state(e,&s) ) FATAL;
  return ( s & 0xff ) != 0;
}

void
pgtk_label_set_bgfg(GtkLabel *label, const GdkColor *label_bg, bool bg)
{
  if( !label_bg )
    {
      gtk_label_set_attributes(label,pango_attr_list_new());
      return;
    }
  const guint16 r = label_bg->red;
  const guint16 g = label_bg->green;
  const guint16 b = label_bg->blue;
  RString original_text(gtk_label_get_text(label));

  PangoAttrList *al;
  char *plain_text;
  if( gtk_label_get_use_markup(label) )
    {
      const bool okay =
        pango_parse_markup
        (original_text,original_text.len(),0,&al,&plain_text,NULL,NULL);
      ASSERTS( okay );
    }
  else
    {
      plain_text = original_text;
      al = pango_attr_list_new();
    }

  PangoAttribute* const att = 
    bg ? pango_attr_background_new(r,g,b) : pango_attr_foreground_new(r,g,b);
  att->start_index = 0;
  att->end_index = strlen(plain_text);
  pango_attr_list_insert_before(al,att);

  gtk_label_set_attributes(label,al);
  gtk_label_set_text(label,plain_text);
}

void
pgtk_label_set_background(GtkLabel *label, const GdkColor *label_bg)
{
  pgtk_label_set_bgfg(label,label_bg,true);
}

void
pgtk_label_set_foreground(GtkLabel *label, const GdkColor *label_fg)
{
  pgtk_label_set_bgfg(label,label_fg,false);
}

// 
// Warp Pointer -- Full access to XWarpPointer, warp only if
// pointer is within source rectangle.
//
void
pgtk_XWarpPointer(GdkWindow *display, GdkWindow *src_w, GdkWindow *dest_w,
                  int src_x, int src_y, unsigned src_width,
                  unsigned src_height, int dest_x, int dest_y)
{
  Window xsrc_w = src_w ? GDK_WINDOW_XID(src_w) : None;
  Window xdest_w = dest_w ? GDK_WINDOW_XID(dest_w) : None;
  Display *x_display = display ? GDK_WINDOW_XDISPLAY(display) : gdk_display;

  XWarpPointer(x_display, xsrc_w, xdest_w, src_x, src_y, src_width,
               src_height, dest_x, dest_y);
}

//
// Only intrawindow relative or absolute warps possible
//
void
pgtk_XWarpPointer(GtkWidget *widget, int dest_x, int dest_y)
{
  Window xdest_w = widget ? GDK_WINDOW_XID(widget->window) : None;
  Display *x_display =
    widget ? GDK_WINDOW_XDISPLAY(widget->window) : gdk_display;

  XWarpPointer(x_display, None, xdest_w, 0, 0, 0, 0, dest_x, dest_y);
}
