//  -*- c++ -*-
/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2014 Louisiana State University

// $Id$

//
// Handlers for see window events.
//

#ifndef HANDLERS_H
#define HANDLERS_H

#include <gtk/gtk.h>

void menu_cb_copy(gpointer pwptr, int action, GtkWidget *w);
void menu_cb_zoom(gpointer swptr, int action, GtkWidget *w);
void menu_cb_animate(gpointer swptr, int action, GtkWidget *w);
void menu_cb_dis(gpointer swptr, int action, GtkWidget *w);
void menu_cb_open(gpointer swptr, int action, GtkWidget *w);
void menu_cb_close(gpointer swptr, int quit_all, GtkWidget *w);
void menu_cb_clone(gpointer swptr, int action, GtkWidget *w);
void menu_cb_data(gpointer swptr, int action, GtkWidget *w);
void menu_cb_colorkey(gpointer swptr, int action, GtkWidget *w);
void menu_cb_ps(gpointer swptr, int action, GtkWidget *w);
void menu_cb_about(gpointer swptr, int action, GtkWidget *w);
void menu_cb_insn_cursor(gpointer swptr, int action, GtkWidget *w);
void menu_cb_select_done(GtkWidget *context_menu, gpointer swptr);
void menu_cb_clip(gpointer swptr, int action, GtkWidget *w);
void menu_cb_pref(gpointer swptr, int action, GtkWidget *w);
void menu_cb_font(gpointer swptr, int action, GtkWidget *w);
void menu_cb_dis_window(gpointer swptr, int action, GtkWidget *w);

void data_cb_selc(GtkTreeSelection *data_var_selected, gpointer data);
void pref_show_gridlines(PSE_Window *pw);

void bttn_cb_decode_pos(GtkWidget *w, gpointer swptr);
void bttn_cb_clip(GtkWidget *w, gpointer swptr);
void bttn_cb_ped(GtkWidget *w, gpointer ptr);
void bttn_cb_ps(GtkWidget *w, gpointer swptr);
void bttn_cb_display(GtkWidget *w, gpointer swptr);
void bttn_cb_dis_window(GtkWidget *w, gpointer swptr);
void bttn_cb_insn(GtkWidget *w, gpointer cdptr);
void bttn_cb_insn_pressed(GtkWidget *w, gpointer cdptr);
void bttn_cb_insn_released(GtkWidget *w, gpointer cdptr);
void bttn_cb_misc(GtkWidget *w, gpointer cdptr);
void bttn_cb_zoom(GtkWidget *w, gpointer cdptr);
void bttn_cb_data(GtkWidget *w, gpointer swptr);
void bttn_cb_dis_window(GtkWidget *w, gpointer swptr);
void bttn_cb_seg_insn_order(GtkWidget *w, gpointer swptr);

gint evnt_cb_motion(GtkWidget *w, GdkEventMotion *ee, gpointer swptr);
gint evnt_cb_button_down(GtkWidget *w, GdkEventButton *be, gpointer swptr);
gint evnt_cb_button_up(GtkWidget *w, GdkEventButton *be, gpointer swptr);
gint evnt_cb_scroll(GtkWidget *w, GdkEventScroll *be, gpointer swptr);
gint evnt_cb_key_press(GtkWidget *w, GdkEventKey *event, gpointer swptr);

void evnt_cb_expose(GtkWidget *w, GdkEventExpose *ee, gpointer swptr);
void evnt_cb_delete(GtkWidget *main_w, GdkEvent *event, gpointer swptr);

gint evnt_cb_data_button_up(GtkWidget *w, GdkEventButton *be, gpointer swptr);

void close_and_maybe_quit(PSE_Window *sw, int quit_all);

#endif
