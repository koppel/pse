/// PSE - A micro-architecture simulation dataset viewer.
/// Copyright 2015 Louisiana State University

// $Id$

// To Do List ./ToDo
// Style, Terminology, and Naming Conventions:  ./Style


#include <gtk/gtk.h>
#include <unistd.h>
#include <stdlib.h>
#include "enames.h"
#include "messages.h"
#include "dataset.h"
#include "pse.h"
#include "misc.h"
#include "rti_all.h"
#define RTI_REG
#include "rti_all.h"
#include "ds_extract.h"

App app;

App::App()
{
  gtk_inited = false;
  ds = NULL;
  gui = true; // Reset if certain command options used.
  run_time_initialization = new Run_Time_Initialization;
  ds_extract = new DS_Extract(this);
}

App::~App()
{
  if( ds ) { delete ds;  ds = NULL; }
  if(run_time_initialization) delete run_time_initialization;
}

void
App::init_color(const char *spec, GdkColor &cc, const char* backup_spec)
{
  gdk_color_parse(spec,&cc);
  if( !gdk_color_alloc(cmap,&cc) )
    {
      if( backup_spec ) init_color(backup_spec, cc, NULL);
      else fatal("Could not allocate color %s.",spec);
    }
}

void
App::init_color(const char *spec, Color_Info &ci)
{
  ci.spec = spec;
  init_color(spec,ci.gdkcolor);
}

void
App::init()
{
  char* const file_path =
    option_manager.orphans ? option_manager.orphans.peek() : NULL;
  if( gui ) init_gui();
  ds = new Dataset(gui,file_path);
}

void
App::init_gui()
{
  cmap = gdk_colormap_get_system();
  init_color("black",gdk_black);
  init_color("blue",gdk_blue);
  init_color("gray",gdk_gray);
  init_color("white",gdk_white);
  init_color("purple",gdk_purple);
  init_color("#555",c_ovr_plot_shadow);
  init_color("blue",c_dis_outline);
  init_color("black",c_dis_outline_locked);
  init_color("#ddd",c_cursor);
  init_color("yellow",c_cursor_locked);
  init_color("red",gdk_red);
  init_color("#ff7f00",gdk_amber);
  init_color("#bbb",c_rob_half_full);
  init_color("#0f0",c_rob_half_empty);
  init_color("#00af00",gdk_same_sin);
  init_color("#00af00",c_same_line);
  init_color("khaki4",gdk_pointed);
  init_color("DodgerBlue1",gdk_search_hit);
  init_color("#aaa",gdk_obscure);
  init_color("white",c_pointer_bg);
  init_color("#ffffaa",c_cursor_locked_bg);
  init_color("#ffffdd",c_cursor_unlocked_bg);
  init_color("#eee",c_dis_window_locked);
  init_color("black",gdk_color_snip_pointed);
  init_color("red",gdk_color_snip_pointed_pred_1);
  init_color("dark salmon",gdk_color_snip_pointed_pred_x);
  init_color("blue",gdk_color_snip_pointed_succ_1);
  init_color("light blue",gdk_color_snip_pointed_succ_x);
  init_color("gray",gdk_color_snip_separator_in_order);
  init_color("orange",gdk_color_snip_separator_gap);
  init_color("red",gdk_color_snip_separator_out_of_order);
  init_color("#e6e6e6",c_grid);
  init_color("#404040",c_tear_dark);
  init_color("#c0c0c0",c_tear_light);

  init_color("#e0e0e0",c_ovr_bg_gray_0);
  init_color("#ececec",c_ovr_bg_gray_1);

  init_color("brown",dependency_color[OR_rd]);
  init_color("deep pink",dependency_color[OR_rs1]);
  init_color("light sea green",dependency_color[OR_rs2]);
  init_color("bisque4",dependency_color[OR_cc]);
  init_color("DodgerBlue1",dependency_color[OR_mem]);


  for(int i = 0; i < OR_num; i++ )
    if( !dependency_color[i] ) init_color("dark salmon",dependency_color[i]);

  tips = gtk_tooltips_new();

  {
#   include "icon.xpm"
    GdkPixbuf* const icon_pixbuf1 =
      gdk_pixbuf_new_from_xpm_data((const char**)noname);
    icon_pixbuf = gdk_pixbuf_add_alpha(icon_pixbuf1,true,255,255,255);
    GList icon_elt;
    icon_elt.data = (gpointer) icon_pixbuf;
    icon_elt.next = icon_elt.prev = NULL;
    // The call below does not seem to affect the top-level window.
    gtk_window_set_default_icon_list(&icon_elt);
  }

  app.gtk_inited = true;

  static unsigned char corners[] =
    { 0x1e, 0xf0,  0x02, 0x80, 0x02, 0x80, 0x02, 0x80,
      0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0,
      0x02, 0x80, 0x02, 0x80, 0x02, 0x80, 0x1e, 0xf0 };

  static unsigned char corners_mask[] =
    { 0x1e, 0xf0,  0x1e, 0xf0, 0x06, 0xc0, 0x06, 0xc0,
      0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0,
      0x06, 0xc0, 0x06, 0xc0, 0x1e, 0xf0, 0x1e, 0xf0 };

  GdkPixmap *source = gdk_bitmap_create_from_data(NULL, (gchar*)corners, 16, 16);
  GdkPixmap *mask = gdk_bitmap_create_from_data(NULL, (gchar*)corners_mask, 16, 16);
  cursor_corners =
    gdk_cursor_new_from_pixmap(source, mask, &gdk_black, &gdk_white, 8, 8);

  cursor_arrow = gdk_cursor_new(GDK_ARROW);
  cursor_watch = gdk_cursor_new(GDK_WATCH);
  cursor_crosshair = gdk_cursor_new(GDK_CROSSHAIR);
  cursor_fleur = gdk_cursor_new(GDK_FLEUR);  // Not de-lis.
  cursor_dis_drag = gdk_cursor_new(GDK_FLEUR);
  cursor_insn_follow = gdk_cursor_new(GDK_CROSSHAIR);
}

const char *preferences_filename = ".pse_prefs";

int
main(int argc, char **argv)
{
  debug_malloced_init();
  const bool gui_available = gtk_init_check(&argc,&argv); 
  app.option_manager.process(argc,argv);
  app.run_time_initialization->file_parse(preferences_filename);
  app.run_time_initialization->option_examine_save(rtiv_rti_save);
  app.hooks_after_args.invoke();
  if( app.gui && !gui_available )
    fatal("PSE requires but cannot initialize a graphical display.");
  app.init();
  app.hooks_after_init.invoke();
  if( app.gui ) gtk_main();
  app.run_time_initialization->file_write(preferences_filename);

  return 0;
}
