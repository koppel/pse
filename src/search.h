/// PSE - A micro-architecture simulation dataset viewer.  -*- c++ -*-
/// Copyright (c) 2007 Louisiana State University

// $Id$

#ifndef SEARCH_H
#define SEARCH_H

#include <gtk/gtk.h>
#include "pse.h"

// Provides history for a GtkEntry widget.  If this is improved
// further move to gtk_utility. (GtkComboBoxEntry can be used instead.)
// DMK 29 September 2005,  9:47:03 CDT
class Entry_History;

enum Search_Scan_Action {
  SSA_Insn_Prev, SSA_Insn_Next,
  SSA_Insn_First_Search_Target, SSA_Insn_Last_Search_Target,
  SSA_Insn_Prev_Search_Target, SSA_Insn_Next_Search_Target
};

enum Search_Target_Type {
  STT_Unset, STT_Invalid, STT_PC, STT_Tag, STT_Cycle
};

void search_dataset_init(Dataset *ds);
GtkWidget* search_bar_construct(PSE_Window *pw);
void search_window_and_dataset_init(PSE_Window *pw);

void search_segment_init(Segment_Data *sd);
void search_bttn_sensitivity_set(PSE_Window *pw);
void search_at_overview(PSE_Window *pw);
bool search_segment_contains_target_pc
(PSE_Window *pw, Segment_Data *sd);

bool
search_pw_cursor_move(PSE_Window *pw, Search_Scan_Action action);


#endif
